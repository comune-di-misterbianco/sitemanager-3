﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.DBSessionProvider;
using NHibernate;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using NHibernate.Criterion;
using System.Collections;
using NetCms.Users.Search;
using NetService.Utility.ValidatedFields;
using NetCms.Users.CustomAnagrafe;
using System.Reflection;
using NetCms.Networks.WebFS;
using LabelsManager;
using NHibernate.Util;
using System.Web.Security;

namespace NetCms.Users
{
    public static class UsersBusinessLogic
    {
        public static Labels Labels
        {
            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(RegistrazioneLabels)) as Labels;
            }
        }

        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static ISession GetCurrentMapSession()
        {
            return GetCurrentSession().GetSession(EntityMode.Map);
        }

        public static ICollection<User> FindAll()
        {
            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(GetCurrentSession());
            return dao.FindAll();
        }

        public static User GetById(int id, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(session);
            return dao.GetById(id);
        }

        public static string GetUserNameByID(int id, ISession session)
        {
            if (session == null)
                session = GetCurrentSession();

            NetCms.Users.User utente = NetCms.Users.UsersBusinessLogic.GetById(id, session);
            return (utente != null) ? utente.UserName : "utente non disponibile";
        }

        public static User GetUserOldID(int oldID, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(session);

            SearchParameter usernameSP = new SearchParameter(null, "OldID", oldID, Finder.ComparisonCriteria.Equals, false);
            
            return dao.GetByCriteria(new SearchParameter[] { usernameSP });
        }

        public static User GetUserByUserName(string userName, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(session);

            SearchParameter usernameSP = new SearchParameter(null, "UserName", userName, Finder.ComparisonCriteria.Equals, false);
           // SearchParameter deletedSP = new SearchParameter(null, "DeletedInt", 0, Finder.ComparisonCriteria.Equals, false);

            //return dao.GetByCriteria(new SearchParameter[] { usernameSP, deletedSP });

           return dao.GetByCriteria(new SearchParameter[] { usernameSP });
        }
        public static User GetUserByUserName(string userName, int deleted, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(session);

            SearchParameter usernameSP = new SearchParameter(null, "UserName", userName, Finder.ComparisonCriteria.Equals, false);
            SearchParameter deletedSP = new SearchParameter(null, "DeletedInt", deleted, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { usernameSP, deletedSP });

            //return dao.GetByCriteria(new SearchParameter[] { usernameSP });
        }

        public static User GetUserByUserName(string userName, int deleted, int state, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(session);

            SearchParameter usernameSP = new SearchParameter(null, "UserName", userName, Finder.ComparisonCriteria.Equals, false);
            SearchParameter deletedSP = new SearchParameter(null, "DeletedInt", deleted, Finder.ComparisonCriteria.Equals, false);
            SearchParameter stateSP = new SearchParameter(null, "State", state, Finder.ComparisonCriteria.Equals, false);
            
            //return dao.GetByCriteria(new SearchParameter[] { usernameSP, deletedSP, stateSP });
            var res = dao.FindByCriteria(new SearchParameter[] { usernameSP, deletedSP, stateSP });
            //return dao.GetByCriteria(new SearchParameter[] { usernameSP });

            if (res != null)
                if (res.Count == 1)
                    return res.FirstOrDefault();
                
            return null;
        }

        public static User GetUserByEmail(string email)
        {
            User user = null;
            try
            {
                ISession mapSession = GetCurrentMapSession();
                ICriteria criteria = mapSession.CreateCriteria("AnagraficaBase");
                criteria.Add(Restrictions.Eq("Email", email));
                Hashtable anagraficaHashTable = (Hashtable)criteria.UniqueResult();
                if (anagraficaHashTable != null)
                {
                    Hashtable userHashTable = (Hashtable)anagraficaHashTable["User"];
                    user = GetById(int.Parse(userHashTable["ID"].ToString()));
                }
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore nella ricerca dell'utente con email: " + email);
            }
            return user;
        }

        /// <summary>
        /// Verifica l'esistenza di utente con l'email indicata
        /// </summary>
        /// <param name="email"></param>
        /// <param name="sess"></param>
        /// <returns></returns>
        public static bool CheckEmailIsUsed(string email, ISession sess = null)
        {
            bool esito = false;
            try
            {
                //ISession mapSession = GetCurrentMapSession();

                //ICriteria criteria = mapSession.CreateCriteria("AnagraficaBase","anagrafe");
                //criteria.Add(Restrictions.Eq("anagrafe.Email", email));                
                //criteria.CreateAlias("User.AnagraficaMapped", "anagrafe.ID", NHibernate.SqlCommand.JoinType.InnerJoin);                
                //criteria.Add(Restrictions.Eq("AnagraficaId", "anagrafe.ID"));

                //Hashtable anagraficaHashTable = (Hashtable)criteria.UniqueResult();
                //if (anagraficaHashTable != null)
                //{
                //    Hashtable userHashTable = (Hashtable)anagraficaHashTable["User"];
                //    user = GetById(int.Parse(userHashTable["ID"].ToString()));
                //}


                if (sess == null)
                    sess = GetCurrentSession();

                IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(sess);
                List<SearchParameter> sps = new List<SearchParameter>();

                SearchParameter emailSP = new SearchParameter(null, "Email", email, Finder.ComparisonCriteria.Equals, false);
                
                SearchParameter spUserAnagrafe = new SearchParameter("", "AnagraficaMapped", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { emailSP });
                sps.Add(spUserAnagrafe);

                SearchParameter deletedSP = new SearchParameter(null, "DeletedInt", 0, Finder.ComparisonCriteria.Equals, false);
                
                sps.Add(deletedSP);

                var results = dao.FindByCriteria(sps.ToArray());

                esito = (results.Any());
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore nella ricerca dell'utente con email: " + email);
            }
            return esito;
        }

        public static User GetActiveUserByUserName(string userName, ISession sess = null)
        {
            if (sess == null)
                sess = GetCurrentSession();

            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(sess);

            SearchParameter usernameSP = new SearchParameter(null, "UserName", userName, Finder.ComparisonCriteria.Equals, false);
            SearchParameter statoSP = new SearchParameter(null, "State", 1, Finder.ComparisonCriteria.Equals, false);
            SearchParameter deletedSP = new SearchParameter(null, "DeletedInt", 0, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { usernameSP, statoSP, deletedSP });
        }

        public static ICollection<User> FindAllActiveUsersOrderByUsername()
        {
            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(GetCurrentSession());

            SearchParameter statoSP = new SearchParameter(null, "State", 1, Finder.ComparisonCriteria.Equals, false);
            SearchParameter deletedSP = new SearchParameter(null, "DeletedInt", 0, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria("UserName", false, new SearchParameter[] { statoSP, deletedSP });
        }

        public static User GetUserByUserNameAndPassword(string userName, string password, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            ICriteria criteria = session.CreateCriteria(typeof(User));
            criteria.Add(Restrictions.Eq("UserName", userName));            
            criteria.Add(Restrictions.Eq("DeletedInt", 0));            
            criteria.Add(Restrictions.Eq("Password", password));                        

            return (User)criteria.UniqueResult();        

        }

        public static User GetUserByActivationKey(int activationKey, ISession sess = null)
        {
            if (sess == null)
                sess = GetCurrentSession();
            ICriteria criteria = sess.CreateCriteria(typeof(User));
            criteria.Add(Restrictions.Eq("ActivationKey", activationKey.ToString()));
            criteria.Add(Restrictions.Eq("DeletedInt", 0));
            criteria.Add(Restrictions.Eq("State", 0));

            return (User)criteria.UniqueResult();
        }

        public static User GetActiveUserByUserNameAndPassword(string userName, string password, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            ICriteria criteria = session.CreateCriteria(typeof(User));
            criteria.Add(Restrictions.Eq("UserName", userName));
            criteria.Add(Restrictions.Eq("State", 1));
            criteria.Add(Restrictions.Eq("DeletedInt", 0));
            Disjunction or = new Disjunction();
            or.Add(Restrictions.Eq("Password", password));
            or.Add(Restrictions.Eq("NuovaPassword", password));
            criteria.Add(or);

            return (User)criteria.UniqueResult();
        }
       
        public static void UpdateLastAccess(User user, ISession sess = null)
        {
            if (sess == null)
            {
                sess = GetCurrentSession();
            }
            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(sess);
            user.LastAccess = DateTime.Now;
            dao.SaveOrUpdate(user);
        }

        public static void UpdateUserIfNotDeleted(User user,ISession sess = null)
        {
            if (sess == null)
                sess = GetCurrentSession();
            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(sess);

            if (user.DeletedInt == 0)
                dao.SaveOrUpdate(user);
        }

        public static void UpdateActivationKey(User user, string activationKey, ISession sess = null)
        {
            if (sess == null)
                sess = GetCurrentSession();
            
            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(sess);

            user.ActivationKey = activationKey;
            dao.SaveOrUpdate(user);
        }



        public static void SaveOrUpdateUser(User user,ISession session =null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(session);
            dao.SaveOrUpdate(user);
        }

        public static void MergeUser(User user,ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();
            
            session.Merge(user);
            //IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(GetCurrentSession());
            //dao.SaveOrUpdate(user);
        }

        public static void DeleteUser(User user, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(session);
            IGenericDAO<GroupAssociation> gDao = new CriteriaNhibernateDAO<GroupAssociation>(session);

            var groups = user.Groups;

            foreach (Group group in groups)
            {
                GroupAssociation ga = user.AssociatedGroups.Where(x => x.Group.ID == group.ID).First();
                group.AssociatedUsers.Remove(ga);
            }

            foreach (GroupAssociation ga in user.AssociatedGroups)
            {
                //ga.Group.AssociatedUsers.Remove(ga);
                //GroupsBusinessLogic.DeleteGroupAssociation(ga, false, session);
                gDao.Delete(ga);
            }
            user.AssociatedGroups.Clear();

            IList<Notifica> notificheInviate = GetNotificaByResponsabile(user, session);
            foreach (Notifica notifica in notificheInviate)
                DeleteNotifica(notifica, session);

            dao.Delete(user);
        }

        public static ICollection<User> FindNonGrouppedUsers(int pageStart, int pageSize)
        {
            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(GetCurrentSession());

            SearchParameter deletedSP = new SearchParameter(null, "DeletedInt", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter isEmptyGroups = new SearchParameter(null, "AssociatedGroups", SearchParameter.NullValue, Finder.ComparisonCriteria.IsEmpty, false);

            return dao.FindByCriteria(pageStart, pageSize, "UserName", false, new SearchParameter[] { isEmptyGroups, deletedSP });
        }

        public static int FindNonGrouppedUsersCount()
        {
            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(GetCurrentSession());

            SearchParameter deletedSP = new SearchParameter(null, "DeletedInt", 0, Finder.ComparisonCriteria.Equals, false);
            SearchParameter isEmptyGroups = new SearchParameter(null, "AssociatedGroups", SearchParameter.NullValue, Finder.ComparisonCriteria.IsEmpty, false);

            return dao.FindByCriteriaCount(new SearchParameter[] { isEmptyGroups, deletedSP });
        }

        public static AnagraficaBase GetAnagraficaBaseUser(User user)
        {
            ISession mapSession = GetCurrentMapSession();

            Hashtable map = new Hashtable();
            ICriteria criteria = mapSession.CreateCriteria(user.AnagraficaType);
            criteria.Add(Restrictions.IdEq(user.AnagraficaID));
            map = (Hashtable)criteria.UniqueResult();
            AnagraficaBase anagrafica = new AnagraficaBase();
            anagrafica.ID = user.AnagraficaID;
            anagrafica.Email = map["Email"].ToString();
            return anagrafica;
        }

        public static bool ActivateUser(int activationKey, out string messages)
        {
            bool status = false;
            messages = string.Empty;
            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    User currentUser = GetUserByActivationKey(activationKey, session);
                    
                    currentUser.LastAccess = DateTime.Now;
                    currentUser.LastUpdate = DateTime.Now;
                    currentUser.State = 1;

                    UsersBusinessLogic.SaveOrUpdateUser(currentUser, session);

                    // send activation email
                    currentUser.SendEnabledMail();

                    tx.Commit();
                    status = true;
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    string msg = "" + ex.Message;
                    messages += "Errore durante la procedura di attivazione dell'account utente.";
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, msg);
                }
            }
            return status;
        }

        public static bool Activate(VfManager form, Labels labels, out string messages)
        {
            bool status = false;
            messages = string.Empty;
            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                try
                {
                    ActivateUserControl activateForm = form as ActivateUserControl;

                    string username = activateForm.NomeUtente.PostBackValue;
                    
                    string password = activateForm.Password.PostBackValue;
                    string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();

                    string activationCode = activateForm.Codice.PostBackValue;

                    User currentUser = GetUserByUserNameAndPassword(username, MD5Pwd, session);
                    if (currentUser != null)
                    {
                        if (currentUser.ActivationKey != activationCode)
                        {
                            messages += labels[UserActivateLabels.LabelList.ActivationSuccessMsg];
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Tentativo di attivazione dell'account con nome utente " + username + " e codice di attivazione non valido " + activationCode );
                        }
                        else
                        {
                            // ok 
                            currentUser.LastAccess = DateTime.Now;
                            currentUser.LastUpdate = DateTime.Now;
                            currentUser.State = 1;

                            UsersBusinessLogic.SaveOrUpdateUser(currentUser, session);
                            
                            // send activation email
                            currentUser.SendEnabledMail();

                            // autenticazione automatica
                            // NetCms.Users.AccountManager.LoginUser(currentUser.ID.ToString(),session);
                            
                            tx.Commit();
                            status = true;

                        }
                    }
                    else
                    {
                        messages += labels[UserActivateLabels.LabelList.ActivationError];
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "Tentativo di attivazione non finalizzato dell'account con nome utente " + username + " in quanto i dati inseriti non sono riconducibili ad un account esistente");
                    }

                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    string msg = "" + ex.Message;
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, msg);
                }
            }
            return status;
        }

        private static ICriteria UserIAdminPrivate(User user, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICriteria criteria = GetCurrentSession().CreateCriteria(typeof(User));

            DetachedCriteria detCriteria = DetachedCriteria.For<User>();
            detCriteria.Add(Restrictions.Eq("DeletedInt", 0));

            DetachedCriteria profileCriteria = detCriteria.CreateCriteria("Profile");
            DetachedCriteria associatedGroupCriteria = detCriteria.CreateCriteria("AssociatedGroups");
            DetachedCriteria groupCriteria = associatedGroupCriteria.CreateCriteria("Group", "gr");

            if (additionalJoins != null)
            {
                foreach (SearchJoin join in additionalJoins)
                {
                    switch (join.Criteria)
                    {
                        case SearchJoin.CriteriaType.User: detCriteria.CreateCriteria(join.Property);
                            break;
                        case SearchJoin.CriteriaType.Profile: profileCriteria.CreateCriteria(join.Property);
                            break;
                        case SearchJoin.CriteriaType.GroupAssociation: associatedGroupCriteria.CreateCriteria(join.Property);
                            break;
                        case SearchJoin.CriteriaType.Group: groupCriteria.CreateCriteria(join.Property);
                            break;
                    }
                }
            }

            // MyGroupsConditions
            Disjunction groupsOr = new Disjunction();
            foreach (GroupAssociation ga in user.AssociatedGroupsOrdered)
            {
                Conjunction con = new Conjunction();
                bool addConjunction = false;
                if (ga.GrantsRole != NetCms.Users.GrantAdministrationTypes.User)
                {
                    addConjunction = true;
                    con.Add(Restrictions.Like("gr.Tree", ga.Group.Tree, MatchMode.Start));
                    if (ga.GrantsRole == NetCms.Users.GrantAdministrationTypes.Administrator)
                        con.Add(Restrictions.Eq("Type", 0));
                }
                if (addConjunction)
                    groupsOr.Add(con);
            }

            associatedGroupCriteria.Add(groupsOr);

            if (!user.CanHandleItSelf)
            {
                detCriteria.Add(Restrictions.Not(Restrictions.IdEq(user.ID)));
            }

            if (additionalConditions != null)
            {
                foreach (SearchCondition condition in additionalConditions)
                {
                    BuildCriteriaRestriction(detCriteria, profileCriteria, associatedGroupCriteria, groupCriteria, condition);
                }
            }
            detCriteria.SetProjection(Projections.Distinct(Projections.Id()));

            criteria = criteria.Add(Subqueries.PropertyIn("ID", detCriteria));
            //criteria = criteria.SetResultTransformer(new NHibernate.Transform.DistinctRootEntityResultTransformer());
            return criteria;
        }

        public static ICollection<User> UserIAdmin(int pageStart, int pageSize, User user, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICriteria criteriaResult = UserIAdminPrivate(user, additionalJoins, additionalConditions);

            return criteriaResult.AddOrder(new Order("UserName", true)).SetFirstResult(pageStart * pageSize).SetMaxResults(pageSize).List<User>();

        }

        public static ICollection<User> UserIAdminByAnagraficaType(int pageStart, int pageSize, User user, string anagraficaType, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICriteria criteriaResult = UserIAdminPrivate(user, additionalJoins, additionalConditions);
            criteriaResult.Add(Restrictions.Eq("AnagraficaType", anagraficaType));

            return criteriaResult.AddOrder(new Order("UserName", true)).SetFirstResult(pageStart * pageSize).SetMaxResults(pageSize).List<User>();

        }

        public static ICollection<User> FindUsersByAnagraficaType(string anagraficaType, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(session);
            SearchParameter anagraficaTypeSp = new SearchParameter(null, "AnagraficaType", anagraficaType, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteria(new SearchParameter[] { anagraficaTypeSp });
        }

        public static ICollection<User> FindNotDeletedUsersByAnagraficaType(string anagraficaType, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(session);
            SearchParameter anagraficaTypeSp = new SearchParameter(null, "AnagraficaType", anagraficaType, Finder.ComparisonCriteria.Equals, false);
            SearchParameter notDeletedSp = new SearchParameter(null, "DeletedInt", 1, Finder.ComparisonCriteria.Not, false);

            return dao.FindByCriteria(new SearchParameter[] { anagraficaTypeSp, notDeletedSp });

        }

        public static ICollection<User> FindUsersByFolderFavorite(NetworkFolder folder, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<User> dao = new CriteriaNhibernateDAO<User>(session);

            SearchParameter folderFavoritesSP = new SearchParameter(null, "FavoritesFolders", folder, Finder.ComparisonCriteria.In, false);

            return dao.FindByCriteria(new SearchParameter[] { folderFavoritesSP });

        }

        public static int UserIAdminCount(User user, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICriteria criteriaResult = UserIAdminPrivate(user, additionalJoins, additionalConditions);
            criteriaResult.SetProjection(Projections.CountDistinct("ID"));

            return criteriaResult.UniqueResult<int>();
        }

        public static int UserIAdminCountByAnagraficaType(User user, string anagraficaType, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICriteria criteriaResult = UserIAdminPrivate(user, additionalJoins, additionalConditions);
            criteriaResult.Add(Restrictions.Eq("AnagraficaType", anagraficaType));
            criteriaResult.SetProjection(Projections.CountDistinct("ID"));

            return criteriaResult.UniqueResult<int>();
        }

        public static void SendConfirmMail(User user, string password, string activateUrl, bool activateViaSMS = false)
        {
            string testoMail = user.CustomAnagrafe.TestoEmailAttivazione;

            testoMail = FillStringPlaceholders(testoMail, user, password, activateUrl, activateViaSMS);
            
            string subject = "" + NetCms.Configurations.PortalData.Nome + ": " + Labels[RegistrazioneLabels.RegistrazioneLabelsList.RegistrationConfirmMailSubject];
            string targetEmail = user.Anagrafica.Email;

            if (NetCms.Configurations.Debug.GenericEnabled)
                targetEmail = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailToReport"].ToString();

            G2Core.Common.MailSender msg = new G2Core.Common.MailSender(
                                                                            targetEmail,
                                                                            subject,
                                                                            testoMail,
                                                                            NetCms.Configurations.PortalData.eMail,
                                                                            NetCms.Configurations.PortalData.Nome);
            msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
            msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
            msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;
            msg.DisableServerCertificateValidation = NetCms.Configurations.PortalData.DisableServerCertificateValidation;
            msg.SendAsHtml = true;
            msg.SendAlsoPlain = true;

            if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
            {
                msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
                msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
            }

            msg.Send();
        }

        public static void SendNewPasswordMail(User user, string password)
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();

            string body = Labels[RegistrazioneLabels.RegistrazioneLabelsList.NewPasswordSendMailBody]
                         .Replace("{NOME_COGNOME}", user.NomeCompleto)
                         .Replace("{USERNAME}", user.UserName)
                         .Replace("{NEWPASSWORD}", password)
                         .Replace("{IP}", ip)
                         .Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome)
                         .Replace("{EMAIL_PORTALE}", NetCms.Configurations.PortalData.eMail);

            string subject = "" + NetCms.Configurations.PortalData.Nome + ": " + Labels[RegistrazioneLabels.RegistrazioneLabelsList.NewPasswordSendMailSubject];
            string targetEmail = user.Anagrafica.Email;

            if (NetCms.Configurations.Debug.GenericEnabled)
                targetEmail = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailToReport"].ToString();

            G2Core.Common.MailSender msg = new G2Core.Common.MailSender(
                                                                            targetEmail,
                                                                            subject,
                                                                            body,
                                                                            NetCms.Configurations.PortalData.eMail,
                                                                            NetCms.Configurations.PortalData.Nome);
            msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
            msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
            msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;
            msg.DisableServerCertificateValidation = NetCms.Configurations.PortalData.DisableServerCertificateValidation;
            msg.SendAsHtml = true;
            msg.SendAlsoPlain = true;

            if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
            {
                msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
                msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
            }

            msg.Send();
        }


        public static void SendRejectMail(User user, string motivazione)
        {
            string testoMail = user.CustomAnagrafe.TestoEmailRegistrazioneNegata;
            testoMail = FillStringPlaceholders(testoMail, user, motivazione);
            string subject = "" + NetCms.Configurations.PortalData.Nome + ": " + Labels[Users.RegistrazioneLabels.RegistrazioneLabelsList.RegistrationRejectMailSubject];
            string targetEmail = user.Anagrafica.Email;

            if (NetCms.Configurations.Debug.GenericEnabled)
                targetEmail = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailToReport"].ToString();

            G2Core.Common.MailSender msg = new G2Core.Common.MailSender(
                                                                            targetEmail,
                                                                            subject,
                                                                            testoMail,
                                                                            NetCms.Configurations.PortalData.eMail,
                                                                            NetCms.Configurations.PortalData.Nome);
            msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
            msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
            msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;
            msg.DisableServerCertificateValidation = NetCms.Configurations.PortalData.DisableServerCertificateValidation;
            msg.SendAsHtml = true;

            if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
            {
                msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
                msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
            }

            msg.Send();
        }

        public static void SendNotifyRegistrationRequestMail(string eMailTo, string cc, string nomeCompletoUtente, string userName)
        {
            //NetCms.Users.RegistrazioneLabels Labels = new RegistrazioneLabels();

            string body = Labels[NetCms.Users.RegistrazioneLabels.RegistrazioneLabelsList.TestoNotificaRichiestaRegistrazione];

            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();

            body = body.Replace("{NOME_COGNOME}", nomeCompletoUtente);
            body = body.Replace("{USERNAME}", userName);
            body = body.Replace("{IP}", ip);
            body = body.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
            body = body.Replace("{EMAILPORTALE}", NetCms.Configurations.PortalData.eMail);

            string subject = "" + NetCms.Configurations.PortalData.Nome + ": " + Labels[RegistrazioneLabels.RegistrazioneLabelsList.TitoloNotificaRichiestaRegistrazione];

            // Invio Email di registrazione confermata
            G2Core.Common.MailSender msg = new G2Core.Common.MailSender(
                                                                            eMailTo,
                                                                            cc,
                                                                            subject,
                                                                            body,
                                                                            NetCms.Configurations.PortalData.eMail,
                                                                            NetCms.Configurations.PortalData.Nome);
            msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
            msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
            msg.DisableServerCertificateValidation = NetCms.Configurations.PortalData.DisableServerCertificateValidation;
            msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;

            if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
            {
                msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
                msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
            }

            msg.Send();
        }

        //private static string FillStringPlaceholders(string text, User user, string password, string currentNetworkPath, bool activateViaSMS)
        //{
        //    string value = text;

        //    value = value.Replace("{USERNAME}", user.UserName);
        //    value = value.Replace("{PASSWORD}", password);
        //    if (!activateViaSMS)
        //        value = value.Replace("{KEY}", user.ActivationKey);
        //    else
        //        value = value.Replace("{KEY}", Labels[Users.RegistrazioneLabels.RegistrazioneLabelsList.SmsKeyHelpMsg]);

        //    string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();
        //    value = value.Replace("{IP}", ip);
        //    value = value.Replace("{ACTIVATE_URL}", "http://" + ip + currentNetworkPath + "/attivazione/default.aspx");

        //    value = value.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
        //    value = value.Replace("{EMAIL_PORTALE}", NetCms.Configurations.PortalData.eMail);

        //    return value;
        //}
    
        private static string FillStringPlaceholders(string text, User user, string password, string activateUrl, bool activateViaSMS)
        {
            //TODO gestire la casistica dell'attivazione via sms

            string value = text;

            value = value.Replace("{USERNAME}", user.UserName);
            value = value.Replace("{PASSWORD}", password);
            
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();
            value = value.Replace("{IP}", ip);

            string attivazioneUrl = string.Format(activateUrl, user.ActivationKey);

            value = value.Replace("{ACTIVATE_URL}", attivazioneUrl);

            value = value.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
            value = value.Replace("{EMAIL_PORTALE}", NetCms.Configurations.PortalData.eMail);

            return value;
        }


        private static string FillStringPlaceholders(string text, User user, string motivazione)
        {
            string value = text;

            value = value.Replace("{MOTIVAZIONE}", motivazione);
            value = value.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
            value = value.Replace("{EMAIL_PORTALE}", NetCms.Configurations.PortalData.eMail);

            return value;
        }

        public static bool UpdateEmail(int anagraficaID, string anagraficaType, string nuovaMail)
        {
             using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
             using (ITransaction tx = sess.BeginTransaction())
             {
                 User usr = GetUserByAnagraficaId(anagraficaID,sess);
                 //ISession mapSession = GetCurrentMapSession();
                // ISession mapSession = sess.GetSession(EntityMode.Map);              
                 
                 //Hashtable map = new Hashtable();
                 try
                 {
                     usr.AnagraficaMapped["Email"] = nuovaMail;
                     SaveOrUpdateUser(usr, sess);
                     //map.Add("ID", usr.AnagraficaID);
                     //map.Add("Email", nuovaMail);
                     //foreach (CustomField cf in usr.CustomAnagrafe.CustomFields)
                     //{
                     //    map.Add(cf.Name, usr.AnagraficaMapped[cf.Name]);
                     //}                     
                     //mapSession.Merge("AnagraficaBase", map);
                     //mapSession.Flush();
                     tx.Commit();
                 }
                 catch (Exception ex)
                 {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore durante l'aggiornamento dell'email dell'utente con anagrafeID "+ anagraficaID + " "+ ex.Message);
                    tx.Rollback();
                     return false;
                 }
             }
            return true;
        }

        public static User GetUserByAnagraficaId(int anagraficaID, ISession session = null)
        {
            User user = null;
            try
            {
                if (session == null)
                    session = GetCurrentSession();

                ISession mapSession = session.GetSession(EntityMode.Map);

                ICriteria criteria = mapSession.CreateCriteria("AnagraficaBase");
                criteria.Add(Restrictions.IdEq(anagraficaID));
                Hashtable anagraficaHashTable = (Hashtable)criteria.UniqueResult();
                Hashtable userHashTable = (Hashtable)anagraficaHashTable["User"];
                user = GetById(int.Parse(userHashTable["ID"].ToString()), session);
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore nella ricerca dell'utente con id anagrafica: " + anagraficaID);
            }
            return user;

        }

        public static Hashtable GetAnagraficaMapUser(User user, ISession mapSession = null)
        {
            if (mapSession == null)
                mapSession = GetCurrentMapSession();

            Hashtable map = new Hashtable();
            ICriteria criteria = mapSession.CreateCriteria(user.AnagraficaType);
            criteria.Add(Restrictions.IdEq(user.AnagraficaID));
            map = (Hashtable)criteria.UniqueResult();
            return map;
        }

        private static void BuildCriteriaRestriction(DetachedCriteria userCriteria, DetachedCriteria profileCriteria, DetachedCriteria groupAssociationCriteria, DetachedCriteria groupCriteria, SearchCondition condition)
        {
            DetachedCriteria criteria = null;

            switch (condition.Criteria)
            {
                case SearchCondition.CriteriaType.User: criteria = userCriteria;
                    break;
                case SearchCondition.CriteriaType.Profile: criteria = profileCriteria;
                    break;
                case SearchCondition.CriteriaType.GroupAssociation: criteria = groupAssociationCriteria;
                    break;
                case SearchCondition.CriteriaType.Group: criteria = groupCriteria;
                    break;
            }

            switch (condition.Comparison)
            {
                case NetCms.Users.Search.SearchCondition.ComparisonType.Equals:
                    criteria.Add(Restrictions.Eq(condition.Property, condition.Value));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.EqualsIgnoreCase:
                    criteria.Add(Restrictions.Eq(condition.Property, condition.Value).IgnoreCase());
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.GreaterEquals:
                    criteria.Add(Restrictions.Ge(condition.Property, condition.Value));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.GreaterThan:
                    criteria.Add(Restrictions.Gt(condition.Property, condition.Value));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.In:
                    {
                        ICollection values = null;
                        if (condition.Value is ICollection)
                        {
                            values = (ICollection)condition.Value;
                            criteria.Add(Restrictions.In(condition.Property, values));
                        }
                        break;
                    }

                case NetCms.Users.Search.SearchCondition.ComparisonType.LessEquals:
                    criteria.Add(Restrictions.Le(condition.Property, condition.Value));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.LessThan:
                    criteria.Add(Restrictions.Lt(condition.Property, condition.Value));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.Like:
                    criteria.Add(Restrictions.InsensitiveLike(condition.Property, condition.Value.ToString(), MatchMode.Anywhere));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.LikeStart:
                    criteria.Add(Restrictions.InsensitiveLike(condition.Property, condition.Value.ToString(), MatchMode.Start));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.LikeEnd:
                    criteria.Add(Restrictions.InsensitiveLike(condition.Property, condition.Value.ToString(), MatchMode.End));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.Not:
                    criteria.Add(Restrictions.Not(Restrictions.Eq(condition.Property, condition.Value)));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.NotNull:
                    criteria.Add(Restrictions.IsNotNull(condition.Property));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.Null:
                    criteria.Add(Restrictions.IsNull(condition.Property));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.IsEmpty:
                    criteria.Add(Restrictions.IsEmpty(condition.Property));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.IsNotEmpty:
                    criteria.Add(Restrictions.IsNotEmpty(condition.Property));
                    break;
            }

            switch (condition.Criteria)
            {
                case SearchCondition.CriteriaType.User: userCriteria = criteria;
                    break;
                case SearchCondition.CriteriaType.Profile: profileCriteria = criteria;
                    break;
                case SearchCondition.CriteriaType.GroupAssociation: groupAssociationCriteria = criteria;
                    break;
                case SearchCondition.CriteriaType.Group: groupCriteria = criteria;
                    break;
            }
        }

        public static bool UpdateUserAndProfile(int idUser, string newUserName)
        {                       
            
            User user = GetById(idUser);
            user.UserName = newUserName;
            Profile upr = user.Profile;
            upr.Nome = newUserName;
            ProfileBusinessLogic.SaveOrUpdateProfile(upr);
            SaveOrUpdateUser(user);
                
             return true;
        }

        public static bool UpdateUserAnagrafica(VfManager formDatiAnagrafe, int anagraficaID, string anagraficaType )
        {
            using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    User user = GetUserByAnagraficaId(anagraficaID, sess);
                    ISession mapSession = sess.GetSession(EntityMode.Map);

                    Hashtable anagrafeToUpdate = new Hashtable();
                    string anagrafeName = anagraficaType.Replace("Anagrafica", "");

                    anagrafeToUpdate.Add("ID", anagraficaID);

                    foreach (VfGeneric field in formDatiAnagrafe.Fields.Where(x => x.Enabled))
                    {
                        //condizione aggiunta per evitare di far lanciare l'eccezione quando in una dropdown (non obbligatoria) che fa riferimento ad una 
                        //classe mappata non seleziona alcun valore facendo si che il PostbackValueObject, lanci un'eccezione.
                        if (!string.IsNullOrEmpty(field.PostBackValue)  )
                        {
                            if(field.Key != "Email")
                                anagrafeToUpdate.Add(field.Key.Replace(anagrafeName.Replace(" ", "_") + "_", "").Replace("_", " "), field.PostbackValueObject);
                            else
                                anagrafeToUpdate.Add(field.Key.Replace(anagrafeName.Replace(" ", "_") + "_", "").Replace("_", " "), user.Anagrafica.Email);
                        }                      
                    }

                    NetCms.Users.CustomAnagrafe.CustomAnagrafe strutturaAnagrafe = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(user.AnagraficaName, sess);
                    CustomField customFieldIsUsername = strutturaAnagrafe.CustomFields.Where(x => x.IsUsername).FirstOrDefault();
                    if (customFieldIsUsername != null)
                    {
                        string oldIsUsername = user.AnagraficaMapped[customFieldIsUsername.Name].ToString();
                        string newIsUsername = formDatiAnagrafe.Fields.Where(x => x.Key == customFieldIsUsername.CampoInAnagrafe.Key).First().PostBackValue;
                        if (string.Compare(oldIsUsername, newIsUsername, true) != 0)
                            UpdateUserAndProfile(user.ID, newIsUsername);
                    }
                   
                    mapSession.Merge(anagraficaType, anagrafeToUpdate);
                    mapSession.Flush();

                    tx.Commit();

                }
                catch (Exception ex)
                {
                    tx.Rollback();
                    return false;
                }
            }

            return true;
        }

        public static Hashtable GetAnagraficaByUserName(string anagraficaType,string userNameProp, string userNameValue, ISession mapSession)
        {
            Hashtable map = new Hashtable();
            try
            {             
                ICriteria criteria = mapSession.CreateCriteria(anagraficaType);
                criteria.Add(Restrictions.Eq(userNameProp, userNameValue));
                map = (Hashtable)criteria.UniqueResult();
            }
            catch(Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Il metodo GetAnagraficaByUserName ha causato un errore: " + ex.Message);
            }
            return map;
        }        

        // implementato per alcuni test - ma non usabile in quanto - viola le linee gdpr - riesumando l'account il sistema riesumerebbe anche le attività pregresse dell'utente venendo meno l'azione di eliminazione dei dati richiesta dall'utente
        //public static User ResumeUserAccount(int idUserToResume, string userName, string password, VfManager formDatiAnagrafe, string anagraficaType, int idGroupToInsert, bool isFrontend = false, string activationKey = "", string currentNetworkPath = "")
        //{
        //    using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
        //    using (ITransaction tx = sess.BeginTransaction())
        //    {
        //        User user;
        //        try
        //        {


        //            IGenericDAO<User> userDao = new CriteriaNhibernateDAO<User>(sess);

        //            userName = userName.Replace("'", "''");

        //            user = UsersBusinessLogic.GetById(idUserToResume, sess);

        //            if (System.Web.Configuration.WebConfigurationManager.AppSettings["ManageRegRequests"] == "0" || !isFrontend)
        //                user.Registrato = true;

        //            if (isFrontend)
        //                user.CurrentNetworkPath = currentNetworkPath;

        //            user.UserName = userName;
        //            user.Password = password;
        //            user.CreationTime = DateTime.Now;
        //            if (isFrontend)
        //            {
        //                user.LastAccess = DateTime.Now;
        //            }
        //            else
        //            {
        //                user.LastAccess = DateTime.MinValue;
        //            }
        //            user.LastUpdate = DateTime.Now;
        //            user.AnagraficaType = anagraficaType;
        //            user.ActivationKey = activationKey;
        //            user.State = 0;
        //            user.DeletedInt = 0;

        //            //UserProfile profile = new UserProfile();
        //            //profile.Nome = userName;

        //            //user.Profile = profile;

        //            Hashtable anagrafica = new Hashtable();

        //            ISession mapSession = sess.GetSession(EntityMode.Map);

        //            string anagrafeName = anagraficaType.Replace("Anagrafica", "");

        //            int idAnagrafica = 0;
        //            CustomAnagrafe.CustomAnagrafe cAnagrafe = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(anagrafeName, sess);
        //            if (cAnagrafe != null)
        //            {
        //                if (cAnagrafe.EmailAsUserName)
        //                {
        //                    Hashtable anagrafe = GetAnagraficaByUserName(anagraficaType, "Email", userName, mapSession);
        //                    if (anagrafe != null)
        //                    {
        //                        idAnagrafica = int.Parse(anagrafe["ID"].ToString());
        //                        //anagrafica = anagrafe;
        //                    }
        //                }
        //                else
        //                {
        //                    CustomField usernameField = cAnagrafe.CustomFields.ToList().Find(x => x.IsUsername);
        //                    if (usernameField != null)
        //                    {
        //                        Hashtable anagrafe = GetAnagraficaByUserName(anagraficaType, usernameField.Name, userName, mapSession);
        //                        if (anagrafe != null)
        //                        {
        //                            idAnagrafica = int.Parse(anagrafe["ID"].ToString());
        //                            //anagrafica = anagrafe;
        //                        }
        //                    }
        //                }
        //            }


        //            foreach (VfGeneric field in formDatiAnagrafe.Fields.Where(x => x.Enabled))
        //            {
        //                //condizione aggiunta per evitare di far lanciare l'eccezione quando in una dropdown (non obbligatoria) che fa riferimento ad una 
        //                //classe mappata non seleziona alcun valore facendo si che il PostbackValueObject, lanci un'eccezione.
        //                if (!string.IsNullOrEmpty(field.PostBackValue))
        //                {
        //                    //if (anagrafica.ContainsKey(field.Key.Replace(anagrafeName.Replace(" ", "_") + "_", "").Replace("_", " ")))
        //                    //{
        //                    //    string key = field.Key.Replace(anagrafeName.Replace(" ", "_") + "_", "").Replace("_", " ");
        //                    //    anagrafica[key] = field.PostbackValueObject;
        //                    //}
        //                    //else
        //                    anagrafica.Add(field.Key.Replace(anagrafeName.Replace(" ", "_") + "_", "").Replace("_", " "), field.PostbackValueObject);
        //                }
        //            }

        //            if (idAnagrafica > 0)
        //            {
        //                anagrafica.Add("ID", idAnagrafica);
        //                mapSession.Merge(anagraficaType, anagrafica);
        //                mapSession.Flush();
        //            }
        //            else
        //            {
        //                mapSession.SaveOrUpdate(anagraficaType, anagrafica);
        //                mapSession.Flush();
        //            }

        //            user.AnagraficaMapped = anagrafica;

        //            userDao.SaveOrUpdate(user);

        //            //Group groupToInsertTo = GroupsBusinessLogic.GetGroupById(idGroupToInsert, sess);

        //            //groupToInsertTo.AddUserToGroup(user, GrantAdministrationTypes.User, ContentAdministrationTypes.Write, sess);

        //            tx.Commit();

        //        }
        //        catch (Exception ex)
        //        {
        //            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore nella creazione dell'utente: " + ex.Message);

        //            tx.Rollback();
        //            return null;
        //        }
        //        return user;
        //    }
        //}

        public static User CreateUser(string userName, string password, VfManager formDatiAnagrafe, string anagraficaType, int idGroupToInsert, bool isFrontend = false, string activationKey = "", string currentNetworkPath = "")
        {
            using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                User user;
                try
                {
                    IGenericDAO<User> userDao = new CriteriaNhibernateDAO<User>(sess);

                    userName = userName.Replace("'", "''");

                    user = new User();

                    if (System.Web.Configuration.WebConfigurationManager.AppSettings["ManageRegRequests"] == "0" || !isFrontend)
                        user.Registrato = true;

                    if (isFrontend)
                        user.CurrentNetworkPath = currentNetworkPath;

                    user.UserName = userName;
                    user.Password = password;
                    user.CreationTime = DateTime.Now;
                    if (isFrontend)
                    {
                        user.LastAccess = DateTime.Now;
                    }
                    else
                    {
                        user.LastAccess = DateTime.MinValue;
                    }
                    user.LastUpdate = DateTime.Now;
                    user.AnagraficaType = anagraficaType;
                    user.ActivationKey = activationKey;

                    UserProfile profile = new UserProfile();
                    profile.Nome = userName;

                    user.Profile = profile;

                    Hashtable anagrafica = new Hashtable();

                    ISession mapSession = sess.GetSession(EntityMode.Map);

                    string anagrafeName = anagraficaType.Replace("Anagrafica", "");

                    //int idAnagrafica = 0;
                    //CustomAnagrafe.CustomAnagrafe cAnagrafe = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(anagrafeName, sess);
                    //if (cAnagrafe != null)
                    //{
                    //    if (cAnagrafe.EmailAsUserName)
                    //    {
                    //        Hashtable anagrafe = GetAnagraficaByUserName(anagraficaType, "Email", userName, mapSession);
                    //        if (anagrafe != null)
                    //        {
                    //            idAnagrafica = int.Parse(anagrafe["ID"].ToString());
                    //            //anagrafica = anagrafe;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        CustomField usernameField = cAnagrafe.CustomFields.ToList().Find(x => x.IsUsername);
                    //        if (usernameField != null)
                    //        {
                    //            Hashtable anagrafe = GetAnagraficaByUserName(anagraficaType, usernameField.Name, userName, mapSession);
                    //            if (anagrafe != null)
                    //            {
                    //                idAnagrafica = int.Parse(anagrafe["ID"].ToString());
                    //                //anagrafica = anagrafe;
                    //            }
                    //        }
                    //    }
                    //}


                    foreach (VfGeneric field in formDatiAnagrafe.Fields.Where(x => x.Enabled))
                    {
                        //condizione aggiunta per evitare di far lanciare l'eccezione quando in una dropdown (non obbligatoria) che fa riferimento ad una 
                        //classe mappata non seleziona alcun valore facendo si che il PostbackValueObject, lanci un'eccezione.
                        if (!string.IsNullOrEmpty(field.PostBackValue))
                        {
                            //if (anagrafica.ContainsKey(field.Key.Replace(anagrafeName.Replace(" ", "_") + "_", "").Replace("_", " ")))
                            //{
                            //    string key = field.Key.Replace(anagrafeName.Replace(" ", "_") + "_", "").Replace("_", " ");
                            //    anagrafica[key] = field.PostbackValueObject;
                            //}
                            //else
                                anagrafica.Add(field.Key.Replace(anagrafeName.Replace(" ", "_") + "_", "").Replace("_", " "), field.PostbackValueObject);
                        }
                    }

                    // Rimossa possibilità di associare ad un nuovo utente la precedente anagrafica - problemi col gdpr e errori a varie query di nhibernate in quanto gli risulterebbero p

                    //if (idAnagrafica > 0)
                    //{
                    //    anagrafica.Add("ID", idAnagrafica);
                    //    mapSession.Merge(anagraficaType, anagrafica);
                    //    mapSession.Flush();
                    //}
                    //else
                    //{
                        mapSession.SaveOrUpdate(anagraficaType, anagrafica);
                        mapSession.Flush();
                    //}

                    user.AnagraficaMapped = anagrafica;

                    userDao.SaveOrUpdate(user);

                    Group groupToInsertTo = GroupsBusinessLogic.GetGroupById(idGroupToInsert, sess);

                    groupToInsertTo.AddUserToGroup(user, GrantAdministrationTypes.User, ContentAdministrationTypes.Write, sess);

                    tx.Commit();

                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore nella creazione dell'utente: " + ex.Message);

                    tx.Rollback();
                    return null;
                }
                return user;
            }
        }

        /// <summary>
        /// Da usare nell'importazione
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="creationTime"></param>
        /// <param name="lastAccess"></param>
        /// <param name="lastUpdate"></param>
        /// <param name="dataToInsert"></param>
        /// <param name="anagraficaType">"Anagrafica" + CustomAnagrafe.Nome</param>
        /// <param name="idGroupToInsert"></param>
        /// <param name="activationKey"></param>
        /// <returns></returns>
        public static User CreateUser(ISession sessionExternal, string userName, string password, DateTime creationTime, DateTime lastAccess, DateTime lastUpdate, int state, Hashtable dataToInsert, string anagraficaType, ICollection<Group> groupsToAssociateTo, bool manageRegRequests, string activationKey = "")
        {
            User user = new User();

            IGenericDAO<User> userDao = new CriteriaNhibernateDAO<User>(sessionExternal);

            userName = userName.Replace("'", "''");

            if (manageRegRequests)
                user.Registrato = true;

            user.UserName = userName;
            user.Password = password;

            user.CreationTime = creationTime;
            user.LastAccess = lastAccess;
            user.LastUpdate = lastUpdate;
            user.State = state;            

            user.AnagraficaType = anagraficaType;
            user.ActivationKey = activationKey;

            UserProfile profile = new UserProfile();
            profile.Nome = userName;

            user.Profile = profile;

            ISession mapSession = sessionExternal.GetSession(EntityMode.Map);

            string anagrafeName = anagraficaType.Replace("Anagrafica", "");

            mapSession.SaveOrUpdate(anagraficaType, dataToInsert);
            mapSession.Flush();

            user.AnagraficaMapped = dataToInsert;

            userDao.SaveOrUpdate(user);

            foreach (Group group in groupsToAssociateTo)
            {
                group.AddUserToGroup(user, GrantAdministrationTypes.User, ContentAdministrationTypes.Write, sessionExternal);
            }
            return user;
        }

        /// <summary>
        /// Da usare nell'importazione
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="creationTime"></param>
        /// <param name="lastAccess"></param>
        /// <param name="lastUpdate"></param>
        /// <param name="dataToInsert"></param>
        /// <param name="anagraficaType">"Anagrafica" + CustomAnagrafe.Nome</param>
        /// <param name="idGroupToInsert"></param>
        /// <param name="activationKey"></param>
        /// <returns></returns>
        public static User CreateUser(ISession sessionExternal, string userName, string password, DateTime creationTime, DateTime lastAccess, DateTime lastUpdate, int state, Hashtable dataToInsert, string anagraficaType, ICollection<Group> groupsToAssociateTo, bool manageRegRequests, int oldID, string activationKey = "")
        {
            User user = new User();

            IGenericDAO<User> userDao = new CriteriaNhibernateDAO<User>(sessionExternal);

            userName = userName.Replace("'", "''");

            if (manageRegRequests)
                user.Registrato = true;

            user.UserName = userName;
            user.Password = password;

            user.CreationTime = creationTime;
            user.LastAccess = lastAccess;
            user.LastUpdate = lastUpdate;
            user.State = state;

            user.OldID = oldID;

            user.AnagraficaType = anagraficaType;
            user.ActivationKey = activationKey;

            UserProfile profile = new UserProfile();
            profile.Nome = userName;

            user.Profile = profile;

            ISession mapSession = sessionExternal.GetSession(EntityMode.Map);

            string anagrafeName = anagraficaType.Replace("Anagrafica", "");

            mapSession.SaveOrUpdate(anagraficaType, dataToInsert);
            mapSession.Flush();

            user.AnagraficaMapped = dataToInsert;

            userDao.SaveOrUpdate(user);

            foreach (Group group in groupsToAssociateTo)
            {
                group.AddUserToGroup(user, GrantAdministrationTypes.User, ContentAdministrationTypes.Write, sessionExternal);
            }
            return user;
        }

        /// <summary>
        /// Creazione utente con richiesta di aggiornamento password al primo accesso
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="formDatiAnagrafe"></param>
        /// <param name="anagraficaType"></param>
        /// <param name="idGroupToInsert"></param>
        /// <param name="isFrontend"></param>
        /// <param name="activationKey"></param>
        /// <param name="currentNetworkPath"></param>
        /// <returns></returns>
        public static User CreateUserWithPasswordRefresh(string userName, string password, VfManager formDatiAnagrafe, string anagraficaType, int idGroupToInsert, bool isFrontend = false, string activationKey = "", string currentNetworkPath = "")
        {
            using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                User user;
                try
                {
                    IGenericDAO<User> userDao = new CriteriaNhibernateDAO<User>(sess);

                    userName = userName.Replace("'", "''");

                    user = new User();

                    if (System.Web.Configuration.WebConfigurationManager.AppSettings["ManageRegRequests"] == "0" || !isFrontend)
                        user.Registrato = true;

                    if (isFrontend)
                        user.CurrentNetworkPath = currentNetworkPath;

                    user.UserName = userName;
                    user.Password = password;

                    user.NuovaPassword = new G2Core.Common.PasswordGenerator(8).Generate();

                    user.CreationTime = DateTime.Now;
                    if (isFrontend)
                    {
                        user.LastAccess = DateTime.Now;
                    }
                    else
                    {
                        user.LastAccess = DateTime.MinValue;
                    }
                    user.LastUpdate = DateTime.Now;
                    user.AnagraficaType = anagraficaType;
                    user.ActivationKey = activationKey;

                    

                    UserProfile profile = new UserProfile();
                    profile.Nome = userName;

                    user.Profile = profile;

                    Hashtable anagrafica = new Hashtable();

                    ISession mapSession = sess.GetSession(EntityMode.Map);

                    string anagrafeName = anagraficaType.Replace("Anagrafica", "");

                    int idAnagrafica = 0;
                    CustomAnagrafe.CustomAnagrafe cAnagrafe = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(anagrafeName, sess);
                    if (cAnagrafe != null)
                    {
                        if (cAnagrafe.EmailAsUserName)
                        {
                            Hashtable anagrafe = GetAnagraficaByUserName(anagraficaType, "Email", userName, mapSession);
                            if (anagrafe != null)
                            {
                                idAnagrafica = int.Parse(anagrafe["ID"].ToString());
                                //anagrafica = anagrafe;
                            }
                        }
                        else
                        {
                            CustomField usernameField = cAnagrafe.CustomFields.ToList().Find(x => x.IsUsername);
                            if (usernameField != null)
                            {
                                Hashtable anagrafe = GetAnagraficaByUserName(anagraficaType, usernameField.Name, userName, mapSession);
                                if (anagrafe != null)
                                {
                                    idAnagrafica = int.Parse(anagrafe["ID"].ToString());
                                    //anagrafica = anagrafe;
                                }
                            }
                        }
                    }


                    foreach (VfGeneric field in formDatiAnagrafe.Fields.Where(x => x.Enabled))
                    {
                        //condizione aggiunta per evitare di far lanciare l'eccezione quando in una dropdown (non obbligatoria) che fa riferimento ad una 
                        //classe mappata non seleziona alcun valore facendo si che il PostbackValueObject, lanci un'eccezione.
                        if (!string.IsNullOrEmpty(field.PostBackValue))
                        {
                            //if (anagrafica.ContainsKey(field.Key.Replace(anagrafeName.Replace(" ", "_") + "_", "").Replace("_", " ")))
                            //{
                            //    string key = field.Key.Replace(anagrafeName.Replace(" ", "_") + "_", "").Replace("_", " ");
                            //    anagrafica[key] = field.PostbackValueObject;
                            //}
                            //else
                            anagrafica.Add(field.Key.Replace(anagrafeName.Replace(" ", "_") + "_", "").Replace("_", " "), field.PostbackValueObject);
                        }
                    }

                    if (idAnagrafica > 0)
                    {
                        anagrafica.Add("ID", idAnagrafica);
                        mapSession.Merge(anagraficaType, anagrafica);
                        mapSession.Flush();
                    }
                    else
                    {
                        mapSession.SaveOrUpdate(anagraficaType, anagrafica);
                        mapSession.Flush();
                    }

                    user.AnagraficaMapped = anagrafica;

                    userDao.SaveOrUpdate(user);

                    Group groupToInsertTo = GroupsBusinessLogic.GetGroupById(idGroupToInsert, sess);

                    groupToInsertTo.AddUserToGroup(user, GrantAdministrationTypes.User, ContentAdministrationTypes.Write, sess);

                    tx.Commit();

                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, "Errore nella creazione dell'utente: " + ex.Message);

                    tx.Rollback();
                    return null;
                }
                return user;
            }
        }




        public static List<IDictionary> BuilAllegatiDictionaries(ICollection<AllegatoUtente> allegati)
        {
            List<IDictionary> dizionario = new List<IDictionary>();
            foreach (AllegatoUtente allegato in allegati)
            {
                IDictionary diz = new Hashtable();
                diz.Add("ID", null);
                diz.Add("File", allegato.File);
                diz.Add("FileName", allegato.FileName);
                diz.Add("ContentType", allegato.ContentType);
                dizionario.Add(diz);
            }
            return dizionario;
        }

        public static void CleanAnagrafeData(ISession sessionExternal, Hashtable dataToInsert, List<IDictionary> allegatiDictionaries)
        {
            Hashtable updates = new Hashtable();

            dataToInsert.Remove("ID");
            dataToInsert.Remove("User");
            dataToInsert.Remove("$type$");

            if (allegatiDictionaries.Count == 0)
                dataToInsert.Remove("Allegati");
            else
                dataToInsert["Allegati"] = allegatiDictionaries;

            foreach (DictionaryEntry entry in dataToInsert)
            {
                if (entry.Value != null && entry.Key.ToString() != "Allegati")
                {
                    // prendo comuni, province, regione e in generale tutto quello con le chiavi esterne
                    if (!TypeIsPrimitive(entry.Value.GetType()) && entry.Value is ICollection && !(entry.Value is ICollection<IDictionary>))
                    {
                        Hashtable dati = new Hashtable();

                        Hashtable datiDaImportare = entry.Value as Hashtable;

                        Type tipo = CustomAnagrafeBusinessLogic.GetTypeFromTypeName(datiDaImportare["$type$"].ToString());

                        int id = int.Parse(datiDaImportare["ID"].ToString());

                        var oggetto = sessionExternal.Get(tipo, id);

                        PropertyInfo[] proprieta = oggetto.GetType().GetProperties();
                        foreach (PropertyInfo pi in proprieta)
                        {
                            dati.Add(pi.Name, pi.GetValue(oggetto, null));
                        }

                        updates.Add(entry.Key, dati);

                        continue;
                    }
                }
            }

            // effettuo gli aggiornamenti
            foreach (DictionaryEntry upd in updates)
            {
                dataToInsert[upd.Key] = upd.Value;
            }
        }

        public static void SetCollectionsAnagrafeData(ISession sessionExternal, Hashtable dataToInsert, ICollection<AnagraficaCollection> collections)
        {
            Hashtable updates = new Hashtable();

            foreach (AnagraficaCollection collection in collections)
            {
                List<IDictionary> dizionario = new List<IDictionary>();

                foreach (ItemOfCollection item in collection.items)
                {
                    Hashtable dati = new Hashtable();
                    Type tipo = CustomAnagrafeBusinessLogic.GetTypeFromTypeName(item.type);

                    var oggetto = sessionExternal.Get(tipo, item.id);

                    PropertyInfo[] proprieta = oggetto.GetType().GetProperties();
                    foreach (PropertyInfo pi in proprieta)
                    {
                        dati.Add(pi.Name, pi.GetValue(oggetto, null));
                    }

                    dizionario.Add(dati);
                }

                updates.Add(collection.key, dizionario);
            }

            foreach (DictionaryEntry upd in updates)
            {
                dataToInsert[upd.Key] = upd.Value;
            }
        }

        private static bool TypeIsPrimitive(Type t)
        {
            return (t.IsPrimitive || t == typeof(Decimal) || t == typeof(String)
                || t == typeof(DateTime) || t == typeof(TimeSpan) || t == typeof(DateTimeOffset));

        }

        #region Notifiche
        public static int CountNotificheFrontendNonLette(User user)
        {
            IGenericDAO<NotificaFrontend> dao = new CriteriaNhibernateDAO<NotificaFrontend>(GetCurrentSession());

            SearchParameter destinatarioSP = new SearchParameter(null, "Destinatario", user, Finder.ComparisonCriteria.Equals, false);
            SearchParameter networkSP = new SearchParameter(null, "Network", NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID , Finder.ComparisonCriteria.Equals, false);
            SearchParameter nonLetteSP = new SearchParameter(null, "Letta", false, Finder.ComparisonCriteria.Equals, false);

            return dao.FindByCriteriaCount(new SearchParameter[] { destinatarioSP, nonLetteSP });
        }

        public static void SaveOrUpdateNotifica(Notifica notifica)
        {
            IGenericDAO<Notifica> dao = new CriteriaNhibernateDAO<Notifica>(GetCurrentSession());
            dao.SaveOrUpdate(notifica);
        }

        public static void SaveOrUpdateNotifica(Notifica notifica, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Notifica> dao = new CriteriaNhibernateDAO<Notifica>(innerSession);
            dao.SaveOrUpdate(notifica);
        }

        public static void DeleteNotifica(Notifica notifica, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Notifica> dao = new CriteriaNhibernateDAO<Notifica>(innerSession);
            notifica.Destinatario.Notifiche.Remove(notifica);
            notifica.Responsabile = null;
            dao.Delete(notifica);
        }

        public static IList<Notifica> GetNotificaByResponsabile(NetCms.Users.User responsabile, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            CriteriaNhibernateDAO<Notifica> dao = new CriteriaNhibernateDAO<Notifica>(innerSession);

            SearchParameter userSP = new SearchParameter(null, "Responsabile", responsabile, Finder.ComparisonCriteria.Equals, false);


            return dao.FindByCriteria(new SearchParameter[] { userSP });
        }

        public static Notifica GetNotificaById(int idNotifica, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Notifica> dao = new CriteriaNhibernateDAO<Notifica>(innerSession);
            return dao.GetById(idNotifica);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="destinatario">Indicare l'oggetto utente </param>
        /// <param name="tipoNotifica">Specificare la tipologia della notifica</param>
        /// <param name="idBasicNetwork">Specificare zero se non si vuole filtrare per network</param>
        /// <param name="innerSession">Sessione nhibernate non obbligatorio</param>
        /// <returns></returns>
        public static List<Notifica> GetNotifiche(NetCms.Users.User destinatario, Notifica.TipoNotifica tipoNotifica, int idBasicNetwork, ISession innerSession = null) 
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            List<Notifica> notifiche = new List<Notifica>();

            List<SearchParameter> searchParameters = new List<SearchParameter>();

            CriteriaNhibernateDAO<Notifica> dao = new CriteriaNhibernateDAO<Notifica>(innerSession);

            SearchParameter userSP = new SearchParameter(null, "Destinatario", destinatario, Finder.ComparisonCriteria.Equals, false);
            searchParameters.Add(userSP);
            
            if (idBasicNetwork > 0)
            {
                NetCms.Networks.BasicNetwork basicNetwork = NetCms.Networks.NetworksBusinessLogic.GetById(idBasicNetwork, innerSession);
                SearchParameter networkSP = new SearchParameter(null, "Network", basicNetwork, Finder.ComparisonCriteria.Equals, false);
                searchParameters.Add(networkSP);
            }

            //SearchParameter typeSP = new SearchParameter(null, "" , tipoNotifica, Finder.ComparisonCriteria.Equals, false);
            //searchParameters.Add(typeSP);

            IDictionary<string, bool> order = new Dictionary<string, bool>();
            order.Add(Notifica.Columns.Data.ToString(), true);

            List<Notifica> results = dao.FindByCriteria(order,searchParameters.ToArray()).ToList();
          
            if (results != null && results.Count > 0)
                notifiche = results.Where(x => x.Tipologia == tipoNotifica).ToList();

            return notifiche;
        }

        public static List<Notifica> GetNotificheNonCancellate(NetCms.Users.User destinatario, Notifica.TipoNotifica tipoNotifica, int idBasicNetwork, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            CriteriaNhibernateDAO<Notifica> dao = new CriteriaNhibernateDAO<Notifica>(innerSession);

            NetCms.Networks.BasicNetwork basicNetwork = NetCms.Networks.NetworksBusinessLogic.GetById(idBasicNetwork, innerSession);

            SearchParameter userSP = new SearchParameter(null, "Destinatario", destinatario, Finder.ComparisonCriteria.Equals, false);
            SearchParameter networkSP = new SearchParameter(null, "Network", basicNetwork, Finder.ComparisonCriteria.Equals, false);
            SearchParameter deletedSP = new SearchParameter(null, "Cancellata", false, Finder.ComparisonCriteria.Equals, false);

            List<Notifica> notifiche = new List<Notifica>();

            List<Notifica> results = dao.FindByCriteria(new SearchParameter[] { userSP, networkSP,deletedSP }).ToList();
            if (results != null && results.Count > 0)
                notifiche = results.Where(x => x.Tipologia == tipoNotifica).ToList();

            return notifiche;
        }

        /// <summary>
        /// Metodo implementato per individuare specificatamente le notifiche utente non dipendenti dalla network (quindi si ipotizza di backoffice molto probabilmente).
        /// In questo modo le applicazioni verticale non di network possono utilizzare comunque le notifiche, lasciando a null il campo Network.
        /// </summary>
        /// <param name="destinatario">Utente destinatario della notifica</param>
        /// <param name="tipoNotifica">Tipo notifica</param>
        /// <param name="innerSession">Sessione DB opzionale</param>
        /// <returns></returns>
        public static List<Notifica> GetNotificheNonCancellateNonDiNetwork(NetCms.Users.User destinatario, Notifica.TipoNotifica tipoNotifica, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            CriteriaNhibernateDAO<Notifica> dao = new CriteriaNhibernateDAO<Notifica>(innerSession);

            SearchParameter userSP = new SearchParameter(null, "Destinatario", destinatario, Finder.ComparisonCriteria.Equals, false);
            SearchParameter networkSP = new SearchParameter(null, "Network", null, Finder.ComparisonCriteria.Null, false);
            SearchParameter deletedSP = new SearchParameter(null, "Cancellata", false, Finder.ComparisonCriteria.Equals, false);

            List<Notifica> notifiche = new List<Notifica>();

            List<Notifica> results = dao.FindByCriteria(new SearchParameter[] { userSP, networkSP, deletedSP }).ToList();
            if (results != null && results.Count > 0)
                notifiche = results.Where(x => x.Tipologia == tipoNotifica).ToList();

            return notifiche;
        }

        public static Notifica GetNotificaByTesto(NetCms.Users.User destinatario,string testo, Notifica.TipoNotifica tipoNotifica, int idBasicNetwork, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            CriteriaNhibernateDAO<Notifica> dao = new CriteriaNhibernateDAO<Notifica>(innerSession);

            NetCms.Networks.BasicNetwork basicNetwork = NetCms.Networks.NetworksBusinessLogic.GetById(idBasicNetwork, innerSession);

            SearchParameter userSP = new SearchParameter(null, "Destinatario", destinatario, Finder.ComparisonCriteria.Equals, false);
            SearchParameter networkSP = new SearchParameter(null, "Network", basicNetwork, Finder.ComparisonCriteria.Equals, false);
            SearchParameter textSP = new SearchParameter(null, "Testo", testo, Finder.ComparisonCriteria.Equals, false);
                        
            Notifica results = dao.GetByCriteria(new SearchParameter[] { userSP, networkSP,textSP });
            
            return results;
        }

        public static ICollection<NotificaBackoffice> FindAllNotificheBackoffice()
        {
            IGenericDAO<NotificaBackoffice> dao = new CriteriaNhibernateDAO<NotificaBackoffice>(GetCurrentSession());
            return dao.FindAll();
        }

        public static void AddSysNotify(int idMittUser, int idDestUser, string actionTitle, string actionDescription, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            NotificaSistema notifica = new NotificaSistema();
            notifica.Data = DateTime.Now;
            notifica.Destinatario = UsersBusinessLogic.GetById(idDestUser, innerSession);
            notifica.Titolo = actionTitle;
            notifica.Testo = actionDescription;
            
            if (idMittUser != 0) // indica una notifica di sistema
            notifica.Responsabile = UsersBusinessLogic.GetById(idMittUser, innerSession);

            NetCms.Users.UsersBusinessLogic.SaveOrUpdateNotifica(notifica, innerSession);
        }

        #endregion

        #region Metodi per Users-Networks

        public static bool EnableNetwork(User user, int netId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<UserNetwork> dao = new CriteriaNhibernateDAO<UserNetwork>(innerSession);

            UserNetwork association = new UserNetwork();
            association.Utente = user;
            association.Network = NetCms.Networks.NetworksManager.GlobalIndexer.GetNetworkByID(netId);

            user.AssociatedNetworks.Add(association);

            dao.SaveOrUpdate(association);
            return true;
        }

        public static bool DisableNetwork(User user, int netId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<UserNetwork> dao = new CriteriaNhibernateDAO<UserNetwork>(innerSession);

            UserNetwork assToDelete = user.AssociatedNetworks.Where(x => x.Network.ID == netId).First();
            user.AssociatedNetworks.Remove(assToDelete);
            dao.Delete(assToDelete);
            return true;
        }


        public static bool DisableAllUsersByNetwork(int netId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<UserNetwork> dao = new CriteriaNhibernateDAO<UserNetwork>(innerSession);

            SearchParameter networkSP = new SearchParameter(null, "Network.ID", netId, Finder.ComparisonCriteria.Equals, false);
            IList<UserNetwork> lista = dao.FindByCriteria(new SearchParameter[] { networkSP });
            IList<int> Ids = new List<int>();
            foreach (UserNetwork ass in lista)
            {
                Ids.Add(ass.ID);
                ass.Utente.AssociatedNetworks.Remove(ass);
            }
            foreach (int id in Ids)
            {
                UserNetwork assToDelete = dao.GetById(id);
                dao.Delete(assToDelete);
            }
            return true;
        }


        public static ICollection<UserNetwork> FindNetworksByUser(User user)
        {
            IGenericDAO<UserNetwork> dao = new CriteriaNhibernateDAO<UserNetwork>(GetCurrentSession());
            SearchParameter userSp = new SearchParameter(null, "Utente", user, Finder.ComparisonCriteria.Equals, false);
            return dao.FindByCriteria(new SearchParameter[] { userSp });
        }

        #endregion
    }
}
