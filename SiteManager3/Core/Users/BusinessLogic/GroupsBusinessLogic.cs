﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NetCms.DBSessionProvider;
using NHibernate.Criterion;
using NHibernate.Transform;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using NetCms.Users.Search;
using System.Collections;
using NetService.Utility.ValidatedFields;

namespace NetCms.Users
{
    public static class GroupsBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        #region Metodi relativi ai Gruppi

        public static Group GetGroupById(int id, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Group> dao = new CriteriaNhibernateDAO<Group>(session);
            return dao.GetById(id);
        }

        public static Group GetRootGroup(ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Group> dao = new CriteriaNhibernateDAO<Group>(session);

            SearchParameter depthSP = new SearchParameter(null, "Depth", 0, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { depthSP });
        }

        public static Group SaveOrUpdateGroup(Group group, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Group> dao = new CriteriaNhibernateDAO<Group>(innerSession);
            return dao.SaveOrUpdate(group);
        }

        public static ICollection<Group> FindAllOrderedByTree()
        {
            IGenericDAO<Group> dao = new CriteriaNhibernateDAO<Group>(GetCurrentSession());
            return dao.FindAll("Tree", false);
        }

        public static GroupAssociation SaveOrUpdateGroupAssociation(GroupAssociation groupAssociation, bool flush=false, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<GroupAssociation> dao = new CriteriaNhibernateDAO<GroupAssociation>(session);
            GroupAssociation result = dao.SaveOrUpdate(groupAssociation);
            if (flush)
                session.Flush();
            return result;
        }

        public static GroupAssociation GetGroupAssociationByUserAndGroup(int idUser, int idGroup, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<GroupAssociation> dao = new CriteriaNhibernateDAO<GroupAssociation>(session);
            SearchParameter userSP = new SearchParameter(null, "User.ID", idUser, Finder.ComparisonCriteria.Equals, false);
            SearchParameter groupSP = new SearchParameter(null, "Group.ID", idGroup, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { userSP, groupSP });
        }

        public static bool DeleteGroupAssociation(GroupAssociation groupAssociation, bool flush = false, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<GroupAssociation> dao = new CriteriaNhibernateDAO<GroupAssociation>(session);

            try
            {
                groupAssociation.User.AssociatedGroups.Remove(groupAssociation);
                groupAssociation.Group.AssociatedUsers.Remove(groupAssociation);
                dao.Delete(groupAssociation);
                if (flush)
                    session.Flush();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static bool RemoveUserFromGroupTree(Group group, User user)
        {
            try
            {
                ICriteria criteria = GetCurrentSession().CreateCriteria(typeof(GroupAssociation));
                criteria.Add(Restrictions.Eq("User", user));

                DetachedCriteria groupCriteria = DetachedCriteria.For(typeof(Group)).SetProjection(Projections.Property("ID"));
                groupCriteria.Add(Restrictions.Like("Tree", group.Tree, MatchMode.Start));

                criteria.Add(Subqueries.PropertyIn("Group.ID", groupCriteria));

                var gatoDelete = criteria.List();

                foreach (GroupAssociation ga in gatoDelete)
                {
                    group.AssociatedUsers.Remove(ga);
                    user.AssociatedGroups.Remove(ga);
                    GetCurrentSession().Delete(ga);
                }
            }
            catch { return false; }
            return true;
        }

        public static Group GetGroupByNameAndParent(string name, Group parent, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Group> dao = new CriteriaNhibernateDAO<Group>(session);

            SearchParameter nameSP = new SearchParameter(null, "Name", name, Finder.ComparisonCriteria.Equals, false);
            SearchParameter parentSP = new SearchParameter(null, "Parent", parent, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { nameSP, parentSP });
        }

        public static Group GetGroupByNameAndParentName(string name, string parentName, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Group> dao = new CriteriaNhibernateDAO<Group>(session);

            SearchParameter nameSP = new SearchParameter(null, "Name", name, Finder.ComparisonCriteria.Equals, false);

            SearchParameter parentNameSP = new SearchParameter(null, "Name", parentName, Finder.ComparisonCriteria.Equals, false);
            SearchParameter joinParentSP = new SearchParameter(null, "Parent", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { parentNameSP });

            return dao.GetByCriteria(new SearchParameter[] { nameSP, joinParentSP });
        }

        public static ICollection<Group> FindGroupsByTreeStart(string treeStart, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Group> dao = new CriteriaNhibernateDAO<Group>(session);
            SearchParameter treeStartSP = new SearchParameter(null, "Tree", treeStart, Finder.ComparisonCriteria.LikeStart, false);
            SearchParameter[] searchParameters = new SearchParameter[] { treeStartSP };
            return dao.FindByCriteria(searchParameters);
        }

        public static void DeleteGroup(Group group)
        {
            IGenericDAO<Group> dao = new CriteriaNhibernateDAO<Group>(GetCurrentSession());
            group.Parent.Groups.Remove(group);
            foreach (GroupAssociation associazione in group.AssociatedUsers)
                associazione.User.AssociatedGroups.Remove(associazione);
            dao.Delete(group);
        }
        
        public static ICollection<Group> FindGroupsAssociatedToUser(User user, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<GroupAssociation> dao = new CriteriaNhibernateDAO<GroupAssociation>(session);
            SearchParameter userSP = new SearchParameter(null, "User", user, Finder.ComparisonCriteria.Equals, false);
            return dao.FindByCriteria(new SearchParameter[] { userSP }).Select(x=>x.Group).OrderBy(x=>x.Tree).ToList();
        }

        public static ICollection<Group> FindGroupsByUser(User user, int pageStart = -1, int pageSize = -1)
        {
            ICriteria criteria = FindGroupByUserCriteria(user);

            if (pageStart > -1 && pageSize > -1)
            {
                criteria.SetFirstResult(pageStart);
                criteria.SetMaxResults(pageSize);
            }

            return criteria.List<Group>();
        }

        public static int FindGroupsByUserCount(User user)
        {
            ICriteria countCriteria = CriteriaTransformer.TransformToRowCount(FindGroupByUserCriteria(user));
            return countCriteria.UniqueResult<int>();
        }

        private static ICriteria FindGroupByUserCriteria(User user)
        {
            ICriteria criteria = GetCurrentSession().CreateCriteria(typeof(Group));
            criteria.AddOrder(Order.Asc("Tree"));

            ICriteria associatedGroupCriteria = criteria.CreateCriteria("AssociatedUsers","ga", NHibernate.SqlCommand.JoinType.LeftOuterJoin);
            ICriteria macroGroupCriteria = criteria.CreateCriteria("MacroGroup", NHibernate.SqlCommand.JoinType.LeftOuterJoin);

            // MyGroupsConditions
            Disjunction groupsOr = new Disjunction();

            //associatedGroupCriteria.CreateAlias("Group", "gr");

            foreach (GroupAssociation ga in user.AssociatedGroupsOrdered)
            {
                Conjunction con = new Conjunction();
                if (ga.GrantsRole != NetCms.Users.GrantAdministrationTypes.User)
                {
                    con.Add(Restrictions.Like("Tree", ga.Group.Tree, MatchMode.Start));
                    if (ga.GrantsRole == NetCms.Users.GrantAdministrationTypes.Administrator)
                        con.Add(Restrictions.Eq("ga.Type", 0));
                }
                groupsOr.Add(con);
            }

            criteria.Add(groupsOr);

            criteria = criteria.SetResultTransformer(new NHibernate.Transform.DistinctRootEntityResultTransformer());

            return criteria;
        }

        private static ICriteria GroupsIAdminPrivate(User user, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICriteria criteria = GetCurrentSession().CreateCriteria(typeof(Group));

            ICriteria profileCriteria = criteria.CreateCriteria("Profile");

            // MyGroupsConditions
            Disjunction groupsOr = new Disjunction();
            foreach (GroupAssociation ga in user.AssociatedGroupsOrdered)
            {
                if (ga.GrantsRole != NetCms.Users.GrantAdministrationTypes.User)
                {
                    groupsOr.Add(Restrictions.Like("Tree", ga.Group.Tree, MatchMode.Start));
                }
            }

            criteria.Add(groupsOr);

            if (additionalJoins != null)
            {
                foreach (SearchJoin join in additionalJoins)
                {
                    switch (join.Criteria)
                    {
                        case SearchJoin.CriteriaType.Group: criteria.CreateCriteria(join.Property);
                            break;
                        case SearchJoin.CriteriaType.Profile: profileCriteria.CreateCriteria(join.Property);
                            break;
                    }
                }
            }

            if (additionalConditions != null)
            {
                foreach (SearchCondition condition in additionalConditions)
                {
                    BuildCriteriaRestriction(criteria, profileCriteria, condition);
                }
            }
            return criteria;
        }

        public static ICollection<Group> GroupsIAdmin(int pageStart, int pageSize, User user, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICriteria criteriaResult = GroupsIAdminPrivate(user, additionalJoins, additionalConditions);

            return criteriaResult.SetFirstResult(pageStart).SetMaxResults(pageSize).List<Group>();
        }

        public static int GroupsIAdminCount(User user, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICriteria criteriaResult = GroupsIAdminPrivate(user, additionalJoins, additionalConditions);

            ICriteria countCriteria = CriteriaTransformer.TransformToRowCount(criteriaResult);
            return countCriteria.UniqueResult<int>();
        }

        public enum SearchFor { ID, Key };

        public static Group FindGroup(Group startGroup, string key, SearchFor searchFor)
        {
            Group foundNode = null;

            switch (searchFor)
            {
                case SearchFor.ID:
                    {
                        foundNode = FindGroupByID(startGroup, key);
                        break;
                    }

                case SearchFor.Key:
                    {
                        foundNode = FindGroupByKey(startGroup, key);
                        break;
                    }
            }

            if (foundNode != null)
                return foundNode;

            IList<Group> list = GetCurrentSession().CreateCriteria(typeof(Group)).Add(Expression.Eq("Parent", startGroup)).SetFetchMode("Groups", FetchMode.Join)
                .SetResultTransformer(new DistinctRootEntityResultTransformer())
                .List<Group>();

            return TraverseFolderTree(list, key, searchFor);

        }

        private static Group TraverseFolderTree(IList<Group> list, string key, SearchFor searchFor)
        {
            Group foundNode = null;

            foreach (Group group in list)
            {
                switch (searchFor)
                {
                    case SearchFor.ID:
                        {
                            foundNode = FindGroupByID(group, key);
                            break;
                        }

                    case SearchFor.Key:
                        {
                            foundNode = FindGroupByKey(group, key);
                            break;
                        }
                }

                if (foundNode != null)
                    return foundNode;

                Group result = null;
                if (group.Groups.Count > 0)
                    result = TraverseFolderTree(group.Groups.Cast<Group>().ToList(), key, searchFor);

                if (result != null)
                    return result;
            }
            return null;
        }

        private static Group FindGroupByID(Group group, string key)
        {
            if (group.ID == int.Parse(key)) return group;
            return null;
        }

        private static Group FindGroupByKey(Group group, string key)
        {
            if (group.SystemName == key) return group;
            return null;
        }

        private static void BuildCriteriaRestriction(ICriteria groupCriteria, ICriteria profileCriteria, SearchCondition condition)
        {
            ICriteria criteria = null;

            switch (condition.Criteria)
            {
                case SearchCondition.CriteriaType.Profile: criteria = profileCriteria;
                    break;
                case SearchCondition.CriteriaType.Group: criteria = groupCriteria;
                    break;
            }

            switch (condition.Comparison)
            {
                case NetCms.Users.Search.SearchCondition.ComparisonType.Equals:
                    criteria.Add(Restrictions.Eq(condition.Property, condition.Value));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.EqualsIgnoreCase:
                    criteria.Add(Restrictions.Eq(condition.Property, condition.Value).IgnoreCase());
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.GreaterEquals:
                    criteria.Add(Restrictions.Ge(condition.Property, condition.Value));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.GreaterThan:
                    criteria.Add(Restrictions.Gt(condition.Property, condition.Value));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.In:
                    {
                        ICollection values = null;
                        if (condition.Value is ICollection)
                        {
                            values = (ICollection)condition.Value;
                            criteria.Add(Restrictions.In(condition.Property, values));
                        }
                        break;
                    }

                case NetCms.Users.Search.SearchCondition.ComparisonType.LessEquals:
                    criteria.Add(Restrictions.Le(condition.Property, condition.Value));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.LessThan:
                    criteria.Add(Restrictions.Lt(condition.Property, condition.Value));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.Like:
                    criteria.Add(Restrictions.InsensitiveLike(condition.Property, condition.Value.ToString(), MatchMode.Anywhere));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.LikeStart:
                    criteria.Add(Restrictions.InsensitiveLike(condition.Property, condition.Value.ToString(), MatchMode.Start));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.LikeEnd:
                    criteria.Add(Restrictions.InsensitiveLike(condition.Property, condition.Value.ToString(), MatchMode.End));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.Not:
                    criteria.Add(Restrictions.Not(Restrictions.Eq(condition.Property, condition.Value)));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.NotNull:
                    criteria.Add(Restrictions.IsNotNull(condition.Property));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.Null:
                    criteria.Add(Restrictions.IsNull(condition.Property));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.IsEmpty:
                    criteria.Add(Restrictions.IsEmpty(condition.Property));
                    break;

                case NetCms.Users.Search.SearchCondition.ComparisonType.IsNotEmpty:
                    criteria.Add(Restrictions.IsNotEmpty(condition.Property));
                    break;
            }

            switch (condition.Criteria)
            {
                case SearchCondition.CriteriaType.Profile: profileCriteria = criteria;
                    break;
                case SearchCondition.CriteriaType.Group: groupCriteria = criteria;
                    break;
            }
        }

        #endregion

        #region Metodi relativi ai MacroGruppi

        public static MacroGroup GetMacroGroupById(int id)
        {
            IGenericDAO<MacroGroup> dao = new CriteriaNhibernateDAO<MacroGroup>(GetCurrentSession());

            return dao.GetById(id);
        }

        public static MacroGroup GetMacroGroupByKey(string key, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<MacroGroup> dao = new CriteriaNhibernateDAO<MacroGroup>(session);

            SearchParameter keySP = new SearchParameter(null, "Key", key, Finder.ComparisonCriteria.Equals, false);

            return dao.GetByCriteria(new SearchParameter[] { keySP });
        }

        public static ICollection<MacroGroup> FindAllMacroGroups(ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<MacroGroup> dao = new CriteriaNhibernateDAO<MacroGroup>(session);
            return dao.FindAll();
        }

        public static MacroGroup CreateMacroGroup(VfManager form)
        {
            IGenericDAO<MacroGroup> dao = new CriteriaNhibernateDAO<MacroGroup>(GetCurrentSession());

            MacroGroup macrogruppo = new MacroGroup();
            form.FillObject(macrogruppo);

            return dao.SaveOrUpdate(macrogruppo);
        }

        public static MacroGroup ModifyMacroGroup(VfManager form,MacroGroup macrogroup)
        {
            IGenericDAO<MacroGroup> dao = new CriteriaNhibernateDAO<MacroGroup>(GetCurrentSession());

            form.FillObject(macrogroup);

            return dao.SaveOrUpdate(macrogroup);
        }

        #endregion
    }
}
