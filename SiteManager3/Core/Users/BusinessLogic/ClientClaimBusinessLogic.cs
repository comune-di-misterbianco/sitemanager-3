﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NetCms.DBSessionProvider;
using GenericDAO.DAO;
using Users.Models.Client;
using NetService.Utility.RecordsFinder;

namespace NetCms.Users
{    
    public static class ClientClaimBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static ClientClaim SaveOrUpdateProfile(ClientClaim claim, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ClientClaim> dao = new CriteriaNhibernateDAO<ClientClaim>(innerSession);

            return dao.SaveOrUpdate(claim);
        }

        public static ClientClaim GetById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ClientClaim> dao = new CriteriaNhibernateDAO<ClientClaim>(innerSession);

            return dao.GetById(id);
        }

        public static ClientClaim GetByClient(string clientId, string claimType = null, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ClientClaim> dao = new CriteriaNhibernateDAO<ClientClaim>(innerSession);
            SearchParameter clientIdSP = new SearchParameter(null, "ClientId", clientId, Finder.ComparisonCriteria.Like, false);
            SearchParameter clientSP = new SearchParameter("", "ClientRef", SearchParameter.RelationTypes.OneToOne, new SearchParameter[] { clientIdSP });
            SearchParameter clientTypeSP = null;
            if (!string.IsNullOrEmpty(claimType))
                clientTypeSP = new SearchParameter(null, "ClaimType", claimType, Finder.ComparisonCriteria.Like, false);
            return dao.GetByCriteria(clientTypeSP !=  null ? new SearchParameter[] { clientSP, clientTypeSP } : new SearchParameter[] { clientSP });
        }

        public static void Delete(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<ClientClaim> dao = new CriteriaNhibernateDAO<ClientClaim>(innerSession);
            ClientClaim claim = dao.GetById(id);
            dao.Delete(claim);
        }
    }
}
