﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NetCms.DBSessionProvider;
using GenericDAO.DAO;
using Users.Models.Client;
using NetService.Utility.RecordsFinder;

namespace NetCms.Users
{    
    public static class ClientBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static Client SaveOrUpdateProfile(Client client, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Client> dao = new CriteriaNhibernateDAO<Client>(innerSession);

            return dao.SaveOrUpdate(client);
        }

        public static Client GetById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Client> dao = new CriteriaNhibernateDAO<Client>(innerSession);

            return dao.GetById(id);
        }

        public static IList<Client> FindAll(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Client> dao = new CriteriaNhibernateDAO<Client>(innerSession);

            return dao.FindAll();
        }

        public static Client GetByClientId(string clientId, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Client> dao = new CriteriaNhibernateDAO<Client>(innerSession);
            SearchParameter clientIdSP = new SearchParameter(null, "ClientId", clientId, Finder.ComparisonCriteria.Like, false);
            return dao.GetByCriteria(new SearchParameter[] { clientIdSP });
        }

        public static void Delete(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<Client> dao = new CriteriaNhibernateDAO<Client>(innerSession);
            Client client = dao.GetById(id);
            dao.Delete(client);
        }
    }
}
