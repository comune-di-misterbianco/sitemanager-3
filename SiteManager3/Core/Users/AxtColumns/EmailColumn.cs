﻿//using System.Data;
//using System.Web.UI.HtmlControls;
//using G2Core.AdvancedXhtmlTable;
//using NetCms.Users;
///// <summary>
///// Summary description for StructureGrants
///// </summary>

//namespace NetCms.Users
//{
//    public class EmailColumn : AxtGenericColumn
//    {
//        public bool SkipRowIfNotExists { get; set; }

//        public EmailColumn(string fieldname, string caption)
//            : base(fieldname, caption)
//        {
//            SkipRowIfNotExists = true;
//            //if (NetCms.Configurations.Generics.DebugMode != Configurations.Generics.DebugModeStates.Off)
//            //  this.ColSpan = 2;
//        }
//        public override HtmlGenericControl GetCellControl(DataRow row)
//        {
//            string value = row[this.DatabaseFieldName].ToString();
//            string anagraficaType = row["AnagraficaType_User"].ToString();
//            HtmlGenericControl td = new HtmlGenericControl("td");

//            if (!string.IsNullOrEmpty(anagraficaType))
//            {
//                AnagraficaBase Anagrafica = Users.Anagraphics.AnagraphicsClassesPool.BuildInstanceOf(value, anagraficaType);
//                if (Anagrafica == null)
//                    this.SkipRow = true;
//                else
//                {
//                    if (!Anagrafica.Exist)
//                    {
//                        this.SkipRow = SkipRowIfNotExists;
//                    }
//                    else
//                    {
//                        td.InnerHtml = Anagrafica.Email;
//                        this.SkipRow = false;
//                    }
//                }
//            }
//            else
//            {
//                this.SkipRow = SkipRowIfNotExists;
//            }

//            return td;
//        }

//        public override string GetExportValue(DataRow row)
//        {
//            string value = row[this.DatabaseFieldName].ToString();
//            string anagraficaType = row["AnagraficaType_User"].ToString();
//            AnagraficaBase Anagrafica = Users.Anagraphics.AnagraphicsClassesPool.BuildInstanceOf(value, anagraficaType);

//            if (Anagrafica != null && Anagrafica.Exist)
//                return Anagrafica.Email;

//            return null;
//        }
//    }
//}