﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Models.Client
{
    public class ClientClaim
    {
        public const string ClientClaimRole = "Roles";
        /// <summary>
        /// Unique identifier
        /// </summary>
        public virtual int Id { get; set; }

        /// <summary>
        /// Client reference
        /// </summary>
        public virtual Client ClientRef { get; set; }

        /// <summary>
        /// Claim type
        /// </summary>
        public virtual string ClaimType { get; set; }
        /// <summary>
        /// Claim value
        /// </summary>
        public virtual string ClaimValue { get; set; }
    }
}
