﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Models.Client
{
    /// <summary>
    /// Client class
    /// </summary>
    public class Client
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public virtual int Id { get; set; }
        /// <summary>
        /// Client app identifier
        /// </summary>
        public virtual string ClientId { get; set; }
        /// <summary>
        /// Client app secret
        /// </summary>
        public virtual string ClientSecret { get; set; }

        public virtual DateTime? DisableTimestamp { get; set; }
        public virtual bool Disabled { get; set; }
        public virtual string DisableBy { get; set; }
    }
}
