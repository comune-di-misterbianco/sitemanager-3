﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Models.Token
{
    public class JWTRequest
    {
        public string AuthenticationToken { get; set; }   
        public string UserInfo { get; set; }
    }
}
