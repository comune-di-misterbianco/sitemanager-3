﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Models.Token.UserTokens
{
    public class UserToken
    {
        public virtual int Id { get; set; }
        public virtual int UserId { get; set; }
        public virtual string LoginProvider { get; set; }
        public virtual string Name { get; set; }
        public virtual string Value { get; set; }
    }
}
