﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Models.Token
{
    public class TokenRequest
    {
        public readonly IEnumerable<string> SupportedGrantTypes = new List<string>() { "client_credentials", "password" };

        /// <summary>
        /// Unique identifier of the client application 
        /// </summary>
        [Required]
        public string ClientId { get; set; }

        /// <summary>
        /// The client application secret key
        /// </summary>
        [Required]
        public string ClientSecret { get; set; }

        /// <summary>
        /// grant type = client_credentials or password
        /// </summary>
        [Required]
        public string GrantType { get; set; }

        public string Username { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [DataType(DataType.Password)]
        public string Password { get; set; }

        /// <summary>
        /// Check if the target grant type string contains 
        /// </summary>
        public bool IsValidGrantType()
        {
            return SupportedGrantTypes.Contains(GrantType);
        }
    }    
}
