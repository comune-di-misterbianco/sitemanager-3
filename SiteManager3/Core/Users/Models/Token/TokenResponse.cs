﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Models.Token
{
    public class TokenResponse
    {
        public TokenResponse(string token, string tokentype, int expiresIn, string refreshToken)
        {
            Token = token;
            TokenType = tokentype;
            ExpiresIn = expiresIn;
            RefreshToken = refreshToken;
        }

        /// <summary>
        /// Token
        /// </summary>
        public string Token { get; }
        /// <summary>
        /// Token Type
        /// </summary>
        public string TokenType { get; }
        /// <summary>
        /// ExpiresIn
        /// </summary>
        public int ExpiresIn { get; }
        /// <summary>
        /// Refresh token
        /// </summary>
        public string RefreshToken { get; }

        public string ToString(TokenResponse tr)
        {
            return JsonConvert.SerializeObject(tr);
        }
        public TokenResponse ToTokenResponse(string str)
        {
            return JsonConvert.DeserializeObject<TokenResponse>(str);
        }
    }
}
