using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using NetCms.Connections;
using System.Collections.Generic;
using LabelsManager;
using NHibernate;
using Users.Cookie;
using Newtonsoft.Json;
using Users.Models.Token;
using System.Linq;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users
{
    //public enum OSSOLOGIN_ResultTypes
    //{
    //    NotChecked=0,
    //    NoSSOUserLogged=1,
    //    SSOUserLoggedNotImportedInSAX=2,
    //    SSOUserLoggedinSAX=3,
    //    DPIUserNotEnabled = 4,
    //    GenericError = 5        
    //}

    public class AccountManager
    {
        private const String UserAlreadyLoggedForThisPageKey = "UserAlreadyLoggedForThisPageKey";
        private const String UserAlreadyLoggedOutForThisPageKey = "UserAlreadyLoggedOutForThisPageKey";

        private static void SetLogged(User user)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Items[UserAlreadyLoggedForThisPageKey] = user;
                HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserID] = user.ID;
                HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserName] = user.UserName;
            }
        }
        public static bool UserAlreadyLogged()
        {
            return HttpContext.Current != null ? HttpContext.Current.Items.Contains(UserAlreadyLoggedForThisPageKey):false;
        }

        public static Labels UserLabels
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(UsersLabels)) as Labels;
            }
        }

        public static bool Logged
        {
            get
            {
                User current = GetCurrentAccount();
                if (GetCurrentAccount() == null)
                    current = CurrentAccount;

                return current !=null;
            }
        }

        #region SHARED COOKIE
        public static void RemoveSharedCookie(string cookieName, string domain)
        {
            HttpCookie sameSiteCookie = new HttpCookie(cookieName);
            sameSiteCookie.Domain = domain;
            sameSiteCookie.Value = string.Empty;
            sameSiteCookie.Secure = true;
            sameSiteCookie.HttpOnly = false;
            sameSiteCookie.SameSite = SameSiteMode.Strict;            
            HttpContext.Current.Response.Cookies.Add(sameSiteCookie);
        }
        public static void AddSharedCookie(string cookieName, string domain, string userId = null, string sessionId = null)
        {
            #region  Save user.ID in to shared cookie
            string cookieVal = null;
            string[] values = new string[2];
            if (!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(sessionId))
            {
                values[0] = userId;
                values[1] = sessionId;                
            }
            else
            {
                User userLogged = CurrentAccount;
                if (userLogged == null || string.IsNullOrEmpty(HttpContext.Current.Session.SessionID) )
                    throw new ArgumentNullException(nameof(userLogged));

                values[0] = userLogged.ID.ToString();
                values[1] = HttpContext.Current.Session.SessionID;
            }
            cookieVal = JsonConvert.SerializeObject(values);
            #endregion

            HttpCookie userIdCookie = new HttpCookie("shared", cookieVal);
            HttpCookie encrypteduserIdCookie = HttpSecureCookie.Encode(userIdCookie);

            // Create the cookie
            HttpCookie sameSiteCookie = new HttpCookie(cookieName);
            sameSiteCookie.Domain = domain;
            // Set a value for the cookie
            sameSiteCookie.Value = encrypteduserIdCookie.Value;
            // Set the secure flag, which Chrome's changes will require for SameSite none.
            // Note this will also require you to be running on HTTPS
            sameSiteCookie.Secure = true;
            // Set the cookie to HTTP only which is good practice unless you really do need
            // to access it client side in scripts.
            sameSiteCookie.HttpOnly = false;
            // Add the SameSite attribute, this will emit the attribute with a value of none.
            // To not emit the attribute at all set the SameSite property to -1.
            sameSiteCookie.SameSite = SameSiteMode.Strict;
            // Add the cookie to the response cookie collection
            HttpContext.Current.Response.Cookies.Add(sameSiteCookie);
        }
        #endregion

        public static void DoLogOut()
        {
            // add notifica di sistema - richiesta recupero pw 

            // TODO: aggiungere verifica su disponibilit� elementi in sessione 

            //15/04/2022 aggiungo una condizione per verificare che queste variabili di sessione siano valide e piene perch� nello sviluppo dell'applicazione per le convenzioni
            //al logout dava errore 500 e si fermava in questa chiamata al metodo addsysnotify generando eccezione di nullreference, di conseguenza non esegue tutto
            //il codice sotto non effettuando cosi il logout dal sistema.
            if(HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserID]!=null 
                && HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserName]!=null)
                NetCms.Users.UsersBusinessLogic.AddSysNotify((int)HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserID],
                                                         (int)HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserID],
                                                         string.Format(UserLabels[UsersLabels.UsersLabelsList.UserLogoutMessage], 
                                                         HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserName]), "");

            G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
            cache.RemoveCurrentUserCachedObjects();

            Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Info, "L'utente '" + HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserName] + "' ha effettuato il Logout dal sistema.");

            int idUser = -1;
            if (HttpContext.Current.Session != null)
            {
                if (HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserID] != null)
                {
                    int id = (int)HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserID];
                    idUser = id;
                    if (HttpContext.Current.Session["FoldersCanViewUSER_" + id] != null)
                        HttpContext.Current.Session["FoldersCanViewUSER_" + id] = null;
                }
                HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserID] = null;
                HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserName] = null;

            }
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Session.RemoveAll();
            if (HttpContext.Current.Request.Cookies["ASP.NET_SessionId"] != null)
            {
                HttpContext.Current.Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                HttpContext.Current.Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            #region Delete shared Token
            RemoveSharedCookie("sharedCookie", NetCms.Configurations.Generics.SsoSharedCookieDomain);
            #endregion
                       
            /// Delete all jwt
            /// 
            HttpContext.Current.Application.Remove("JWT_" + HttpContext.Current.Session.SessionID);
            HttpContext.Current.Application.Remove("AccountSessionID_USER" + idUser.ToString());

            if (!NetCms.Configurations.Debug.GenericEnabled)
                {
                    G2Core.Common.RequestVariable gotoAddress = new G2Core.Common.RequestVariable("goto", G2Core.Common.RequestVariable.RequestType.QueryString);
                    if (gotoAddress.IsValidString)
                        NetCms.Diagnostics.Diagnostics.Redirect(gotoAddress.StringValue);
                }
                if (HttpContext.Current.Request.UrlReferrer != null)
                    NetCms.Diagnostics.Diagnostics.Redirect(HttpContext.Current.Request.UrlReferrer.ToString());
           
            if ( HttpContext.Current != null )HttpContext.Current.Items[UserAlreadyLoggedOutForThisPageKey] = true;
        }
        private static bool UserAlreadyLoggedOut()
        {
            return  HttpContext.Current != null ? HttpContext.Current.Items.Contains(UserAlreadyLoggedOutForThisPageKey): false;
        }

        private static User GetCurrentAccount()
        {
            return HttpContext.Current != null ? (User)HttpContext.Current.Items[UserAlreadyLoggedForThisPageKey] : null;
        }

        public static User CurrentAccount
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    #region Logout
                    if (!UserAlreadyLoggedOut())
                    {
                        if (HttpContext.Current.Session != null &&
                            HttpContext.Current.Request.Form["SubmitSource"] != null &&
                            HttpContext.Current.Request.Form["SubmitSource"] == "Logout")
                        {
                            DoLogOut();                            
                            return null;
                        }

                        G2Core.Common.RequestVariable logout = new G2Core.Common.RequestVariable("do", G2Core.Common.RequestVariable.RequestType.QueryString);
                        if (logout.IsValidString && logout.StringValue == "logout")
                        {
                            DoLogOut();
                            return null;
                        }
                    }
                    #endregion

                    #region Login
                    if (!UserAlreadyLogged())
                    {
                        //Controllo se � stato richiesto il cambio di utenza dal Debug Login Men�
                        if (NetCms.Configurations.Debug.DebugLoginEnabled)
                        {
                            //ATTENZIONE: con il debug attivo, se si usa uid o uID come querystring in altre applicazioni o pagine, si verifica un bug che permette di impersonare l'utente che si sta modificando
                            //MAI LASCIARE IL DEBUG ATTIVO!!!
                            G2Core.Common.RequestVariable OverwriteUserID = new G2Core.Common.RequestVariable("uid", G2Core.Common.RequestVariable.RequestType.QueryString);
                            if (OverwriteUserID.IsValidInteger)
                            {
                                User usr = (User)LoginUser(OverwriteUserID.StringValue);
                                SetLogged(usr);

                                return usr;
                            }
                        }

                        //Controllo se esiste la sessione con l'account dell'utente
                        if (HttpContext.Current.Session != null)
                        {
                            object obj = HttpContext.Current.Session[Configurations.Generics.BackAccountSessionKey];
                            if (obj != null)
                            {
                                User usr = (User)obj;
                                //usr.Update();
                                var ssid = HttpContext.Current.Application["AccountSessionID_USER" + usr.ID];
                                if (ssid == null || ssid.ToString() == usr.SessionID.ToString())
                                {
                                    SetLogged(usr);
                                    return usr;
                                }
                                else
                                {
                                    HttpContext.Current.Session.Clear();
                                    HttpContext.Current.Session["AnotherBrowserLogged"] = true;
                                }
                            }

                            //Controllo se esiste la sessione con il token dell'account utente
                            /*obj = HttpContext.Current.Session[Configurations.Generics.BackAccountTokenID];
                            if (obj != null)
                            {
                                G2Core.Common.AccountToken token = (G2Core.Common.AccountToken)obj;
                                User usr = (User)User.Fabricate(token.ID.ToString());
                                SetLogged(usr);
                                return usr;
                            }*/
                            //controlla se l'utente � stato autenticato da oracle sso tramite il cookie SSO                   
                            //if (HttpContext.Current.Request.Cookies != null)
                            //{
                            //    if (HttpContext.Current.Request.Cookies["ORASSO_AUTH_HINT"] != null)
                            //    {
                            //        HttpCookie cookie = HttpContext.Current.Request.Cookies["ORASSO_AUTH_HINT"];                                    
                            //        //&& HttpContext.Current.Session["SSOUSER_NOTENABLED"] == null
                            //        if (cookie.Value != "INVALID")
                            //        {
                            //            // recupero dello stato  
                            //            string osso_state = "";
                            //            if (HttpContext.Current.Session[cookie.Value] != null)
                            //            {
                            //                osso_state = (string)HttpContext.Current.Session[cookie.Value];
                            //                // Messaggio all'utente
                            //                //HttpContext.Current.Response.Write(osso_state);
                            //            }
                            //            else
                            //            {
                            //                HttpContext.Current.Session["SSOUSER_NOTENABLED"] = null;
                            //                NetCms.Diagnostics.Diagnostics.Redirect(System.Web.Configuration.WebConfigurationManager.AppSettings["EnableOSSO"].ToLower() + "?fromurl=" + HttpContext.Current.Request.Url.ToString());

                            //            }
                            //        }
                            //    }
                            //}                            
                        }

                    }
                    else
                    {
                        return GetCurrentAccount();
                    }
                    #endregion
                }

                return null;
            }
        }

        public static User LoginUser(string id, ISession session = null)
        {
            User user;

            HttpContext.Current.Session.Clear();            
            user = UsersBusinessLogic.GetById(int.Parse(id), session);
            
            //Aggiunti perch� dava eccezione nell'uso dell'utente dopo la chiamata
            NHibernate.NHibernateUtil.Initialize(user.Profile);
            NHibernate.NHibernateUtil.Initialize(user.AssociatedGroups);
            NHibernate.NHibernateUtil.Initialize(user.AssociatedNetworks);
            NHibernate.NHibernateUtil.Initialize(user.FavoritesFolders);

            user.UpdateLastAccess();
            HttpContext.Current.Session[Configurations.Generics.BackAccountSessionKey] = user;
            HttpContext.Current.Items[UserAlreadyLoggedForThisPageKey] = user;
            HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserID] = user.ID;
            HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserName] = user.UserName;
            HttpContext.Current.Session[Configurations.Generics.BackAccountSessionKey] = user;
            HttpContext.Current.Application["AccountSessionID_USER" + user.ID] = HttpContext.Current.Session.SessionID;

            #region JWT Bearer Token            
            //if (ClientBusinessLogic.FindAll() != null && ClientBusinessLogic.FindAll().Count > 0)
            //{
            //    var clients = ClientBusinessLogic.FindAll();
            //    string jwtBearerToken = null;
            //    /// seleziono client
            //    /// di default dal web.config
            //    /// dovrebbe essere quello dell'applicazione cms
            //    /// che contiene tutti i claims
            //    /// 
            //    TokenRequest tokenRequest = new TokenRequest()
            //    {
            //        ClientId = Configurations.PortalData.DefaultClientId,
            //        ClientSecret = ClientBusinessLogic.GetByClientId(Configurations.PortalData.DefaultClientId).ClientSecret,
            //        GrantType = "password",
            //        Username = user.UserName,
            //        Password = user.Password
            //    };
            //    TokenResponse token = TokenManager.GenerateAccessToken(tokenRequest);
            //    jwtBearerToken = JsonConvert.SerializeObject(token);
            //    #region  Save user.ID in to shared cookie
            //    AddSharedCookie("sharedCookie", NetCms.Configurations.Generics.SsoSharedCookieDomain, user.ID.ToString(), HttpContext.Current.Session.SessionID);
            //    #endregion
            //    HttpCookie cookie = new HttpCookie("JWT_" + HttpContext.Current.Session.SessionID, jwtBearerToken);
            //    HttpCookie encodedCookie = HttpSecureCookie.Encode(cookie);
            //    HttpContext.Current.Application["JWT_" + HttpContext.Current.Session.SessionID] = encodedCookie.Value;
            //}
            #endregion

            return user;
        }

        public static LoginResult TryLoginUser(User user, string usedPasswordMD5)
        {
            NHibernate.NHibernateUtil.Initialize(user.Profile);
            NHibernate.NHibernateUtil.Initialize(user.AssociatedGroups);
            NHibernate.NHibernateUtil.Initialize(user.AssociatedNetworks);
            NHibernate.NHibernateUtil.Initialize(user.FavoritesFolders);

            user.UpdateLastAccess();

            LoginResult result = null;

            #region JWT Bearer Token
            try
            {
                #region SAVE COOKIES
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session[Configurations.Generics.BackAccountSessionKey] = user;
                HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserID] = user.ID;
                HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserName] = user.UserName;
                HttpContext.Current.Application["AccountSessionID_USER" + user.ID] = HttpContext.Current.Session.SessionID;
                #endregion
                result = new LoginResult(LoginResult.LoginResults.Logged, user, null);

                if (ClientBusinessLogic.FindAll() != null && ClientBusinessLogic.FindAll().Count > 0)
                {
                    var clients = ClientBusinessLogic.FindAll();
                    string jwtBearerToken = null;

                    /// seleziono client
                    /// di default dal web.config
                    /// dovrebbe essere quello dell'applicazione cms
                    /// che contiene tutti i claims
                    /// 
                    TokenRequest tokenRequest = new TokenRequest()
                    {
                        ClientId = Configurations.PortalData.DefaultClientId, 
                        ClientSecret = ClientBusinessLogic.GetByClientId(Configurations.PortalData.DefaultClientId).ClientSecret,
                        GrantType = "password",
                        Username = user.UserName,
                        Password = usedPasswordMD5
                    };

                    TokenResponse token = TokenManager.GenerateAccessToken(tokenRequest);
                    jwtBearerToken = JsonConvert.SerializeObject(token);


                    #region  Save user.ID in to shared cookie
                    AddSharedCookie("sharedCookie", NetCms.Configurations.Generics.SsoSharedCookieDomain, user.ID.ToString(), HttpContext.Current.Session.SessionID);
                    #endregion

                    HttpCookie cookie = new HttpCookie("JWT_" + HttpContext.Current.Session.SessionID, jwtBearerToken);
                    HttpCookie encodedCookie = HttpSecureCookie.Encode(cookie);
                    HttpContext.Current.Application["JWT_" + HttpContext.Current.Session.SessionID] = encodedCookie.Value;

                    result = new LoginResult(LoginResult.LoginResults.Logged, user, token);
                   
                }
                #endregion
            
            return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void UpdateCurrentUser()
        {
            LoginUser(CurrentAccount.ID.ToString());
        }
        
        // COMMENTATO IN QUANTO USATO IN SAX
        //public static User BuildLoginSSO(string OSSOUserName,out OSSOLOGIN_ResultTypes result)
        //{                      
        //    User account = null;    
        //    //we need the ID in the CMS Database user table :
        //    //1. from sax.dpi_anagrafe get the anagrafeID 
        //    //2. use the anagraficaID  and AnagraficaType_User("SAXApp.DPI.AnagraficaDPIUser") to get the userID           
        //    if (OSSOUserName != null && OSSOUserName.Length > 0)
        //    {
        //        NetCms.Connections.Connection Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
        //        string password = FormsAuthentication.HashPasswordForStoringInConfigFile("OID", "MD5").ToString();
        //        DataTable table = Connection.SqlQuery("SELECT * FROM users WHERE AnagraficaType_User='SAXApp.DPI.AnagraficaDPIUser' AND Password_User='" + password + "' AND Name_User ='" + OSSOUserName + "'");                                
                
        //        if (table.Rows.Count > 0)
        //        {
        //            if (Convert.ToInt16(table.Rows[0]["Deleted_User"]) == 0 && Convert.ToInt16(table.Rows[0]["State_User"]) == 1)
        //            {
        //                account = LoginUser(table.Rows[0]["id_User"].ToString());
        //                SetLogged(account);
        //                HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserID] = account.ID;
        //                HttpContext.Current.Session[Diagnostics.DiagnosticUserData.DiagnosticCurrentUserName] = account.UserName;
        //                result = OSSOLOGIN_ResultTypes.SSOUserLoggedinSAX;
        //            }
        //            else
        //                result = OSSOLOGIN_ResultTypes.DPIUserNotEnabled;
        //        }
        //        else
        //            result = OSSOLOGIN_ResultTypes.SSOUserLoggedNotImportedInSAX;
        //    }
        //    else
        //        result = OSSOLOGIN_ResultTypes.NoSSOUserLogged;

        //    if (HttpContext.Current.Request.Cookies["ORASSO_AUTH_HINT"] != null)
        //    {
        //        HttpCookie cookie = HttpContext.Current.Request.Cookies["ORASSO_AUTH_HINT"];
        //        if (cookie.Value != "INVALID" && HttpContext.Current.Session["SSOUSER_NOTENABLED"] == null)
        //        {
        //            HttpContext.Current.Session[cookie.Value] = result.ToString();
        //        }
        //    }
        //    return account;
        //}
        

        public static User CheckLogin()
        {
            //NetCms.Connections.Connection conn = NetCms.Connections.DataSource.LoginConnection;
            User account = null;

            G2Core.Common.RequestVariable SubmitSource = new G2Core.Common.RequestVariable("SubmitSource");

            if (SubmitSource.IsPostBack && SubmitSource.IsValidString)
            {
                switch (SubmitSource.StringValue)
                {
                    case "DebugLogin":
                        {
                            G2Core.Common.RequestVariable id = new G2Core.Common.RequestVariable("Accounts");
                            if (id.IsValidInteger && id.IntValue > 0)
                                account = LoginUser(id.StringValue);
                        }
                        break;
                    case "Logout":
                        {
                            DoLogOut();
                        }
                        break;
                }
            }

            return account;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomecognome"></param>
        /// <param name="oldemail"></param>
        /// <param name="newemail"></param>
        /// <param name="userID"></param>
        /// <param name="currentActiveNetwork">usa null per usare il portale di default</param>
        public static void SendConfirmationEmailUpdate(string nomecognome, string oldemail,string newemail, string userID, NetCms.Networks.Network currentActiveNetwork)
        {
            try
            {

                //if (currentActiveNetwork == null)
                //{
                //    string sql = "SELECT * FROM Networks";
                //    DataTable data = Connection.SqlQuery(sql);
                //    DataRow[] rows = data.Select("Default_Network = 1");
                //    currentActiveNetwork = rows[0]["Nome_Network"].ToString();
                //}
                string networkPath = "";
                if (currentActiveNetwork != null)
                {
                    networkPath = currentActiveNetwork.Paths.AbsoluteFrontRoot;
                }

                string encryptedinfo=userID+"#"+oldemail.Trim()+"#"+newemail.Trim();
                encryptedinfo = G2Core.Common.Utility.Encrypt(encryptedinfo);

                //ConfermaEmailUpdateLabels Labels = new ConfermaEmailUpdateLabels();
                //ConfermaEmailUpdateLabels Labels = LabelsManager.LabelsCache.GetTypedLabels(typeof(ConfermaEmailUpdateLabels)) as ConfermaEmailUpdateLabels;
                //string body;
                //body = Labels[ConfermaEmailUpdateLabels.ConfermaEmailUpdateLabelsList.TestoEmailConfermaUpdateEmail];
                LabelsManager.Labels Labels = LabelsManager.LabelsCache.GetTypedLabels(typeof(RegistrazioneLabels)) as LabelsManager.Labels;
                string body;
                body = Labels[RegistrazioneLabels.RegistrazioneLabelsList.TestoEmailConfermaUpdateEmail];

                string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();

                string activateUrl = ""; 
                if (string.IsNullOrEmpty(networkPath))
                    activateUrl = "http://" + ip + NetCms.Configurations.Paths.AbsoluteRoot + "/user/confirm_emailchange.aspx?emailconfirmation=" + encryptedinfo + "&end=1";
                else
                    activateUrl = "http://" + ip + networkPath + "/user/confirm_emailchange.aspx?emailconfirmation=" + encryptedinfo + "&end=1";

                body = body.Replace("{NOME_COGNOME}", nomecognome);
                body = body.Replace("{IP}", ip);
                body = body.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
                body = body.Replace("{PATH}", NetCms.Configurations.Paths.AbsoluteRoot + "/");
                body = body.Replace("{EMAILPORTALE}", NetCms.Configurations.PortalData.eMail);
                body = body.Replace("{OLDEMAIL}", oldemail);
                body = body.Replace("{NEWEMAIL}", newemail);
                body = body.Replace("{ACTIVATE}", activateUrl);                
                
                // Invio Email di conferma registrazione
                G2Core.Common.MailSender msg = new G2Core.Common.MailSender(
                                                                                    newemail,
                                                                                    "Richiesta Conferma Modifica Email",
                                                                                    body,
                                                                                    NetCms.Configurations.PortalData.eMail,
                                                                                    NetCms.Configurations.PortalData.Nome);
                msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
                msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
                msg.DisableServerCertificateValidation = NetCms.Configurations.PortalData.DisableServerCertificateValidation;
                msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;

                if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
                {
                    msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
                    msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
                }

                msg.Send();
               
            }
            catch (Exception ex)
            {   
                NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, "Conferma Email", NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);                
            }

        }

        public static ConfirmEmailUpdate GetEmailUpdateDataFromQueryString()
        {
            ConfirmEmailUpdate result = new ConfirmEmailUpdate();
            if (System.Web.HttpContext.Current.Request.QueryString["emailconfirmation"] != null)
            {
                string encrypted_content = System.Web.HttpContext.Current.Request.QueryString["emailconfirmation"];
                encrypted_content = encrypted_content.Replace(" ", "+");
                string content = G2Core.Common.Utility.Decrypt(encrypted_content);
                string[] values = content.Split('#');               
                if (values.Length == 3)
                {
                    result.IDuser =   values[0];
                    result.oldemail = values[1];
                    result.newemail = values[2];                    
                }
            }
            return (result);
        }
    }

    public class ConfirmEmailUpdate
    {
        public string IDuser;
        public string oldemail;
        public string newemail;        
    }

    public class LoginResult
    {
        public User User
        {
            get { return _User; }
            private set { _User = value; }
        }
        private User _User;

        /// <summary>
        /// JWT AUTHORIZATION
        /// BEARER TOKEN
        /// </summary>
        public TokenResponse JWTBearerToken
        {
            get { return _JWTBearerToken; }
            private set { _JWTBearerToken = value; }
        }
        private TokenResponse _JWTBearerToken;

        public enum LoginResults
        {
            Logged, Disabled
        }

        public LoginResults Result
        {
            get { return _Result; }
            private set { _Result = value; }
        }
        private LoginResults _Result;

        public LoginResult(LoginResults result, User user, TokenResponse jwtBearerToken)
        {
            this.Result = result;
            this.User = user;
            this.JWTBearerToken = jwtBearerToken;
        }
        public LoginResult(LoginResults result, User user)
            : this(result, user, null)
        {
            this.Result = result;
            this.User = user;
        }
        public LoginResult(LoginResults result)
            : this(result, null)
        {
        }
    }
}