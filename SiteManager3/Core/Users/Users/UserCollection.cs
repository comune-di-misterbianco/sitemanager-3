//using System;
//using System.Collections;
///// <summary>
///// Summary description for xNodeCollection
///// </summary>

//namespace NetCms.Users
//{
//    public class UserCollection : IEnumerable
//    {
//        private G2Collection coll;

//        public int Count { get { return coll.Count; } }

//        public UserCollection()
//        {
//            coll = new G2Collection();
//        }

//        public void Add(User user)
//        {
//            coll.Add(user.ID.ToString(), user);
//        }

//        public User this[int i]
//        {
//            get
//            {
//                User value = (User)coll[coll.Keys[i]];
//                return value;
//            }
//        }

//        public User this[string key]
//        {
//            get
//            {
//                return (User)coll[key];
//            }
//        }
        
//        public bool Contains(string key)
//        {
//                return this[key]!=null;
//        }
        
//        #region Enumerator

//    public IEnumerator GetEnumerator()
//    {
//        return new CollectionEnumerator(this);
//    }

//    private class CollectionEnumerator : IEnumerator
//    {
//        private int CurentPos = -1;
//        private UserCollection Collection;
//        public CollectionEnumerator(UserCollection coll)
//        {
//            Collection = coll;
//        }
//        public object Current
//        {
//            get
//            {
//                return Collection[CurentPos];
//            }
//        }
//        public bool MoveNext()
//        {
//            if (CurentPos < Collection.Count - 1)
//            {
//                CurentPos++;
//                return true;
//            }
//            else
//                return false;
//        }
//        public void Reset()
//        {
//            CurentPos = -1;
//        }
//    }
//    #endregion
//    }
//}