using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Xml;
//using NetRegistrazione;

namespace NetCms.Users
{     
    public class SitemanagerUserGUI
    {
        public enum RichTextEditors
        {
            FreeTextBox, TinyMCE
        }
        public RichTextEditors Editor
        {
            get
            {
                return _Editor;
            }
        }
        private RichTextEditors _Editor;

        public enum PreviewTypes { Floating, Static }
        public enum FoldersGUIs { Separate, Colapsed }

        public FoldersGUIs FoldersGUI
        {
            get
            {
                return IsUsingOldIE ? FoldersGUIs.Separate : FoldersGUIs.Colapsed;
            }
        }
        public PreviewTypes PreviewType
        {
            get
            {
                return _PreviewType;
            }
        }

        public bool IsUsingOldIE
        {
            get
            {
                HttpBrowserCapabilities browserData = HttpContext.Current.Request.Browser;
                return browserData.Browser == "IE" && browserData.MajorVersion < 7;
            }
        }

        private PreviewTypes _PreviewType;
        private FoldersGUIs _FoldersGUI;

        public SitemanagerUserGUI(RichTextEditors editorType, PreviewTypes previewType, FoldersGUIs foldersGUI)
        {
            _Editor = editorType;
            _PreviewType = previewType;
            _FoldersGUI = foldersGUI;
        }
    }
}
