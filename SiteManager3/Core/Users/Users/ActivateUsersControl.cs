using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Users
{
    public class ActivateUsersControl : WebControl
    {
        WebControl p_captcha;
        TextBox username = new TextBox();
        TextBox password = new TextBox();
        //TextBox passwordNew = new TextBox();
        //TextBox passwordConfirm = new TextBox();
        TextBox codice = new TextBox();
        //NetForms.NetCaptcha captcha = new NetForms.NetCaptcha("Verifica", "captcha", false);

        ReCaptcha.ReCaptchaV2 recaptcha = new ReCaptcha.ReCaptchaV2(new ReCaptcha.ReCaptchaV2Data());

        //private NetService.Utility.ValidatedFields.VfReCaptcha reCaptcha;
        //private NetService.Utility.ValidatedFields.VfReCaptcha ReCaptchaField
        //{
        //    get
        //    {
        //        if (reCaptcha == null)
        //        {
        //            reCaptcha = new NetService.Utility.ValidatedFields.VfReCaptcha("ReCaptcha");
        //            reCaptcha.Required = false;
        //        }
        //        return reCaptcha;
        //    }
        //}

        public WebControl ErrorsControl
        {
            get
            {
                if (_ErrorsControl == null)
                {
                    _ErrorsControl = new WebControl(HtmlTextWriterTag.Div);
                    _ErrorsControl.CssClass = "msgs";
                }
                return _ErrorsControl;
            }
        }
        private WebControl _ErrorsControl;

        //private NetCms.Connections.Connection _Conn;
        //private NetCms.Connections.Connection Conn
        //{
        //    get
        //    {
        //        //if (_Connection == null)
        //        //    _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
        //        //return _Connection;
        //        NetCms.Connections.ConnectionsManager ConnManager = new NetCms.Connections.ConnectionsManager();
        //        return ConnManager.CommonConnection;

        //    }
        //}

        private string redirectPath = "";

        public bool NoErrors
        {
            get { return ErrorsControl.Controls.Count == 0; }
        }

        public bool Activated
        {
            get { return _Activated; }
            private set { _Activated = value; }
        }
        private bool _Activated;

        public ActivateUsersControl(string redirectPath)
            : base(HtmlTextWriterTag.Div)
        {
            this.redirectPath = redirectPath;
            password.ID = "password_act";
            //passwordNew.ID = "passwordNew";
            //passwordConfirm.ID = "passwordConfirm";
            username.ID = "username_act";
            codice.ID = "codice";

            password.TextMode = TextBoxMode.Password;
            //passwordNew.TextMode = TextBoxMode.Password;
            //passwordConfirm.TextMode = TextBoxMode.Password;

        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Label usernameLabel = new Label();
            usernameLabel.Text = "Nome Utente";
            usernameLabel.AssociatedControlID = username.ID;

            Label passwordLabel = new Label();
            passwordLabel.Text = "Password";
            passwordLabel.AssociatedControlID = password.ID;

            //Label newPasswordLabel = new Label();
            //newPasswordLabel.Text = "Nuova Password";
            //newPasswordLabel.AssociatedControlID = passwordNew.ID;

            //Label newPasswordConfirmLabel = new Label();
            //newPasswordConfirmLabel.Text = "Conferma Nuova Password";
            //newPasswordLabel.AssociatedControlID = this.passwordConfirm.ID;

            Label codiceLabel = new Label();
            codiceLabel.Text = "Codice di Attivazione";
            codiceLabel.AssociatedControlID = codice.ID;

            Button button = new Button();
            button.ID = "submitButton";
            button.CssClass = "btn btn-primary";
            button.Text = "Attiva Account";
            button.Click += new EventHandler(button_Click);

            G2Core.XhtmlControls.Fieldset set = new G2Core.XhtmlControls.Fieldset("Attivazione account", new HtmlGenericControl("div"));
            set.Class = "form-user";
            this.Controls.Add(set);
            this.CssClass = "user-activation";

            set.Controls.Add(this.ErrorsControl);

            WebControl p = new WebControl(HtmlTextWriterTag.P);
            p.Controls.Add(usernameLabel);
            p.Controls.Add(username);
            set.Controls.Add(p);

            p = new WebControl(HtmlTextWriterTag.P);
            p.Controls.Add(passwordLabel);
            p.Controls.Add(password);
            set.Controls.Add(p);

            p = new WebControl(HtmlTextWriterTag.P);
            p.Controls.Add(codiceLabel);
            p.Controls.Add(codice);
            set.Controls.Add(p);

            //p = new WebControl(HtmlTextWriterTag.P);
            //p.Controls.Add(newPasswordLabel);
            //p.Controls.Add(passwordNew);
            //set.Controls.Add(p);

            //p = new WebControl(HtmlTextWriterTag.P);
            //p.Controls.Add(newPasswordConfirmLabel);
            //p.Controls.Add(passwordConfirm);
            //set.Controls.Add(p);

            p_captcha = new WebControl(HtmlTextWriterTag.P);
            //p.Controls.Add(captcha.getControl());
            set.Controls.Add(p_captcha);

            p = new WebControl(HtmlTextWriterTag.P);
            p.Controls.Add(button);
            set.Controls.Add(p);

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            // p_captcha.Controls.Add(captcha.getControl());
            p_captcha.Controls.Add(recaptcha.GetControl);
        }

        private void button_Click(object sender, EventArgs e)
        {
            string usernameValue = username.Text.Replace("'", "''").Trim();
            string passwordValue = password.Text.Replace("'", "''").Trim();
            string codiceValue = codice.Text.Replace("'", "''").Trim();
            //string passwordNewValue = this.passwordNew.Text.Replace("'", "''").Trim();
            //string passwordConfirmValue = this.passwordConfirm.Text.Replace("'", "''").Trim();
            int idUserOnDB = 0;

            //Controllo Captcha
            //string CaptchaValidation = captcha.validateInput(captcha.RequestVariable.StringValue);
            //if (NoErrors)
            //    if (!string.IsNullOrEmpty(CaptchaValidation))
            //        ErrorsControl.Controls.Add(new LiteralControl(CaptchaValidation));

            ReCaptcha.ReCaptchaV2Result results = recaptcha.Verify();
            if (!results.Success)
                ErrorsControl.Controls.Add(new LiteralControl("<li>Dimostra di non essere un robot</li>"));

            //Controllo il nome utente inserito
            //string sql = "Name_User LIKE '" + usernameValue + "'";
            //DataRow row = Connection.SqlQuery("SELECT * FROM users WHERE Deleted_User = 0 AND " + sql).Select().FirstOrDefault();

            User user = UsersBusinessLogic.GetUserByUserName(usernameValue, 0, 0);

            if (user != null)
            {
                idUserOnDB = user.ID;

                //Controllo se l'utente non � gi� attivo
                if (ErrorsControl.Controls.Count == 0)
                    if (user.State == 1)
                        ErrorsControl.Controls.Add(new LiteralControl("<li>L'utente risulta gi� attivo.</li>"));

                string passwordOnDB = user.Password;
                string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(passwordValue, "MD5").ToString();
                if (NoErrors)
                    if (passwordOnDB != MD5Pwd)
                        ErrorsControl.Controls.Add(new LiteralControl("<li>La password non � corretta.</li>"));

                string activationKeyOnDB = user.ActivationKey;
                if (NoErrors)
                    if (activationKeyOnDB != codiceValue)
                        ErrorsControl.Controls.Add(new LiteralControl("<li>Il codice di attivazione non � corretto.</li>"));

                ////Controllo se sono uguali password e conferma
                //if (NoErrors)
                //    if (passwordConfirmValue != passwordNewValue) 
                //        ErrorsControl.Controls.Add(new LiteralControl("<li>La nuova password non coincide con la conferma.</li>"));

                ////Controllo formato nuova password
                //string reg = @"(?=^.{8,255}$)((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*";
                //Regex regex = new Regex(reg, RegexOptions.IgnorePatternWhitespace);
                //if (NoErrors)
                //    if (!regex.IsMatch(passwordNewValue)) 
                //        ErrorsControl.Controls.Add(new LiteralControl("<li>La nuova password deve contenere almeno 3 di questi 4 tipi di carattere: lettera minuscola, lettera maiuscola, simbolo, numero.</li>"));

                ////Controllo che la vecchia password sia diversa da quella nuova
                //if (NoErrors)
                //    if (passwordValue == passwordNewValue) 
                //        ErrorsControl.Controls.Add(new LiteralControl("<li>La nuova password deve essere diversa da quella vecchia.</li>"));

            }
            else ErrorsControl.Controls.Add(new LiteralControl("<li>Non � stato possibile procedere con l'attivazione.</li>"));
            if (NoErrors && idUserOnDB > 0)
            {
                //string MD5PwdNew = FormsAuthentication.HashPasswordForStoringInConfigFile(passwordNewValue, "MD5").ToString();

                user.LastAccess = DateTime.Now;
                user.LastUpdate = DateTime.Now;
                user.State = 1;
                //user.Password = MD5PwdNew;

                UsersBusinessLogic.SaveOrUpdateUser(user);

                //Invio la notifica di attivazione account
                user.SendEnabledMail();
                //sql = "UPDATE Users SET LastAccess_User = "+G2Core.Connections.OdbcConnection.OdbcFilterDateTime(DateTime.Now) + ",LastUpdate_User = "+G2Core.Connections.OdbcConnection.OdbcFilterDateTime(DateTime.Now)+", State_User = 1, Password_User = '" + MD5PwdNew + "' WHERE id_User = " + row["id_User"].ToString();
                //this.Connection.Execute(sql);

                NetCms.Users.AccountManager.LoginUser(idUserOnDB.ToString());

                //if(redirectPath!=string.Empty)
                if (redirectPath.Length == 0)
                    redirectPath = "/";

                NetCms.Diagnostics.Diagnostics.Redirect(redirectPath);
            }
            else
            {
                ErrorsControl.Controls.AddAt(0, new LiteralControl("<div class=\"alert alert-danger\">Si sono verificati i seguenti errori nell'attivazione: <ul class=\"list-unstyled\">"));
                ErrorsControl.Controls.Add(new LiteralControl("</ul></div>"));
            }
        }
    }
}