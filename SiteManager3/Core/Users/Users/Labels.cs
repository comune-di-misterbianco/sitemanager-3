using System;
using System.Web;
using System.Xml;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Users
{
    public class RegistrazioneLabels : LabelsManager.Labels
    {      
        public const String ApplicationKey = "registrazione";

        public RegistrazioneLabels()
            : base(ApplicationKey)
        {
        }

        public enum RegistrazioneLabelsList
        {
            OggettoEmailConfermaAttivazione,
            TestoEmailConfermaAttivazione,
            TestoEmailConfermaReinoltrata,
            TestoNotificaNuovoAccountDaConvalidare,
            OggettoEmailDisattivazione,
            TestoEmailDisattivazione,
            OggettoEmailUserProfileDisable,
            TestoEmailUserProfileDisable,
            TestoEmailRecupero,
            TestoRichiestaRegistrazioneInviata,
            TestoEmailConfermaCambioPwd,
            TitoloNotificaRichiestaRegistrazione,
            TestoNotificaRichiestaRegistrazione,
            UserRegisterTitle,
            CustomAnagrafeNotFoundError,
            NoRecipientForSms,
            SmsActivationTitle,
            SmsActivationText,
            SmsActivationSuccess,
            SmsActivationError,
            ManagerNotifyTitle,
            ManagerNotifyText,
            RegistrationSuccessMsg,
            RegistrationErrorMsg,
            EmailAlreadyExists,
            UsernameIsUsed,
            CFNotMatchWithInputData,
            RegistrationConfirmMailSubject,
            RegistrationRejectMailSubject,
            SmsKeyHelpMsg,
            TestoEmailConfermaUpdateEmail,
            RegistrationSuccess,
            RegistrationError,
            NewPasswordSendMailSubject,
            NewPasswordSendMailBody,
            TermsAndConditions,
            Email,
            Registration,
            TypesOfRegistrations,
            AnagraficaRegistration,
            PortalRegistration
        }       
    }    

    public class LoginLabels : LabelsManager.Labels
    {
        public const String ApplicationKey = "login";

        public LoginLabels()
            : base(ApplicationKey) 
        {
        }

        public enum LabelsList
        {
            AccessButtonLabel,
            AccessButtonLabelAD,
            AccessFromOtherBrowser,
            NotGrantedUser,
            YourAccountWasDisabled,
            NotValidUserAndPass,
            NewPasswordSentByMail,
            Username,
            Password,
            PasswordRecovery,
            UserRegistration,
            AskPassword,
            Cancel,
            SentRequestForChangePw,
            NotExistingUser, 
            UserIsRequired,
            NotCorrectInputForPwReset,
            LoginTitleLabel,
            DebugAccessButtonLabel,
            ForgotTitleLabel,
            ReservedArea, 
            WelcomeSSO,
            UnauthenticatedUser,
            UserNotFoundAD,
            RegistrationPath,
            DefaultPath
        }
    }

    public class UserActivateLabels : LabelsManager.Labels
    {
        public const String ApplicationKey = "useractivate";

        public UserActivateLabels()
            :base (ApplicationKey)
        { }

        public enum LabelList
        {
            ActivationCode,
            Username,
            Password,
            PageTitle,
            PageDescription,
            CaptchaError,
            ActivationError,
            UserAlreadyActive,
            ActivationSuccessMsg,
            Activation,
            Name,
            Description,
            CheckCitizenship,
            AlreadyActive,
            UnableActivate,
            ActiveService,
            InvalidUserType,
            SignAssumptionOfResponsibility,
            CitizenNotFound,
            AutomaticActivation
        }
    }

    public class SolrSearchLabels : LabelsManager.Labels
    {
        public const String ApplicationKey = "solrsearch";

        public SolrSearchLabels()
            : base(ApplicationKey)
        {
        }

        public enum SolrSearchLabelsList
        {
            NoResult,
            ServiceUnavailable,
            Approximately,
            ResultsFound
        }
    }

    public class NotificheLabels : LabelsManager.Labels
    {
        public const String ApplicationKey = "notifiche";

        public NotificheLabels()
            : base(ApplicationKey)
        {
        }

        public enum LabelsList
        {
            NoNotification,
            List,
            Title,
            Text,
            Date
        }
    }

}