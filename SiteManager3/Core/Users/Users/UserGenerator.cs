//NON SEMBRA ESSERE USATA

//using System;
//using System.Data;
//using System.Data.Common;
//using System.Configuration;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
//using NetCms.Connections;

///// <summary>
///// Summary description for StructureGrants
///// </summary>

//namespace NetCms.Users
//{
//    public class UserGenerator
//    {
//        public string MailBodyTemplate
//        {
//            get
//            {
//                return _MailBodyTemplate;
//            }
//            set { _MailBodyTemplate = value; }
//        }
//        private string _MailBodyTemplate;

//        private NetCms.Connections.Connection _Connection;
//        public NetCms.Connections.Connection Connection
//        {
//            get
//            {
//                return _Connection;
//            }
//        }

//        public UserGenerator(NetCms.Connections.Connection connection)
//        {
//            _Connection = connection;
//        }

//        public int Create(string userName, int anagraficaID, string password, Users.UsersTypes type, string anagraficaType,string email)
//        {
//            string insertProfile = @"
//            INSERT INTO profiles (Name_Profile, Type_Profile) VALUES ('" + userName + @"',0)";
//            int profileID = Connection.ExecuteInsert(insertProfile);


//            string Name_User = userName;
//            string Anagrafica_User = anagraficaID.ToString();
//            string Profile_User = profileID.ToString();
//            string Password_User = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();
//            string Type_User = type == UsersTypes.Frontend ? "0" : (type == UsersTypes.Backend ? "1" : "2");
//            string AnagraficaType_User = anagraficaType;
//            string Key_User = new Random().Next(9999999).ToString(); ;
//            string NewPassword_User = "";
//            string CreationTime_User = Connection.FilterDateTime(DateTime.Now);
//            string LastAccess_User = Connection.FilterDateTime(DateTime.MinValue);


//            string insert = @"
//            INSERT INTO Users (
//                                    Name_User,
//                                    Anagrafica_User,
//                                    Profile_User,
//                                    Password_User,
//                                    Type_User,
//                                    AnagraficaType_User,
//                                    Key_User,
//                                    NewPassword_User,
//                                    CreationTime_User,
//                                    LastAccess_User,
//                                    LastUpdate_User,
//                                    FoldersGUI_User,
//                                    State_User,
//                                    Deleted_User,
//                                    Editor_User
//                               )
//                               VALUES(
//                                    '{0}',
//                                    {1},
//                                    {2},
//                                    '{3}',
//                                    {4},
//                                    '{5}',
//                                    '{6}',
//                                    '{7}',
//                                    {8},
//                                    {9},
//                                    {10},
//                                    1,
//                                    2,
//                                    0,
//                                    0
//                                    )
//                                ";
            

//            string[] parameters = { Name_User, 
//                                    Anagrafica_User, 
//                                    Profile_User, 
//                                    Password_User, 
//                                    Type_User, 
//                                    AnagraficaType_User, 
//                                    Key_User, 
//                                    NewPassword_User, 
//                                    CreationTime_User, 
//                                    LastAccess_User, 
//                                    CreationTime_User};

//            string sql = string.Format(insert, parameters);
//            int createdUserID = Connection.ExecuteInsert(sql);

//            if(!string.IsNullOrEmpty(this.MailBodyTemplate))
//                SendActivationMail(password, Key_User, Name_User,email);

//            return createdUserID;
//        }

//        private void SendActivationMail(string password, string codice_convalida,string username,string email)
//        {
//            string body = MailBodyTemplate;

//            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();

//            body = body.Replace("{NOME_COGNOME}", username);
//            body = body.Replace("{IP}", ip);
//            body = body.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
//            body = body.Replace("{PASSWORD}", password);
//            body = body.Replace("{KEY}", codice_convalida);
//            body = body.Replace("{PATH}", NetCms.Configurations.Paths.AbsoluteRoot + "/");
//            body = body.Replace("{EMAILPORTALE}", NetCms.Configurations.PortalData.eMail);
//            body = body.Replace("{USERNAME}", username);

//            string subject = "" + NetCms.Configurations.PortalData.Nome + ": email di conferma registrazione al portale.";

//            // Invio Email di conferma registrazione
//            G2Core.Common.MailSender msg = new G2Core.Common.MailSender(
//                                                                            email,
//                                                                            subject,
//                                                                            body,
//                                                                            NetCms.Configurations.PortalData.eMail,
//                                                                            NetCms.Configurations.PortalData.Nome);
//            msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
//            msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
//            msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;

//            if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
//            {
//                msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
//                msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
//            }

//            msg.Send();
//        }
//    }
//}