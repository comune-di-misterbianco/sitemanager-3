using System;
using System.Data;
using System.Data.Common;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using NetCms.GUI;
using NetForms;
using System.Web.Security;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users
{
    public class UsersChangePasswordControl:WebControl
    {
        //public delegate void AfterPostback(bool success);
        //public event AfterPostback OnAfterPostback;
       
        public NetCms.Users.User User { get; private set; }

        //public bool AskForOldPassword { get; set; } 

        //public NetCms.Connections.Connection Connection
        //{
        //    get
        //    {
        //        return _Connection ?? (_Connection = NetCms.Connections.ConnectionsManager.CommonConnection);
        //    }
        //}
        //private NetCms.Connections.Connection _Connection;

        public string InformationText
        {
            get
            {
                if (_InformationText == null)
                    _InformationText = "La tua password � scaduta, per continuare dovrai aggiornarla. La nuova password dovr� contenere almeno 3 di questi elementi: Lettera Maiuscola, Lettera Minuscola, Numero, Simbolo. Ed essere lunga almeno 8 caratteri.";
                return _InformationText;
            }
            set { _InformationText = value; }
        }
        private string _InformationText;

        public VfManager ChangePasswordForm
        {
            get
            {
                if (_ChangePasswordForm == null)
                {
                    _ChangePasswordForm = new VfManager("changePassword");
                    _ChangePasswordForm.SubmitButton.Text = "Aggiorna";
                    _ChangePasswordForm.InfoControl.Controls.Add(new LiteralControl(InformationText));

                    VfPassword password = new VfPassword("nuovapassword");
                    password.Required = true;
                    _ChangePasswordForm.Fields.Add(password);
                }
                return _ChangePasswordForm;
            }
        }
        private VfManager _ChangePasswordForm;

        //public G2Core.AdvancedXhtmlForms.AxfGenericTable Table
        //{
        //    get
        //    {
        //        if (_Table == null)
        //        {
        //            _Table = new G2Core.AdvancedXhtmlForms.AxfGenericTable("users", "id_user", Connection);
        //            _Table.RelatedRecordID = User.ID;

        //            G2Core.AdvancedXhtmlForms.Fields.AxfPassword Password = new G2Core.AdvancedXhtmlForms.Fields.AxfPassword("Nuova Password", "Password_User");
        //            Password.CyperType = G2Core.AdvancedXhtmlForms.Fields.AxfPassword.CyperingType.MD5;
        //            Password.Required = true;
                    
        //            if (User.Password.Length > 0)
        //            {
        //                if(AskForOldPassword)
        //                    Password.OldPasswordCheck = true;
        //                else
        //                    Password.OldPasswordCheckWithoutShowOldPasswordField = true;

        //                Password.OldPassword = User.Password;
        //            }
        //            else Password.OldPasswordCheck = false;

        //            Password.HighSecurityField = true;
        //            _Table.Fields.Add(Password);

        //            G2Core.AdvancedXhtmlForms.Fields.AxfHiddenField hidden = new G2Core.AdvancedXhtmlForms.Fields.AxfHiddenField("LastUpdate_User");
        //            hidden.Value = Connection.FilterDateTime(DateTime.Now);
        //            _Table.Fields.Add(hidden);

        //            hidden = new G2Core.AdvancedXhtmlForms.Fields.AxfHiddenField("NewPassword_User");
        //            hidden.Value = "\"\"";
        //            _Table.Fields.Add(hidden);
        //        }
        //        return _Table;
        //    }
        //}
        //private G2Core.AdvancedXhtmlForms.AxfGenericTable _Table;

        public string AddressToRedirect { get; set; } 

        public UsersChangePasswordControl(User user)
            :base(System.Web.UI.HtmlTextWriterTag.Div)
        {
            this.User = user;
            //AskForOldPassword = true;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            HtmlGenericControls.Fieldset fieldset = new HtmlGenericControls.Fieldset("Aggiornamento password");

            fieldset.Controls.Add(ChangePasswordForm);

            this.Controls.Add(fieldset);

            //G2Core.AdvancedXhtmlForms.AxfForm form = new G2Core.AdvancedXhtmlForms.AxfForm();
            //form.Tables.Add(Table);
            //form.SubmitLabel = "Aggiorna";

            //if (Table.IsPostBack)
            //{
            //    var result = form.serializedUpdate();
            //    OnAfterPostback(result == G2Core.AdvancedXhtmlForms.AxfForm.LastOperationValues.OK);
            //}

            //this.Controls.Add(form.getControl());
        }
    }
}