using System;
using System.Data;
using System.Reflection;
using System.Web;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetService.Utility.RecordsFinder;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users.Search
{
    public class CustomSearchField : NetCms.GUI.DetailsSheetRow
    {
        public string Label
        {
            get { return _Label; }
            private set { _Label = value; }
        }
        private string _Label;

        //public string FieldName
        //{
        //    get { return _FieldName; }
        //    private set { _FieldName = value; }
        //}
        //private string _FieldName;

        private SearchParameter _SearchParameter;

        public G2Core.Common.RequestVariable SearchRequest
        {
            get
            {
                if (_SearchRequest == null)
                    _SearchRequest = new G2Core.Common.RequestVariable("field_" + this.Key.ToLower(), G2Core.Common.RequestVariable.RequestType.Form_QueryString);
                return _SearchRequest;
            }
        }
        private G2Core.Common.RequestVariable _SearchRequest;

        public string Key
        {
            get { return _Key; }
            private set { _Key = value; }
        }
        private string _Key;

        public string QueryString
        {
            get { return "field_" + Key.ToLower() + "=" + SearchRequest.StringValue; }
        }

        public SearchParameter GetSearchParameter()
        {
            return new SearchParameter(_SearchParameter.Label, _SearchParameter.Name, SearchParameterValue(), _SearchParameter.ComparisonCriteria, _SearchParameter.IsProducedByDefault);
        }

        private Type _ReturnObjectType = null;

        private object SearchParameterValue()
        {
            object value = null;
            //Codice nuovo aggiunto da Angelo per gestire i return type
            if (_ReturnObjectType != null)
            {
                if (_ReturnObjectType == typeof(int))
                    value = SearchRequest.IntValue;
                //Se non si controlla che la stringa sia piena, restituisce un'eccezione, pertanto ho aggiunto il controllo
                if (_ReturnObjectType == typeof(DateTime) && !string.IsNullOrEmpty(SearchRequest.StringValue))
                    value = DateTime.Parse(SearchRequest.StringValue);
                if (_ReturnObjectType == typeof(string))
                    value = SearchRequest.StringValue;
            }
            else
            {
                //Questo deve essere cos� per mantenere la compatibilit� con la vecchia modalit�
                value = string.IsNullOrEmpty(SearchRequest.StringValue) ? null : SearchRequest.StringValue;
            }

            //return string.IsNullOrEmpty(Field.Request.StringValue) ? null : Field.Request.StringValue;
            return value;
        }

        public CustomSearchField(string key, string label,SearchParameter sp, Type returnObjectType = null)
            : base("")
        {
            this.Label = label;
            this.Key = key;
            this._SearchParameter = sp;
            this._ReturnObjectType = returnObjectType;
        }

        protected override void OnInit(EventArgs e)
        {
            InitControls();
            base.OnInit(e);
        }

        private void InitControls()
        {

            TextBox textBox = new TextBox();
            textBox.ID = "field_" + Key.ToLower();
            textBox.Text = SearchRequest.StringValue;
            textBox.Columns = 100;

            this.Title = "<label for=\"" + textBox.ID + "\">" + this.Key;
            //bool first = true;
            //foreach (CustomSearchField field in this.CustomSearchFields)
            //{
            //    if (!Title.Contains(field.Label))
            //    {
            //        this.Title += (first ? "(" : ",") + field.Label;
            //        first = false;
            //    }
            //}
            //if (!first) this.Title += ")";
            this.Title += "</label>";
            this.ControlsToAppendRight.Add(textBox);

        }
    }
}