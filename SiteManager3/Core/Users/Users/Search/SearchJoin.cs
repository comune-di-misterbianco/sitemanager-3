﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Users.Search
{
    public class SearchJoin
    {
        public string Property
        {
            get;
            set;
        }

        public CriteriaType Criteria
        {
            get;
            set;
        }

        public enum CriteriaType
        {
            User,
            Profile,
            GroupAssociation,
            Group
        }

        public SearchJoin(string property, CriteriaType criteria)
        {
            this.Property = property;
            this.Criteria = criteria;
        }
    }
}
