﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Users
{
    public class UserSearchRowTemplate
    {
        public UserSearchRowTemplate()
        { 
        
        }

        public int id_User
        {
            get;
            set;
        }

        public string Name_User
        {
            get;
            set;
        }

        public int State_User
        {
            get;
            set;
        }

        public int Anagrafica_User
        {
            get;
            set;
        }

        public string AnagraficaNome
        {
            get;
            set;
        }

        public int Profile_User
        {
            get;
            set;
        }

        public string AnagraficaType_User
        {
            get;
            set;
        }

        public bool Registered_User
        {
            get;
            set;
        }

        public string UserStateActionLabel
        {
            get { 
                //return State_User == 0 ? "Abilita":"Disabilita";

                string statoUtente = "";

                switch (State_User)
                {
                    case 1:
                        statoUtente = "Disabilita";
                        break;
                    case 0:
                        statoUtente = "Abilita";
                        break;
                    case 2:
                        statoUtente = "Abilita";
                        break;
                }

                return statoUtente;
            }
        }

        public string UserStateActionHref
        {
            get {

                string statoUtente = "";

                switch (State_User)
                {
                    case 1:
                        statoUtente = "DisableUserField";
                        break;
                    case 0:
                        statoUtente = "EnableUserField";
                        break;
                    case 2:
                        statoUtente = "EnableUserField";
                        break;
                }

                return statoUtente;

                //return State_User == 0 ? "EnableUserField" : "DisableUserField"; 
            }
        }
    }
}
