using System;
using System.Data;
using System.Reflection;
using System.Web;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using NetService.Utility.RecordsFinder;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users.Search
{
    public class UserRelated
    {
        public NetCms.Users.User User
        {
            get { return _User; }
            private set { _User = value; }
        }
        private NetCms.Users.User _User;

        //private NetCms.Connections.Connection _Connection;
        //private NetCms.Connections.Connection Connection
        //{
        //    get
        //    {
        //        return _Connection;
        //    }
        //    set { _Connection = value; }
        //}

        public UserRelated(NetCms.Users.User user)
        {
            this.User = user;
            //Connection = connection;
        }

        //public DataTable GroupsIAdmin(string additionalSqlJoins, string additionalSqlConditions)
        //{
        //    string conditions = "";
        //    for (int i = 0; i < this.User.Groups.Count; i++)
        //    {
        //        NetCms.Users.GroupAssociation GroupAssociation = this.User.AssociatedGroupsOrdered.ElementAt(i);

        //        if (GroupAssociation.GrantsRole != NetCms.Users.GrantAdministrationTypes.User)
        //        {
        //            conditions += " OR ";
        //            conditions += " Tree_Group LIKE '" + GroupAssociation.Group.Tree + "%'";
        //        }
        //    }
        //    //La condizione 1=0 serve ad esculudere tutti i record tranne quelli selezionati dalle
        //    //condizioni specificate nella variabile conditions
        //    string sql = "SELECT * FROM groups INNER JOIN profiles ON (groups.Profile_Group = profiles.id_Profile) " + additionalSqlJoins + " WHERE (" + additionalSqlConditions + ") AND (1=0" + conditions + ")";

        //    DataTable dataTable = this.Connection.SqlQuery(sql);

        //    return this.Connection.SqlQuery(sql);
        //}

        public ICollection<Group> GroupsIAdmin(int pageStart, int pageSize, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            return GroupsBusinessLogic.GroupsIAdmin(pageStart, pageSize, this.User, additionalJoins, additionalConditions);
        }

        public int GroupsIAdminCount(SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            return GroupsBusinessLogic.GroupsIAdminCount(this.User, additionalJoins, additionalConditions);
        }

        //public DataTable UsersIAdmin(string additionalSqlJoins, string additionalSqlConditions, string additionalSqlFields)
        //{
        //    //Fields
        //    string sql = "SELECT DISTINCT Users.*";
        //    if (!string.IsNullOrEmpty(additionalSqlFields.Trim()))
        //        sql += ", " + additionalSqlFields;

        //    //Joins
        //    sql += " FROM Users";
        //    sql += " INNER JOIN Profiles ON (Profile_User = id_Profile)";
        //    sql += " INNER JOIN UsersGroups ON (User_UserGroup = id_User)";
        //    sql += " INNER JOIN Groups ON (Group_UserGroup = id_Group)";
        //    sql +=  " " + additionalSqlJoins;

        //    //Conditions
        //    sql += " WHERE Deleted_User = 0 AND (1=0 " + MyGroupsConditions + ")";
        //    if (!this.User.CanHandleItSelf)
        //        sql += " AND id_User != " + User.ID;
        //    if (!string.IsNullOrEmpty(additionalSqlConditions.Trim()))
        //        sql += " AND (" + additionalSqlConditions + ")";

        //    DataTable dataTable = this.Connection.SqlQuery(sql);

        //    return this.Connection.SqlQuery(sql);
        //}

        public ICollection<User> UsersIAdmin(int pageStart, int pageSize, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            return UsersBusinessLogic.UserIAdmin(pageStart, pageSize, this.User, additionalJoins, additionalConditions);
        }

        public ICollection<User> UsersIAdminByAnagraficaType(int pageStart, int pageSize, string anagraficaType, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            return UsersBusinessLogic.UserIAdminByAnagraficaType(pageStart, pageSize, this.User, anagraficaType, additionalJoins, additionalConditions);
        }

        public int UsersIAdminCount(SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            return UsersBusinessLogic.UserIAdminCount(this.User, additionalJoins, additionalConditions);
        }

        public int UsersIAdminCountByAnagraficaType(string anagraficaType, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            return UsersBusinessLogic.UserIAdminCountByAnagraficaType(this.User, anagraficaType, additionalJoins, additionalConditions);
        }

        //private string _MyGroupsConditions;
        //public string MyGroupsConditions
        //{
        //    get
        //    {
        //        if (_MyGroupsConditions == null)
        //        {
        //            string conditions = "";
        //            for (int i = 0; i < this.User.Groups.Count; i++)
        //            {
        //                NetCms.Users.GroupAssociation GroupAssociation = this.User.AssociatedGroupsOrdered.ElementAt(i);

        //                if (GroupAssociation.GrantsRole != NetCms.Users.GrantAdministrationTypes.User)
        //                {
        //                    conditions += " OR ";
        //                    conditions += " (Tree_Group LIKE '" + GroupAssociation.Group.Tree + "%'";
        //                    if (GroupAssociation.GrantsRole == NetCms.Users.GrantAdministrationTypes.Administrator)
        //                        conditions += " AND Type_UserGroup = 0";
        //                    conditions += " )";

        //                }
        //            }
        //            _MyGroupsConditions = conditions;
        //        }
        //        return _MyGroupsConditions;
        //    }
        //}
    }

}