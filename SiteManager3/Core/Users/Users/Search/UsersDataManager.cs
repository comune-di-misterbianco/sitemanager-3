using System;
using System.Data;
using System.Reflection;
using System.Web;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users.Search
{
    public class UsersDataManager
    {
        //private NetCms.Connections.Connection _Connection;
        //private NetCms.Connections.Connection Connection
        //{
        //    get
        //    {
        //        return _Connection;
        //    }
        //}
        
        //public string NonGrouppedUsersSQL
        //{
        //    get
        //    {
        //        if (_NonGrouppedUsersSQL == null)
        //        {
        //            _NonGrouppedUsersSQL = @"SELECT Users.* FROM Users WHERE id_User NOT IN (SELECT id_User FROM Users INNER JOIN UsersGroups ON (User_UserGroup = id_User) INNER JOIN Groups ON (Group_UserGroup = id_Group)) AND Deleted_User = 0";
        //        }
        //        return _NonGrouppedUsersSQL;
        //    }
        //}
        //private string _NonGrouppedUsersSQL;
        			
        public NetCms.Users.User User
        {
            get { return _User; }
            private set { _User = value; }
        }
        private NetCms.Users.User _User;


        //public UsersDataManager(NetCms.Users.User user, NetCms.Connections.Connection connection)
        //{
        //    this._Connection = connection;
        //    this.User = user;
        //}

        public UsersDataManager(NetCms.Users.User user)
        {
            this.User = user;
        }

        public int PageStart
        {
            get 
            {
                return _PageStart;
            }
            set
            {
                _PageStart = value;
            }
        }
        private int _PageStart;

        public int PageSize
        {
            get
            {
                return _PageSize;
            }
            set
            {
                _PageSize = value;
            }
        }
        private int _PageSize;

        //public DataTable NonGrouppedUsersTable
        //{
        //    get
        //    {
        //        if (_NonGrouppedUsersTable == null)
        //        {
        //            _NonGrouppedUsersTable = Connection.SqlQuery(NonGrouppedUsersSQL);
        //        }
        //        return _NonGrouppedUsersTable;
        //    }
        //}
        //private DataTable _NonGrouppedUsersTable;

        public ICollection<User> NonGrouppedUsersTable
        {
            get
            {
                if (_NonGrouppedUsersTable == null)
                {
                    if (PageSize == 0)
                        PageSize = NonGrouppedUsersTableCount;
                    _NonGrouppedUsersTable = UsersBusinessLogic.FindNonGrouppedUsers(PageStart,PageSize);
                }
                return _NonGrouppedUsersTable;
            }
        }
        private ICollection<User> _NonGrouppedUsersTable;

        public int NonGrouppedUsersTableCount
        {
            get 
            {
                return UsersBusinessLogic.FindNonGrouppedUsersCount();
            }
        }

        //public DataTable MyUsersTable
        //{
        //    get
        //    {
        //        if (_MyUsersTable == null)
        //        {
        //            Search.UserRelated st = new Search.UserRelated(User, Connection);
        //            _MyUsersTable = st.UsersIAdmin("", "", "");
        //            //_MyUsersTable = Connection.SqlQuery(MyUsersQuery);
        //        }
        //        return _MyUsersTable;
        //    }
        //}
        //private DataTable _MyUsersTable;

        public ICollection<User> MyUsersTable
        {
            get
            {
                if (_MyUsersTable == null)
                {
                    Search.UserRelated st = new Search.UserRelated(User);
                    if (PageSize == 0)
                        PageSize = MyUsersTableCount;
                    _MyUsersTable = st.UsersIAdmin(PageStart,PageSize,null,null);
                }
                return _MyUsersTable;
            }
        }
        private ICollection<User> _MyUsersTable;

        public int MyUsersTableCount
        { 
            get 
            {
                Search.UserRelated st = new Search.UserRelated(User);
                return st.UsersIAdminCount(null, null);
            }
        }

        public ICollection<User> GetMyUsersTableByAnagraficaType(string anagraficaType)
        {
            Search.UserRelated st = new Search.UserRelated(User);
            if (PageSize == 0)
                PageSize = MyUsersTableCount;
            return st.UsersIAdminByAnagraficaType(PageStart, PageSize, anagraficaType, null, null);
        }

        public int GetUserTableCountByAnagraficaType(string anagraficaType)
        {
            Search.UserRelated st = new Search.UserRelated(User);
            return st.UsersIAdminCountByAnagraficaType(anagraficaType, null, null);
        }
    }
}