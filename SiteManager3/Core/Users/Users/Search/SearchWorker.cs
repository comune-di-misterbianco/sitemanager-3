using System;
using System.Linq;
using System.Data;
using System.Reflection;
using System.Web;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users.Search
{
    public class SearchWorker
    {
        //Oggetto che si occuper� di effettuare la ricerca all'interno delle anagrafiche
        public AnagraficaSearch Finder
        {
            get
            {
                if (_Finder == null)
                {
                    _Finder = new AnagraficaSearch();
                }
                return _Finder;
            }
        }
        private AnagraficaSearch _Finder;

        public SearchWorker(/*ICollection<User> users*/)
        {
            //this._Users = users;
        }

        /// <summary>
        /// Restituisci una DataTable che immagazzina le chiavi di indicizzazione della Cache
        /// </summary>
        public ICollection<UserSearchRowTemplate> Result
        {
            get
            {
                if (_Result == null)
                    _Result = new List<UserSearchRowTemplate>();
                return _Result;
            }
        }
        private ICollection<UserSearchRowTemplate> _Result;

        //public ICollection<User> Users
        //{
        //    get
        //    {
        //        return _Users;
        //    }
        //}
        //private ICollection<User> _Users;

        public int GetSearchResultCount(NetCms.Users.User userToFilter, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            return Finder.SearchResultsCount(userToFilter,additionalJoins,additionalConditions);
        }

        public int GetSearchResultCountByAnagraficaType(User userToFilter, string anagraficaType, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            return Finder.SearchResultsCountByAnagraficaType(userToFilter, anagraficaType, additionalJoins, additionalConditions);
        }

        public ICollection<UserSearchRowTemplate> GetSearchResult(int pageStart, int pageSize, NetCms.Users.User userToFilter, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICollection<User> searchResults = Finder.SearchResults(userToFilter,pageStart,pageSize,additionalJoins,additionalConditions);

            if (searchResults != null)
            {
                //DataRow[] userfilter = searchResults.Select("false " + rels.MyGroupsConditions);
                foreach (User user in searchResults)
                {
                    try
                    {
                        if (Result.Where(x=>x.id_User == user.ID).Count() == 0)
                        {                         
                            Result.Add(new UserSearchRowTemplate() { 
                                id_User = user.ID, 
                                Name_User = user.UserName, 
                                State_User = user.State, 
                                AnagraficaType_User = user.AnagraficaType, 
                                Anagrafica_User = user.AnagraficaID, 
                                AnagraficaNome = user.NomeCompleto,
                                Profile_User = user.Profile.ID,
                                Registered_User = user.Registrato
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        //trace
                    }
                }
            }

            return Result;
        }

        public ICollection<UserSearchRowTemplate> GetSearchResultByAnagraficaType(int pageStart, int pageSize, User userToFilter, string anagraficaType, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICollection<User> searchResults = Finder.SearchResultsByAnagraficaType(userToFilter, anagraficaType, pageStart, pageSize, additionalJoins, additionalConditions);

            if (searchResults != null)
            {
                //DataRow[] userfilter = searchResults.Select("false " + rels.MyGroupsConditions);
                foreach (User user in searchResults)
                {
                    try
                    {
                        if (Result.Where(x => x.id_User == user.ID).Count() == 0)
                        {
                            Result.Add(new UserSearchRowTemplate()
                            {
                                id_User = user.ID,
                                Name_User = user.UserName,
                                State_User = user.State,
                                AnagraficaType_User = user.AnagraficaType,
                                Anagrafica_User = user.AnagraficaID,
                                AnagraficaNome = user.NomeCompleto,
                                Profile_User = user.Profile.ID,
                                Registered_User = user.Registrato
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        //trace
                    }
                }
            }

            return Result;
        }
    }
}