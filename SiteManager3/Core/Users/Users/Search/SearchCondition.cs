﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Users.Search
{
    public class SearchCondition
    {
        public string Property
        {
            get;
            set;
        }

        public object Value
        {
            get;
            set;
        }

        public ComparisonType Comparison
        {
            get;
            set;
        }

        public CriteriaType Criteria
        {
            get;
            set;
        }

        public enum ComparisonType
        {
            Equals,
            EqualsIgnoreCase,
            LessEquals,
            GreaterEquals,
            GreaterThan,
            LessThan,
            In,
            Like,
            LikeStart,
            LikeEnd,
            Not,
            NotNull,
            Null,
            IsEmpty,
            IsNotEmpty
        }

        public enum CriteriaType
        {
            User,
            Profile,
            GroupAssociation,
            Group
        }

        public SearchCondition(string property, object value, ComparisonType comparison, CriteriaType criteria)
        {
            this.Property = property;
            this.Value = value;
            this.Comparison = comparison;
            this.Criteria = criteria;
        }
    }
}
