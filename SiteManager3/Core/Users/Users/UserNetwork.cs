﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Networks;

namespace NetCms.Users
{
    public class UserNetwork
    {
        public UserNetwork()
        { }

        public virtual int ID
        { get; set; }

        public virtual User Utente
        { get; set; }

        public virtual BasicNetwork Network
        { get; set; }

        public override int GetHashCode()
        {
            int hashCode = 0;
            hashCode = hashCode ^ Utente.ID.GetHashCode() ^ Network.ID.GetHashCode();
            return hashCode;
        }

        public override bool Equals(object obj)
        {
            var toCompare = obj as UserNetwork;
            if (toCompare == null)
            {
                return false;
            }
            return (this.GetHashCode() == toCompare.GetHashCode());
        }
    }
}
