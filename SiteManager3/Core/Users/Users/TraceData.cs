﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Models
{
    public class TraceData
    {
        public virtual int ID { get; set; }
        public virtual string PublicIp { get; set; }
        public virtual string OS { get; set; }
        public virtual string Browser { get; set; }
        public virtual string Url { get; set; }
        public virtual string Referer { get; set; }
        public virtual string SessionID { get; set; }
        public virtual string Cookies { get; set; }
        public virtual string AcceptLanguage { get; set; }
        public virtual DateTime DateOra { get; set; }

        public virtual string HypotheticUserType { get; set; }

        public virtual PositionStatusEnum PositionStatus { get; set; } 
        public virtual string Latitudine { get; set; }
        public virtual string Longitudine { get; set; }

    }

    public enum PositionStatusEnum
    {
        [Description("ok")]
        ok = 4, 
        [Description("The request to get user location timed out.")]
        timeout = 3, 
        [Description("Location information is unavailable.")]
        position_unavailable = 2, 
        [Description("User denied the request for Geolocation.")]
        permission_denied = 1, 
        [Description ("An unknown error occurred.")]
        unknown = 0
    }
}
