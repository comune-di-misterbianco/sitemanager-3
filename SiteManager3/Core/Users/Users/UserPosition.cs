﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users
{
    public struct Position
    {
        public string lat;
        public string lon;
        public Users.Models.PositionStatusEnum PositionStatus;
    }
}
