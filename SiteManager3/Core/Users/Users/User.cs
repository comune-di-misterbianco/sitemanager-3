using NetCms.Connections;
using NetCms.Networks.WebFS;
using NetCms.Users.CustomAnagrafe;
using NetCms.Users.Search;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace NetCms.Users
{
    public enum GrantAdministrationTypes
    {
        User = 0, 
        Administrator = 1,
        SuperAdministrator = 2,
    }

    public enum ContentAdministrationTypes
    {
        Write = 0,
        Review = 1,
        Publish = 2,
    }

    public enum UsersTypes
    {
        Frontend = 0,
        Backend = 1
    }    
    
    public enum UsersGrantsOverrideModes { Default, Ghost, God  }

    public class User : Entity
    {
        #region Propriet� da mappare

        public virtual int ID
        {
            get;
            set;
        }

        public virtual int OldID
        {
            get;
            set;
        }

        public virtual int PreviewType
        {
            get;
            set;
        }

        public virtual string UserName
        {
            get;
            set;
        }

        public virtual Hashtable AnagraficaMapped
        {
            get;
            set;
        }

        public virtual UserProfile Profile
        {
            get;
            set;
        }

        public virtual string Password
        {
            get;
            set;
        }

        public virtual string NuovaPassword
        {
            get;
            set;
        }

        public virtual string ActivationKey 
        { 
            get; 
            set; 
        }

        public virtual int State
        {
            get;
            set;
        }

        public virtual int DeletedInt
        {
            get;
            set;
        }

        public virtual int FoldersGUI
        {
            get;
            set;
        }

        public virtual int Editor
        {
            get;
            set;
        }

        //public virtual int Type
        //{
        //    get;
        //    set;
        //}

        //Sar� l'entity-name della corrispondente tipologia di anagrafica contenuta nel file di mapping AnagraficaBase.hbm 
        //Servir� a recuperare la corretta istanza dell'anagrafica
        public virtual string AnagraficaType
        {
            get;
            set;
        }

        public virtual DateTime CreationTime
        {
            get;
            set;
        }

        public virtual DateTime LastAccess
        {
            get;
            set;
        }

        public virtual DateTime LastUpdate
        {
            get;
            set;
        }

        public virtual int God
        {
            get;
            set;
        }

        public virtual int Debug
        {
            get;
            set;
        }

        // aggiunto per gestire opzionalmente le richieste di registrazione
        // nel caso in cui non si vogliano gestire le richieste, questo campo viene settato a 1 e partir� automaticamente l'email di abilitazione
        // nel caso in cui si vogliano gestire le richieste, esso verr� settato da un operatore e solo dopo questo intervento verr� mandata l'email di abilitazione
        public virtual bool Registrato
        {
            get;
            set;
        }

        // usato per gli utenti che si registrano da frontend e serve, quando sono nel backoffice e accetto una registrazione, a sapere qual'� il link 
        // per l'attivazione utente in base alla network da mandare via mail
        public virtual string CurrentNetworkPath
        {
            get;
            set;
        }

        public virtual ICollection<GroupAssociation> AssociatedGroups
        {
            get;
            set;
        }

        // le notifiche destinate all'utente corrente
        public virtual ICollection<Notifica> Notifiche
        {
            get;
            set;
        }

        public virtual ICollection<UserNetwork> AssociatedNetworks
        {
            get;
            set;
        }

        public virtual ICollection<NetworkFolder> FavoritesFolders
        {
            get;
            set;
        }

        #endregion

        #region Propriet�


        public virtual int AnagraficaID
        {
            get 
            {
                return int.Parse(AnagraficaMapped["ID"].ToString());
            }
        }

        public virtual AnagraficaBase Anagrafica
        {
            get
            {
                return UsersBusinessLogic.GetAnagraficaBaseUser(this);
            }
        }

        public virtual string AnagraficaName
        {
            get
            {
                return this.AnagraficaType.Replace("Anagrafica", "");
            }
        }

        public virtual CustomAnagrafe.CustomAnagrafe CustomAnagrafe
        {
            get
            {
                return CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(this.AnagraficaName);
            }
        }

        public virtual string NomeCompleto
        {
            get
            {
                string nomeCompleto = "";
                CustomAnagrafe.CustomAnagrafe ca = this.CustomAnagrafe;
                if (ca != null)
                {
                    foreach (CustomField field in ca.FrontendFields)
                    {
                        nomeCompleto += this.AnagraficaMapped[field.Name].ToString() + " ";
                    }
                }
                return nomeCompleto;
            }
        }

        public virtual LabelsManager.Labels Labels
        {
            get
            {
                //if (_Labels == null)
                //    _Labels = new NetCms.Users.RegistrazioneLabels();
                //return _Labels;
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(RegistrazioneLabels)) as LabelsManager.Labels;
            }
        }
        private NetCms.Users.RegistrazioneLabels _Labels;
        
        private int _Revisor;
        public virtual int Revisor
        {
            get { return _Revisor; }
            set { _Revisor = value; }
        }

        private string _Label;
        public virtual string Label
        {
            get 
            {
                if (_Label == null)
                    _Label = this.NomeCompleto + " (" + this.UserName + ")";
                return _Label; 
            }
        }

        public virtual string SessionID
        {
            get
            {
                if (_SessionID == null)
                    _SessionID = System.Web.HttpContext.Current.Session.SessionID;
                return _SessionID;
            }
        }
        private string _SessionID;

        private bool _Publisher;
        public virtual bool Publisher
        {
            get 
            {
                foreach (GroupAssociation assoc in AssociatedGroups)
                {
                    _Publisher = _Publisher || assoc.ContentsRole == ContentAdministrationTypes.Publish;
                }
                return _Publisher; 
            }
            set { _Publisher = value; }
        }

        public virtual bool CanHandleItSelf
        {
            get
            {
                foreach (GroupAssociation assoc in this.AssociatedGroups)
                    if (assoc.Group.SystemName == "root" && assoc.GrantsRole == GrantAdministrationTypes.SuperAdministrator)
                        return true;

                return false;
            }
        }

        private SitemanagerUserGUI _GUI;
        public virtual SitemanagerUserGUI GUI
        {
            get
            {
                _GUI = new SitemanagerUserGUI(Editor == 1 ? SitemanagerUserGUI.RichTextEditors.TinyMCE : SitemanagerUserGUI.RichTextEditors.FreeTextBox,
                                          PreviewType == 1 ? SitemanagerUserGUI.PreviewTypes.Static : SitemanagerUserGUI.PreviewTypes.Floating,
                                          ((FoldersGUI == 1) ? SitemanagerUserGUI.FoldersGUIs.Colapsed : SitemanagerUserGUI.FoldersGUIs.Separate));
                return _GUI;
            }
        }

        private NetCms.Connections.Connection _Conn;
        public virtual NetCms.Connections.Connection Conn
        {
            get
            {
                //if (_Connection == null)
                //    _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
                //return _Connection;
                ConnectionsManager ConnManager = new ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }

        public virtual ICollection<KeyValuePair<int, bool>> FoldersCanView
        {
            get
            {
                if (HttpContext.Current.Session["FoldersCanViewUSER_" + ID] != null)
                {
                    _FoldersCanView = (ICollection<KeyValuePair<int, bool>>)HttpContext.Current.Session["FoldersCanViewUSER_" + ID];
                }
                else
                {
                    _FoldersCanView = new List<KeyValuePair<int, bool>>();

                    HttpContext.Current.Session["FoldersCanViewUSER_" + ID] = _FoldersCanView;
                }

                return _FoldersCanView;
            }
            set
            {
                HttpContext.Current.Session["FoldersCanViewUSER_" + ID] = value;
            }
        }
        private ICollection<KeyValuePair<int,bool>> _FoldersCanView;

        //private AnagraficaBase _Anagrafica;
        //public AnagraficaBase Anagrafica
        //{
        //    get { return _Anagrafica; }
        //}

        //private Profile _Profile;
        //public virtual Profile Profile
        //{
        //    get { return _Profile; }
        //    protected set { _Profile = value; }
        //}

        private bool _AllowBackofficeAccess;
        public virtual bool AllowBackofficeAccess
        {
            get { throw new NotImplementedException(); return _AllowBackofficeAccess; }
            protected set { _AllowBackofficeAccess = value; }
        }

        public virtual bool NeedToBeDeactivated
        {
            get
            {
                if (_NeedToBeDeactivated == 0)
                {
                    TimeSpan sixMonths = TimeSpan.FromDays(30*6);
                    TimeSpan daysSinceLastLogin = DateTime.Now - this.LastAccess;
                    bool isManuallyDeactivated  = this.LastAccess == DateTime.MinValue;
                    bool asNotUsedAccountInSixMonths = daysSinceLastLogin > sixMonths;
                    _NeedToBeDeactivated = (asNotUsedAccountInSixMonths || isManuallyDeactivated) ? 1 : -1;
                }
                return _NeedToBeDeactivated == 1;
            }
        }
        private int _NeedToBeDeactivated;

        // TODO: Logica errata - bisogna aggiungere un nuovo  campo LastUpdatePW e considerare questo nella verifica della scadenza. Inoltre il campo va aggiornato solo quando viene cambiata la PW.        
        // al metodo � stato aggiunto un controllo su una chiave da impostare in web.config nella sezione appSettings. La chiave si chiama EnableResetPassword (<add key="EnableResetPassword" value="false" />)
        // se la chiave � esistente ed il valore � true il cms valuter� se � necessario rimpostare la password. se � false non sar� mai necessario rimpostare la password per gli utenti
        // se la chiave non � settata rimane il comportamento che ha sempre avuto il cms cio� di far cambiare la password ogni 90 giorni.
        public virtual bool NeedPasswordRefresh
        {
            get
            {               
                if (!Configurations.Generics.EnableResetPassword || (this.CustomAnagrafe != null && !this.CustomAnagrafe.UseSpid) )               
                    return CheckNeedToUpdatePassword();                
                else
                    return false;                                     
            }
        }
        private bool CheckNeedToUpdatePassword()
        {
            DateTime luPlusDuration = this.LastUpdate.AddDays(90);
            return luPlusDuration < DateTime.Now || !string.IsNullOrEmpty(this.NuovaPassword);
        }

        private WebFileSystem.GUI.History.HistoryManager history;
        public virtual WebFileSystem.GUI.History.HistoryManager History
        {
            get
            {
                if (history == null)
                    history = new WebFileSystem.GUI.History.HistoryManager();
                return history;
            }

        }

        //protected NetRegistrazione.ServiceCollection _Servizi;
        //public NetRegistrazione.ServiceCollection Servizi
        //{
        //    get { return _Servizi; }
        //}

        public virtual int Depth
        {
            get
            {
                int _Depth = 0;
                if (this.AssociatedGroupsOrdered.Count > 0)
                    _Depth = this.AssociatedGroupsOrdered.ElementAt(0).Group.Depth;
                return _Depth;
            }
        }

        public virtual UsersGrantsOverrideModes OverrideMode
        {
            get 
            {
                switch (God)
                {
                    case 1: _OverrideMode = UsersGrantsOverrideModes.Ghost; break;
                    case 2: _OverrideMode = UsersGrantsOverrideModes.God; break;
                    default: _OverrideMode = UsersGrantsOverrideModes.Default; break;
                }
                return _OverrideMode; 
            }
            set { _OverrideMode = value; }
        }
        private UsersGrantsOverrideModes _OverrideMode;

        public virtual bool DebugMode
        {
            get
            {
                _DebugMode = Debug == 1;
                return _DebugMode;
            }
        }
        private bool _DebugMode;

        private int _IsAdministrator;
        public virtual bool IsAdministrator
        {
            get
            {
                if (_IsAdministrator == 0)
                {
                    _IsAdministrator = -1;
                    foreach (GroupAssociation link in this.AssociatedGroups)
                        if (link.GrantsRole != GrantAdministrationTypes.User)
                        {
                            _IsAdministrator = 1;
                            break;
                        }
                }
                return _IsAdministrator == 1;
            }
        }

        public virtual bool UseExternalAuthSystem
        {
            get
            {
                if (AnagraficaName == "ActiveDirectory")
                    return true;
                return false;
            }
        }


        //private string _AnagraficaType;
        //public string AnagraficaType
        //{
        //    get { return _AnagraficaType; }
        //}

        //public DateTime LastAccess
        //{
        //    get { return _LastAccess; }
        //    private set { _LastAccess = value; }
        //}
        //private DateTime _LastAccess;
        
        //public DateTime LastUpdate
        //{
        //    get { return _LastUpdate; }
        //    private set { _LastUpdate = value; }
        //}
        //private DateTime _LastUpdate;

        public virtual string AdminDetailLink
        {
            get
            {
                return NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/applications/users/user_details.aspx?id=" + this.ID;
            }
        }

        public enum Stati { Disabilitato, Abilitato, MaiLoggato }
        
        public virtual Stati Stato 
        {
            get
            {
                switch (State)
                {
                    case 0: _Stato = Stati.Disabilitato; break;
                    case 1: _Stato = Stati.Abilitato; break;
                    case 2: _Stato = Stati.MaiLoggato; break;
                }
                return _Stato;
            }
        }
        private Stati _Stato;
        
        public virtual bool Abilitato { get { return Stato == Stati.Abilitato; } }

        protected bool _Deleted;
        public virtual bool Deleted
        {
            get
            {
                _Deleted = DeletedInt == 1;
                return _Deleted;
            }
            protected set { _Deleted = value; }
        }

        //protected int _ID;
        //public virtual int ID
        //{
        //    get
        //    {
        //        return _ID;
        //    }
        //    protected set { _ID = value; }
        //}

        //public string ActivationKey { get; private set; } 

        //protected string _UserName;
        //public virtual string UserName
        //{
        //    get { return _UserName; }
        //    protected set { _UserName = value; }
        //}

        //protected string _Password;
        //public string Password
        //{
        //    get { return _Password; }
        //    protected set { _Password = value; }
        //}
        
        //protected string _NuovaPassword;
        //public string NuovaPassword
        //{
        //    get { return _NuovaPassword; }
        //    protected set { _NuovaPassword = value; }
        //}

        private ContentAdministrationTypes _ContentAdministrationLevel;
        public virtual ContentAdministrationTypes ContentAdministrationLevel
        {
            get { return _ContentAdministrationLevel; }
            protected set { _ContentAdministrationLevel = value; }
        }

        private GrantAdministrationTypes _AdministrationLevel;
        public virtual GrantAdministrationTypes AdministrationLevel
        {
            get { return _AdministrationLevel; }
            protected set { _AdministrationLevel = value; }
        } 
	
        #endregion

        #region Costruttori e Factory

        //public User(DataRow UserData)
        //{
        //    InitUser(UserData);
        //}

        public User()
        {
            AssociatedGroups = new HashSet<GroupAssociation>();
            Notifiche = new HashSet<Notifica>();
            AssociatedNetworks = new HashSet<UserNetwork>();
            FavoritesFolders = new HashSet<NetworkFolder>();
            //foreach (GroupAssociation assoc in AssociatedGroups)
            //{
            //    this.Publisher = this.Publisher || assoc.ContentsRole == ContentAdministrationTypes.Publish;
            //}

            //switch (State)
            //{
            //    case 0: Stato = Stati.Disabilitato; break;
            //    case 1: Stato = Stati.Abilitato; break;
            //    case 2: Stato = Stati.MaiLoggato; break;
            //}

            //Deleted = DeletedInt == 1;

            //DateTime luPlusDuration = this.LastUpdate.AddDays(90);
            //this._NeedPasswordRefresh = luPlusDuration < DateTime.Now || !string.IsNullOrEmpty(this.NuovaPassword);

            //_GUI = new SitemanagerUserGUI(Editor == 1 ? SitemanagerUserGUI.RichTextEditors.TinyMCE : SitemanagerUserGUI.RichTextEditors.FreeTextBox,
            //                              PreviewType == 1 ? SitemanagerUserGUI.PreviewTypes.Static : SitemanagerUserGUI.PreviewTypes.Floating,
            //                              ((FoldersGUI == 1) ? SitemanagerUserGUI.FoldersGUIs.Colapsed : SitemanagerUserGUI.FoldersGUIs.Separate));

            //_DebugMode = Debug == 1;

            //switch (God)
            //{
            //    case 1: OverrideMode = UsersGrantsOverrideModes.Ghost; break;
            //    case 2: OverrideMode = UsersGrantsOverrideModes.God; break;
            //    default: OverrideMode = UsersGrantsOverrideModes.Default; break;
            //}

            //RefreshNetworks();
            
        }

        //public User(string userID)
        //{
        //    DataRow row = this.Connection.SqlQuery("SELECT * FROM users WHERE Deleted_User = 0 AND id_User = " + userID).Rows[0];
        //    InitUser(row);
        //}
        //public User(int userID)
        //    : this(userID.ToString())
        //{
        //}

        //public void UpdateUser()
        //{
        //    DataRow UserData = Connection.SqlQuery("SELECT * FROM users WHERE Deleted_User = 0 AND id_User = " + _ID).Rows[0];
        //    InitUser(UserData);
        //}

        //protected void InitUser(DataRow UserData)
        //{
        //    DataRow row = UserData;

            //ID = int.Parse(row["id_User"].ToString());
            //Profile = new Profile(row["Profile_User"].ToString());
            //UserName = row["Name_User"].ToString();
            //Password = row["Password_User"].ToString();
            //NuovaPassword = row["NewPassword_User"].ToString();
            //ActivationKey = row["Key_User"].ToString();
            //switch (row["State_User"].ToString())
            //{
            //    case "0": Stato = Stati.Disabilitato; break;
            //    case "1": Stato = Stati.Abilitato; break;
            //    case "2": Stato = Stati.MaiLoggato; break;
            //}
            
            //Deleted = row["Deleted_User"].ToString() == "1";
            //DateTime.TryParse(row["LastAccess_User"].ToString(), out _LastAccess);
            //DateTime.TryParse(row["LastUpdate_User"].ToString(), out _LastUpdate);

            //DateTime luPlusDuration = this.LastUpdate.AddDays(90);
            //this._NeedPasswordRefresh = luPlusDuration < DateTime.Now || !string.IsNullOrEmpty(this.NuovaPassword);

            //this.AllowBackofficeAccess = this.Type != AdministratorTypes.FrontUser;

            //_Anagrafica = NetCms.Users.Anagraphics.AnagraphicsClassesPool.BuildInstanceOf(row["Anagrafica_User"].ToString(), row["AnagraficaType_User"].ToString());

            //_AnagraficaType = UserData["AnagraficaType_User"].ToString();

            //_GUI = new SitemanagerUserGUI(UserData.Table.Columns.Contains("Editor_User") && UserData["Editor_User"].ToString() == "1" ? SitemanagerUserGUI.RichTextEditors.TinyMCE : SitemanagerUserGUI.RichTextEditors.FreeTextBox,
            //                              UserData["PreviewType_User"].ToString() == "1" ? SitemanagerUserGUI.PreviewTypes.Static : SitemanagerUserGUI.PreviewTypes.Floating,//PreviewType
            //                              ((UserData.Table.Columns.Contains("FoldersGUI_User") && UserData["FoldersGUI_User"].ToString() == "1") ? SitemanagerUserGUI.FoldersGUIs.Colapsed : SitemanagerUserGUI.FoldersGUIs.Separate));

            //InitGroups();

            ///*
            //if (UserData.Table.Columns.Contains("Type_User"))
            //    _IsNetServiceAdministrator = UserData["Type_User"].ToString() == "2";
            //else
            //    _IsNetServiceAdministrator = false;
            //*/
            //if (UserData.Table.Columns.Contains("Debug_User"))
            //    _DebugMode = UserData["Debug_User"].ToString() == "1";
            //else
            //    _DebugMode = false;
            ////NetCms.Configurations.Generics.SetDebugMode(this.DebugMode);

            //if (UserData.Table.Columns.Contains("God_User"))
            //{
            //    switch (UserData["God_User"].ToString())
            //    {
            //        case "1": OverrideMode = UsersGrantsOverrideModes.Ghost; break;
            //        case "2": OverrideMode = UsersGrantsOverrideModes.God; break;
            //        default: OverrideMode = UsersGrantsOverrideModes.Default; break;
            //    }
            //}
            //else
            //    OverrideMode = UsersGrantsOverrideModes.Default;

            //RefreshNetworks();
        //}

        //public static User Fabricate(string userID)
        //{
        //    NetCms.Connections.Connection Connection = NetCms.Connections.ConnectionsManager.CommonConnection;

        //    DataRow row = Connection.SqlQuery("SELECT * FROM users WHERE Deleted_User = 0 AND id_User = " + userID).Rows[0];

        //    return Fabricate(row);
        //}

        //public static User FabricateFromAnagrafeID(string userAnagrafeID,string anagrafeType)
        //{
        //    NetCms.Connections.Connection Connection = Connections.ConnectionsManager.CommonConnection;
        //    DataTable dt = Connection.SqlQuery("SELECT * FROM users WHERE Deleted_User = 0 AND Anagrafica_User =" + userAnagrafeID + " AND AnagraficaType_User ='" + anagrafeType+"'");
        //    if (dt.Rows.Count > 0)
        //    {
        //        return Fabricate(dt.Rows[0]);
        //    }
        //    return null;            
        //}

        //public static User Fabricate(DataRow data)
        //{
        //    switch (data["Type_User"].ToString())
        //    {
        //        //case "0": return new FrontUser(data);
        //        default: return new User(data);
        //    }
        //}
        #endregion

        #region Metodi relativi alle password e agli accessi

        public virtual void UpdateLastAccess()
        {
            UsersBusinessLogic.UpdateLastAccess(this);
        }

        //public void SetNewPasswordAsCurrent()
        //{
        //    string sql = "UPDATE Users SET Password_User = NewPassword_User, NewPassword_User = '' WHERE id_User = '" + this.ID + "'";
        //    this.Connection.Execute(sql);
        //}

        //Costruisco un hash che mi permette di identificare l'utente in funzione di username, data creazione, anagrafica id e tipo e con la 
        //validit� di un giorno. Utilizzato per creare il link di conferma reset password quando l'utente non � loggato poich� ha perso la pwd
        //e ne ha fatto nuova richiesta.
        public virtual string HashInfoForValidationCheck()
        {
            string infoToHash = this.UserName + this.CreationTime + this.AnagraficaID + this.AnagraficaType + DateTime.Now.Date;
            return FormsAuthentication.HashPasswordForStoringInConfigFile(infoToHash, "MD5").ToString();
        }

        public virtual void SendNewPassword()
        {
            string password = ChangePassword();
            SendNewPasswordMail(password);
        }

        //protected string ChangePassword()
        //{
        //    string password = new G2Core.Common.PasswordGenerator(NetCms.Configurations.Generics.MinimumPasswordLength).Generate();
        //    string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();

        //    string sql = "UPDATE Users SET NewPassword_User = '" + MD5Pwd + "', Password_User = '' WHERE Name_User = '" + this.UserName + "' AND Deleted_User = 0";
        //    Connection.Execute(sql);
        //    return password;
        //}

        protected string ChangePassword()
        {
            string password = new G2Core.Common.PasswordGenerator(NetCms.Configurations.Generics.MinimumPasswordLength).Generate();
            string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();

            this.NuovaPassword = MD5Pwd;
            
            //this.Password = "";

            UsersBusinessLogic.UpdateUserIfNotDeleted(this);

            //string sql = "UPDATE Users SET NewPassword_User = '" + MD5Pwd + "', Password_User = '' WHERE Name_User = '" + this.UserName + "' AND Deleted_User = 0";
            //Connection.Execute(sql);
            return password;
        }

        public virtual void SendConfirmChangePwdMail(string urlToReport)
        {
            #region Formattazione Body

            string body = this.Labels[RegistrazioneLabels.RegistrazioneLabelsList.TestoEmailConfermaCambioPwd];
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();
            body = body.Replace("{NOME_COGNOME}", this.NomeCompleto);
            body = body.Replace("{IP}", ip);
            body = body.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
            body = body.Replace("{ACTIVATE}", urlToReport);
            body = body.Replace("{EMAILPORTALE}", NetCms.Configurations.PortalData.eMail);

            #endregion

            string subject = "" + NetCms.Configurations.PortalData.Nome + ": email di conferma reimpostazione password.";

            string targetEmail;
            if (NetCms.Configurations.TestMode.GenericEnabled)
                targetEmail = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailToReport"].ToString();
            else
                targetEmail = this.Anagrafica.Email;

            G2Core.Common.MailSender msg = new G2Core.Common.MailSender(targetEmail,
                                                                        subject,
                                                                        body,
                                                                        NetCms.Configurations.PortalData.eMail,
                                                                        NetCms.Configurations.PortalData.Nome);

            msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
            msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
            msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;
            msg.DisableServerCertificateValidation = NetCms.Configurations.PortalData.DisableServerCertificateValidation;
            if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
            {
                msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
                msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
            }
            msg.Send();
            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "Spedita email di conferma reimpostazione password " + this.Anagrafica.Email, "", "", "");
        }

        private void SendNewPasswordMail(string password)
        {
            //string body = new RegistrazioneLabels()[RegistrazioneLabels.RegistrazioneLabelsList.TestoEmailRecupero];
            string body = Labels[RegistrazioneLabels.RegistrazioneLabelsList.TestoEmailRecupero];

            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();

            body = body.Replace("{NOME_COGNOME}", this.NomeCompleto);
            body = body.Replace("{IP}", ip);
            body = body.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
            body = body.Replace("{PASSWORD}", password);
            body = body.Replace("{PATH}", NetCms.Configurations.Paths.AbsoluteRoot + "/");
            body = body.Replace("{EMAILPORTALE}", NetCms.Configurations.PortalData.eMail);
            body = body.Replace("{USERNAME}", this.UserName);

            string subject = "" + NetCms.Configurations.PortalData.Nome + ": recupero credenziali.";

            G2Core.Common.MailSender msg = new G2Core.Common.MailSender(
                                                                            this.Anagrafica.Email,
                                                                            subject,
                                                                            body,
                                                                            NetCms.Configurations.PortalData.eMail,
                                                                            NetCms.Configurations.PortalData.Nome);


            msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
            msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
            msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;
            msg.DisableServerCertificateValidation = NetCms.Configurations.PortalData.DisableServerCertificateValidation;

            if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
            {
                msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
                msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
            }

            msg.Send();
        }

        //public virtual void SendActivationMail()
        //{
        //    #region Aggiornamento dati su DB
            
        //    string codice_convalida = new Random().Next(9999999).ToString();
        //    //string CreationTime_User = Connection.FilterDateTime(DateTime.Now);
        //    //string LastAccess_User = Connection.FilterDateTime(DateTime.MinValue);
        //    //string LastUpdate_User = Connection.FilterDateTime(DateTime.MinValue);
        //    string password = new G2Core.Common.PasswordGenerator(NetCms.Configurations.Generics.MinimumPasswordLength).Generate();
        //    string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();
         

        //    this.NuovaPassword = "";
        //    this.CreationTime = DateTime.Now;
        //    this.LastAccess = DateTime.MinValue;
        //    this.LastUpdate = DateTime.MinValue;
        //    this.Password = MD5Pwd;
        //    this.ActivationKey = codice_convalida;
        //    this.State = 0;         

        //    UsersBusinessLogic.UpdateUserIfNotDeleted(this);

        //    #endregion

        //    #region Formattazione Body

        //    string body = this.Labels[RegistrazioneLabels.RegistrazioneLabelsList.TestoEmailConfermaReinoltrata];
        //    string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();
        //    body = body.Replace("{NOME_COGNOME}", this.NomeCompleto);
        //    body = body.Replace("{USERNAME}", this.UserName);
        //    body = body.Replace("{IP}", ip);
        //    body = body.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
        //    body = body.Replace("{PASSWORD}", password);
        //    body = body.Replace("{KEY}", codice_convalida);
        //    body = body.Replace("{PATH}", NetCms.Configurations.Paths.AbsoluteRoot + "/");
        //    body = body.Replace("{EMAILPORTALE}", NetCms.Configurations.PortalData.eMail);

        //    #endregion

        //    string subject = "" + NetCms.Configurations.PortalData.Nome + ": email di conferma registrazione al portale.";

        //    string targetEmail;
        //    if (NetCms.Configurations.TestMode.GenericEnabled)
        //        targetEmail = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailToReport"].ToString();
        //    else
        //        targetEmail = this.Anagrafica.Email;

        //    G2Core.Common.MailSender msg = new G2Core.Common.MailSender(targetEmail,
        //                                                                subject,
        //                                                                body,
        //                                                                NetCms.Configurations.PortalData.eMail,
        //                                                                NetCms.Configurations.PortalData.Nome);

        //    msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
        //    msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
        //    msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;
        //    msg.DisableServerCertificateValidation = NetCms.Configurations.PortalData.DisableServerCertificateValidation;

        //    if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
        //    {
        //        msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
        //        msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
        //    }
        //    msg.Send();
        //    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "Spedita email di attivazione a " + this.Anagrafica.Email, "", "", "");
        //}
        public virtual bool SendActivationMail()
        {
            bool esito = false;
            try
            {
                #region Aggiornamento dati su DB

                string codice_convalida = new Random().Next(9999999).ToString();
                string password = new G2Core.Common.PasswordGenerator(NetCms.Configurations.Generics.MinimumPasswordLength).Generate();
                string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();


                this.NuovaPassword = "";
                //this.CreationTime = DateTime.Now;
                //this.LastAccess = DateTime.MinValue;
                //this.LastUpdate = DateTime.MinValue;
                this.Password = MD5Pwd;
                this.ActivationKey = codice_convalida;
                this.State = 0;

                UsersBusinessLogic.UpdateUserIfNotDeleted(this);

                #endregion

                Uri url = System.Web.HttpContext.Current.Request.Url;
                string baseUrl = url.GetLeftPart(UriPartial.Authority);

                string activateUrl = baseUrl + NetCms.Configurations.Generics.ActivationUrl;

                UsersBusinessLogic.SendConfirmMail(this, password, activateUrl, false);

                esito = true;
            }
            catch(Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceException(ex, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
            }

            return esito;
        }
        /// <summary>
        /// Genera una nuova password e imposta la data di ultimo aggiornamento utente a min value per forzare il cambio pw al primo accesso; infine la invia alla email associata all'utente.
        /// </summary>
        /// <returns>A boolean </returns>
        public virtual bool SendNewPasswordFromBackoffice()
        {
            bool esito = false;
            try
            {
                string password = new G2Core.Common.PasswordGenerator(NetCms.Configurations.Generics.MinimumPasswordLength).Generate();
                string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();

                this.Password = MD5Pwd;
                this.LastUpdate = DateTime.MinValue;

                UsersBusinessLogic.UpdateUserIfNotDeleted(this);

                UsersBusinessLogic.SendNewPasswordMail(this, password);

                esito = true;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceException(ex, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), NetCms.Users.AccountManager.CurrentAccount.UserName);
            }
            return esito;
        }

        #endregion

        #region Metodi Relativi ai Gruppi

        //        protected virtual void InitGroups()
        //        {
        //            DataRow[] rows = Connection.SqlQuery(@"SELECT * FROM Users 
        //                                                        INNER JOIN usersgroups ON (users.id_User = usersgroups.User_UserGroup) 
        //                                                        INNER JOIN groups ON (usersgroups.Group_UserGroup = groups.id_Group) 
        //                                                        ORDER BY Depth_Group DESC,Tree_Group").Select("id_User = " + ID, "Depth_Group DESC,Tree_Group");
        //            _AssociatedGroups = new GroupAssociationCollection();
        //            foreach (DataRow row in rows)
        //            {
        //                GroupAssociation assoc = new GroupAssociation(row, this);
        //                AssociatedGroups.Add(assoc);
        //                this.Publisher = this.Publisher || assoc.ContentsRole == ContentAdministrationTypes.Publish;
        //            }
        //        }


        //public GroupAssociationCollection AssociatedGroups
        //{
        //    get
        //    {
        //        return _AssociatedGroups;
        //    }
        //}
        //protected GroupAssociationCollection _AssociatedGroups;

        //private GroupsCollection _Groups;
        //public GroupsCollection Groups
        //{
        //    get
        //    {
        //        if (_Groups == null)
        //        {
        //            _Groups = new GroupsCollection(null);
        //            foreach (GroupAssociation link in this.AssociatedGroups)
        //                _Groups.Add(link.Group);
        //        }

        //        return _Groups;
        //    }

        //}


        // NEI SUCCESSIVI UTILIZZI IN LETTURA VA USATO QUESTO, CHE � LA VERSIONE ORDINATA DELLA COLLEZIONE CARICATA DA DB (AssociatedGroups)
        private ICollection<GroupAssociation> _AssociatedGroupsOrdered;
        public virtual ICollection<GroupAssociation> AssociatedGroupsOrdered
        {
            get
            {
                if (_AssociatedGroupsOrdered == null)
                {
                    _AssociatedGroupsOrdered = AssociatedGroups.OrderByDescending(x => x.Group.Depth).ThenBy(x => x.Group.Tree).ToList();
                }

                return _AssociatedGroupsOrdered;
            }

        }

        private ICollection<Group> _Groups;
        public virtual ICollection<Group> Groups
        {
            get
            {
                if (_Groups == null)
                {
                    _Groups = AssociatedGroupsOrdered.Select(x => x.Group).ToList();
                }

                return _Groups;
            }

        }

        #endregion

        #region Metodi e Properiet� relative alle reti

        public virtual Dictionary<int, string> EnabledNetworks
        {
            get
            {
                if (_EnabledNetworks == null)
                {
                    _EnabledNetworks = this.AssociatedNetworks.ToDictionary(x => x.Network.ID, x => x.Network.SystemName);
                    
                }
                return _EnabledNetworks;
            }
        }
        private Dictionary<int, string> _EnabledNetworks;

        public virtual bool HasNeworksEnabled
        {
            get { return EnabledNetworks.Count > 0; }
        }

        public virtual bool MultiNetworksUser
        {
            get { return EnabledNetworks.Count > 1; }
        }

        public virtual bool NetworkEnabledForUser(int id)
        {
            return this.EnabledNetworks.ContainsKey(id);
        }

        public virtual void EnableNetwork(int networkID)
        {
            UsersBusinessLogic.EnableNetwork(this, networkID);

        }

        public virtual void DisableNetwork(int networkID)
        {
            UsersBusinessLogic.DisableNetwork(this, networkID);

        }
        
        #endregion

        #region Metodi per Abilitare e Disabilitare gli Utenti

        public virtual void Enable()
        {
            this.LastAccess = DateTime.Now;
            this.State = 1;

            //this.Connection.Execute("UPDATE Users SET  LastAccess_User = " + Connection.FilterDateTime(DateTime.Now) + ", State_User = 1 WHERE id_User = " + this.ID);

            UsersBusinessLogic.SaveOrUpdateUser(this);

            if (!string.IsNullOrEmpty(this.Anagrafica.Email))
                this.SendEnabledMail();
        }

        public virtual void EnableSimple()
        {
             this.State = 1;

             UsersBusinessLogic.SaveOrUpdateUser(this);

             if (!string.IsNullOrEmpty(this.Anagrafica.Email))
                this.SendEnabledMail();            
        }

        public virtual void DisableAccount()
        {
            Disable();
        }

        public virtual void Disable()
        {
            this.State = 0;
            UsersBusinessLogic.SaveOrUpdateUser(this);

            //this.Connection.Execute("UPDATE Users SET State_User = 0 WHERE id_User = " + this.ID);
            
            if (!string.IsNullOrEmpty(this.Anagrafica.Email))
                this.SendDisableMail();
        }

        public virtual void DisableProfile()
        {
            this.State = 0;
            UsersBusinessLogic.SaveOrUpdateUser(this);           
        }

        public virtual void Delete()
        {
            if (this.Stato == Stati.MaiLoggato)
                HardDelete();
            else
            {
                //this.Connection.Execute("UPDATE Users SET Deleted_User = 1 WHERE id_User = " + this.ID);
                //Impostazione stato a disattivato per evitare problemi di successiva creazione utenti
                this.State = 0;
                this.DeletedInt = 1;
                UsersBusinessLogic.SaveOrUpdateUser(this);
            }
        }

        public virtual void SendDisableMail()
        {
            string body = this.Labels[NetCms.Users.RegistrazioneLabels.RegistrazioneLabelsList.TestoEmailDisattivazione];

            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();

            body = body.Replace("{NOME_COGNOME}", this.NomeCompleto);
            body = body.Replace("{IP}", ip);
            body = body.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
            body = body.Replace("{EMAILPORTALE}", NetCms.Configurations.PortalData.eMail);

            //string subject = "" + NetCms.Configurations.PortalData.Nome + ": email di disattivazione account.";
            string subject = this.Labels[NetCms.Users.RegistrazioneLabels.RegistrazioneLabelsList.OggettoEmailDisattivazione]
                             .Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);

            // Invio Email di registrazione negata
            G2Core.Common.MailSender msg = new G2Core.Common.MailSender(
                                                                            this.Anagrafica.Email,
                                                                            subject,
                                                                            body,
                                                                            NetCms.Configurations.PortalData.eMail,
                                                                            NetCms.Configurations.PortalData.Nome);
            msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
            msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
            msg.DisableServerCertificateValidation = NetCms.Configurations.PortalData.DisableServerCertificateValidation;
            msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;
            msg.SendAsHtml = true;
            msg.SendAlsoPlain = true;

            if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
            {
                msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
                msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
            }

            msg.Send();
        }


        public virtual void SendUserProfileDisableMail()
        {
            string body = this.Labels[NetCms.Users.RegistrazioneLabels.RegistrazioneLabelsList.TestoEmailUserProfileDisable];

            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();

            body = body.Replace("{NOME_COGNOME}", this.NomeCompleto);
            body = body.Replace("{IP}", ip);
            body = body.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
            body = body.Replace("{EMAILPORTALE}", NetCms.Configurations.PortalData.eMail);

            string subject = this.Labels[NetCms.Users.RegistrazioneLabels.RegistrazioneLabelsList.OggettoEmailUserProfileDisable]
                             .Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);

            // Invio Email di registrazione negata
            G2Core.Common.MailSender msg = new G2Core.Common.MailSender(
                                                                            this.Anagrafica.Email,
                                                                            subject,
                                                                            body,
                                                                            NetCms.Configurations.PortalData.eMail,
                                                                            NetCms.Configurations.PortalData.Nome);
            msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
            msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
            msg.DisableServerCertificateValidation = NetCms.Configurations.PortalData.DisableServerCertificateValidation;
            msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;
            msg.SendAsHtml = true;
            msg.SendAlsoPlain = true;

            if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
            {
                msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
                msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
            }

            msg.Send();
        }


        public virtual void SendEnabledMail()
        {
            string body = this.Labels[NetCms.Users.RegistrazioneLabels.RegistrazioneLabelsList.TestoEmailConfermaAttivazione];

            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();

            body = body.Replace("{NOME_COGNOME}", this.NomeCompleto);
            body = body.Replace("{IP}", ip);
            body = body.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
            body = body.Replace("{EMAILPORTALE}", NetCms.Configurations.PortalData.eMail);

            string subject = this.Labels[NetCms.Users.RegistrazioneLabels.RegistrazioneLabelsList.OggettoEmailConfermaAttivazione]
                             .Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);

            // Invio Email di registrazione confermata
            G2Core.Common.MailSender msg = new G2Core.Common.MailSender(
                                                                            this.Anagrafica.Email,
                                                                            subject,
                                                                            body,
                                                                            NetCms.Configurations.PortalData.eMail,
                                                                            NetCms.Configurations.PortalData.Nome);
            msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
            msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
            msg.DisableServerCertificateValidation = NetCms.Configurations.PortalData.DisableServerCertificateValidation;
            msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;
            msg.SendAsHtml = true;
            msg.SendAlsoPlain = true;

            if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
            {
                msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
                msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
            }

            msg.Send();
        }

        public virtual void SendRegistrationRequestMail()
        {
            string body = this.Labels[NetCms.Users.RegistrazioneLabels.RegistrazioneLabelsList.TestoRichiestaRegistrazioneInviata];

            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();

            body = body.Replace("{NOME_COGNOME}", this.NomeCompleto);
            body = body.Replace("{IP}", ip);
            body = body.Replace("{NOME_PORTALE}", NetCms.Configurations.PortalData.Nome);
            body = body.Replace("{EMAILPORTALE}", NetCms.Configurations.PortalData.eMail);

            string subject = "" + NetCms.Configurations.PortalData.Nome + ": email di richiesta registrazione al portale.";

            // Invio Email di registrazione confermata
            G2Core.Common.MailSender msg = new G2Core.Common.MailSender(
                                                                            this.Anagrafica.Email,
                                                                            subject,
                                                                            body,
                                                                            NetCms.Configurations.PortalData.eMail,
                                                                            NetCms.Configurations.PortalData.Nome);
            msg.SetSmtpHost = NetCms.Configurations.PortalData.SmtpHost;
            msg.SetSmtpPort = int.Parse(NetCms.Configurations.PortalData.SmtpPort);
            msg.DisableServerCertificateValidation = NetCms.Configurations.PortalData.DisableServerCertificateValidation;
            msg.EnableSsl = NetCms.Configurations.PortalData.EnableSSL;

            if (NetCms.Configurations.PortalData.SmtpUser != null && NetCms.Configurations.PortalData.SmtpPassword != null)
            {
                msg.SmtpUser = NetCms.Configurations.PortalData.SmtpUser;
                msg.SmtpPassword = NetCms.Configurations.PortalData.SmtpPassword;
            }

            msg.Send();
        } 

        #endregion


        public virtual bool IsAdministratorForUser(int userID)
        {
            //VERSIONE NON OTTIMIZZATA
            //NetCms.Users.Search.UsersDataManager UsersDataManager = new NetCms.Users.Search.UsersDataManager(this);
            ////return UsersDataManager.MyUsersTable.Select("id_User = " + userID).Length > 0;
            //return UsersDataManager.MyUsersTable.Where(x=>x.ID == userID).Count() > 0;
            
            //VERSIONE OTTIMIZZATA DEL METODO
            Search.UserRelated st = new Search.UserRelated(this);

            SearchCondition[] additionalConditions = new SearchCondition[] { new SearchCondition("ID", userID, SearchCondition.ComparisonType.Equals, SearchCondition.CriteriaType.User) };

            return st.UsersIAdminCount(null,additionalConditions) > 0;

        }

        public virtual void HardDelete()
        {
            //this.Anagrafica.HardDelete();

            //this.Connection.Execute("DELETE FROM Users WHERE id_User = " + this.ID);

            UsersBusinessLogic.DeleteUser(this);
        }

        //public DataRow SourceDataRow
        //{
        //    get { return _SourceDataRow; }
        //    private set { _SourceDataRow = value; }
        //}
        //private DataRow _SourceDataRow;
    }    
}
