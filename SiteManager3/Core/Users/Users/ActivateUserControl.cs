﻿using NetCms.Users;
using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCms.Users
{
    public class ActivateUserControl : VfManager
    {
        protected LabelsManager.Labels Labels;

        public ActivateUserControl(string controlId, LabelsManager.Labels usersLabels)
            : base(controlId)
        {
            Labels = usersLabels;
            this.Rows = 4;
            this.BootstrapVer = VfManager.BootstrapVerEnum.Bootstrap4;

            this.Fields.Add(NomeUtente);
            this.Fields.Add(Password);
            this.Fields.Add(Codice);
            this.Fields.Add(VfCaptcha);

        }

        private VfTextBox _NomeUtente;
        public VfTextBox NomeUtente
        {
            get
            {
                if (_NomeUtente == null)
                {
                    _NomeUtente = new VfTextBox("username_act",
                        Labels[UserActivateLabels.LabelList.Username],
                            "username_act", InputTypes.Text);
                    _NomeUtente.Required = true;
                    _NomeUtente.ColumCssClass = "col-md-6";
                    _NomeUtente.RowOrder = 1;
                    _NomeUtente.UseBootstrap = true;
                }
                return _NomeUtente;
            }
        }
    
        private VfPassword _Password;
        public VfPassword Password
        {
            get
            {
                if (_Password == null)
                {
                    _Password = new VfPassword("password_act", false);                    
                    _Password.Required = true;
                    _Password.ColumCssClass = "col-md-6";
                    _Password.RowOrder = 2;
                    _Password.UseBootstrap = true;
                    _Password.Type = VfPassword.FieldType.NoConfirmField;
                  //  _Password.Label = Labels[UserActivateLabels.LabelList.password];
                }
                return _Password;
            }
        }

        private VfTextBox _Codice;
        public VfTextBox Codice
        {
            get
            {
                if (_Codice == null)
                {
                    _Codice = new VfTextBox("codice",
                        Labels[UserActivateLabels.LabelList.ActivationCode],
                            "codice", InputTypes.Text);
                    _Codice.Required = true;
                    _Codice.ColumCssClass = "col-md-6";
                    _Codice.RowOrder = 3;
                    _Codice.UseBootstrap = true;
                }
                return _Codice;
            }
        }

        private VfReCaptcha vfCaptcha;
        public VfReCaptcha VfCaptcha
        {
            get
            {
                if (vfCaptcha == null)
                {
                    vfCaptcha = new VfReCaptcha("reCaptcha");
                    vfCaptcha.ColumCssClass = "col-md-6";
                    vfCaptcha.RowOrder = 4;
                    VfCaptcha.UseBootstrap = true;
                }
                return vfCaptcha;
            }
        }
    }
}
