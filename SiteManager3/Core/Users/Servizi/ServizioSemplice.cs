﻿
namespace NetCms.Users
{
    public abstract class ServizioSemplice : Servizio
    {
        public override Servizio.TipiServizi TipoServizio
        {
            get { return TipiServizi.Semplice; }
        }
    }
}
