﻿
using NHibernate;
namespace NetCms.Users
{
    public abstract class ServizioAutomatico : Servizio
    {
        public override Servizio.TipiServizi TipoServizio
        {
            get { return TipiServizi.Automatico; }
        }

        public override bool UtenteAttivo(User user, ISession session = null)
        {
            return true;
        }

        public override bool AttivazioneDifferita
        {
            get { return false; }
        }
    }
}
