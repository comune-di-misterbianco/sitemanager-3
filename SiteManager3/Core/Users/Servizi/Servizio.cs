﻿
using System;
using NHibernate;
using NetService.Utility.ValidatedFields;
using System.Collections.Generic;
namespace NetCms.Users
{
    public abstract class Servizio
    {
        /// <summary>
        /// Automatico: la procedura di attivazione avviene automaticamente non appena l'utente accede la prima volta all'applicativo;
        /// Semplice: la procedura di attivazione avviene in seguito ad un click post registrazione;
        /// Differenziato: la procedura di attivazione richiede un feedback da backoffice in seguito a ricezione dell'eventuale documentazione;
        /// Forte: la procedura di attivazione richiede che l'utente si presenti fisicamente dal gestore consegnando un eventuale modulo di responsabilità ed un documento di
        ///         riconoscimento valido con il quale viene identificato dal gestore.
        /// </summary>
        public enum TipiServizi
        {
            Automatico = 1,
            Semplice = 2,
            Differenziato = 3,
            Forte = 4
        }

        public Servizio()
        {

        }

        public abstract string Nome 
        { 
            get; 
        }

        public abstract string Descrizione
        {
            get;
        }

        public abstract bool AttivazioneDifferita
        {
            get;
        }

        public abstract TipiServizi TipoServizio
        {
            get;
        }

        public virtual string LevelKey
        {
            get
            {
                return "";
            }
        }

        public abstract void Esegui(User user, bool abilitaEsecuzioneServiziPariLivello = false, ISession session = null, VfManager form = null, ICollection<Servizio> serviziAttivati = null);

        public virtual  void EseguiConVerificaCittadino(User user, bool abilitaEsecuzioneServiziPariLivello = false, ISession session = null, VfManager form = null, ICollection<Servizio> serviziAttivati = null)
        {

             //Esegui(user, abilitaEsecuzioneServiziPariLivello, session, form, serviziAttivati);
             throw new NotImplementedException();
        }
       
        public abstract bool UtenteAttivo(User user, ISession session = null);

        public abstract void Disattiva(User user, ISession session = null);

        public bool RichiedeDatiAggiuntivi
        {
            get
            {
                return (int)(this.TipoServizio) > 2;
            }
        }


        //Flag inserito per consentire la scelta di invio dell'e-mail di notifica o meno all'utente.
        //Questo permette l'uso del metodo Esegui all'interno di una procedura più complessa e far 
        //si che l'e-mail venga inviata non alla fine del metodo Esegui, ma alla fine del metodo 
        //complesso che si intende realizzare.
        public bool SendNotifyMail
        {
            get
            { return _SendNotifyMail; }
            set
            { _SendNotifyMail = value; }
        }
        private bool _SendNotifyMail = true;

    }
}
