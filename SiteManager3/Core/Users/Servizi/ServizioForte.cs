﻿
namespace NetCms.Users
{
    public abstract class ServizioForte : ServizioConDatiAggiuntivi
    {
        public override Servizio.TipiServizi TipoServizio
        {
            get { return TipiServizi.Forte; }
        }

        public abstract string IstruzioniAttivazione
        {
            get;
        }
        /// <summary>
        /// Questa proprietà, se è settata a true permette di usufruire dell'attivazione automatica anche per il servizio forte, previa verifica della cittadinanza.
        /// N.B. Bisogna però implementare la connessione al database dell'anagrafe del Comune tramite il progetto di Core AnagrafeEnteConnection
        /// </summary>
        public virtual bool AutomaticStrongService
        {
            get
            {
                return false;
            }
        }
    }
}
