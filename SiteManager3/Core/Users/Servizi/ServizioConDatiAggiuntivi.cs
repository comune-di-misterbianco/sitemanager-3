﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;

namespace NetCms.Users
{
    public abstract class ServizioConDatiAggiuntivi : Servizio
    {
        public abstract VfManager FormAttivazione
        {
            get;
        }
    }
}
