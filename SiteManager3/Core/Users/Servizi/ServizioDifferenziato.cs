﻿
namespace NetCms.Users
{
    public abstract class ServizioDifferenziato : ServizioConDatiAggiuntivi
    {
        public override Servizio.TipiServizi TipoServizio
        {
            get { return TipiServizi.Differenziato; }
        }
    }
}
