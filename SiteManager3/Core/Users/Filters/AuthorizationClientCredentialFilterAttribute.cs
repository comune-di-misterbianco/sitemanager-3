﻿using NetCms.Diagnostics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Security;

namespace NetCms.Users.Filters
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class AuthorizationClientCredentialFilterAttribute : AuthorizationFilterAttribute
    {
        public AuthorizationClientCredentialFilterAttribute()
        {

        }
        private bool _isRefresh = false;
        [DefaultValue(false)]
        public bool isRefresh
        {
            get
            {
                return _isRefresh;
            }
            set
            {
                _isRefresh = value;
            }
        }
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            ClaimsPrincipal principal = null;
            var accessToken = FetchFromHeader(actionContext);
            bool isValidToken = false;
            try
            {
                if (accessToken != null)
                {
                    var jwtToken = new JwtSecurityToken(accessToken);
                    if (jwtToken != null)
                    {                       
                        if (NetCms.Users.TokenManager.ValidateClientCredentialToken(accessToken, out principal,  isRefresh))
                        {
                            Thread.CurrentPrincipal = principal;
                            if (HttpContext.Current != null)
                            {
                                HttpContext.Current.User = principal;
                                isValidToken = true;
                            }
                        }
                    }
                }
                if (!isValidToken)
                {
                    actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                    actionContext.Response.Headers.Add("WWW-Authenticate", "Bearer");
                    return;
                }
            }
            catch (AccessViolationException ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(AuthorizationClientCredentialFilterAttribute));
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                actionContext.Response.Headers.Add("WWW-Authenticate", "Bearer");
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(AuthorizationClientCredentialFilterAttribute));
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                actionContext.Response.Headers.Add("WWW-Authenticate", "Bearer");
            }
            base.OnAuthorization(actionContext);
        }

        /// <summary>
        /// retrive header detail from the request 
        /// </summary>
        /// <param name="actionContext"></param>
        /// <returns></returns>
        private string FetchFromHeader(HttpActionContext actionContext)
        {
            string requestToken = null;

            var authRequest = actionContext.Request.Headers.Authorization;
            if (authRequest != null && !string.IsNullOrEmpty(authRequest.Scheme) && authRequest.Scheme == "Bearer")
                requestToken = authRequest.Parameter;

            return requestToken;
        }
    }
}
