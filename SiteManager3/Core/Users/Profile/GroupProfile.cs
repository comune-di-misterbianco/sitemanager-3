﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;

namespace NetCms.Users
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
 
        }

        public override int Depth
        {
            get { return this.Group.Depth; }
        }

        public override string EntityLabel
        {
            get 
            { 
                return "Il gruppo '" + this.Group.Name + "'"; 
            }
        }

        public override string EntityName
        {
            get 
            {
                return this.Group.Name;
            }
        }

        public override bool IsGroup
        {
            get { return true; }
        }

        public override bool IsUser
        {
            get { return false; }
        }

        public override ICollection<Group> ParentsGroups
        {
            get 
            {
                if (_ParentsGroups == null)
                {
                    _ParentsGroups = new HashSet<Group>();
                    if (Group.Parent != null)
                        _ParentsGroups.Add(Group.Parent);
                } 
                return _ParentsGroups;
            }
        }
        private ICollection<Group> _ParentsGroups;

        //public Group Group
        //{
        //    get
        //    {
        //        if (_Group == null)
        //            _Group = NetCms.Users.GroupsHandler.RootGroup.FindGroup(EntityID);
        //        return _Group;
        //    }
        //}
        //private Group _Group;

        public virtual Group Group
        {
            get;
            set;
        }

        public virtual GroupProfile Clone(Group group)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<GroupProfile, GroupProfile>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Group, o => o.Ignore());
            });
            config.AssertConfigurationIsValid();
            var newGroupProfile = new GroupProfile();
            
            var mapper = config.CreateMapper();
            mapper.Map(this, newGroupProfile);

            newGroupProfile.Group = group;
            return newGroupProfile;
        }
    }
}
