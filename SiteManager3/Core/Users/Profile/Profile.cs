using System;
using System.Data;
using System.Collections.Generic;

namespace NetCms.Users
{
    public abstract class Profile
    {
        #region ProprietÓ da mappare

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Nome
        {
            get;
            set;
        }
        
        #endregion

        public abstract bool IsUser
        {
            get;
        }

        public abstract bool IsGroup
        {
            get;
        }

        //public string ID
        //{
        //    get { return _ID; }
        //    private set { _ID = value; }
        //}
        //private string _ID;

        //private NetCms.Connections.Connection _Connection;
        //private NetCms.Connections.Connection Connection
        //{
        //    get
        //    {
        //        if (_Connection == null)
        //            _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
        //        return _Connection;
        //    }
        //}

        //public bool IsUser
        //{
        //    get
        //    {
        //        return Type == ProfileTypes.User;
        //    }
        //}

        //public bool IsGroup
        //{
        //    get
        //    {
        //        return Type == ProfileTypes.Group;
        //    }
        //}
        //public enum ProfileTypes { User,Group }

        //public ProfileTypes Type
        //{
        //    get { return _Type; }
        //    private set { _Type = value; }
        //}
        //private ProfileTypes _Type;

        //public NetCms.Users.Group Group
        //{
        //    get
        //    {
        //        if (_Group == null && this.IsGroup)
        //            _Group = NetCms.Users.GroupsHandler.RootGroup.FindGroup(EntityID);
        //        return _Group;
        //    }
        //}
        //private NetCms.Users.Group _Group;

        //public NetCms.Users.User User
        //{
        //    get
        //    {
        //        if (_User == null && this.IsUser)
        //            _User = new NetCms.Users.User(EntityID);
        //        return _User;
        //    }
        //}
        //private NetCms.Users.User _User;

        public virtual bool HasParent
        {
            get
            {
               return this.ParentsGroups.Count>0;
            }
        }

        public abstract ICollection<Group> ParentsGroups
        {
            get;
        }

        //public GroupsCollection ParentsGroups
        //{
        //    get
        //    {
        //        if (_ParentsGroups == null)
        //        {
        //            _ParentsGroups = new GroupsCollection();
        //            if (this.IsGroup)
        //            {
        //                if( Group.Parent != null)
        //                    _ParentsGroups.Add(Group.Parent);
        //            }
        //            else _ParentsGroups = this.User.Groups;
        //        }
        //        return _ParentsGroups;
        //    }
        //}
        //private GroupsCollection _ParentsGroups;

        //public int Depth
        //{
        //    get { return this.IsGroup ? this.Group.Depth : int.MaxValue; }
        //}

        public abstract int Depth
        {
            get;
        }
        
        //public string EntityID
        //{
        //    get
        //    {
        //        return _EntityID;
        //    }
        //}
        //private string _EntityID;

        public Profile(/*string id*/)
        {
            //this.ID = id;
            //DataTable table = Connection.SqlQuery("SELECT * FROM Profiles");
            //DataRow[] rows = table.Select("id_Profile = " + id);
            //this.Type = rows[0]["Type_Profile"].ToString() == "0" ? ProfileTypes.User : ProfileTypes.Group;
            //if (this.IsUser)
            //{
            //    table = Connection.SqlQuery("SELECT * FROM Users");
            //    rows = table.Select("Profile_User = " + id);
            //    _EntityID = rows[0]["id_User"].ToString();
            //}
            //else
            //{
            //    table = Connection.SqlQuery("SELECT * FROM Groups");
            //    rows = table.Select("Profile_Group = " + id);
            //    _EntityID = rows[0]["id_Group"].ToString();
            //}
        }

        public abstract string EntityLabel
        {
            get;
        }

        //private string _EntityLabel;
        //public string EntityLabel
        //{
        //    get
        //    {
        //        if (_EntityLabel == null)
        //        {
        //            if (this.IsGroup)
        //                _EntityLabel = "Il gruppo '" + this.Group.Name + "'";
        //            else
        //                _EntityLabel = "L'utente '" + this.User.NomeCompleto+ "'";

        //        }
        //        return _EntityLabel;
        //    }
        //}

        public abstract string EntityName
        {
            get;
        }

        //private string _EntityName;
        //public string EntityName
        //{
        //    get
        //    {
        //        if (_EntityName == null)
        //        {
        //            if (this.IsGroup)
        //                _EntityName = this.Group.Name;
        //            else
        //                _EntityName = this.User.NomeCompleto;

        //        }
        //        return _EntityName;
        //    }
        //}

        public override string ToString()
        {
            return this.ID.ToString();
        }
        
        public static Profile CurrentProfile 
        {
            get
            {
                if (NetCms.Users.AccountManager.CurrentAccount != null)
                    return NetCms.Users.AccountManager.CurrentAccount.Profile;
                else return null;
            }
        }
    }
}
