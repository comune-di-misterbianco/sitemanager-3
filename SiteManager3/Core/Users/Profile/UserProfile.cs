﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Users
{
    public class UserProfile : Profile
    {
        public UserProfile()
        { }

        public override int Depth
        {
            get 
            { 
                return int.MaxValue; 
            }
        }

        public override string EntityLabel
        {
            get 
            { 
                return "L'utente '" + this.User.NomeCompleto + "'"; 
            }
        }

        public override string EntityName
        {
            get 
            {
                return this.User.NomeCompleto;
            }
        }

        public override bool IsGroup
        {
            get { return false; }
        }

        public override bool IsUser
        {
            get { return true; }
        }

        public override ICollection<Group> ParentsGroups
        {
            get { return this.User.Groups; }
        }

        //public NetCms.Users.User User
        //{
        //    get
        //    {
        //        if (_User == null)
        //            _User = new NetCms.Users.User(EntityID);
        //        return _User;
        //    }
        //}
        //private NetCms.Users.User _User;

        public virtual User User
        {
            get;
            set;
        }
    }
}
