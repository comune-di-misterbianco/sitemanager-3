﻿using Newtonsoft.Json;
using NHibernate;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using Users.Cookie;
using Users.Models;
using Users.Models.Token;

namespace NetCms.Users.WebAPIAuthentication
{
    public class AuthController : ApiController
    {

        [HttpPost]
        [ActionName("authorize")]
        public IHttpActionResult Authorize([FromBody] JWTRequest jwtRequest)
        {
            if (string.IsNullOrEmpty(jwtRequest.AuthenticationToken) || string.IsNullOrEmpty(jwtRequest.UserInfo))
                return BadRequest();

            try
            {
                TokenResponse returnValue = null;

                string userId = "";
                string sessionId = null;
                HttpCookie encodedInputCookie = new HttpCookie("sharedCookie");
                encodedInputCookie.Domain = NetCms.Configurations.Generics.SsoSharedCookieDomain;
                encodedInputCookie.Value = jwtRequest.UserInfo;
                encodedInputCookie.Secure = true;
                encodedInputCookie.HttpOnly = false;
                encodedInputCookie.SameSite = SameSiteMode.Strict;

                var decodedIDCookie = HttpSecureCookie.Decode(encodedInputCookie);
                var values = JsonConvert.DeserializeObject<string[]>(decodedIDCookie.Value);
                if (values != null && values.Length > 0)
                {
                    userId = values.FirstOrDefault();
                    string sessionIdVal = values.LastOrDefault();

                    if(HttpContext.Current.Application["AccountSessionID_USER" + userId]!= null)
                    {
                        sessionId = HttpContext.Current.Application["AccountSessionID_USER" + userId].ToString();
                        if (sessionId == null || (sessionId != null && sessionId != sessionIdVal))
                            return Unauthorized();
                    }
                   
                }
                if (sessionId != null)
                {
                    var jwtCookie = HttpContext.Current.Application["JWT_" + sessionId];
                    if (jwtCookie != null)
                    {
                        HttpCookie encodedCookie = new HttpCookie("JWT_" + sessionId, jwtCookie.ToString());
                        var decodedCookie = HttpSecureCookie.Decode(encodedCookie);
                        returnValue = JsonConvert.DeserializeObject<TokenResponse>(decodedCookie.Value);

                        return Ok(returnValue);
                    }
                }
            }
            catch (Exception e)
            {
                return BadRequest();
            }
            return Unauthorized();
        }

        [HttpPost]
        [ActionName("token")]
        [AllowAnonymous]
        public IHttpActionResult Token([FromBody] TokenRequest tokenRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            try
            {
                TokenResponse token = TokenManager.GenerateAccessToken(tokenRequest);
                return Ok(token);
            }
            catch (AccessViolationException e)
            {
                Diagnostics.Diagnostics.TraceException(e, "", "", typeof(AuthController) + "/Token");
                return Unauthorized();
            }
            catch (ArgumentNullException e)
            {
                Diagnostics.Diagnostics.TraceException(e, "", "", typeof(AuthController) + "/Token");
                return NotFound();
            }
            catch (ArgumentException e)
            {
                Diagnostics.Diagnostics.TraceException(e, "", "", typeof(AuthController) + "/Token");
                return BadRequest(e.Message);
            }
            catch (InvalidOperationException e)
            {
                Diagnostics.Diagnostics.TraceException(e, "", "", typeof(AuthController) + "/Token");
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                Diagnostics.Diagnostics.TraceException(e, "", "", typeof(TokenManager));
                throw e;
            }
        }

        [HttpPost]
        [Route("refresh")]
        [NetCms.Users.Filters.AuthorizationBaseFilterAttribute(isRefresh = true)]
        public IHttpActionResult Refresh([FromBody] RefreshTokenRequest Input)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                TokenResponse refreshtoken = TokenManager.GenerateRefreshToken(Input);
                return Ok(refreshtoken);
            }
            catch (UnauthorizedAccessException ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(AuthController) + "/RefreshToken");
                return Unauthorized();
            }
            catch (AccessViolationException ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(AuthController) + "/RefreshToken");
                return Unauthorized();
            }
            catch (InvalidOperationException ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(AuthController) + "/RefreshToken");
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(AuthController) + "/RefreshToken");
                throw ex;
            }
        }

        
    }
}
