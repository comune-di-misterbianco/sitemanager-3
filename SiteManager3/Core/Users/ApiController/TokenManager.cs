﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Security;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NHibernate;
using Users.BusinessLogic;
using Users.Models.Client;
using Users.Models.Token;
using Users.Models.Token.UserTokens;

namespace NetCms.Users
{
    public class TokenManager
    {
        #region TOKEN CONFIG
        private static string TokenKey = Configurations.PortalData.TokenKey;
        private static string TokenAuthenticationIssuer = Configurations.PortalData.TokenAuthenticationIssuer;
        private static double ExpiryMinutes = Configurations.PortalData.ExpiryMinutes;
        private static string[] TokenAuthenticationAudience = Configurations.PortalData.TokenAuthenticationAudience;
        private static string LoginProvider = Configurations.PortalData.PortalAddress;
        #endregion

        public TokenManager()
        {
        }

        private static TokenResponse GenerateClientAuthResponse(Client targetClient)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(TokenKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var nowUtc = DateTime.Now.ToUniversalTime();
            var expires = nowUtc.AddMinutes(ExpiryMinutes).ToUniversalTime();

            List<Claim> claims = new List<Claim>();

            var jwt = new JwtSecurityToken(
                    TokenAuthenticationIssuer,
                    targetClient.ClientId,
                    claims,
                    null,
                    expires: /*expires*/ null,
                    signingCredentials: creds);

            TokenResponse response = new TokenResponse(
                new JwtSecurityTokenHandler().WriteToken(jwt),
                "Bearer",
                /*(int)(ExpiryMinutes * 60)*/ 0,
                ""
                );

            return response;
        }

        private static TokenResponse GenerateUserAuthResponse(string clientId, int userId, string clientclaim, string deviceCode = null)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(TokenKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var nowUtc = DateTime.Now.ToUniversalTime();
            var expires = nowUtc.AddMinutes(ExpiryMinutes).ToUniversalTime();


            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = session.BeginTransaction())
            {
                User targetUser = UsersBusinessLogic.GetById(userId, session);

                #region ROLE CLAIM
                var roles = GetUserGroup(targetUser).Split(',').ToArray();
                string[] clientclaims = JsonConvert.DeserializeObject<string[]>(clientclaim);
                bool rolesMatch = clientclaims.Any(x => roles.Any(y => y.Equals(x)));
                if (!rolesMatch)
                    throw new AccessViolationException("invalid_role");
                #endregion

                List<Claim> claims = new List<Claim>();

                List<Claim> audClaims = new List<Claim>();

                #region USER FIELDS
                claims.Add(new Claim(ClaimTypes.NameIdentifier, targetUser.ID.ToString()));
                claims.Add(new Claim(JwtRegisteredClaimNames.Sub, targetUser.UserName));
                #endregion

                #region AUDIENCES
                if (TokenAuthenticationAudience != null)
                {
                    foreach (var aud in TokenAuthenticationAudience)
                    {
                        if(! aud.Equals(clientId) )
                            audClaims.Add(new Claim(JwtRegisteredClaimNames.Aud, aud));
                    }
                }
                #endregion

                #region USER GROUPS
                // Get User roles and add them to claims
                List<Group> groups = GroupsBusinessLogic.FindGroupsAssociatedToUser(targetUser, session).ToList();
                foreach (var associatedGroup in groups)
                {
                    claims.Add(new Claim(ClaimTypes.Role, associatedGroup.Name));
                }
                #endregion

                var jwt = new JwtSecurityToken(
                                      TokenAuthenticationIssuer,
                                      clientId,
                                      (claims.ToArray().Union(audClaims)),
                                      null,
                                      expires: expires,
                                      signingCredentials: creds);

                #region Delete OLD REFRESH
                IList<UserToken> usertokens = UserTokenBusinessLogic.GetUserTokens(targetUser.ID, LoginProvider, "RefreshToken", session);
                if(usertokens != null && usertokens.Count > 0)
                {
                    int[] userTokenIds = usertokens.Select(t => t.Id).ToArray();
                    foreach(var utId in userTokenIds)
                        UserTokenBusinessLogic.Delete(utId, session);
                }
                #region CREATE NEW REFRESH TOKEN
                var refreshTokenId = Guid.NewGuid().ToString("n");
                
                var jwtRefreshToken = new JwtSecurityToken(
                                     TokenAuthenticationIssuer,
                                     clientId,
                                     (new Claim[] 
                                     { 
                                         new Claim("refreshTokenId", refreshTokenId)
                                     }.Union(audClaims) ),
                                     null,
                                     expires: null,
                                     signingCredentials: creds);
                #endregion
                var newRefreshToken = new JwtSecurityTokenHandler().WriteToken(jwtRefreshToken);
                var usertoken = new UserToken()
                {
                    LoginProvider = LoginProvider,
                    Name = "RefreshToken",
                    UserId = targetUser.ID,
                    Value = newRefreshToken
                };
                UserTokenBusinessLogic.SaveOrUpdate(usertoken, session);
                tx.Commit();
                #endregion

                TokenResponse response = new TokenResponse(
                       new JwtSecurityTokenHandler().WriteToken(jwt),
                       "password",
                       (int)(ExpiryMinutes * 60),
                       newRefreshToken
                       );

                return response;
            }
        }

        public static bool IsMD5(string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return false;
            }

            return Regex.IsMatch(input, "^[0-9a-fA-F]{32}$", RegexOptions.Compiled);
        }

        public static TokenResponse GenerateAccessToken
            (TokenRequest authRequest)
        {
            try
            {
                if (!authRequest.IsValidGrantType())
                    throw new AccessViolationException("grant_type"); //400    

                using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                {
                    Client targetClient = ClientBusinessLogic.GetByClientId(authRequest.ClientId, session);

                    if (targetClient == null)
                        throw new ArgumentNullException("ClientId"); //401
                    if (targetClient.Disabled)
                        throw new ArgumentException("DISABLED CLIENT ACCESS ATTEMPT");

                    if (targetClient.ClientSecret != authRequest.ClientSecret)
                        throw new ArgumentException("ClientSecret"); //401      

                    if (authRequest.GrantType.Equals("client_credentials"))
                    {
                        TokenResponse response = GenerateClientAuthResponse(targetClient);
                        return response;
                    }

                    else if (authRequest.GrantType.Equals("password"))
                    {
                        if (string.IsNullOrEmpty(authRequest.Username) || string.IsNullOrEmpty(authRequest.Password))
                            throw new InvalidOperationException("email_password"); //400
                        User user = null;
                        user = UsersBusinessLogic.GetUserByUserName(authRequest.Username, session);
                        if (user == null)
                            throw new ArgumentNullException("user_not_found"); //404

                        string md5PWD = authRequest.Password;
                        if(!IsMD5(authRequest.Password))
                        {
                            md5PWD = /*FormsAuthentication.HashPasswordForStoringInConfigFile(user.Password, "MD5").ToString()*/
                                MD5Hash(authRequest.Password);
                        }

                        //if (!CheckCredential(user.UserName, md5PWD, out user))
                        if(!string.IsNullOrEmpty(user.NuovaPassword)&& !user.NuovaPassword.Equals(md5PWD))
                        {
                            throw new AccessViolationException("Unable to login user. Wrong password.");
                        }
                        else if (string.IsNullOrEmpty(user.NuovaPassword)&&!user.Password.Equals(md5PWD))                            
                            throw new AccessViolationException("Unable to login user. Wrong password.");
                        else
                        {
                            ClientClaim clientclaim = ClientClaimBusinessLogic.GetByClient(targetClient.ClientId, ClientClaim.ClientClaimRole, session);
                            if (clientclaim == null)
                                throw new AccessViolationException("No roles matched: check on \"clientclaims\" table for correct claimvalue fields and authorized group.");
                            TokenResponse response = GenerateUserAuthResponse(targetClient.ClientId, user.ID, clientclaim.ClaimValue /*authRequest.DeviceCode*/);
                            return response;
                        }
                    }
                    else
                        throw new InvalidOperationException("400", new Exception("No action for GrantType required"));
                }
            }
            catch (Exception e)
            {
                Diagnostics.Diagnostics.TraceException(e, "", "", typeof(TokenManager));
                throw e;
            }
        }

        public static TokenResponse GenerateRefreshToken(RefreshTokenRequest refreshRequest)
        {
            try
            {
                using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = session.BeginTransaction())
                {
                    var jwtold = new JwtSecurityToken(refreshRequest.AccessToken);
                    int userId;
                    if (!Int32.TryParse(jwtold.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).FirstOrDefault().Value, out userId))
                        throw new InvalidOperationException("userId");
                    User targetUser = UsersBusinessLogic.GetById(userId, session);

                    #region USER and CLIENT Roles
                    //string ClientId = jwtold.Audiences.FirstOrDefault();
                    string ClientId = jwtold.Audiences.Where(a => a.Equals(refreshRequest.ClientId)).FirstOrDefault();
                    if (string.IsNullOrEmpty(ClientId))
                        throw new AccessViolationException("invalid_client");
                    ClientClaim clientclaim = ClientClaimBusinessLogic.GetByClient(ClientId, ClientClaim.ClientClaimRole, session);

                    if (clientclaim == null)
                        throw new AccessViolationException("invalid_role");

                    List<Group> groups = GroupsBusinessLogic.FindGroupsAssociatedToUser(targetUser, session).ToList();
                    string[] clientclaims = JsonConvert.DeserializeObject<string[]>(clientclaim.ClaimValue);
                    bool rolesMatch = clientclaims.Any(x => groups.Select(g => g.Name).Any(y => y.Equals(x)));
                    if (!rolesMatch)
                        throw new AccessViolationException("invalid_role");
                    #endregion

                    var userToken = UserTokenBusinessLogic.GetUserTokens(targetUser.ID, LoginProvider, "RefreshToken", session).FirstOrDefault();
                    if (userToken == null)
                        throw new InvalidOperationException("userToken");
                    string refreshToken = userToken.Value;

                    var jwtRefreshToken = new JwtSecurityToken(refreshToken);
                    var auds = jwtRefreshToken.Claims.Where(c => c.Type == JwtRegisteredClaimNames.Aud).Select(c => c.Value).ToList();
                    if (!auds.Contains(ClientId) || !refreshToken.Equals(refreshRequest.RefreshToken) )
                        throw new InvalidOperationException("clientId");

                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(TokenKey));
                    var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                    var nowUtc = DateTime.Now.ToUniversalTime();
                    var expires = nowUtc.AddMinutes(ExpiryMinutes).ToUniversalTime();
                    
                    var claims = jwtold.Claims;

                    #region REFRESH JWT
                    var refreshTokenId = Guid.NewGuid().ToString("n");

                    jwtRefreshToken = new JwtSecurityToken(
                                         TokenAuthenticationIssuer,
                                         ClientId,
                                         (new Claim[]
                                         {
                                         new Claim("refreshTokenId", refreshTokenId),
                                         }.Union(jwtRefreshToken.Claims.Where(c => c.Type == JwtRegisteredClaimNames.Aud) )),
                                         null,
                                         expires: null,
                                         signingCredentials: creds);
                    var newRefreshToken = new JwtSecurityTokenHandler().WriteToken(jwtRefreshToken);
                    userToken = new UserToken()
                    {
                        Id = userToken.Id,
                        LoginProvider = LoginProvider,
                        Name = "RefreshToken",
                        UserId = targetUser.ID,
                        Value = newRefreshToken
                    };
                    UserTokenBusinessLogic.SaveOrUpdate(userToken, session);
                    tx.Commit();
                    #endregion

                    #region JWT                     
                    var jwt = new JwtSecurityToken(
                                             TokenAuthenticationIssuer,
                                             refreshRequest.ClientId,
                                             claims,
                                             null,
                                             expires: expires,
                                             signingCredentials: creds);

                    TokenResponse _accessToken = new TokenResponse(
                        new JwtSecurityTokenHandler().WriteToken(jwt),
                        "password",
                        (int)(ExpiryMinutes * 60),
                        newRefreshToken
                        );
                    return _accessToken;
                    #endregion
                }
            }
            catch (AccessViolationException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
            throw new UnauthorizedAccessException();
        }

        
        private static string GetClientId(string token)
        {
            string[] TokenArray = token.Split(new string[] { "Bearer " }, StringSplitOptions.None);
            string Token = TokenArray[1];
            string ClientId = new System.IdentityModel.Tokens.Jwt.JwtSecurityToken(Token).Audiences.FirstOrDefault();
            return ClientId;
        }
        /// <summary>
        /// Validazione del token e controllo dei claims
        /// </summary>
        /// <param name="token">Token in formato jwt</param>
        /// <param name="roles">Ruoli passati dal controller all'authorization filter</param>
        /// <param name="anagraficaName">Nomi delle anagrafiche passati dal controller all'authorization filter</param>
        /// <param name="principal">Parametro ClaimsPrincipal passato come output contenente le info delle utente</param>
        /// <returns>bool</returns>
        public static bool ValidateToken(string token, out ClaimsPrincipal principal, string[] roles = null, bool isRefresh = false)
        {
            bool status = false;
            principal = null;
            try
            {
                TokenValidationParameters tokenValidationParameters = new TokenValidationParameters() {                    
                    ValidIssuer = TokenAuthenticationIssuer,
                    ValidAudiences = TokenAuthenticationAudience,
                    ValidateLifetime = !isRefresh ? true : false,
                    RequireExpirationTime = !isRefresh ? true : false,                    
                    IssuerSigningKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.Default.GetBytes(TokenKey)),
                };

                SecurityToken sToken = null;
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
               
                principal = handler.ValidateToken(token, tokenValidationParameters, out sToken);

                using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                {
                    NetCms.Users.User targetUser = null;
                    if (!string.IsNullOrEmpty(principal.Identity.Name))
                    {
                        string username = principal.Identity.Name;
                       targetUser = NetCms.Users.UsersBusinessLogic.GetActiveUserByUserName(username, session);
                    }
                    else
                    {                        
                        int userId;
                        if (!Int32.TryParse(principal.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).FirstOrDefault().Value, out userId))
                            throw new InvalidOperationException("userId");
                        targetUser = UsersBusinessLogic.GetById(userId, session);
                    }
                   
                    if (targetUser != null)
                    {
                        // verificare se i custom claim corrispondono
                        var RoleClaims = principal.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).ToList();                       
                        var userGroups = GroupsBusinessLogic.FindGroupsAssociatedToUser(targetUser, session).Select(g => g.Name).ToList();
                        if (RoleClaims.All(userGroups.Contains) && RoleClaims.Count == userGroups.Count)
                        {
                            if(roles != null)
                            {
                                bool[] rolesPresent = (from role in roles
                                                       select (RoleClaims.Contains(role) ? true : false)).ToArray();
                                if (rolesPresent.Contains(true))
                                    status = true;
                                else
                                    throw new AccessViolationException();
                            }
                            else
                                status = true;
                        }
                    }
                }

            }
            catch(System.ArgumentException ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(TokenManager));
            }
            catch (SecurityTokenExpiredException ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(TokenManager));
            }
            catch (SecurityTokenInvalidIssuerException ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(TokenManager));
            }
            catch (SecurityTokenInvalidAudienceException ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(TokenManager));
            }
            catch (SecurityTokenInvalidSignatureException ex)             
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(TokenManager));
            }

            return status;
        }

        
        /// <summary>
        /// Validazione del token solo in modalità client_credential e controllo dei claims
        /// </summary>
        /// <param name="token">Token in formato jwt</param>       
        /// <param name="anagraficaName">Nomi delle anagrafiche passati dal controller all'authorization filter</param>
        /// <param name="principal">Parametro ClaimsPrincipal passato come output contenente le info delle utente</param>
        /// <returns>bool</returns>
        public static bool ValidateClientCredentialToken(string token, out ClaimsPrincipal principal, bool isRefresh = false)
        {
            bool status = false;
            principal = null;
            try
            {
                TokenValidationParameters tokenValidationParameters = new TokenValidationParameters()
                {
                    ValidIssuer = TokenAuthenticationIssuer,
                    ValidAudiences = TokenAuthenticationAudience,
                    ValidateLifetime = false,
                    RequireExpirationTime = false,
                    IssuerSigningKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.Default.GetBytes(TokenKey)),
                };

                SecurityToken sToken = null;
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
                handler.TokenLifetimeInMinutes = 60;
                principal = handler.ValidateToken(token, tokenValidationParameters, out sToken);
                if (!string.IsNullOrEmpty(principal.Identity.AuthenticationType))
                {
                    if (principal.Identity.IsAuthenticated)
                        status = true;
                    else
                        throw new AccessViolationException();
                }

            }
            catch (System.ArgumentException ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(TokenManager));
            }
            catch (SecurityTokenExpiredException ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(TokenManager));
            }
            catch (SecurityTokenInvalidIssuerException ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(TokenManager));
            }
            catch (SecurityTokenInvalidAudienceException ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(TokenManager));
            }
            catch (SecurityTokenInvalidSignatureException ex)
            {
                Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(TokenManager));
            }

            return status;
        }
        private static string GetUserGroup(User user)
        {
            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                List<Group> group = GroupsBusinessLogic.FindGroupsAssociatedToUser(user, session).ToList();
                return string.Join(",", group.Select(o => o.Name));
            }
        }

        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString().ToUpper();
        }

        private static bool CheckCredential(string username, string MD5Pwd, out User user)
        {
            bool status = false;
            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            {
                //string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();
                user = NetCms.Users.UsersBusinessLogic.GetActiveUserByUserNameAndPassword(username, MD5Pwd, session);

                if (user != null)
                    status = true;
            }
            return status;
        }
    }
   


}
