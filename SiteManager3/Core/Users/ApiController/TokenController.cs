﻿//using NHibernate;
//using System;
//using System.Collections.Generic;
//using System.IdentityModel.Tokens.Jwt;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Web.Http;
//using System.Web.Security;
//using Users.Models;

//namespace NetCms.Users.WebAPIAuthentication
//{
//    public class TokenController : ApiController
//    {


//        [HttpPost]
//        [ActionName("Token")]
//        [AllowAnonymous]
//        public IHttpActionResult GetToken([FromBody] TokenRequest tokenRequest)
//        {
//            if (!ModelState.IsValid)
//                return BadRequest();

//            User user = null;

//            if (!CheckCredential(tokenRequest.Username, tokenRequest.Password, out user))
//                return Unauthorized();

//            var token = TokenManager.GenerateToken(user);

//            return Ok(new {token = new JwtSecurityTokenHandler().WriteToken(token), expiration = token.ValidTo  } );
//        }


//        private bool CheckCredential(string username, string password, out User user)
//        {
//            bool status = false;
//            using (ISession session = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
//            {
//                string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToString();
//                user = NetCms.Users.UsersBusinessLogic.GetActiveUserByUserNameAndPassword(username, MD5Pwd, session);
              
//                if (user != null)
//                    status = true;
//            }
//            return status;
//        }
//    }
//}
