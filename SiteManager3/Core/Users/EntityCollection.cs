//using System;
//using System.Collections;
///// <summary>
///// Summary description for xNodeCollection
///// </summary>

//namespace NetCms.Users
//{
//    public class EntityCollection : IEnumerable
//    {
//        private G2Collection coll;

//        public int Count { get { return coll.Count; } }

//        public EntityCollection()
//        {
//            coll = new G2Collection();
//        }

//        public void Add(Entity entity)
//        {
//            coll.Add(entity.Profile.ToString(), entity);
//        }

//        public Entity this[int i]
//        {
//            get
//            {
//                Entity value = (Entity)coll[coll.Keys[i]];
//                return value;
//            }
//        }

//        public Entity this[string key]
//        {
//            get
//            {
//                Entity field = (Entity)coll[key];
//                return field;
//            }
//        }
//    #region Enumerator

//    public IEnumerator GetEnumerator()
//    {
//        return new CollectionEnumerator(this);
//    }

//    private class CollectionEnumerator : IEnumerator
//    {
//        private int CurentPos = -1;
//        private EntityCollection Collection;
//        public CollectionEnumerator(EntityCollection coll)
//        {
//            Collection = coll;
//        }
//        public object Current
//        {
//            get
//            {
//                return Collection[CurentPos];
//            }
//        }
//        public bool MoveNext()
//        {
//            if (CurentPos < Collection.Count - 1)
//            {
//                CurentPos++;
//                return true;
//            }
//            else
//                return false;
//        }
//        public void Reset()
//        {
//            CurentPos = -1;
//        }
//    }
//    #endregion
//    }
//}