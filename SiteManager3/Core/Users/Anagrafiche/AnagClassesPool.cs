//using System;
//using System.Linq;
//using System.Web;
//using System.Collections.Generic;
//using System.Reflection;
//using NetCms.DBSessionProvider;

///// <summary>
///// Summary description for StructureGrants
///// </summary>

//namespace NetCms.Users.Anagraphics
//{
//    public static class AnagraphicsClassesPool 
//    {
//        private const string AnagraphicsClassesKey = "NetCms.Users.Anagraphics.AnagraphicsClassesPool";

//        public static AssembliesCollection Assemblies
//        {
//            get
//            {
//                if (_Assemblies == null)
//                    _Assemblies = new AssembliesCollection();

//                return _Assemblies;
//            }
//        }
//        private static AssembliesCollection _Assemblies;

//        public static Dictionary<string, Type> Anagrafiche
//        {
//            get
//            {
//                if (_Anagrafiche == null)
//                    _Anagrafiche = new Dictionary<string, Type>();

//                return _Anagrafiche;
//            }
//        }
//        private static Dictionary<string, Type> _Anagrafiche;

//        public static Dictionary<string, Type> AnagraficheSearchs
//        {
//            get
//            {
//                if (_AnagraficheSearchs == null)
//                    _AnagraficheSearchs = new Dictionary<string, Type>();

//                return _AnagraficheSearchs;
//            }
//        }
//        private static Dictionary<string, Type> _AnagraficheSearchs;

//        static AnagraphicsClassesPool()
//        {
//            Switches.Add(new MainAnagraficaSwitch());
//        }

//        public static List<IAnagraficaSwitch> Switches
//        {
//            get
//            {
//                return _Switches ?? (_Switches = new List<IAnagraficaSwitch>());
//            }
//        }
//        private static List<IAnagraficaSwitch> _Switches;

//        public static NetCms.Users.AnagraficaBase BuildInstanceOf(string anagraficaID, string anagraficaTypeID)
//        {
//            if(Switches.Where(s => s.ContainsAnagrafica(anagraficaTypeID)).Any())
//            {
//                Type type = Switches.FirstOrDefault(s => s.ContainsAnagrafica(anagraficaTypeID)).GetAnagrafica(anagraficaTypeID);
//                object[] parameters = { anagraficaID };
//                return (NetCms.Users.AnagraficaBase)System.Activator.CreateInstance(type, parameters);
//            }
//            return null;
//        }

//        public static void ParseType(Type type, Assembly asm)
//        {
//            if (typeof(NetCms.Users.AnagraficaBase).IsAssignableFrom(type))
//            {
//                Assemblies.Add(type.FullName, asm);
//                Anagrafiche.Add(type.FullName, type);
//                //FluentSessionProvider.FluentAssemblies.Add(asm);
//            }
//            if (typeof(NetCms.Users.AbstractAnagraficaSearch).IsAssignableFrom(type) && !type.IsAbstract)
//            {
//                AnagraficheSearchs.Add(type.FullName, type);
//            }
//        }
//    }

//    public class MainAnagraficaSwitch : IAnagraficaSwitch
//    {
//        public bool ContainsAnagrafica(string anagraficaTypeName)
//        {
//            return AnagraphicsClassesPool.Anagrafiche.ContainsKey(anagraficaTypeName);
//        }
//        public Type GetAnagrafica(string anagraficaTypeName)
//        {
//            return AnagraphicsClassesPool.Anagrafiche[anagraficaTypeName];
//        }
//    }

//    public interface IAnagraficaSwitch
//    {
//        Type GetAnagrafica(string anagraficaTypeID);
//        bool ContainsAnagrafica(string anagraficaTypeID);
//    }
//}