﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using AutoMapper;

namespace NetCms.Users
{
    public class AllegatoUtente
    {
        public AllegatoUtente()
        {

        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string FileName
        {
            get;
            set;
        }

        public virtual string ContentType
        {
            get;
            set;
        }

        public virtual byte[] File
        {
            get;
            set;
        }

        public virtual Hashtable AnagraficaMapped
        {
            get;
            set;
        }

        public virtual AllegatoUtente Clone()
        {
            //  Mapper.Reset();
            //Mapper.CreateMap<AllegatoUtente, AllegatoUtente>().ForMember(d => d.ID, o => o.Ignore())
            //    .ForMember(d => d.AnagraficaMapped, o => o.Ignore());

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AllegatoUtente, AllegatoUtente>().ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.AnagraficaMapped, o => o.Ignore());
            });
            config.AssertConfigurationIsValid();
            var newAllegato = new AllegatoUtente();
            
            //Mapper.Map(this, newAllegato);
            var mapper = config.CreateMapper();
            mapper.Map(this, newAllegato);

            return newAllegato;
        }
    }
}
