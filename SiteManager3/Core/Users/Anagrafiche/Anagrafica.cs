//using System;
//using System.Data;
//using System.Data.Common;
//using System.Configuration;
//using System.Web;
//using System.Collections.Generic;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;
//using NetCms.Connections;
//using G2Core.AdvancedXhtmlForms;
//using G2Core.AdvancedXhtmlForms.Fields;
//using G2Core.AdvancedXhtmlForms.Fields.Reference;
//using NetCms.Users.Search;
//using NetForms;

///// <summary>
///// Summary description for StructureGrants
///// </summary>

//namespace NetCms.Users 
//{
//    public class Anagrafica : AnagraficaBase
//    {
//        public override string NomeCompleto 
//        {
//            get
//            {
//                return this.Nome + " " + this.Cognome;
//            }
//        }

//        public string Nome
//        {
//            get
//            {
//                if (_Nome == null)
//                {
//                    _Nome = Data["Nome_Anagrafica"].ToString();
//                }
//                return _Nome;
//            }
//        }
//        private string _Nome;

//        public string Cognome
//        {
//            get
//            {
//                if (_Cognome == null)
//                {
//                    _Cognome = Data["Cognome_Anagrafica"].ToString();
//                }
//                return _Cognome;
//            }
//        }
//        private string _Cognome;

//        public override string Citt�
//        {
//            get
//            {
//                if (_Citt� == null)
//                {
//                    _Citt� = Data["Citta_Anagrafica"].ToString();
//                }
//                return _Citt�;
//            }
//        }
//        private string _Citt�;

//        public override string Indirizzo
//        {
//            get
//            {
//                if (_Indirizzo == null)
//                {
//                    _Indirizzo = Data["Indirizzo_Anagrafica"].ToString();
//                }
//                return _Indirizzo;
//            }
//        }
//        private string _Indirizzo;

//        public override string Telefono
//        {
//            get
//            {
//                if (_Telefono == null)
//                {
//                    _Telefono = Data["Telefono_Anagrafica"].ToString();
//                }
//                return _Telefono;
//            }
//        }
//        private string _Telefono;

//        public string Cellulare
//        {
//            get
//            {
//                if (_Cellulare == null)
//                {
//                    _Cellulare = Data["Cellulare_Anagrafica"].ToString();
//                }
//                return _Cellulare;
//            }
//        }
//        private string _Cellulare;

//        public override string CAP
//        {
//            get
//            {
//                if (_CAP == null)
//                {
//                    _CAP = Data["CAP_Anagrafica"].ToString();
//                }
//                return _CAP;
//            }
//        }
//        private string _CAP;

//        public string CodiceFiscale
//        {
//            get
//            {
//                if (_CodiceFiscale == null)
//                {
//                    _CodiceFiscale = Data["CodiceFiscale_Anagrafica"].ToString();
//                }
//                return _CodiceFiscale;
//            }
//        }
//        private string _CodiceFiscale;

//        public override string Codice
//        {
//            get
//            {
//                return CodiceFiscale;
//            }
//        }

//        public string DataNascita
//        {
//            get
//            {
//                if (_DataNascita == null)
//                {
//                    _DataNascita = Data["Data_Anagrafica"].ToString();
//                    if (_DataNascita.Length > 10)
//                        _DataNascita = _DataNascita.Remove(10);
//                }
//                return _DataNascita;
//            }
//        }
//        private string _DataNascita;

//        public string Azienda
//        {
//            get
//            {
//                if (_Azienda == null)
//                {
//                    _Azienda = Data["Azienda_Anagrafica"].ToString();
//                }
//                return _Azienda;
//            }
//        }
//        private string _Azienda;

//        public string Sesso
//        {
//            get
//            {
//                if (_Sesso == null)
//                {
//                    _Sesso = Data["Sesso_Anagrafica"].ToString() == "1" ? "Femmina" : "Maschio";
//                }
//                return _Sesso;
//            }
//        }
//        private string _Sesso;

//        public string Provincia
//        {
//            get
//            {
//                if (_Provincia == null)
//                {
//                    int pid;
//                    if (int.TryParse(Data["Provincia_Anagrafica"].ToString(), out pid))
//                    {
//                        DataTable table = Conn.SqlQuery("SELECT * FROM Provincie WHERE id_Provincia= " + pid);
//                        if (table.Rows.Count > 0)
//                        {
//                            _Provincia = table.Rows[0]["Nome_Provincia"].ToString();
//                        }
//                    }
//                }
//                return _Provincia;
//            }
//        }
//        private string _Provincia;

//        public override string Email
//        {
//            get
//            {
//                if (_Email == null)
//                {
//                    _Email = Data["Email_Anagrafica"].ToString();
//                }
//                return _Email;
//            }
//        }
//        private string _Email;

//        private NetCms.Connections.Connection _Connection;
//        private NetCms.Connections.Connection Conn
//        {
//            get
//            {
//                if (_Connection == null)
//                    _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
//                return _Connection;
//            }
//        }

//        public override DataRow Data
//        {
//            get {
//                if (_Data == null)
//                {
//                    DataTable table = Conn.SqlQuery("SELECT * FROM Anagrafica WHERE id_Anagrafica = " + ID);
//                    if (table.Rows.Count > 0)
//                    {
//                        _Data = table.Rows[0];
//                    }
//                }
//                return _Data;
//            }
//        }
//        private DataRow _Data;
        
//        //public DataRow Data
//        //{
//        //    get
//        //    {
//        //        if (_Data == null)
//        //        {
//        //            DataTable table = Conn.SqlQuery("SELECT * FROM Anagrafica WHERE id_Anagrafica = " + ID);
//        //            if (table.Rows.Count > 0)
//        //            {
//        //                _Data = table.Rows[0];
//        //            }
//        //        }
//        //        return _Data;
//        //    }
//        //}

//        public override bool Exist
//        {
//            get { return Data != null; }
//        }

//        public Anagrafica():base() { }

//        public Anagrafica(string id)
//            : base(id)
//        {
//        }

//        private List<NetCms.Users.AnagraficaDetail> _DetailsCollection;
//        public override List<NetCms.Users.AnagraficaDetail> Details
//        {
//            get
//            {
//                if (_DetailsCollection == null)
//                {
//                    _DetailsCollection = new List<NetCms.Users.AnagraficaDetail>();

//                    _DetailsCollection.Add(new AnagraficaDetail("Nome", this.Nome));
//                    _DetailsCollection.Add(new AnagraficaDetail("Cognome", this.Cognome));
//                    _DetailsCollection.Add(new AnagraficaDetail("Codice fiscale", this.CodiceFiscale));
//                    _DetailsCollection.Add(new AnagraficaDetail("Email", this.Email));
//                    _DetailsCollection.Add(new AnagraficaDetail("Data di Nascita", this.DataNascita));
//                    _DetailsCollection.Add(new AnagraficaDetail("Sesso", this.Sesso));

//                    _DetailsCollection.Add(new AnagraficaDetail("Citt�", this.Citt�));
//                    _DetailsCollection.Add(new AnagraficaDetail("Indirizzo", this.Indirizzo));
//                    _DetailsCollection.Add(new AnagraficaDetail("CAP", this.CAP));
//                    _DetailsCollection.Add(new AnagraficaDetail("Provincia", this.Provincia));

//                    _DetailsCollection.Add(new AnagraficaDetail("Telefono", this.Telefono));
//                    _DetailsCollection.Add(new AnagraficaDetail("Cellulare", this.Cellulare));
//                    _DetailsCollection.Add(new AnagraficaDetail("Azienda", this.Azienda));
//                }

//                return _DetailsCollection;
//            }
//        }

//        public override bool? NonLegalCodeDeclared
//        {
//            get { return null; }
//        }

//        public override bool ValidateCF(G2Core.AdvancedXhtmlForms.AxfGenericTable anagrafeTable)
//        {
//            return true;
//        }

//        public override WebControl GetProfileDetails(StateBag _viewstate, bool _IsPostBack)
//        {
//            //WebControl field = new WebControl(HtmlTextWriterTag.Fieldset);
//            //field.CssClass = "NetCms_profile";

//            //HtmlGenericControl legend = new HtmlGenericControl("legend");
//            //legend.InnerHtml = "Dettaglio profilo";
//            //field.Controls.Add(legend);

//            //HtmlGenericControl user_detail = new HtmlGenericControl("ul");

//            //foreach (AnagraficaDetail Detail in this.Details)
//            //{
//            //    HtmlGenericControl li_detail = new HtmlGenericControl("li");
//            //    li_detail.InnerHtml = Detail.Title + ": " + Detail.Value;
//            //    user_detail.Controls.Add(li_detail);
//            //}

//            //field.Controls.Add(user_detail);

//            //return field;

//            WebControl div = new WebControl(HtmlTextWriterTag.Div);
//            div.CssClass = "Details";

//            WebControl field = new WebControl(HtmlTextWriterTag.Fieldset);
//            field.CssClass = "NetCms_profile";

//            HtmlGenericControl legend = new HtmlGenericControl("legend");
//            legend.InnerHtml = "Dettaglio profilo";
//            field.Controls.Add(legend);

//            HtmlGenericControl user_detail = new HtmlGenericControl("table");
//            user_detail.Attributes["class"] = "DetailsContainer";
//            bool alternate = false;
//            foreach (NetCms.Users.AnagraficaDetail Detail in this.Details)
//            {
//                HtmlGenericControl row_detail = new HtmlGenericControl("tr");
//                if (!alternate)
//                    row_detail.Attributes["class"] = " DetailsRow DetailsRowNormal";
//                else
//                    row_detail.Attributes["class"] = " DetailsRow DetailsRowAlternate";

//                HtmlGenericControl row_label = new HtmlGenericControl("td");
//                row_label.Attributes["class"] = "DetailsLabel";
//                row_label.InnerHtml = "<strong>" + Detail.Title + ": " + "</strong>";

//                HtmlGenericControl row_value = new HtmlGenericControl("td");
//                row_value.InnerHtml = Detail.Value;

//                row_detail.Controls.Add(row_label);
//                row_detail.Controls.Add(row_value);

//                user_detail.Controls.Add(row_detail);

//                if (!alternate)
//                    alternate = true;
//                else
//                    alternate = false;
//            }

//            field.Controls.Add(user_detail);

//            div.Controls.Add(field);
//            return div;
//        }

//        public override WebControl GetProfileForm(StateBag _viewstate, bool _IsPostBack)
//        {
//            string oldemail = "";
//            string newemail = "";
//            bool sendupdateemailconfirmation = false;

//            WebControl field = new WebControl(HtmlTextWriterTag.Fieldset);
//            field.CssClass = "NetCms_profileForm";

//            HtmlGenericControl legend = new HtmlGenericControl("legend");
//            legend.InnerHtml = "Modifica profilo";
//            field.Controls.Add(legend);

//            NetForms.NetForm MyAnagraficaForm = new NetForms.NetForm();

//            NetForms.NetFormTable anagrafica = new NetForms.NetFormTable("Anagrafica", "id_Anagrafica");
            
//            NetForms.NetTextBoxPlus nome = new NetForms.NetTextBoxPlus("Nome", "Nome_Anagrafica");
//            nome.Size = 60;
//            nome.Required = true;
//            nome.Value = Nome;           

//            NetForms.NetTextBoxPlus cognome = new NetForms.NetTextBoxPlus("Cognome", "Cognome_Anagrafica");
//            cognome.Size = 60;
//            cognome.Required = true;
//            cognome.Value = Cognome;

//            NetForms.NetTextBoxPlus codicefiscale = new NetForms.NetTextBoxPlus("Email", "CodiceFiscale_Anagrafica", NetForms.NetTextBoxPlus.ContentTypes.NormalText);
//            codicefiscale.Size = 16;
//            codicefiscale.MaxLenght = 16;
//            codicefiscale.Value = CodiceFiscale;

//            NetForms.NetTextBoxPlus email = new NetForms.NetTextBoxPlus("Email", "Email_Anagrafica", NetForms.NetTextBoxPlus.ContentTypes.eMail);
//            email.Size = 60;
//            email.Required = true;
//            email.Value = Email;
//            oldemail = Email;

//            NetForms.NetTextBoxPlus cap = new NetForms.NetTextBoxPlus("CAP", "Cap_Anagrafica");
//            cap.Size = 60;
//            cap.Value = CAP;
//            cap.MaxLenght = 5;            

//            NetForms.NetTextBoxPlus telefono = new NetForms.NetTextBoxPlus("Telefono", "Telefono_Anagrafica");
//            telefono.Size = 12;
//            telefono.Value = Telefono;
//            //citta.Required = true;
//            telefono.MaxLenght = 12;
//            telefono.ContentType = NetForms.NetTextBoxPlus.ContentTypes.Phone;           

//            NetForms.NetTextBoxPlus cellulare = new NetForms.NetTextBoxPlus("Cellulare", "Cellulare_Anagrafica");
//            cellulare.Size = 12;
//            //citta.Required = true;            
//            cellulare.MaxLenght = 12;
//            cellulare.ContentType = NetForms.NetTextBoxPlus.ContentTypes.Phone;
//            cellulare.Value = this.Cellulare;

//            NetForms.NetTextBoxPlus azieda = new NetForms.NetTextBoxPlus("Cellulare", "Azienda_Anagrafica");
//            cellulare.Value = this.Azienda;

//            NetForms.NetDateField data = new NetForms.NetDateField("Data di Nascita", "Data_Anagrafica", Conn);
//            data.Value = this.Azienda;

//            NetForms.NetTextBoxPlus citta = new NetForms.NetTextBoxPlus("Citt�", "Citta_Anagrafica");
//            citta.Value = this.Citt�;

//            NetForms.NetTextBoxPlus indirizzo = new NetForms.NetTextBoxPlus("Indirizzo", "Indirizzo_Anagrafica");
//            indirizzo.Value = this.Indirizzo;

//            NetForms.NetDropDownList provincia = new NetDropDownList("Provincia", "Provincia_Anagrafica");
//            provincia.ClearList();
//            provincia.addItem("", "0");
//            provincia.setDataBind("SELECT Nome_Provincia,id_Provincia FROM Provincie ORDER BY Nome_Provincia", Conn);
//            //citta.Required = true;
//            anagrafica.addField(provincia);
            
//            NetForms.NetDropDownList sesso = new NetDropDownList("Sesso", "Sesso_Anagrafica");
//            sesso.addItem("M", "0");
//            sesso.addItem("F", "1");
//            sesso.Required = true;
//            anagrafica.addField(sesso);

//            #region MyRegion
//            //NetForms.NetTextBoxPlus codicefiscale = new NetForms.NetTextBoxPlus("CodiceFiscale", "CodiceFiscale_Anagrafica");
//            //codicefiscale.Size = 16;
//            //codicefiscale.Value = NetCms.Users.AccountManager.CurrentAccount.Anagrafica.Codice;
//            //codicefiscale.MaxLenght = 16;            
//            //anagrafica.addField(codicefiscale);

//            //NetForms.NetDropDownList sesso = new NetForms.NetDropDownList("Sesso", "Sesso_Anagrafica");
//            //sesso.addItem("M", "0");
//            //sesso.addItem("F", "1");
//            //sesso.Required = true;
//            //sesso.Value = NetCms.Users.AccountManager.CurrentAccount.Anagrafica.s
//            //anagrafica.addField(sesso);

//            //NetForms.NetTextBoxPlus azienda = new NetForms.NetTextBoxPlus("Azienda/Ufficio", "Azienda_Anagrafica");
//            //codicefiscale.Size = 60;
//            //anagrafica.addField(azienda); 
//            #endregion

//            anagrafica.addField(nome);
//            anagrafica.addField(cognome);
//            anagrafica.addField(email);
//            anagrafica.addField(sesso);
//            anagrafica.addField(codicefiscale);
//            anagrafica.addField(indirizzo);
//            anagrafica.addField(citta);
//            anagrafica.addField(cap);
//            anagrafica.addField(provincia);
//            anagrafica.addField(telefono);
//            anagrafica.addField(cellulare);
//            anagrafica.addField(azieda);

//            MyAnagraficaForm.AddTable(anagrafica);

//            NetUtility.RequestVariable btn_action = new NetUtility.RequestVariable("action", NetUtility.RequestVariable.RequestType.Form);

//            if (_IsPostBack && btn_action.StringValue == "Modifica")
//            {
//                string error = MyAnagraficaForm.serializedValidation();
//                if (error == null)
//                {
//                    newemail = email.RequestVariable.StringValue;
//                    if (string.Compare(oldemail, newemail, true) != 0)
//                    {                       
//                        sendupdateemailconfirmation = true;
//                    }
//                    // eseguire update dei campi del form
//                    string sql = "UPDATE anagrafica "
//                               + "SET nome_anagrafica = " + nome.validateValue(nome.RequestVariable.StringValue) + ", "
//                               + " cognome_anagrafica = " + cognome.validateValue(cognome.RequestVariable.StringValue) + ", "
//                               //+ " email_anagrafica = " + email.validateValue(email.RequestVariable.StringValue) + ", "
//                               + " CAP_anagrafica = " + cap.validateValue(cap.RequestVariable.StringValue) + ", "
//                               + " telefono_anagrafica = " + telefono.validateValue(telefono.RequestVariable.StringValue) + ", "
//                               + " cellulare_anagrafica = " + cellulare.validateValue(cellulare.RequestVariable.StringValue) + ""
//                               + " WHERE id_anagrafica = " + NetCms.Users.AccountManager.CurrentAccount.Anagrafica.ID;
//                    Conn.Execute(sql);
//                    NetCms.Users.AccountManager.UpdateCurrentUser();                   

//                    HtmlGenericControl div_info = new HtmlGenericControl("div");
//                    div_info.Attributes["class"] = "form-info";
//                    div_info.InnerHtml = "<p>Profilo aggiornato con successo!</p>";
//                    if (sendupdateemailconfirmation)
//                    {
//                        NetCms.Users.AccountManager.SendConfirmationEmailUpdate(NetCms.Users.AccountManager.CurrentAccount.NomeCompleto, oldemail, newemail, NetCms.Users.AccountManager.CurrentAccount.ID.ToString(), null);
//                        if (newemail != null && newemail.Length > 0)
//                            div_info.InnerHtml += "<p>Per attuare la modifica dell'email a \"" + newemail + "\", seguite le indicazioni inviateVi per posta elettronica all'indirizzo fornito.</p>";
//                    }
//                    field.Controls.Add(div_info);
//                }
//                else
//                {
//                    HtmlGenericControl div_error = new HtmlGenericControl("div");
//                    div_error.Attributes["class"] = "form-error";
//                    div_error.InnerHtml = error;
//                    field.Controls.Add(div_error);
//                }
//            }

            

//            field.Controls.Add(new System.Web.UI.LiteralControl("<span class=\"smallDescription\">La modifica dell'email richieder� una conferma della nuova email prima di essere attuata.</span>"));            
//            field.Controls.Add(anagrafica.getForm());

//            WebControl p_button = new WebControl(HtmlTextWriterTag.P);
//            p_button.Attributes["class"] = "bottoni";

//            Button action = new Button();
//            action.ID = "action";
//            action.Text = "Modifica";
//            p_button.Controls.Add(action);

//            field.Controls.Add(p_button);

//            return field;
//        }

//        public override AxfGenericTable GetProfileAxtTable(bool editMailFields = true)
        
//        {
//            AxfGenericTable anagrafica = new AxfGenericTable("Anagrafica", "id_Anagrafica",Conn);
//            anagrafica.RelatedRecordID = int.Parse(ID);
//            AxfTextBox nome = new AxfTextBox("Nome", "Nome_Anagrafica");
//            nome.Size = 60;
//            nome.Required = true;
//            nome.Value = Nome;
//            anagrafica.addField(nome);

//            AxfTextBox cognome = new AxfTextBox("Cognome", "Cognome_Anagrafica");
//            cognome.Size = 60;
//            cognome.Required = true;
//            cognome.Value = Cognome;
//            anagrafica.addField(cognome);

//            AxfDatePicker Date = new AxfDatePicker("Data di Nascita", "Data_Anagrafica", Conn, AxfDatePicker.DatePickerTypes.dropdown,false);
//            //Date.Size = 60;
//            Date.Required = true;
//            //Data.Required = true;
//            Date.Value = DataNascita;
//            anagrafica.addField(Date);

//            AxfTextBox address = new AxfTextBox("Indirizzo", "Indirizzo_Anagrafica");
//            address.Size = 60;
//            address.MaxLenght = 1000;
//            //citta.Required = true;
//            address.Value = Indirizzo;
//            anagrafica.addField(address);

//            AxfTextBox citta = new AxfTextBox("Citt�", "Citta_Anagrafica");
//            citta.Size = 60;
//            //citta.Required = true;
//            citta.Value = Citt�;
//            anagrafica.addField(citta);

//            AxfDropDownList provincia = new AxfDropDownList("Provincia", "Provincia_Anagrafica");
//            //provincia.ClearList();
//            provincia.addItem("", "0");
//            provincia.setDataBind("SELECT Nome_Provincia,id_Provincia FROM Provincie ORDER BY Nome_Provincia", Conn);
//            //citta.Required = true;
//            provincia.Value = Data["Provincia_Anagrafica"].ToString();
//            anagrafica.addField(provincia);

//            AxfTextBox cap = new AxfTextBox("CAP", "Cap_Anagrafica");
//            cap.Size = 60;
//            //citta.Required = true;
//            cap.Value = CAP;
//            cap.MaxLenght = 5;
//            anagrafica.addField(cap);

//            AxfTextBox telefono = new AxfTextBox("Telefono", "Telefono_Anagrafica", AxfTextBox.ContentTypes.Phone);
//            telefono.Size = 12;
//            //citta.Required = true;
//            telefono.MaxLenght = 12;
//            telefono.Value = Telefono;
//            //telefono.ContentType = NetTextBoxPlus.ContentTypes.Phone;
//            anagrafica.addField(telefono);

//            AxfTextBox cellulare = new AxfTextBox("Cellulare", "Cellulare_Anagrafica", AxfTextBox.ContentTypes.Phone);
//            cellulare.Size = 12;
//            //citta.Required = true;
//            cellulare.MaxLenght = 12;
//            cellulare.Value = Cellulare;
//            //cellulare.ContentType = NetTextBoxPlus.ContentTypes.Phone;
//            anagrafica.addField(cellulare);

//            AxfTextBox email = new AxfTextBox("e-Mail", "Email_Anagrafica", AxfTextBox.ContentTypes.eMail);
//            email.Size = 60;
//            email.Required = true;
//            email.MaxLenght = 255;
//            email.Value = Email;
//            //email.ContentType = NetTextBoxPlus.ContentTypes.eMail;
//            anagrafica.addField(email);

//            AxfTextBox codicefiscale = new AxfTextBox("CodiceFiscale", "CodiceFiscale_Anagrafica");
//            codicefiscale.Size = 16;
//            codicefiscale.MaxLenght = 16;
//            codicefiscale.Value = CodiceFiscale;
//            anagrafica.addField(codicefiscale);


//            AxfDropDownList sesso = new AxfDropDownList("Sesso", "Sesso_Anagrafica");
//            sesso.addItem("M", "0");
//            sesso.addItem("F", "1");
//            sesso.Required = true;
//            sesso.Value = Data["Sesso_Anagrafica"].ToString();
//            anagrafica.addField(sesso);

//            AxfTextBox azienda = new AxfTextBox("Azienda/Ufficio", "Azienda_Anagrafica");
//            codicefiscale.Size = 60;
//            azienda.Value = Azienda;
//            anagrafica.addField(azienda);

//            return anagrafica;
//        }

//        public override void HardDelete()
//        {
//            this.Conn.Execute("DELETE FROM Anagrafica WHERE id_Anagrafica = " + this.ID);
//        }

//        public override bool UpdateEmail(string newemail)
//        {
//            string sql = "UPDATE anagrafica "
//                               + " SET email_anagrafica = " +G2Core.Common.Utility.ValidateTextValue(newemail)  
//                               + " WHERE id_anagrafica = " + NetCms.Users.AccountManager.CurrentAccount.Anagrafica.ID;
//            bool result=Conn.Execute(sql);            
//            return (result);            
//        }

//        public override int ImportAnagrafica(DataRow datiAnagrafica)
//        {
//            int resID = -1;
//            DataTable table = Conn.SqlQuery("SELECT * FROM Anagrafica WHERE CodiceFiscale_Anagrafica = '" + datiAnagrafica["CodiceFiscale_Anagrafica"].ToString()+ "'");
//            if (datiAnagrafica["CodiceFiscale_Anagrafica"].ToString().Length > 0 && table.Rows.Count > 0)
//            {
//                resID = int.Parse(table.Rows[0]["id_Anagrafica"].ToString());
//            }
//            else
//            {
//                string sqlInsert = @"INSERT INTO 
//                                      anagrafica
//                                    (
//                                      Nome_Anagrafica,
//                                      Cognome_Anagrafica,
//                                      Citta_Anagrafica,
//                                      Indirizzo_Anagrafica,
//                                      CAP_Anagrafica,
//                                      Telefono_Anagrafica,
//                                      Cellulare_Anagrafica,
//                                      Email_Anagrafica,
//                                      CodiceFiscale_Anagrafica,
//                                      Sesso_Anagrafica,
//                                      Provincia_Anagrafica,
//                                      Azienda_Anagrafica,
//                                      Data_Anagrafica
//                                    ) 
//                                    VALUES (
//                                      '{0}',
//                                      '{1}',
//                                      '{2}',
//                                      '{3}',
//                                      '{4}',
//                                      '{5}',
//                                      '{6}',
//                                      '{7}',
//                                      '{8}',
//                                      {9},
//                                      {10},
//                                      '{11}',
//                                      {12}
//                                    );";

//                string[] parameters = { datiAnagrafica["Nome_Anagrafica"].ToString().Replace("'","''"), 
//                                    datiAnagrafica["Cognome_Anagrafica"].ToString().Replace("'","''"), 
//                                    datiAnagrafica["Citta_Anagrafica"].ToString().Replace("'","''"), 
//                                    datiAnagrafica["Indirizzo_Anagrafica"].ToString().Replace("'","''"), 
//                                    datiAnagrafica["CAP_Anagrafica"].ToString(), 
//                                    datiAnagrafica["Telefono_Anagrafica"].ToString(), 
//                                    datiAnagrafica["Cellulare_Anagrafica"].ToString(), 
//                                    datiAnagrafica["Email_Anagrafica"].ToString(), 
//                                    datiAnagrafica["CodiceFiscale_Anagrafica"].ToString(), 
//                                    datiAnagrafica["Sesso_Anagrafica"].ToString(), 
//                                    datiAnagrafica["Provincia_Anagrafica"].ToString(),
//                                    datiAnagrafica["Azienda_Anagrafica"].ToString().Replace("'","''"),
//                                    (this.Conn.FilterDateTime(datiAnagrafica["Data_Anagrafica"].ToString()).Length>0)?this.Conn.FilterDateTime(datiAnagrafica["Data_Anagrafica"].ToString()):"NULL"};

//                resID = this.Conn.ExecuteInsert(string.Format(sqlInsert, parameters));
//            }
//            return resID;
//        }
   
//        public class AnagraficaSearch : AbstractAnagraficaSearch
//        {
//            public override string NamespaceKeyOnUsersTable
//            {
//                get { return "NetCms.Users.Anagrafica"; }
//            }
//            public override string PrimaryKeyField
//            {
//                get { return "id_Anagrafica"; }
//            }

//            protected override Type AnagraficaType
//            {
//                get { return typeof(Anagrafica); }
//            }
//            protected override string ProcudureName
//            {
//                get { return "sp_search_anagrafica"; }
//            }

//            protected override NetCms.Connections.Connection InnerConnection
//            {
//                get
//                {
//                    return Connection;
//                }
//            }
//            protected override NetCms.Connections.Connection OuterConnection
//            {
//                get
//                {
//                    return Connection;
//                }
//            }
//            private NetCms.Connections.Connection _Connection;
//            protected NetCms.Connections.Connection Connection
//            {
//                get
//                {
//                    if (_Connection == null)
//                        _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
//                    return _Connection;
//                }
//            }
//        }
//    }
//}