﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Users.CustomAnagrafe
{
    public class Comune
    {
        public Comune()
        {
 
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string CodiceComune
        {
            get;
            set;
        }

        public virtual string CodiceCatastale
        {
            get;
            set;
        }

        public virtual string Nome
        {
            get;
            set;
        }

        public virtual bool Attivo
        {
            get;
            set;
        }

        public virtual Provincia Provincia
        {
            get;
            set;
        }

        public override string ToString()
        {
            return Nome+" ("+Provincia.Sigla+") ";
        }
    }
}
