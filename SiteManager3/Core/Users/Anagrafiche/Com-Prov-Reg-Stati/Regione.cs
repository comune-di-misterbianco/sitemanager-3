﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Users.CustomAnagrafe
{
    public class Regione
    {
        public Regione()
        {
            Province = new HashSet<Provincia>();
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Nome
        {
            get;
            set;
        }

        public virtual bool Attivo
        {
            get;
            set;
        }

        public virtual ICollection<Provincia> Province
        {
            get;
            set;
        }
    }
}
