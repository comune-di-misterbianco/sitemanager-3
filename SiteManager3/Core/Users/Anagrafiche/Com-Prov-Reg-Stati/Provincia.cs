﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Users.CustomAnagrafe
{
    public class Provincia
    {
        public Provincia()
        {
            Comuni = new HashSet<Comune>();
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string CodiceProvincia
        {
            get;
            set;
        }

        public virtual string Nome
        {
            get;
            set;
        }

        public virtual string Sigla
        {
            get;
            set;
        }

        public virtual bool Attivo
        {
            get;
            set;
        }

        public virtual Regione Regione
        {
            get;
            set;
        }

        public virtual ICollection<Comune> Comuni
        {
            get;
            set;
        }

        public override string ToString()
        {
            return this.Nome;
        }
    }
}
