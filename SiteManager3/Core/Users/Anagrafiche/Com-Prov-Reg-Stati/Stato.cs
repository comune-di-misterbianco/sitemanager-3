﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Users.CustomAnagrafe
{
    public class Stato
    {
        public Stato()
        {
 
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Nome
        {
            get;
            set;
        }

        public virtual bool MembroCEE
        {
            get;
            set;
        }

        public virtual string Cittadinanza
        {
            get;
            set;
        }

        public virtual string CodiceStato
        {
            get;
            set;
        }
    }
}
