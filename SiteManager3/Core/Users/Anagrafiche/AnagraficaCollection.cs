﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Users
{
    public class AnagraficaCollection
    {
        public AnagraficaCollection(string key, List<ItemOfCollection> items)
        {
            this.key = key;
            this.items = items;
        }

        public string key;
        public List<ItemOfCollection> items;
    }

    public class ItemOfCollection
    {
        public ItemOfCollection(int id, string type)
        {
            this.id = id;
            this.type = type;
        }

        public int id;
        public string type;
    }
}
