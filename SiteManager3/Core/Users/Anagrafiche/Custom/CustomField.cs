﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using NetCms.GUI;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Web.UI;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public abstract class CustomField
    {
        public CustomField()
        {
            DependencyValuesCollection = new HashSet<CustomDependencyItem>();
            Enabled = true;
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Name
        {
            get;
            set;
        }

        public virtual string InfoDescription
        {
            get;
            set;
        }

        public virtual bool Required
        {
            get;
            set;
        }

        public virtual bool ShowInDetails
        {
            get;
            set;
        }

        public virtual bool ShowInSearch
        {
            get;
            set;
        }

        public virtual string CriterioRicerca
        {
            get;
            set;
        }

        public virtual bool Enabled
        {
            get;
            set;
        }

        public virtual int PositionInGroup
        {
            get;
            set;
        }

        public virtual string ColumnCssClass
        {
            get;
            set;
        }

        public virtual int RowOrder
        {
            get;
            set;
        }

        public virtual CustomAnagrafe Anagrafe
        {
            get;
            set;
        }

        public virtual CustomGroup CustomGroup
        {
            get;
            set;
        }

        public virtual bool Cancellato
        {
            get;
            set;
        }

        public virtual bool IsUsername
        {
            get;
            set;
        }

        // opzionale, se voglio modificare il nome di un custom field, tengo il riferimento a quello nuovo creato
        public virtual CustomField NewField
        {
            get;
            set;
        }

        protected internal virtual string ControlId
        {
            get
            {   
                if(Anagrafe != null) 
                    return Anagrafe.DatabaseName + "_" + Name.Replace(' ','_');
                return Name.Replace(' ', '_'); 
            }
        }

        public abstract VfGeneric CampoInAnagrafe
        {
            get;
        }

        public enum TipoProprietà
        {
            Testo,
            Dropdown,
            Checkbox,
            Data,
            Comune,
            CheckboxList,
            DropdownProvincia,
            File,
            MoltiAMoltiAClasseMappata,
            MoltiAUnoAClasseMappata
        }

        public abstract TipoProprietà Tipo
        {
            get;
        }

        public abstract List<VfGeneric> FormConfigurazione
        {
            get;
        }

        public virtual List<VfGeneric> CommonFields
        {
            get
            {
                List<VfGeneric> fields = new List<VfGeneric>();

                VfTextBox label = new VfTextBox("label", "Nome campo", "Name", InputTypes.Text);
                label.MaxLenght = 50;

                VfTextBox descrizione = new VfTextBox("description", "Descrizione", "InfoDescription", InputTypes.Text) { Required = false };

                VfCheckBox obbligatorio = new VfCheckBox("obbligatorio", "Obbligatorio", "Required");

                VfCheckBox mostraNeiDettagli = new VfCheckBox("mostraDettagli", "Mostra nei dettagli", "ShowInDetails");

                VfCheckBox mostraNellaRicerca = new VfCheckBox("mostraRicerca", "Mostra nella ricerca", "ShowInSearch");

                VfTextBox columnclass = new VfTextBox("ColumnCssClass", "Classe css bootstrap", "ColumnCssClass");
                columnclass.Required = false;

                VfTextBox formOrder = new VfTextBox("RowOrder", "Riferimento alla riga", "RowOrder");
                formOrder.Required = false;

                VfDropDown criterioSP = new VfDropDown("criterioSP", "Criterio che definisce il metodo di ricerca (Search Parameter)", "CriterioRicerca");
                foreach (var criterio in Enum.GetValues(typeof(NetService.Utility.RecordsFinder.Finder.ComparisonCriteria)))
                    criterioSP.Options.Add(criterio.ToString(), criterio.ToString());

                fields.Add(label);
                fields.Add(descrizione);
                fields.Add(obbligatorio);
                fields.Add(mostraNeiDettagli);
                fields.Add(mostraNellaRicerca);
                fields.Add(columnclass);
                fields.Add(formOrder);
                fields.Add(criterioSP);
                return fields;
            }
        }

        // ad esempio... varchar(200)
        public abstract string DBDataType
        {
            get;
        }

        // ad esempio... string
        public abstract string SystemDataType
        {
            get;
        }

        public virtual List<ToolbarButton> AdditionalToolbar
        {
            get { return new List<GUI.ToolbarButton>(); }
        }

        /// <summary>
        /// Proprietà normale, Chiave esterna ad un'entità, Relazione Molti a Molti o Uno a Molti
        /// </summary>
        public enum MappingEffect
        {
            Property,
            ForeignKey,
            ManyToMany,
            OneToMany
        }

        public virtual MappingEffect EffettoSuMapping
        {
            get { return MappingEffect.Property; }
        }

        public virtual Control DescriptionControl
        {
            get
            {
                return new LiteralControl(InfoDescription);
            }
        }

        public static string GetDescription(Enum en)
        {

            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {

                object[] attrs = memInfo[0].GetCustomAttributes(typeof(Description),
                false);

                if (attrs != null && attrs.Length > 0)

                    return ((Description)attrs[0]).Text;

            }

            return en.ToString();

        }

        public abstract CustomField Clone(CustomAnagrafe ca);

        //Collezione usata nel caso il campo appartenga ad un gruppo e questo dipenda da una dropdown; la collezione conterrà i valori della dropdown per
        //i quali sarà necessario visualizzare il campo
        public virtual ICollection<CustomDependencyItem> DependencyValuesCollection
        { get; set; }
    }
}
