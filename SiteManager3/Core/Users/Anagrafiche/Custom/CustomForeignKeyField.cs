﻿using System;

namespace NetCms.Users.CustomAnagrafe
{
   public abstract class CustomForeignKeyField : CustomField
    {
       public override CustomField.MappingEffect EffettoSuMapping
       {
           get
           {
               return MappingEffect.ForeignKey;
           }
       }
       
       public override string DBDataType
       {
           //Se il custom field è una foreign key questa proprietà non dovrebbe essere usata
           get { throw new NotImplementedException(); }
       }
    }
}
