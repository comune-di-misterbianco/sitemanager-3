﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using NetCms.GUI;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Web.UI;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomDependencyItem
    {
        public CustomDependencyItem()
        { }

        public virtual int ID
        { get; set; }

        public virtual string DependencyDDValue
        { get; set; }

        public virtual bool isRequiredForValue
        { get; set; }

        public virtual CustomField DependentField
        { get; set; }

        public virtual CustomDependencyItem Clone(CustomField newCustomField)
        {         
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomDependencyItem, CustomDependencyItem>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.DependentField, o => o.Ignore());
            });

            config.AssertConfigurationIsValid();

            var newCustomDependencyItem = new CustomDependencyItem();                       
            
            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomDependencyItem);

            newCustomDependencyItem.DependentField = newCustomField;
            return newCustomDependencyItem;
        }
    }
}
