﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Users.CustomAnagrafe
{
    public abstract class LabelValue<T>
    {
        public LabelValue()
        { }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Value
        {
            get;
            set;
        }

        public virtual string Label
        {
            get;
            set;
        }

        public abstract T CustomField
        {
            get;
            set;
        }
    }
}
