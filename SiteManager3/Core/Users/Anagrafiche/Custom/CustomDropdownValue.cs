﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomDropdownValue : LabelValue<CustomDropdownField>
    {
        public CustomDropdownValue()
        { }


        public override CustomDropdownField CustomField
        {
            get
            {
                return _CustomField;
            }
            set
            {
                _CustomField = value;
            }
        }
        private CustomDropdownField _CustomField;

        public virtual CustomDropdownValue Clone(CustomDropdownField newCustomDropdownField)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomDropdownValue, CustomDropdownValue>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.CustomField, o => o.Ignore());
            });

            config.AssertConfigurationIsValid();
            
            var newCustomDropdownValue = new CustomDropdownValue();           

            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomDropdownValue);

            newCustomDropdownValue.CustomField = newCustomDropdownField;
            return newCustomDropdownValue;
        }
    }
}
