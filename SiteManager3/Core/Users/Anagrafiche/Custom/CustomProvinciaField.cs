﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomProvinciaField : CustomForeignKeyField
    {
        public CustomProvinciaField()
        {
        }

        public override CustomField.TipoProprietà Tipo
        {
            get { return TipoProprietà.DropdownProvincia; }
        }

        public override VfGeneric CampoInAnagrafe
        {
            get
            {
                VfDropDownProvince dropdown = new VfDropDownProvince(ControlId, Name, Name, VfDropDownProvince.PostObjType.Hashtable) { Required = this.Required, BindField = false };
                if (!string.IsNullOrEmpty(InfoDescription))
                    dropdown.InfoControl.Controls.Add(DescriptionControl);
                dropdown.Options.Add("", "");
                foreach (Provincia value in CustomAnagrafeBusinessLogic.FindProvince())
                {
                    dropdown.Options.Add(value.Nome, value.ID.ToString());
                }
                return dropdown;
            }
        }

        public override List<VfGeneric> FormConfigurazione
        {
            get
            {
                if (_FormConfigurazione == null)
                {
                    _FormConfigurazione = new List<VfGeneric>();
                    _FormConfigurazione.AddRange(CommonFields);
                }
                return _FormConfigurazione;
            }
        }

        private List<VfGeneric> _FormConfigurazione;

        public override string SystemDataType
        {
            get { return "NetCms.Users.CustomAnagrafe.Provincia"; }
        }

        public override CustomField Clone(CustomAnagrafe ca)
        {           
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomProvinciaField, CustomProvinciaField>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Anagrafe, o => o.Ignore())
                .ForMember(d => d.CustomGroup, o => o.Ignore())
                .ForMember(d => d.DependencyValuesCollection, o => o.Ignore());
            });

            config.AssertConfigurationIsValid();

            var newCustomProvinciaField = new CustomProvinciaField();     

            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomProvinciaField);

            newCustomProvinciaField.DependencyValuesCollection = this.DependencyValuesCollection.Select(item => item.Clone(newCustomProvinciaField)).ToList();
            newCustomProvinciaField.Anagrafe = ca;
            //newCustomProvinciaField.CustomGroup = this.CustomGroup.Clone(ca);
            return newCustomProvinciaField;
        }
    }
}
