﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomDateField : CustomField
    {
        public CustomDateField()
        {
        }


        public virtual DateTime MinDate
        {
            get;
            set;
        }

        public virtual DateTime MaxDate
        {
            get;
            set;
        }

        public virtual NetService.Utility.ValidatedFields.VfDate.DateFormat Formato
        {
            get;
            set;
        }

        public override CustomField.TipoProprietà Tipo
        {
            get { return TipoProprietà.Data; }
        }

        public override VfGeneric CampoInAnagrafe
        {
            get
            {
                VfDate data = new VfDate(ControlId, Name, Name, typeof(DateTime), Formato) { Required = this.Required, BindField = false };
                
                if (!string.IsNullOrEmpty(InfoDescription))
                    data.InfoControl.Controls.Add(DescriptionControl);

                if (Formato != VfDate.DateFormat.SoloOra)
                {
                    if (this.MinDate != null && this.MinDate != DateTime.MinValue)
                        data.MinDate = this.MinDate;
                    if (this.MaxDate != null && this.MaxDate != DateTime.MinValue)
                    data.MaxDate = this.MaxDate;
                }

                //data.ShowLabelAfterField = true;
                data.CustomCssClass = "it-datepicker-wrapper";
                return data;
            }
        }

        public override List<VfGeneric> FormConfigurazione
        {
            get
            {
                if (_FormConfigurazione == null)
                {
                    _FormConfigurazione = new List<VfGeneric>();

                    _FormConfigurazione.AddRange(CommonFields);

                    VfDate dataMin = new VfDate("mindate", "Minima Data permessa", "MinDate") { Required = false };

                    VfDate dataMax = new VfDate("maxdate", "Massima Data permessa", "MaxDate") { Required = false };

                    VfDropDown formatoData = new VfDropDown("formatoData", "Formato", "Formato");

                    foreach (NetService.Utility.ValidatedFields.VfDate.DateFormat type in Enum.GetValues(typeof(NetService.Utility.ValidatedFields.VfDate.DateFormat)))
                    {
                        formatoData.Options.Add(GetDescription(type), ((int)type).ToString());
                    }

                    _FormConfigurazione.Add(dataMin);
                    _FormConfigurazione.Add(dataMax);
                    _FormConfigurazione.Add(formatoData);

                }
                return _FormConfigurazione;
            }
        }

        private List<VfGeneric> _FormConfigurazione;

        public override string DBDataType
        {
            get { return "DateTime"; }
        }

        public override string SystemDataType
        {
            get { return "DateTime"; }
        }

        public override CustomField Clone(CustomAnagrafe ca)
        {         
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomDateField, CustomDateField>().ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Anagrafe, o => o.Ignore())
                .ForMember(d => d.CustomGroup, o => o.Ignore())
                .ForMember(d => d.DependencyValuesCollection, o => o.Ignore());
            });

            config.AssertConfigurationIsValid();

            var newCustomDateField = new CustomDateField();            

            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomDateField);

            newCustomDateField.Anagrafe = ca;
            newCustomDateField.DependencyValuesCollection = this.DependencyValuesCollection.Select(item => item.Clone(newCustomDateField)).ToList();
            //newCustomDateField.CustomGroup = this.CustomGroup.Clone(ca);
            return newCustomDateField;
        }
    }
}
