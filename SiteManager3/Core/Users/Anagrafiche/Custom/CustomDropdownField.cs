﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomDropdownField : CustomField
    {
        public CustomDropdownField()
        {
            Values = new HashSet<CustomDropdownValue>();
        }

        public virtual ICollection<CustomDropdownValue> Values
        {
            get;
            set;
        }

        public override CustomField.TipoProprietà Tipo
        {
            get { return TipoProprietà.Dropdown; }
        }

        public override VfGeneric CampoInAnagrafe
        {
            get
            {
                VfDropDown dropdown = new VfDropDown(ControlId, Name, Name) { Required = this.Required, BindField = false };
                dropdown.CustomCssClass = "bootstrap-select-wrapper";
                if (!string.IsNullOrEmpty(InfoDescription))
                    dropdown.InfoControl.Controls.Add(DescriptionControl);
                dropdown.Options.Add("", "");
                foreach (CustomDropdownValue value in Values)
                {
                    dropdown.Options.Add(value.Label, value.Value);
                }
                return dropdown;
            }
        }

        public override List<VfGeneric> FormConfigurazione
        {
            get
            {
                if (_FormConfigurazione == null)
                {
                    _FormConfigurazione = new List<VfGeneric>();
                    _FormConfigurazione.AddRange(CommonFields);
                }
                return _FormConfigurazione;
            }
        }

        private List<VfGeneric> _FormConfigurazione;

        public override string DBDataType
        {
            get { return "varchar(255)"; }
        }

        public override string SystemDataType
        {
            get { return "string"; }
        }

        public override List<GUI.ToolbarButton> AdditionalToolbar
        {
            get 
            {
                List<GUI.ToolbarButton> toolbar = new List<GUI.ToolbarButton>();
                toolbar.Add(new GUI.ToolbarButton("campocustomdropdown_gest.aspx?cf=" + this.ID, NetCms.GUI.Icons.Icons.Cog_Edit, "Gestione Opzioni Dropdown"));
                return toolbar;
            }
        }

        public override CustomField Clone(CustomAnagrafe ca)
        {           
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomDropdownField, CustomDropdownField>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Anagrafe, o => o.Ignore())
                .ForMember(d => d.CustomGroup, o => o.Ignore())
                .ForMember(d => d.DependencyValuesCollection, o => o.Ignore());
            });

            config.AssertConfigurationIsValid();
            
            var newCustomDropdownField = new CustomDropdownField();
            
            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomDropdownField);

            newCustomDropdownField.Values = this.Values.Select(item => item.Clone(newCustomDropdownField)).ToList();
            newCustomDropdownField.DependencyValuesCollection = this.DependencyValuesCollection.Select(item => item.Clone(newCustomDropdownField)).ToList();
            newCustomDropdownField.Anagrafe = ca;

            //newCustomDropdownField.CustomGroup = this.CustomGroup.Clone(ca);
            return newCustomDropdownField;
        }
    }
}
