﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Users.CustomAnagrafe
{
    public abstract class CustomManyToManyField : CustomField
    {
        public override string DBDataType
        {
            get { throw new NotImplementedException(); }
        }

        public override string SystemDataType
        {
            get { throw new NotImplementedException(); }
        }

        public override MappingEffect EffettoSuMapping
        {
            get
            {
                return MappingEffect.ManyToMany;
            }
        }
        
        // il nome della tabella su db a cui si appoggierà l'anagrafica per la creazione della tabella di mezzo
        public abstract string TableName
        {
            get;
        }

        // il nome della colonna identificativa della tabella con la quale effettuare l'associazione (many to many column name)
        public abstract string IdColumnName
        {
            get;
        }

        // Namespace o entity name della classe con la quale effettuare l'associazione (many to many entity name)
        public abstract string EntityName
        {
            get;
        }
    }
}
