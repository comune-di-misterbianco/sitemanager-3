﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Reflection;
using System.Web.UI;
using AutoMapper;
using System.Web.UI.WebControls;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomTextField : CustomField
    {
        public CustomTextField()
        { }

        public virtual int Columns
        {
            get;
            set;
        }

        public virtual int Rows
        {
            get;
            set;
        }

        public virtual bool isMultiline
        {
            get;
            set;
        }

        public virtual int MaxLength
        {
            get;
            set;
        }

        public virtual int MinLength
        {
            get;
            set;
        }

        public virtual InputTypes InputType
        {
            get;
            set;
        }

        public override CustomField.TipoProprietà Tipo
        {
            get { return TipoProprietà.Testo; }
        }

        public override VfGeneric CampoInAnagrafe
        {
            get
            {
                VfTextBox textBox = new VfTextBox(ControlId, Name, Name, InputType) { MaxLenght = this.MaxLength, MinLenght = this.MinLength != null ? this.MinLength : 0, Columns = this.Columns, TextMode = (isMultiline? TextBoxMode.MultiLine: TextBoxMode.SingleLine), Rows = this.Rows, Required = this.Required, BindField = false };
                if (!string.IsNullOrEmpty(InfoDescription))
                    textBox.InfoControl.Controls.Add(DescriptionControl);
                return textBox;
            }
        }

        public override List<VfGeneric> FormConfigurazione
        {
            get 
            {
                if (_FormConfigurazione == null)
                {
                    _FormConfigurazione = new List<VfGeneric>();

                    _FormConfigurazione.AddRange(CommonFields);

                    VfTextBox maxLenght = new VfTextBox("maxlenght", "Lunghezza Massima", "MaxLength", InputTypes.Number);
                    // così il campo sarà al massimo 999
                    //maxLenght.MaxLenght = 3;

                    VfTextBox minLenght = new VfTextBox("minLenght", "Lunghezza Minima", "MinLength", InputTypes.Number);
                    // così il campo sarà al massimo 999
                    minLenght.MaxLenght = 3;

                    VfTextBox numCaratteri = new VfTextBox("numColumns", "Caratteri visualizzati (0 considera il valore di default)", "Columns", InputTypes.Number);
                    // così il campo sarà al massimo 999
                    numCaratteri.DefaultValue = 0;
                    numCaratteri.MaxLenght = 3;

                    VfCheckBox multiline = new VfCheckBox("multiline", "Multiline", "isMultiline");
                    //multiline.AutoPostBack = true;

                    VfTextBox numRighe = new VfTextBox("numRows", "Numero righe (0 considera il valore di default) - Solo se attivo Multiline", "Rows", InputTypes.Number);
                    // così il campo sarà al massimo 999
                    numRighe.DefaultValue = 0;
                    numRighe.MaxLenght = 3;


                    VfDropDown inputType = new VfDropDown("inputType", "Tipologia", "InputType");

                    foreach (InputTypes type in Enum.GetValues(typeof(InputTypes)))
                    {
                        inputType.Options.Add(GetDescription(type), ((int)type).ToString());
                    }

                    if (this.InputType != InputTypes.Valuta)
                    {
                        _FormConfigurazione.Add(maxLenght);
                        _FormConfigurazione.Add(minLenght);
                    }

                    _FormConfigurazione.Add(numCaratteri);

                    if (this.InputType == InputTypes.Text)
                    {
                        _FormConfigurazione.Add(multiline);
                        //if (bool.Parse(multiline.PostbackValueObject.ToString()) == true)
                        _FormConfigurazione.Add(numRighe);
                    }

                    _FormConfigurazione.Add(inputType);

                }
                return _FormConfigurazione;
            }
        }
        private List<VfGeneric> _FormConfigurazione;

        

        public override string DBDataType
        {
            get 
            {
                switch (InputType)
                {
                    case InputTypes.Floating: return "float(" + MaxLength + ")";
                    case InputTypes.Number: return "int(" + MaxLength + ")";
                    case InputTypes.Valuta: return "decimal(12,2)";
                    default: return "varchar(" + MaxLength + ")";
                }
            }
        }

        public override string SystemDataType
        {
            get
            {
                switch (InputType)
                {
                    case InputTypes.Floating: return "float";
                    case InputTypes.Number: return "int";
                    case InputTypes.Valuta: return "decimal";
                    default: return "string";
                }
            }
        }

        public override CustomField Clone(CustomAnagrafe ca)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomTextField, CustomTextField>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Anagrafe, o => o.Ignore())
                .ForMember(d => d.CustomGroup, o => o.Ignore())
                .ForMember(d => d.DependencyValuesCollection, o => o.Ignore());
            });

            config.AssertConfigurationIsValid();
            
            var newCustomTextField = new CustomTextField();
            
            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomTextField);

            newCustomTextField.DependencyValuesCollection = this.DependencyValuesCollection.Select(item => item.Clone(newCustomTextField)).ToList();
            newCustomTextField.Anagrafe = ca;
            //newCustomTextField.CustomGroup = this.CustomGroup.Clone(ca);
            return newCustomTextField;
        }
    }
}
