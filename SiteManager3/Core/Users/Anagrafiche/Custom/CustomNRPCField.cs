﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using NetService.Utility.ValidatedFields;
//using System.Web.UI;
//using AutoMapper;

//namespace NetCms.Users.CustomAnagrafe
//{
//    public class CustomNRPCField : CustomForeignKeyField
//    {
//        public CustomNRPCField()
//        {

//        }

//        public override CustomField.TipoProprietà Tipo
//        {
//            get { return TipoProprietà.NRPC; }
//        }

//        public override VfGeneric CampoInAnagrafe
//        {
//            get
//            {
//                VfDropDownNRPC comune = new VfDropDownNRPC(ControlId, Name, Name, VfDropDownNRPC.PostObjType.Hashtable) { Required = this.Required, BindField = false };
//                if (!string.IsNullOrEmpty(InfoDescription))
//                    comune.InfoControl.Controls.Add(DescriptionControl);
//                return comune;
//            }
//        }

//        public override List<VfGeneric> FormConfigurazione
//        {
//            get
//            {
//                if (_FormConfigurazione == null)
//                {
//                    _FormConfigurazione = new List<VfGeneric>();
//                    _FormConfigurazione.AddRange(CommonFields);
//                }
//                return _FormConfigurazione;
//            }
//        }
//        private List<VfGeneric> _FormConfigurazione;

//        public override string SystemDataType
//        {
//            get { return "NetCms.Users.CustomAnagrafe.Comune"; } //???
//        }

//        public override CustomField Clone(CustomAnagrafe ca)
//        {
//            Mapper.CreateMap<CustomNRPCField, CustomNRPCField>().ForMember(d => d.ID, o => o.Ignore())
//                .ForMember(d => d.Anagrafe, o => o.Ignore())
//                .ForMember(d => d.CustomGroup, o => o.Ignore())
//                .ForMember(d => d.DependencyValuesCollection, o => o.Ignore());
//            var newCustomNRPCField = new CustomNRPCField();
//            Mapper.Map(this, newCustomNRPCField);
//            newCustomNRPCField.Anagrafe = ca;
//            newCustomNRPCField.DependencyValuesCollection = this.DependencyValuesCollection.Select(item => item.Clone(newCustomNRPCField)).ToList();
//            //newCustomFileField.CustomGroup = this.CustomGroup.Clone(ca);
//            return newCustomNRPCField;
//        }
//    }
//}
