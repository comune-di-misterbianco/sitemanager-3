﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomAnagrafe
    {
        public CustomAnagrafe()
        {
            CustomFields = new HashSet<CustomField>();
            CustomGroups = new HashSet<CustomGroup>();
            Applicazioni = new HashSet<CustomAnagrafeApplicazioni>();
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Nome
        {
            get;
            set;
        }

        public virtual bool Installata
        {
            get;
            set;
        }

        public virtual bool UseSpid
        {
            get;
            set;
        }

        public virtual bool VisibileInFrontend
        {
            get;
            set;
        }

        public virtual bool EmailAsUserName
        {
            get;
            set;
        }

        public virtual ICollection<CustomAnagrafeApplicazioni> Applicazioni
        {
            get;
            set;
        }

        public virtual ICollection<CustomGroup> CustomGroups
        {
            get;
            set;
        }
        
        public virtual ICollection<CustomField> CustomFields
        {
            get;
            set;
        }

        public virtual ICollection<CustomField> FrontendFields
        {
            get;
            set;
        }

        public virtual bool ValidaConCaptcha
        {
            get;
            set;
        }

        //E' il gruppo di default dove verranno inseriti gli utenti iscritti da Frontend
        public virtual Group DefaultGroup
        {
            get;
            set;
        }

        //E' il messaggio che verrà inviato via email all'utente per confermare la registrazione
        public virtual String TestoEmailAttivazione
        {
            get;
            set;
        }

        //E' il messaggio che verrà inviato via email all'utente dopo aver negato la registrazione
        public virtual string TestoEmailRegistrazioneNegata { get; set; }


        //E' la tipologia di rendering delle form di registrazione
        public enum RenderTypes
        {
            Normale,
            FieldSet
        }

        public virtual RenderTypes RenderType
        {
            get;
            set;
        }

        // Nome utilizzato senza spazi nel db e nel mapping
        public virtual string DatabaseName
        {
            get
            {
                return Nome.Replace(" ", "_");
            }
        }

        public virtual bool RichiediConfermaEmail
        {
            get;
            set;
        }

        public virtual bool AggiungiInformativaPrivacy
        { 
            get; 
            set; 
        }

        public virtual String TestoInformativaPrivacy
        {
            get;
            set;
        }

        public virtual string LabelInformativaPrivacy
        {
            get;
            set;
        }

        public virtual string DomandaAccettazioneInformativaPrivacy
        {
            get;
            set;
        }

        public virtual int NumeroRigheDaVisualizzareAreaInformativa
        {
            get;
            set;
        }

        public virtual int NumeroCaratteriDaVisualizzareAreaInformativa
        {
            get;
            set;
        }

        //E' il gruppo di utenti che riceveranno le notifiche di registrazione quando nel sistema è abilitato
        //il flag per la gestione delle richieste di registrazione ed è necessario l'intervento di un operatore
        public virtual Group RegRequestNotifyGroup
        {
            get;
            set;
        }

        public virtual CustomAnagrafe Clone()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomAnagrafe, CustomAnagrafe>()
                    .ForMember(d => d.ID, o => o.Ignore())
                    .ForMember(d => d.CustomFields, o => o.Ignore())
                    .ForMember(d => d.FrontendFields, o => o.Ignore())
                    .ForMember(d => d.Applicazioni, o => o.Ignore())
                    .ForMember(d => d.DefaultGroup, o => o.Ignore())
                    .ForMember(d => d.RegRequestNotifyGroup, o => o.Ignore());
            });
            config.AssertConfigurationIsValid();
            var newCustomAnagrafe = new CustomAnagrafe();            
            
            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomAnagrafe);
            
            newCustomAnagrafe.CustomFields = this.CustomFields.Select(item => item.Clone(newCustomAnagrafe)).ToList();
            newCustomAnagrafe.CustomGroups = this.CustomGroups.Select(item => item.Clone(newCustomAnagrafe)).ToList();
            newCustomAnagrafe.Applicazioni = this.Applicazioni.Select(item => item.Clone(newCustomAnagrafe)).ToList();
            newCustomAnagrafe.FrontendFields = newCustomAnagrafe.CustomFields.Where(x => this.FrontendFields.Where(y => y.Name == x.Name && y.Tipo == x.Tipo).Count() > 0).ToList();
            
            return newCustomAnagrafe;
        }

    }
}
