﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomComuneField : CustomForeignKeyField
    {
        public CustomComuneField()
        {
            
        }

        public override CustomField.TipoProprietà Tipo
        {
            get { return TipoProprietà.Comune; }
        }

        public override VfGeneric CampoInAnagrafe
        {
            get
            {
                VfDropDownComune comune = new VfDropDownComune(ControlId, Name, Name, VfDropDownComune.PostObjType.Hashtable) { Required = this.Required, BindField = false };
                if (!string.IsNullOrEmpty(InfoDescription))
                    comune.InfoControl.Controls.Add(DescriptionControl);
                return comune;
            }
        }

        public override List<VfGeneric> FormConfigurazione
        {
            get
            {
                if (_FormConfigurazione == null)
                {
                    _FormConfigurazione = new List<VfGeneric>();
                    _FormConfigurazione.AddRange(CommonFields);
                }
                return _FormConfigurazione;
            }
        }
        private List<VfGeneric> _FormConfigurazione;

        public override string SystemDataType
        {
            get { return "NetCms.Users.CustomAnagrafe.Comune"; }
        }

        public override CustomField Clone(CustomAnagrafe ca)
        {           
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomComuneField, CustomComuneField>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Anagrafe, o => o.Ignore())
                .ForMember(d => d.CustomGroup, o => o.Ignore())
                .ForMember(d => d.DependencyValuesCollection, o => o.Ignore());
            });
            config.AssertConfigurationIsValid();
            var newCustomComuneField = new CustomComuneField();
            
            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomComuneField);

            newCustomComuneField.Anagrafe = ca;
            newCustomComuneField.DependencyValuesCollection = this.DependencyValuesCollection.Select(item => item.Clone(newCustomComuneField)).ToList();
            //newCustomFileField.CustomGroup = this.CustomGroup.Clone(ca);
            return newCustomComuneField;
        }
    }
}

