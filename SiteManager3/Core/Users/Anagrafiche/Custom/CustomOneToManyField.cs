﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Users.CustomAnagrafe
{
    public abstract class CustomOneToManyField : CustomField
    {
        public override MappingEffect EffettoSuMapping
        {
            get
            {
                return MappingEffect.OneToMany;
            }
        }

        public override string DBDataType
        {
            get { throw new NotImplementedException(); }
        }

    }
}
