﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
using NHibernate;
using System.Collections;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomForeignKeyToMappedTableField : CustomForeignKeyField
    {
        // sarà il namespace completo della classe mappata con cui effettuare l'associazione
        public virtual string MappedEntityName
        {
            get;
            set;
        }

        // sarà il nome della proprietà della classe mappata usata come etichetta nella dropdown di scelta
        public virtual string PropertyName
        {
            get;
            set;
        }

        // sarà il nome della proprietà ID della classe mappata. di default sarà utilizzato 'ID'
        public virtual string CustomId
        {
            get;
            set; 
        }
        public CustomForeignKeyToMappedTableField()
        {

        }

        public override CustomField.TipoProprietà Tipo
        {
            get { return TipoProprietà.MoltiAUnoAClasseMappata; }
        }

        public override VfGeneric CampoInAnagrafe
        {
            get
            {
                VfDropDownForMappedObjects dropdwon = new VfDropDownForMappedObjects(ControlId, Name, MappedEntityName, PropertyName,CustomId) { Required = this.Required, BindField = false };

                using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = sess.BeginTransaction())
                {
                    ISession mapSession = sess.GetSession(EntityMode.Map);
                    dropdwon.Options.Add("", "");
                    string id = "";
                   
                    foreach (IDictionary obj in mapSession.CreateCriteria(MappedEntityName).List())
                    {
                        if (String.IsNullOrEmpty(CustomId))
                        {
                            id = obj["ID"].ToString();
                        }
                        else
                        {
                            id = obj[CustomId].ToString();
                        }
                        dropdwon.Options.Add(obj[PropertyName].ToString(), id);
                    }
                    tx.Commit();
                }
                
                if (!string.IsNullOrEmpty(InfoDescription))
                    dropdwon.InfoControl.Controls.Add(DescriptionControl);
                return dropdwon;
            }
        }

        public override List<VfGeneric> FormConfigurazione
        {
            get
            {
                if (_FormConfigurazione == null)
                {
                    _FormConfigurazione = new List<VfGeneric>();
                    _FormConfigurazione.AddRange(CommonFields);

                    VfTextBox mappedEntityName = new VfTextBox("mappedEntityName", "Namespace della classe mappata", "MappedEntityName", InputTypes.Text);
                    mappedEntityName.InfoControl.Controls.Add(new LiteralControl("Sarà il namespace della classe mappata con la quale effettuare l'associazione molti a uno"));

                    VfTextBox propertyName = new VfTextBox("propertyName", "Nome della proprietà usata come etichetta", "PropertyName", InputTypes.Text);
                    propertyName.InfoControl.Controls.Add(new LiteralControl("Sarà il nome della proprietà della classe mappata usata come etichetta nella dropdown"));

                    VfTextBox CustomId = new VfTextBox("customId", "Nome della proprietà usata come ID", "CustomId", InputTypes.Text);
                    CustomId.InfoControl.Controls.Add(new LiteralControl("Sarà il nome della proprietà ID della classe mappata usata come identificativo nella dropdown. Se non settata , verrà usato 'ID'."));
                    CustomId.Required = false;
                    _FormConfigurazione.Add(mappedEntityName);
                    _FormConfigurazione.Add(propertyName);
                    _FormConfigurazione.Add(CustomId);

                }
                return _FormConfigurazione;
            }
        }
        private List<VfGeneric> _FormConfigurazione;

        public override string SystemDataType
        {
            get { return MappedEntityName; }
        }

        public override CustomField Clone(CustomAnagrafe ca)
        {
           
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomForeignKeyToMappedTableField, CustomForeignKeyToMappedTableField>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Anagrafe, o => o.Ignore())
                .ForMember(d => d.CustomGroup, o => o.Ignore())
                .ForMember(d => d.DependencyValuesCollection, o => o.Ignore());
            });

            config.AssertConfigurationIsValid();
            
            var newCustomForeignKeyToMappedTableField = new CustomForeignKeyToMappedTableField();                       

            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomForeignKeyToMappedTableField);

            newCustomForeignKeyToMappedTableField .DependencyValuesCollection = this.DependencyValuesCollection.Select(item => item.Clone(newCustomForeignKeyToMappedTableField)).ToList();
            newCustomForeignKeyToMappedTableField.Anagrafe = ca;
            //newCustomForeignKeyToMappedTableField.CustomGroup = this.CustomGroup.Clone(ca);
            return newCustomForeignKeyToMappedTableField;
        }
    }
}

