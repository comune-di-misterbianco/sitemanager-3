﻿
using AutoMapper;
namespace NetCms.Users.CustomAnagrafe
{
    public class CustomCheckboxListValue : LabelValue<CustomCheckboxListField>
    {
        public CustomCheckboxListValue()
        { }


        public override CustomCheckboxListField CustomField
        {
            get
            {
                return _CustomField;
            }
            set
            {
                _CustomField = value;
            }
        }
        private CustomCheckboxListField _CustomField;

        public virtual CustomCheckboxListValue Clone(CustomCheckboxListField newCustomCheckboxListField)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomCheckboxListValue, CustomCheckboxListValue>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.CustomField, o => o.Ignore());
            });
            config.AssertConfigurationIsValid();
            var newCustomCheckboxListValue = new CustomCheckboxListValue();                     

            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomCheckboxListValue);

            newCustomCheckboxListValue.CustomField = newCustomCheckboxListField;
            return newCustomCheckboxListValue;
        }

    }
}
