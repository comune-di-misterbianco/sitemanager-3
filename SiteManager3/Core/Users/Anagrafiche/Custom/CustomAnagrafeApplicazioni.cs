﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomAnagrafeApplicazioni
    {
        public CustomAnagrafeApplicazioni()
        {

        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual CustomAnagrafe Anagrafe
        {
            get;
            set;
        }

        public virtual int IdApplicazione
        {
            get;
            set;
        }

        public virtual CustomAnagrafeApplicazioni Clone(CustomAnagrafe ca)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomAnagrafeApplicazioni, CustomAnagrafeApplicazioni>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Anagrafe, o => o.Ignore());
            });
            config.AssertConfigurationIsValid();
            var newCustomAnagrafeApplicazione = new CustomAnagrafeApplicazioni();
                        
            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomAnagrafeApplicazione);

            newCustomAnagrafeApplicazione.Anagrafe = ca;
            return newCustomAnagrafeApplicazione;
        }
    }
}
