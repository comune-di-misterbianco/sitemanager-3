﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomGroup
    {
        public CustomGroup()
        {
            CustomFields = new HashSet<CustomField>();
        }

        public virtual int ID { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Descrizione { get; set; }

        public virtual int Posizione { get; set; }

        public virtual CustomAnagrafe CustomAnagrafe { get; set; }

        public virtual ICollection<CustomField> CustomFields
        {
            get;
            set;
        }

        /*
         I seguenti due campi sono aggiunti per realizzare un meccanismo di dipendenza tra il gruppo e i valodi di un campo custom di tipo dropdown
         */

        //Se true, si terrà conto del campo custom da cui dipende
        public virtual bool DependsOnAnotherField
        { get; set; }

        //Id del campo custom da cui dipende il gruppo (saranno utilizzati solo campi di tipo dropdown)
        public virtual string DependencyCustomFieldName
        { get; set; }



        public virtual CustomGroup Clone(CustomAnagrafe ca)
        {         
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomGroup, CustomGroup>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.CustomAnagrafe, o => o.Ignore())
                .ForMember(d => d.CustomFields, o => o.Ignore());
            });
            
            config.AssertConfigurationIsValid();

            var newCustomGroup = new CustomGroup();
                    
            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomGroup);

            newCustomGroup.CustomAnagrafe = ca;
            newCustomGroup.CustomFields = ca.CustomFields.Where(x => this.CustomFields.Where(y=>y.Name == x.Name && y.Tipo == x.Tipo).Count()>0).ToList();
            //newCustomGroup.DependencyCustomField = ca.CustomFields.Where(x => this.CustomFields.Where(y => y.Name == x.Name && y.Tipo == x.Tipo).Count() > 0).First();
            foreach (CustomField field in newCustomGroup.CustomFields)
                field.CustomGroup = newCustomGroup;
            
            return newCustomGroup;
        }

    }
}
