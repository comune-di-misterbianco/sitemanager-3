﻿//using System;
//using System.Collections.Generic;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using NetService.Utility.ValidatedFields;
//using System.Linq;
//using NetService.Utility.RecordsFinder;
//using System.Data;
//using System.Reflection;
//using System.Collections;
//using System.Web.UI.HtmlControls;

//namespace NetCms.Users.CustomAnagrafe
//{
//    /// <summary>
//    /// Custom VF per gestire i campi Stati, Regioni, Provincie e Comuni
//    /// </summary>
//    public class VfDropDownNRPC : VfGeneric
//    {
//        public void ReadOnlyInfo()
//        {
//            this.RegioneDropDownList.Attributes["disabled"] = "disabled";
//            this.ProvinciaDropDownList.Attributes["disabled"] = "disabled";
//            this.ComuneDropDownList.Attributes["disabled"] = "disabled";
//            base.Enabled = false;
//        }

//        public enum PostObjType
//        {
//            ID,
//            Comune,
//            Hashtable
//        }

//        public PostObjType PostbackValueObjectType
//        {
//            get;
//            set;
//        }

//        public VfDropDownNRPC(string key, string labelResourceKey, string activeRecordPropertyName, PostObjType PostbackValueObjectType = PostObjType.ID)
//            : base(key, labelResourceKey, activeRecordPropertyName)
//        {
//            this.PostbackValueObjectType = PostbackValueObjectType;
//        }

//        public VfDropDownNRPC(string id, string label, PostObjType PostbackValueObjectType = PostObjType.ID)
//           : base(id, label)
//        {
//            this.PostbackValueObjectType = PostbackValueObjectType;
//        }

//        protected override void OnPreRender(EventArgs e)
//        {
//            base.OnPreRender(e);

//            this.NazioneDropDownList.Items.Clear();

//            foreach(var entry in this.OptionsNazione)
//            {
//                ListItem item = new ListItem(NetService.Localization.LabelsManager.GetLabel(entry.Key), entry.Value);
//                if (this.Request.IsPostBack)
//                {
//                    item.Selected = this.Request.IsValidString && this.Request.StringValue == entry.Value;
//                }
//                else
//                {
//                    if (!item.Selected)
//                    {
//                        string value = (DefaultValue != null) ? DefaultValue.GetType().IsEnum ? ((int)DefaultValue).ToString() : DefaultValue.ToString() : null;
//                        if (value != null && value == entry.Value)
//                            item.Selected = true;
//                    }
//                    if (!item.Selected)
//                        item.Selected = this.DefaultValue != null && this.DefaultValue is int && entry.Value == this.DefaultValue.ToString();
//                }

//                this.NazioneDropDownList.Items.Add(item);
//            }


//            this.ComuneDropDownList.Items.Clear();

//            foreach (var entry in this.OptionsComune)
//            {
//                ListItem item = new ListItem(NetService.Localization.LabelsManager.GetLabel(entry.Key), entry.Value);
//                if (this.Request.IsPostBack)
//                {
//                    item.Selected = this.Request.IsValidString && this.Request.StringValue == entry.Value;
//                }
//                else
//                {
//                    if (!item.Selected)
//                    {
//                        string value = (DefaultValue != null) ? DefaultValue.GetType().IsEnum ? ((int)DefaultValue).ToString() : DefaultValue.ToString() : null;
//                        if (value != null && value == entry.Value)
//                            item.Selected = true;
//                    }
//                    if (!item.Selected)
//                        item.Selected = this.DefaultValue != null && this.DefaultValue is int && entry.Value == this.DefaultValue.ToString();
//                }

//                this.ComuneDropDownList.Items.Add(item);
//            }

//            this.ProvinciaDropDownList.Items.Clear();

//            foreach (var entry in this.OptionsProvincia)
//            {
//                ListItem item = new ListItem(NetService.Localization.LabelsManager.GetLabel(entry.Key), entry.Value);
//                if (this.ProvinciaRequest.IsPostBack)
//                {
//                    if (this.ProvinciaRequest.IsValidString && this.ProvinciaRequest.StringValue == entry.Value)
//                        item.Selected = true;
//                }
//                else
//                {
//                    if (!item.Selected)
//                    {
//                        string value = (ProvinciaDefaultValue >= 0) ? ProvinciaDefaultValue.GetType().IsEnum ? ((int)ProvinciaDefaultValue).ToString() : ProvinciaDefaultValue.ToString() : null;
//                        if (value != null && value == entry.Value)
//                            item.Selected = true;
//                    }
//                    if (!item.Selected)
//                        item.Selected = (this.DefaultValue != null && this.DefaultValue is int && entry.Value == this.DefaultValue.ToString()) ||
//                                    (this.ProvinciaDefaultValue > 0 && entry.Value == ProvinciaDefaultValue.ToString());

//                }

//                this.ProvinciaDropDownList.Items.Add(item);
//            }

//            this.RegioneDropDownList.Items.Clear();

//            foreach (var entry in this.OptionsRegione)
//            {
//                ListItem item = new ListItem(NetService.Localization.LabelsManager.GetLabel(entry.Key), entry.Value);
//                if (this.RegioneRequest.IsPostBack)
//                {
//                    if (this.RegioneRequest.IsValidString && this.RegioneRequest.StringValue == entry.Value)
//                        item.Selected = true;
//                }
//                else
//                {
//                    if (!item.Selected)
//                    {
//                        string value = (RegioneDefaultValue >= 0) ? RegioneDefaultValue.GetType().IsEnum ? ((int)RegioneDefaultValue).ToString() : RegioneDefaultValue.ToString() : null;
//                        if (value != null && value == entry.Value)
//                            item.Selected = true;
//                    }
//                    if (!item.Selected)
//                        item.Selected = (this.DefaultValue != null && this.DefaultValue is int && entry.Value == this.DefaultValue.ToString()) ||
//                                    (this.RegioneDefaultValue > 0 && entry.Value == RegioneDefaultValue.ToString());

//                }

//                this.RegioneDropDownList.Items.Add(item);
//            }
//        }

//        public Dictionary<string, string> OptionsComune
//        {
//            get
//            {
//                if (_OptionsComune == null)
//                {
//                    _OptionsComune = new Dictionary<string, string>();
//                }
//                return _OptionsComune;
//            }
//        }
//        private Dictionary<string, string> _OptionsComune;

//        public Dictionary<string, string> OptionsProvincia
//        {
//            get
//            {
//                if (_OptionsProvincia == null)
//                {
//                    _OptionsProvincia = new Dictionary<string, string>();
//                }
//                return _OptionsProvincia;
//            }
//        }
//        private Dictionary<string, string> _OptionsProvincia;


//        public Dictionary<string, string> OptionsRegione
//        {
//            get
//            {
//                if (_OptionsRegione == null)
//                {
//                    _OptionsRegione = new Dictionary<string, string>();
//                }
//                return _OptionsRegione;
//            }
//        }
//        private Dictionary<string, string> _OptionsRegione;

//        public Dictionary<string, string> OptionsNazione
//        {
//            get
//            {
//                if (_OptionsNazione == null)
//                {
//                    _OptionsNazione = new Dictionary<string, string>();
//                }
//                return _OptionsNazione;
//            }
//        }
//        private Dictionary<string, string> _OptionsNazione;


//        public NetUtility.RequestVariable ProvinciaRequest
//        {
//            get
//            {
//                NetUtility.RequestVariable a = new NetUtility.RequestVariable(ProvinciaDropDownList.ID);
//                if (_ProvinciaRequest == null)
//                {
//                    _ProvinciaRequest = new NetUtility.RequestVariable(ProvinciaDropDownList.ID);
//                }
//                return _ProvinciaRequest;
//            }
//        }
//        private NetUtility.RequestVariable _ProvinciaRequest;


//        public NetUtility.RequestVariable RegioneRequest
//        {
//            get
//            {
//                NetUtility.RequestVariable a = new NetUtility.RequestVariable(RegioneDropDownList.ID);
//                if (_RegioneRequest == null)
//                {
//                    _RegioneRequest = new NetUtility.RequestVariable(RegioneDropDownList.ID);
//                }
//                return _RegioneRequest;
//            }
//        }
//        private NetUtility.RequestVariable _RegioneRequest;

//        public NetUtility.RequestVariable NazioneRequest
//        {
//            get
//            {
//                NetUtility.RequestVariable a = new NetUtility.RequestVariable(NazioneDropDownList.ID);
//                if (_NazioneRequest == null)
//                {
//                    _NazioneRequest = new NetUtility.RequestVariable(NazioneDropDownList.ID);
//                }
//                return _NazioneRequest;
//            }
//        }
//        private NetUtility.RequestVariable _NazioneRequest;



//        public DropDownList NazioneDropDownList
//        {
//            get
//            {
//                if (_NazioneDropDownList == null)
//                {
//                    _NazioneDropDownList = new DropDownList { ID = this.Key + "_nazione", AutoPostBack = true };
//                    _NazioneDropDownList.CssClass = "form-control";
//                    InitNazioneList();
//                }
//                return _NazioneDropDownList;
//            }
//        }
//        private DropDownList _NazioneDropDownList;


//        public DropDownList RegioneDropDownList
//        {
//            get
//            {
//                if (_RegioneDropDownList == null)
//                {
//                    _RegioneDropDownList = new DropDownList { ID = this.Key + "_regioni", AutoPostBack = true };
//                    _RegioneDropDownList.CssClass = "form-control";
//                    //    InitRegioniList();
//                    CheckNazioneRequest();
//                }
//                return _RegioneDropDownList;
//            }
//        }
//        private DropDownList _RegioneDropDownList;

//        public DropDownList ProvinciaDropDownList
//        {
//            get
//            {
//                if (_ProvinciaDropDownList == null)
//                {
//                    _ProvinciaDropDownList = new DropDownList { ID = this.Key + "_provincie", AutoPostBack = true };
//                    _ProvinciaDropDownList.CssClass = "form-control";
//                    CheckRegioneRequest();
//                    //InitProvinciaList();
//                }
//                return _ProvinciaDropDownList;
//            }
//        }
//        private DropDownList _ProvinciaDropDownList;

//        public DropDownList ComuneDropDownList
//        {
//            get
//            {
//                if (_ComuneDropDownList == null)
//                {
//                    _ComuneDropDownList = new DropDownList();
//                    _ComuneDropDownList.CssClass = "form-control";
//                    _ComuneDropDownList.ID = this.Key;

//                    CheckProvinciaRequest();
//                }
//                return _ComuneDropDownList;
//            }
//        }
//        private DropDownList _ComuneDropDownList;

//        void CheckProvinciaRequest()
//        {
//            //try
//            bool enabled = false;
//            bool selezionato = false;

//            if (ProvinciaRequest.IsPostBack && ProvinciaRequest.IsValidInteger)
//            {
//                DefaultValue = null;
//                ProvinciaDefaultValue = -1;
//                foreach (var entry in this.OptionsProvincia)
//                    if (this.ProvinciaRequest.StringValue == entry.Value)
//                        selezionato = true;
//                if (selezionato)
//                {
//                    Provincia provinciaSelezionata = CustomAnagrafeBusinessLogic.GetProvinciaById(ProvinciaRequest.IntValue);
//                    if (provinciaSelezionata != null)
//                        InitComuniList(provinciaSelezionata.ID);
//                    enabled = provinciaSelezionata != null;
//                }
//            }
//            this.ComuneDropDownList.Enabled = enabled;
//            this.Enabled = enabled;

//            //catch{}
//        }

//        void CheckRegioneRequest()
//        {
//            //try
//            bool enabled = false;
//            if (RegioneRequest.IsPostBack && RegioneRequest.IsValidInteger)
//            {
//                RegioneDefaultValue = -1;
//                ProvinciaDefaultValue = -1;
//                DefaultValue = null;
//                Regione regioneSelezionata = CustomAnagrafeBusinessLogic.GetRegioneById(RegioneRequest.IntValue);
//                if (regioneSelezionata != null)
//                {
//                    InitProvinceList(regioneSelezionata.ID);
//                }
//                enabled = regioneSelezionata != null;
//                this.ComuneDropDownList.Items.Clear();
//                this.OptionsComune.Clear();
//            }
//            this.ProvinciaDropDownList.Enabled = enabled;
//            //catch{}
//        }

//        void CheckNazioneRequest()
//        {
//            bool enabled = false;
//            if (NazioneRequest.IsPostBack && NazioneRequest.IsValidInteger && NazioneRequest.IntValue != -1)
//            {
//                // nascondere gli altri dropdownlist regione provincia comune
//                RegioneDropDownList.Visible = false;
//                ProvinciaDropDownList.Visible = false;
//            }
//            else // di default selezionata italia con valore a -1
//            {
//                RegioneDefaultValue = -1;
//                ProvinciaDefaultValue = -1;
//                DefaultValue = null;
//                InitRegioniList();
//            }
//        }

//        protected virtual void InitNazioneList()
//        {
//            this.OptionsNazione.Add("Italia", "-1");

//            IList<Comune> nazioni = CustomAnagrafeBusinessLogic.FindComuniByIdProv(1);
//            foreach (Comune nazione in nazioni)
//                OptionsNazione.Add(nazione.Nome, nazione.ID.ToString());
//        }

//        protected virtual void InitRegioniList()
//        {
//            this.OptionsRegione.Add("", "");

//            IList<Regione> regioni = CustomAnagrafeBusinessLogic.FindRegioni();

//            foreach (Regione reg in regioni)
//                OptionsRegione.Add(reg.Nome, reg.ID.ToString());
//        }

//        protected virtual void InitProvinceList(int idRegione)
//        {
//            if (idRegione > 0)
//            {
//                IList<Provincia> provincie = CustomAnagrafeBusinessLogic.FindProvinceByIdReg(idRegione);
//                this.OptionsProvincia.Clear();
//                this.OptionsProvincia.Add("", "");
//                foreach (Provincia prov in provincie)
//                    this.OptionsProvincia.Add(prov.Nome, prov.ID.ToString());
//            }
//        }

//        protected virtual void InitComuniList(int idProvincia)
//        {
//            if (idProvincia > 0)
//            {
//                IList<Comune> comuni = CustomAnagrafeBusinessLogic.FindComuniByIdProv(idProvincia);
//                OptionsComune.Clear();
//                OptionsComune.Add("", "");
//                foreach (Comune comune in comuni)
//                    OptionsComune.Add(comune.Nome, comune.ID.ToString());
//            }
//            else
//            {
//                this.ComuneDropDownList.Items.Clear();
//                OptionsComune.Clear();
//                OptionsComune.Add("", "");
//            }
//        }


//        protected override void AttachFieldControls()
//        {
//            Label selezionaNazione = new Label { AssociatedControlID = NazioneDropDownList.ID, Text = "Seleziona Nazione" + (this.Required ? " *" : ""), CssClass = "control-label vf-label-nazione" };
//            Label selezionaRegione = new Label { AssociatedControlID = RegioneDropDownList.ID, Text = "Seleziona Regione" + (this.Required ? " *" : ""), CssClass = "control-label vf-label-regione" };
//            Label selezionaProvincia = new Label { AssociatedControlID = ProvinciaDropDownList.ID, Text = "Seleziona Provincia" + (this.Required ? " *" : ""), CssClass = "control-label vf-label-provincia" };
//            Label selezionaComune = new Label { AssociatedControlID = ComuneDropDownList.ID, Text = "Seleziona Comune" + (this.Required ? " *" : ""), CssClass = "control-label vf-label-comune" };

//            //h5.ViewStateMode = ViewStateMode.Disabled;

//            selezionaNazione.ViewStateMode = ViewStateMode.Disabled;
//            selezionaRegione.ViewStateMode = ViewStateMode.Disabled;
//            selezionaProvincia.ViewStateMode = ViewStateMode.Disabled;
//            selezionaComune.ViewStateMode = ViewStateMode.Disabled;

//            NazioneDropDownList.ViewStateMode = ViewStateMode.Disabled;
//            RegioneDropDownList.ViewStateMode = ViewStateMode.Disabled;
//            ProvinciaDropDownList.ViewStateMode = ViewStateMode.Disabled;
//            ComuneDropDownList.ViewStateMode = ViewStateMode.Disabled;

//            this.Enabled = true;
//            this.ViewStateMode = ViewStateMode.Disabled;

//            //this.Controls.Add(h5);

//            this.Controls.Add(selezionaNazione);
//            this.Controls.Add(NazioneDropDownList);

//            this.Controls.Add(selezionaRegione);
//            this.Controls.Add(RegioneDropDownList);

//            this.Controls.Add(new LiteralControl("<div class=\"form-group\"></div>"));
//            this.Controls.Add(selezionaProvincia);
//            this.Controls.Add(ProvinciaDropDownList);

//            this.Controls.Add(new LiteralControl("<div class=\"form-group\"></div>"));
//            this.Controls.Add(selezionaComune);
//            this.Controls.Add(ComuneDropDownList);

//            this.Field.ID = this.Key;
//        }

//        protected override void LocalValidate()
//        {
//            if (!this.Request.IsValidString && this.Required)
//            {
//                this.AddError(string.Format(NetService.Localization.LabelsManager.GetLabel("isrequired"), Label));
//            }
//        }

//        public int ProvinciaDefaultValue
//        {
//            get
//            {
//                return _ProvinciaDefaultValue;
//            }
//            set
//            {
//                _ProvinciaDefaultValue = value;
//            }
//        }
//        private int _ProvinciaDefaultValue;

//        public int RegioneDefaultValue
//        {
//            get
//            {
//                return _RegioneDefaultValue;
//            }
//            set
//            {
//                _RegioneDefaultValue = value;
//            }
//        }
//        private int _RegioneDefaultValue;


//        protected override void FillFieldValue()
//        {
//            if (this.RegioneDefaultValue > 0)
//            {
//                this.RegioneDropDownList.SelectedValue = this.RegioneDefaultValue.ToString();
//                Regione regioneSelezionata = CustomAnagrafeBusinessLogic.GetRegioneById(RegioneDefaultValue);
//                if (regioneSelezionata != null)
//                {
//                    InitProvinceList(regioneSelezionata.ID);
//                    this.ProvinciaDropDownList.Enabled = true;
//                }
//            }
//            else
//            {
//                if (this.ProvinciaDefaultValue > 0)
//                {
//                    this.ProvinciaDropDownList.SelectedValue = this.ProvinciaDefaultValue.ToString();
//                    Provincia provinciaSelezionata = CustomAnagrafeBusinessLogic.GetProvinciaById(ProvinciaDefaultValue);
//                    if (provinciaSelezionata != null)
//                    {
//                        InitComuniList(provinciaSelezionata.ID);
//                        this.ComuneDropDownList.Enabled = true;
//                    }
//                }
//                else
//                {
//                    //if (this.DefaultValue != null && (this.DefaultValue is int || this.DefaultValue is Comune))
//                    if (this.DefaultValue != null)
//                    {
//                        switch (PostbackValueObjectType)
//                        {
//                            case PostObjType.Comune:
//                                //Se ho istanziato una dropdown che restituisce un comune, mi aspetto come default value una hashtable
//                                // da cui prendere l'id del comune. Sostituisco infine il defaultValue con l'id estratto dalla HashTable

//                                DefaultValue = (DefaultValue as Comune).ID;
//                                break;
//                            case PostObjType.Hashtable:
//                                DefaultValue = (int)(DefaultValue as Hashtable)["ID"];
//                                break;
//                        }
//                        Comune defaultComune = CustomAnagrafeBusinessLogic.GetComuneById((int)DefaultValue);
//                        ProvinciaDefaultValue = defaultComune.Provincia.ID;
//                        RegioneDefaultValue = defaultComune.Provincia.Regione.ID;

//                        InitComuniList(defaultComune.Provincia.ID);
//                        InitProvinceList(RegioneDefaultValue);
//                        this.ProvinciaDropDownList.Enabled = true;
//                        this.ComuneDropDownList.Enabled = true;
//                    }
//                }
//            }
//        }

//        public override WebControl Field
//        {
//            get { return ComuneDropDownList; }
//        }

//        public override object PostbackValueObject
//        {
//            get
//            {
//                int res;
//                Comune comune;

//                switch (PostbackValueObjectType)
//                {
//                    case PostObjType.ID:
//                        if (int.TryParse(this.Request.OriginalValue, out res))
//                            return int.Parse(this.Request.OriginalValue);
//                        break;
//                    case PostObjType.Comune:
//                        if (int.TryParse(this.Request.OriginalValue, out res))
//                            return CustomAnagrafeBusinessLogic.GetComuneById(int.Parse(this.Request.OriginalValue));
//                        break;
//                    case PostObjType.Hashtable:
//                        if (int.TryParse(this.Request.OriginalValue, out res))
//                        {
//                            Hashtable dati = new Hashtable();
//                            comune = CustomAnagrafeBusinessLogic.GetComuneById(int.Parse(this.Request.OriginalValue));
//                            PropertyInfo[] proprieta = comune.GetType().GetProperties();
//                            foreach (PropertyInfo pi in proprieta)
//                            {
//                                dati.Add(pi.Name, pi.GetValue(comune, null));
//                            }
//                            return dati;
//                        }
//                        break;
//                }

//                //return this.Request.OriginalValue;
//                return null;
//            }
//        }

//        public override VfSearchParameter SearchParameter
//        {
//            get
//            {
//                if (_SearchParameter == null)
//                {
//                    _SearchParameter = new VfDropDownListSearchParameter(this, SearchComparisonCriteria);
//                }
//                return _SearchParameter;
//            }
//        }
//        private VfSearchParameter _SearchParameter;

//        public override Finder.ComparisonCriteria SearchComparisonCriteria
//        {
//            get { return Finder.ComparisonCriteria.Equals; }
//            set { }
//        }

//        public override string ValueForDetails
//        {
//            get
//            {
//                string value = string.Empty;
//                if (Request.IsValidInteger)
//                {
//                    var comuneData = CustomAnagrafeBusinessLogic.GetComuneById(Request.IntValue);
//                    if (comuneData != null)
//                    {
//                        value = comuneData.Nome;//comuneData["comune"].ToString();
//                    }
//                }
//                return value;
//            }
//        }

//        public override string LocalCssClass
//        {
//            get
//            {
//                return "dropdown-vf dropdown-vf-comune";
//            }
//        }
//    }
//}
