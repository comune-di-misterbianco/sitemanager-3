﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetService.Utility.RecordsFinder;
using NetService.Utility.ValidatedFields;
using System.Collections;
using System.Reflection;
using NHibernate.Proxy;

namespace NetCms.Users.CustomAnagrafe
{
    public class VfDropDownProvince : VfDropDown
    {
        public enum PostObjType
        {
            ID,
            Provincia,
            Hashtable
        }

        public PostObjType PostbackValueObjectType
        {
            get;
            set;
        }

        public override object PostbackValueObject
        {
            get
            {
                int res;
                Provincia provincia;

                switch (PostbackValueObjectType)
                {
                    case PostObjType.ID:
                        if (int.TryParse(this.Request.OriginalValue, out res))
                            return int.Parse(this.Request.OriginalValue);
                        break;
                    case PostObjType.Provincia:
                        if (int.TryParse(this.Request.OriginalValue, out res))
                            return CustomAnagrafeBusinessLogic.GetProvinciaById(int.Parse(this.Request.OriginalValue));
                        break;
                    case PostObjType.Hashtable:
                        if (int.TryParse(this.Request.OriginalValue, out res))
                        {
                            Hashtable dati = new Hashtable();
                            provincia = CustomAnagrafeBusinessLogic.GetProvinciaById(int.Parse(this.Request.OriginalValue));
                            PropertyInfo[] proprieta = provincia.GetType().GetProperties();
                            foreach (PropertyInfo pi in proprieta)
                            {
                                dati.Add(pi.Name, pi.GetValue(provincia, null));
                            }
                            return dati;
                        }
                        break;
                }
                
                return null;
            }
        }

        public VfDropDownProvince(string key, string labelResourceKey, string activeRecordPropertyName, PostObjType PostbackValueObjectType = PostObjType.ID)
            : base(key, labelResourceKey, activeRecordPropertyName)
        {
            this.PostbackValueObjectType = PostbackValueObjectType;
        }

        public VfDropDownProvince(string id, string label, PostObjType PostbackValueObjectType = PostObjType.ID)
            : base(id, label)
        {
            this.PostbackValueObjectType = PostbackValueObjectType;
        }

        protected override void FillFieldValue()
        {
            if (this.DefaultValue != null)
            {
                int idProv = 0;
                switch (PostbackValueObjectType)
                {
                    case PostObjType.ID:
                        idProv = (int)DefaultValue;
                        break;
                    case PostObjType.Provincia:
                        idProv = (DefaultValue as Provincia).ID;
                        break;
                    case PostObjType.Hashtable:
                        if (DefaultValue is INHibernateProxy)
                            idProv = (int)( CustomAnagrafeBusinessLogic.GetCurrentMapSession().GetSessionImplementation().PersistenceContext.Unproxy(DefaultValue) as Hashtable)["ID"];
                        else
                            idProv = (int)(DefaultValue as Hashtable)["ID"];
                        break;
                }
                this.DefaultValue = idProv;
            }
        }

        public override string ValueForDetails
        {
            get
            {
                string value = string.Empty;
                if (Request.IsValidInteger)
                {
                    var provinciaData = CustomAnagrafeBusinessLogic.GetProvinciaById(Request.IntValue);
                    if (provinciaData != null)
                    {
                        value = provinciaData.Nome;
                    }
                }
                return value;
            }
        }

        protected override void AttachFieldControls()
        {
            //si è reso necessario mettere questo comando perchè di default la dropdownlist.id è null. se manca l'id la label diventa uno span e non funziona la classe bootstrap
            DropDownList.ID = this.Key;
            
            Label selezionaProvincia = new Label { AssociatedControlID = DropDownList.ID, Text = "Provincia" + (this.Required ? " *" : ""), CssClass = "control-label vf-label-provincia" };

            selezionaProvincia.ViewStateMode = ViewStateMode.Disabled;
            DropDownList.ViewStateMode = ViewStateMode.Disabled;


            this.Enabled = true;
            this.ViewStateMode = ViewStateMode.Disabled;
            
            WebControl div2 = new WebControl(HtmlTextWriterTag.Div);
            div2.CssClass = "bootstrap-select-wrapper";
            
            div2.Controls.Add(selezionaProvincia);
            div2.Controls.Add(DropDownList);
            this.Controls.Add(div2);                   

            this.Field.ID = this.Key;
        }

        public override string LocalCssClass
        {
            get
            {
                return "dropdown-vf dropdown-vf-comune";
            }
        }
    }
}
