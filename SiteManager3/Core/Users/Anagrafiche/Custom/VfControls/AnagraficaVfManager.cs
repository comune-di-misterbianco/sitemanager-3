﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
using System.Collections;

namespace NetCms.Users.CustomAnagrafe
{
    public class AnagraficaVfManager : VfManager
    {

        public bool UsedOnFrontend = false;
        private bool _AddCaptcha;
        private bool _AddPrivacy;
        public bool AddCaptcha { get { return _AddCaptcha; } set { _AddCaptcha = value; } }
        public bool AddPrivacy { get { return _AddPrivacy; } set { _AddPrivacy = value; } }

        public CustomAnagrafe CustomAnagrafe
        {
            get;
            private set;
        }

        public User User
        {
            get;
            private set;
        }

        public Hashtable AnagraficaData
        {
            get
            {
                if (_AnagraficaData == null)
                {
                    _AnagraficaData = UsersBusinessLogic.GetAnagraficaMapUser(User);
                }
                return _AnagraficaData;
            }
        }
        private Hashtable _AnagraficaData;

        private bool _UseBootstrapItalia;

        public bool UseBootstrapItalia
        {
            get { return _UseBootstrapItalia; }
            set { _UseBootstrapItalia = value; }
        }

        private LabelsManager.Labels Labels
        {

            get
            {
                return LabelsManager.LabelsCache.GetTypedLabels(typeof(NotificheLabels)) as LabelsManager.Labels;
            }
        }

        public AnagraficaVfManager
            (
            string controlID, 
            bool isPostBack, 
            CustomAnagrafe ca, 
            bool addSubmitButton,            
            User user = null, 
            bool isfrontend = false, 
            bool addCaptcha = true, 
            bool addPrivacy = true, 
            bool disableEmailField = false             
            ) 
            : base(controlID, isPostBack, isfrontend ? VfManagerRenderType.Frontend : VfManagerRenderType.Backoffice)
        {            
            CustomAnagrafe = ca;
            User = user;
            
            this.AddCaptcha = addCaptcha;
            this.AddPrivacy = addPrivacy;

            this.AddSubmitButton = addSubmitButton;
            
            this.DisableEmailField = disableEmailField;


            bool isUpdate = User != null;

            if (CustomAnagrafe != null && CustomAnagrafe.RenderType != null && CustomAnagrafe.RenderType == CustomAnagrafe.RenderTypes.Normale)
            {
                VfTextBox fieldEmail = new VfTextBox("Email", Labels[RegistrazioneLabels.RegistrazioneLabelsList.Email], "Email", InputTypes.Email);
                fieldEmail.Required = true;
                fieldEmail.BindField = false;
                fieldEmail.ConfermaValoreInserito = CustomAnagrafe.RichiediConfermaEmail;
                fieldEmail.UseBootstrapItalia = UseBootstrapItalia;
                if (isUpdate)
                {
                    fieldEmail.TextBox.Text = AnagraficaData["Email"].ToString();
                    if (CustomAnagrafe.RichiediConfermaEmail)
                        fieldEmail.TextBoxConfirm.Text = AnagraficaData["Email"].ToString();
                    //if (CustomAnagrafe.EmailAsUserName)
                    //    fieldEmail.Enabled = false;
                }

                this.Fields.Add(fieldEmail);
                
                if (ca != null && ca.CustomFields.Count > 0)
                {
                    foreach (CustomField field in ca.CustomFields)
                    {
                        VfGeneric vfField = field.CampoInAnagrafe;
                        vfField.UseBootstrapItalia = UseBootstrapItalia;

                        if (isUpdate)
                            vfField.DefaultValue = AnagraficaData[field.Name];

                        this.Fields.Add(vfField);
                    }
                }

                if (!isUpdate)
                {
                    if (CustomAnagrafe.AggiungiInformativaPrivacy && AddPrivacy)
                    {
                        //VfTermsPrivacyRadioButton termsPrivacy = new VfTermsPrivacyRadioButton(CustomAnagrafe.DatabaseName + "_terms", "Condizioni d'uso e privacy");
                        //termsPrivacy.LabelInformativa = CustomAnagrafe.LabelInformativaPrivacy;
                        //termsPrivacy.TestoInformativa = CustomAnagrafe.TestoInformativaPrivacy;
                        //termsPrivacy.Columns = CustomAnagrafe.NumeroCaratteriDaVisualizzareAreaInformativa;
                        //termsPrivacy.Rows = CustomAnagrafe.NumeroRigheDaVisualizzareAreaInformativa;
                        //termsPrivacy.LabelDomandaAccettazione = CustomAnagrafe.DomandaAccettazioneInformativaPrivacy;
                        //  this.Fields.Add(termsPrivacy);
                        this.Fields.Add(TermsPrivacyField);
                    }
                    
                    if (CustomAnagrafe.ValidaConCaptcha && AddCaptcha)
                    {
                        //VfCaptcha captcha = new VfCaptcha(CustomAnagrafe.DatabaseName + "_captcha", "Codice di controllo");
                        //captcha.Required = true;
                        //this.Fields.Add(captcha);

                       // VfReCaptcha reCaptcha = new VfReCaptcha("recaptcha");
                        //this.Fields.Add(reCaptcha);
                        this.Fields.Add(ReCaptchaField);
                    }
                }
            }

            UsedOnFrontend = isfrontend;
        }

        private bool IsInFront
        {
            get
            {
                bool isinfront = true;

                if (this.Context.Items.Contains("SM2_CurrentFace"))
                    if ((NetCms.Faces)this.Context.Items["SM2_CurrentFace"] == Faces.Admin)
                        isinfront = false;

                return isinfront;
            }

        }

        private VfTermsPrivacyRadioButton _TermsPrivacyfield;
        public VfTermsPrivacyRadioButton TermsPrivacyField 
        {
            get
            {
                if (_TermsPrivacyfield == null)
                {
                    _TermsPrivacyfield = new VfTermsPrivacyRadioButton(CustomAnagrafe.DatabaseName + "_terms", Labels[RegistrazioneLabels.RegistrazioneLabelsList.TermsAndConditions], true,true);
                    _TermsPrivacyfield.LabelInformativa = CustomAnagrafe.LabelInformativaPrivacy;
                    _TermsPrivacyfield.TestoInformativa = CustomAnagrafe.TestoInformativaPrivacy;
                    _TermsPrivacyfield.Columns = CustomAnagrafe.NumeroCaratteriDaVisualizzareAreaInformativa;
                    _TermsPrivacyfield.Rows = CustomAnagrafe.NumeroRigheDaVisualizzareAreaInformativa;
                    _TermsPrivacyfield.LabelDomandaAccettazione = CustomAnagrafe.DomandaAccettazioneInformativaPrivacy;
                    _TermsPrivacyfield.UseBootstrapItalia = UseBootstrapItalia;
                }
                return _TermsPrivacyfield;
            }
        }

        private VfReCaptcha _ReCaptchaField;
        public VfReCaptcha ReCaptchaField { 
            get
            {
                if (_ReCaptchaField == null)
                {
                    _ReCaptchaField = new VfReCaptcha("recaptcha");
                    _ReCaptchaField.UseBootstrapItalia = UseBootstrapItalia;
                }
                return _ReCaptchaField;
            }
        }

        public bool DisableEmailField
        {
            get;
            set;
        }


        protected override void OnInit(EventArgs e)
        {
            bool isUpdate = User != null;

            switch (CustomAnagrafe.RenderType)
            {
                case CustomAnagrafe.RenderTypes.FieldSet:
                    {
                        WebControl risFinale = new WebControl(HtmlTextWriterTag.Div);
                        risFinale.CssClass = "risFinale";

                        VfTextBox fieldEmail = new VfTextBox("Email", Labels[RegistrazioneLabels.RegistrazioneLabelsList.Email], "Email", InputTypes.Email);
                        fieldEmail.Required = true;
                        fieldEmail.BindField = false;
                        fieldEmail.ConfermaValoreInserito = CustomAnagrafe.RichiediConfermaEmail;
                        fieldEmail.UseBootstrapItalia = UseBootstrapItalia;

                        if (DisableEmailField)
                            fieldEmail.Field.Attributes["disabled"] = "disabled";

                        if (isUpdate)
                        {
                            fieldEmail.DefaultValue = AnagraficaData["Email"].ToString();
                            if (CustomAnagrafe.RichiediConfermaEmail)
                                fieldEmail.TextBoxConfirm.Text = AnagraficaData["Email"].ToString();
                        }

                        this.Fields.Add(fieldEmail);

                        BuildFieldset(new List<string>() { fieldEmail.Key }, Labels[RegistrazioneLabels.RegistrazioneLabelsList.Email], "", risFinale); //Il campo email è obbligatorio da sistema

                        foreach (CustomGroup group in CustomAnagrafe.CustomGroups.OrderBy(x=>x.Posizione))
                        {
                            List<string> listaKeyCampi = new List<string>();

                            //Se il gruppo non dipende da una dropdown, lo visualizzo, altrimenti verifico il valore del campo da cui dipende
                            bool printGroupField = true;

                            //Se il gruppo non dipende da una dropdown, stampo il suo nome, altrimenti lo sostituisco con la label del valore selezionato
                            string nomeGruppo = group.Nome;

                            //Memorizzerà l'eventuale valore della dropdown di dipendenza che è stato selezionato
                            string valueDDDependency = null;

                            if (group.DependsOnAnotherField)
                            {
                                //Se il gruppo dipende da una dropdown, qual ora il postback value della dd è nullo o vuoto, non visualizzo il gruppo e neanche i suoi campi
                                if (String.IsNullOrEmpty(this.Fields.First(x => x.Key == CustomAnagrafe.CustomFields.First(y => y.Name == group.DependencyCustomFieldName).ControlId).PostBackValue))
                                    printGroupField = false;
                                else
                                {
                                    //Recupero l'eventuale valore della dropdown di dipendenza che è stato selezionato
                                    valueDDDependency = this.Fields.First(x => x.Key == CustomAnagrafe.CustomFields.First(y => y.Name == group.DependencyCustomFieldName).ControlId).PostBackValue;
                                    //Modifico il nome del gruppo in funzione del valore selezionato
                                    nomeGruppo = CustomAnagrafeBusinessLogic.GetCustomDropdownFieldById(CustomAnagrafe.CustomFields.First(y => y.Name == group.DependencyCustomFieldName).ID).Values.First(v => v.Value == valueDDDependency).Label;
                                }

                                if (!printGroupField)
                                {
                                    if (isUpdate && !IsPostBack)
                                    {

                                        //Recupero l'eventuale valore della dropdown di dipendenza che è stato selezionato
                                        valueDDDependency = this.Fields.First(x => x.Key == CustomAnagrafe.CustomFields.First(y => y.Name == group.DependencyCustomFieldName).ControlId).DefaultValue.ToString();
                                        //Modifico il nome del gruppo in funzione del valore selezionato
                                        nomeGruppo = CustomAnagrafeBusinessLogic.GetCustomDropdownFieldById(CustomAnagrafe.CustomFields.First(y => y.Name == group.DependencyCustomFieldName).ID).Values.First(v => v.Value == valueDDDependency).Label;
                                        printGroupField = true;
                                    }
                                }
                            }

                            if (printGroupField)
                            {
                                foreach (CustomField field in group.CustomFields.OrderBy(x => x.PositionInGroup).ThenBy(x=>x.RowOrder))
                                {
                                    VfGeneric vfField = field.CampoInAnagrafe;
                                    vfField.ColumCssClass = field.ColumnCssClass;
                                    vfField.RowOrder = field.RowOrder;
                                    vfField.UseBootstrapItalia = UseBootstrapItalia;

                                    if (!field.Enabled)
                                    vfField.Field.Attributes["disabled"] = "disabled";

                                    if (vfField is VfDropDown)
                                    {
                                        vfField.EnableViewState = false;
                                        if (CustomAnagrafe.CustomGroups.Count(x => x.DependsOnAnotherField && x.DependencyCustomFieldName == field.Name) > 0)
                                            ((VfDropDown)vfField).DropDownList.AutoPostBack = true;
                                    }

                                    if (isUpdate)
                                        vfField.DefaultValue = AnagraficaData[field.Name];

                                    if (isUpdate && field.Tipo == CustomField.TipoProprietà.File)
                                        vfField.Required = false;

                                    //Se il gruppo non dipende da una dropdown, permane la logica precedente di stampare tutti i campi
                                    if (!group.DependsOnAnotherField)
                                    {
                                        this.Fields.Add(vfField);
                                        listaKeyCampi.Add(vfField.Key);
                                    }
                                    else
                                    {
                                        //Se il gruppo dipende da una dropdown, verifico che il campo da stampare abbia associato il valore della dd selezionata
                                        //altrimenti non viene appeso
                                        if (field.DependencyValuesCollection.Count(x => x.DependencyDDValue == valueDDDependency) > 0)
                                        {
                                            if (field.DependencyValuesCollection.First(x => x.DependencyDDValue == valueDDDependency).isRequiredForValue)
                                                vfField.Required = true;

                                            this.Fields.Add(vfField);
                                            listaKeyCampi.Add(vfField.Key);
                                        }
                                    }
                                }
                                BuildFieldset(listaKeyCampi, nomeGruppo, group.Descrizione,risFinale);
                            }
                        }

                        if (!isUpdate && IsInFront)
                        {
                            List<string> listaCampi = new List<string>();


                            if (CustomAnagrafe.AggiungiInformativaPrivacy && AddPrivacy)
                            {
                                VfTermsPrivacyRadioButton termsPrivacy = new VfTermsPrivacyRadioButton(CustomAnagrafe.DatabaseName + "_terms", Labels[RegistrazioneLabels.RegistrazioneLabelsList.TermsAndConditions], true);
                                termsPrivacy.UseBootstrapItalia = UseBootstrapItalia;
                                termsPrivacy.LabelInformativa = CustomAnagrafe.LabelInformativaPrivacy;
                                termsPrivacy.TestoInformativa = CustomAnagrafe.TestoInformativaPrivacy;
                                termsPrivacy.Columns = CustomAnagrafe.NumeroCaratteriDaVisualizzareAreaInformativa;
                                termsPrivacy.Rows = CustomAnagrafe.NumeroRigheDaVisualizzareAreaInformativa;
                                termsPrivacy.LabelDomandaAccettazione = CustomAnagrafe.DomandaAccettazioneInformativaPrivacy;
                                this.Fields.Add(termsPrivacy);
                                listaCampi.Add(termsPrivacy.Key);
                            }

                            if (CustomAnagrafe.ValidaConCaptcha && AddCaptcha)
                            {
                                VfReCaptcha reCaptcha = new VfReCaptcha("recaptcha");
                                reCaptcha.UseBootstrapItalia = UseBootstrapItalia;
                                this.Fields.Add(reCaptcha);

                                listaCampi.Add(reCaptcha.Key);
                            }
                            if (listaCampi.Any())
                                BuildFieldset(listaCampi, "", "", risFinale);
                        }

                        InitVfManager(risFinale);

                        break;
                    }
                default: 
                    base.OnInit(e);                    
                    break;
            }
        }

        private WebControl BuildFieldset(List<string> listaKeyCampi, string titolo, string descrizione)
        {
            if (UsedOnFrontend)
            {
                WebControl fieldset = new WebControl(HtmlTextWriterTag.Div);
                fieldset.CssClass = "row";

                WebControl innerRow = new WebControl(HtmlTextWriterTag.Div);
                innerRow.CssClass = "col-md-12";

                WebControl legend = new WebControl(HtmlTextWriterTag.Div);
                legend.CssClass = "page-header";
                legend.Controls.Add(new LiteralControl("<h4>" + titolo + "</h4>"));
                innerRow.Controls.Add(legend);

                if (descrizione.Length > 0)
                    innerRow.Controls.Add(new LiteralControl("<div class=\"h4\">" + descrizione + "</div>"));

                int rows = 0;
                VfGeneric vf = this.Fields.OrderByDescending(x => x.RowOrder).FirstOrDefault();
                if (vf != null)
                {
                    rows = vf.RowOrder;
                    // creo le row della form
                    for (int i = 1; i <= rows; i++)
                    {
                        WebControl row = new WebControl(HtmlTextWriterTag.Div);
                        row.CssClass = "row";
                        row.ID = "row_" + i;

                        Control ctrl = this.FindControl("row_" + i);
                        if (ctrl == null)
                            fieldset.Controls.Add(row);
                    }
                }


                foreach (VfGeneric campo in this.Fields)
                {
                    if (listaKeyCampi.Contains(campo.Key))
                    {
                        innerRow.Controls.Add(campo);
                    }
                }

                fieldset.Controls.Add(innerRow);
                return fieldset;
            }
            else
            {
                WebControl fieldset = new WebControl(HtmlTextWriterTag.Fieldset);
                if (descrizione.Length > 0)
                    fieldset.Controls.Add(new LiteralControl(descrizione));
                WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
                fieldset.Controls.Add(legend);

                int rows = 0;
                VfGeneric vf = this.Fields.OrderByDescending(x => x.RowOrder).FirstOrDefault();
                if (vf != null)
                {
                    rows = vf.RowOrder;
                    // creo le row della form
                    for (int i = 1; i <= rows; i++)
                    {
                        WebControl row = new WebControl(HtmlTextWriterTag.Div);
                        row.CssClass = "row";
                        row.ID = "row_" + i;

                        fieldset.Controls.Add(row);
                    }
                }

                foreach (VfGeneric campo in this.Fields)
                {
                    if (listaKeyCampi.Contains(campo.Key))
                    {
                        // se il campo ha la prop RowOrder > 0 lo appendo alla row(RowOrder) 
                        fieldset.Controls.Add(campo);
                    }
                }

                legend.Controls.Add(new LiteralControl(titolo));
                return fieldset;

            }
           
        }


        private void BuildFieldset(List<string> listaKeyCampi, string titolo, string descrizione, WebControl control)
        {
            WebControl panel = new WebControl(HtmlTextWriterTag.Div);
            panel.CssClass = "panel panel-default";

            WebControl panel_body = new WebControl(HtmlTextWriterTag.Div);

            if (UsedOnFrontend)
            {                
                panel_body.CssClass = "panel-body";
                
                WebControl innerRow = new WebControl(HtmlTextWriterTag.Div);
                innerRow.CssClass = "col-md-12";

                if (descrizione.Length > 0)
                    innerRow.Controls.Add(new LiteralControl("<div class=\"h4\">" + descrizione + "</div>"));                

                WebControl row = new WebControl(HtmlTextWriterTag.Div);
                row.CssClass = "row";
                int order_pre = 1;

                foreach (VfGeneric campo in this.Fields)
                {
                    if (listaKeyCampi.Contains(campo.Key))
                    {
                        if (campo.Key == "Email")
                            innerRow.ID = "EmailCol";

                        if (!string.IsNullOrEmpty(campo.ColumCssClass))
                        {                            
                            WebControl column = new WebControl(HtmlTextWriterTag.Div);
                            column.CssClass = campo.ColumCssClass;
                            column.Controls.Add(campo);

                            if (campo.RowOrder == order_pre)
                            {
                                row.Controls.Add(column);
                                innerRow.Controls.Add(row);
                            }
                            else
                            {
                                row = new WebControl(HtmlTextWriterTag.Div);
                                row.CssClass = "row";
                                row.Controls.Add(column);
                                innerRow.Controls.Add(row);
                            }
                            order_pre = campo.RowOrder;
                        }
                        else
                        {
                            innerRow.Controls.Add(campo);
                        }
                    }
                }

                panel_body.Controls.Add(innerRow);
                
            }
            else
            {
               
                if (descrizione.Length > 0)
                    panel_body.Controls.Add(new LiteralControl(descrizione));

                WebControl legend = new WebControl(HtmlTextWriterTag.Legend);
                legend.Controls.Add(new LiteralControl(titolo));
                panel_body.Controls.Add(legend);

                WebControl innerRow = new WebControl(HtmlTextWriterTag.Div);
                innerRow.CssClass = "col-md-12";

                WebControl row = new WebControl(HtmlTextWriterTag.Div);
                row.CssClass = "row";
                int order_pre = 1;

                foreach (VfGeneric campo in this.Fields)
                {
                    if (listaKeyCampi.Contains(campo.Key))
                    {
                        if (!string.IsNullOrEmpty(campo.ColumCssClass))
                        {
                            WebControl column = new WebControl(HtmlTextWriterTag.Div);
                            column.CssClass = campo.ColumCssClass;
                            column.Controls.Add(campo);

                            if (campo.RowOrder == order_pre)
                            {
                                row.Controls.Add(column);
                                innerRow.Controls.Add(row);
                            }
                            else
                            {
                                row = new WebControl(HtmlTextWriterTag.Div);
                                row.CssClass = "row";
                                row.Controls.Add(column);
                                innerRow.Controls.Add(row);
                            }
                            order_pre = campo.RowOrder;
                        }
                        else
                        {
                            innerRow.Controls.Add(campo);
                        }
                    }
                }
                panel_body.Controls.Add(innerRow);

            }

            panel.Controls.Add(panel_body);
            control.Controls.Add(panel);
        }

        private void InitVfManager(WebControl elencoFieldset)        
        {   
            if (ShowMandatoryInfo)
                this.Controls.Add(MandatoryControl);

            if (AutoValidation && IsPostBack)
                Validate();

            if (TitleControl.Controls.Count > 0)
                this.Controls.Add(TitleControl);

            if (InfoControl.Controls.Count > 0)
                this.Controls.Add(InfoControl);

            this.Controls.Add(elencoFieldset);
            
            if (IsValid == VfGeneric.ValidationStates.NotValid)
            {
                this.Controls.Add(ValidationResultsControl());
            }

            if (AddSubmitButton)
            {
                WebControl buttonContainer = new WebControl(HtmlTextWriterTag.Div);
                buttonContainer.CssClass = UsedOnFrontend ? "text-center vf submit-vf mb-4" : "vf submit-vf";
                buttonContainer.Controls.Add(SubmitButton);
                this.Controls.Add(buttonContainer);
                this.DefaultButton = SubmitButton.ID;
            }
        }
    }
}
