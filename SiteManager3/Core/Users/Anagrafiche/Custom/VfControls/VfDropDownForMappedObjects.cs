﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI.WebControls;
using NHibernate;
using System.Collections;

namespace NetCms.Users.CustomAnagrafe
{
    public class VfDropDownForMappedObjects : VfDropDown
    {
        public string EntityName
        {
            get;
            private set;
        }
        public string CustomId
        {
            get;
            private set;
        }
        public string PropertyName
        {
            get;
            private set;
        }

        public VfDropDownForMappedObjects(string id, string label, string entityName, string propertyName,string customId = "")
            : base(id, label)
        {
            EntityName = entityName;
            PropertyName = propertyName;
            CustomId = customId;
        }

        public override object PostbackValueObject
        {
            get
            {
                IDictionary obj = null;
                using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                using (ITransaction tx = sess.BeginTransaction())
                {
                    ISession mapSession = sess.GetSession(EntityMode.Map);
                    obj = (IDictionary) mapSession.Get(EntityName, int.Parse(this.Request.OriginalValue));
                    tx.Commit();
                }
                return obj;
            }
        }
        

        protected override void FillFieldValue()
        {
            if(String.IsNullOrEmpty(CustomId))
                 this.DefaultValue = (int)(DefaultValue as Hashtable)["ID"];
            else
                this.DefaultValue = (int)(DefaultValue as Hashtable)[CustomId];

        }
    }
}
