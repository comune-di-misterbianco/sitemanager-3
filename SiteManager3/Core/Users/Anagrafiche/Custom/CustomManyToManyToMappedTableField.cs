﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
using NHibernate;
using System.Collections;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomManyToManyToMappedTableField : CustomManyToManyField
    {
        public enum TipologiaControlloAssociato
        {
            CheckBoxList,
            SelectMultipla,
            JQueryMultiselect
        }

        public virtual TipologiaControlloAssociato ControlloAssociato
        {
            get;
            set;
        }
        
        // sarà il nome della tabella a cui l'anagrafica sarà associata
        public virtual string MappedTableName
        {
            get;
            set;
        }

        // sarà il namespace completo della classe la cui tabella associata ha nome = MappedTableName
        public virtual string MappedEntityName
        {
            get;
            set;
        }

        // sarà il nome della proprietà che rappresente l'identificativo nella classe mappata associata
        public virtual string NomeCampoIdentificativo
        {
            get;
            set;
        }

        // sarà il nome della proprietà che rappresente l'etichetta degli oggetti della classe mappata associata
        public virtual string NomeCampoEtichetta
        {
            get;
            set;
        }

        public override string TableName
        {
            get { return MappedTableName; }
        }

        public override string IdColumnName
        {
            get { return NomeCampoEtichetta; }
        }

        public override string EntityName
        {
            get { return MappedEntityName; }
        }

        public override VfGeneric CampoInAnagrafe
        {
            get
            {
                VfGeneric control = null;
                switch(ControlloAssociato)
                {
                    case TipologiaControlloAssociato.CheckBoxList:
                        {
                            control = new VfCheckBoxList(ControlId, Name, false, EntityName) { Required = false, BindField = false, AtLeastOneRequired = Required };
                            if (!string.IsNullOrEmpty(InfoDescription))
                                control.InfoControl.Controls.Add(DescriptionControl);

                            using (ISession sess = NetCms.DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
                            using (ITransaction tx = sess.BeginTransaction())
                            {
                                ISession mapSession = sess.GetSession(EntityMode.Map);

                                IList lista = mapSession.CreateCriteria(EntityName).List();

                                foreach (IDictionary value in lista)
                                {
                                    (control as VfCheckBoxList).addItem(value[NomeCampoEtichetta].ToString(), value[NomeCampoIdentificativo].ToString(), false);
                                }
                                tx.Commit();
                            }
                            break;
                        }
                    case TipologiaControlloAssociato.SelectMultipla:
                        {
                            break;
                        }
                    case TipologiaControlloAssociato.JQueryMultiselect:
                        {
                            break;
                        }

                }
                return control;
            }
        }

        public override CustomField.TipoProprietà Tipo
        {
            get { return TipoProprietà.MoltiAMoltiAClasseMappata; }
        }

        public override string DBDataType
        {
            get { throw new NotImplementedException(); }
        }

        public override List<VfGeneric> FormConfigurazione
        {
            get
            {
                if (_FormConfigurazione == null)
                {
                    _FormConfigurazione = new List<VfGeneric>();

                    _FormConfigurazione.AddRange(CommonFields);

                    VfTextBox mappedTableName = new VfTextBox("mappedTableName", "Nome della tabella mappata", "MappedTableName", InputTypes.Text);
                    mappedTableName.InfoControl.Controls.Add(new LiteralControl("Sarà il nome su db della tabella con la quale effettuare l'associazione molti a molti"));

                    VfTextBox mappedEntityName = new VfTextBox("mappedEntityName", "Namespace della classe mappata", "MappedEntityName", InputTypes.Text);
                    mappedEntityName.InfoControl.Controls.Add(new LiteralControl("Sarà il namespace della classe mappata con la quale effettuare l'associazione molti a molti"));

                    VfTextBox nomeCampoIdentificativo = new VfTextBox("nomeCampoIdentificativo", "Nome campo identificativo", "NomeCampoIdentificativo", InputTypes.Text);
                    nomeCampoIdentificativo.InfoControl.Controls.Add(new LiteralControl("Sarà il nome su db del campo identificativo della tabella con la quale effettuare l'associazione molti a molti"));

                    VfTextBox nomeCampoEtichetta = new VfTextBox("nomeCampoEtichetta", "Nome campo etichetta", "NomeCampoEtichetta", InputTypes.Text);
                    nomeCampoEtichetta.InfoControl.Controls.Add(new LiteralControl("Sarà il nome su db del campo da usare come label della tabella con la quale effettuare l'associazione molti a molti"));

                    VfDropDown controlloAssociato = new VfDropDown("controlloAssociato", "Rendering relativo", "ControlloAssociato");

                    foreach (TipologiaControlloAssociato type in Enum.GetValues(typeof(TipologiaControlloAssociato)))
                    {
                        controlloAssociato.Options.Add(type.ToString(), ((int)type).ToString());
                    }

                    _FormConfigurazione.Add(mappedEntityName);
                    _FormConfigurazione.Add(mappedTableName);
                    _FormConfigurazione.Add(nomeCampoIdentificativo);
                    _FormConfigurazione.Add(nomeCampoEtichetta);
                    _FormConfigurazione.Add(controlloAssociato);

                }
                return _FormConfigurazione;
            }
        }
        private List<VfGeneric> _FormConfigurazione;

        public override CustomField Clone(CustomAnagrafe ca)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomManyToManyToMappedTableField, CustomManyToManyToMappedTableField>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Anagrafe, o => o.Ignore())
                .ForMember(d => d.CustomGroup, o => o.Ignore())
                .ForMember(d => d.DependencyValuesCollection, o => o.Ignore());
            });

            config.AssertConfigurationIsValid();

            var newCustomManyToManyToMappedTableField = new CustomManyToManyToMappedTableField();

            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomManyToManyToMappedTableField);

            newCustomManyToManyToMappedTableField.DependencyValuesCollection = this.DependencyValuesCollection.Select(item => item.Clone(newCustomManyToManyToMappedTableField)).ToList();
            newCustomManyToManyToMappedTableField.Anagrafe = ca;
            //newCustomManyToManyToMappedTableField.CustomGroup = this.CustomGroup.Clone(ca);
            return newCustomManyToManyToMappedTableField;
        }
    }
}
