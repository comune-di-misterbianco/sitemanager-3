﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomCheckboxListField : CustomManyToManyField
    {
        public CustomCheckboxListField()
        {
            Values = new HashSet<CustomCheckboxListValue>();
        }

        public virtual ICollection<CustomCheckboxListValue> Values
        {
            get;
            set;
        }

        public virtual bool RepeatHorizontal
        {
            get;
            set;
        }

        public virtual bool AtLeastOneRequired
        {
            get;
            set;
        }

        public override VfGeneric CampoInAnagrafe
        {
            get
            {
                VfCheckBoxList cbl = new VfCheckBoxList(ControlId, Name, RepeatHorizontal, EntityName) { Required = false, BindField = false, AtLeastOneRequired = AtLeastOneRequired };
                if (!string.IsNullOrEmpty(InfoDescription))
                    cbl.InfoControl.Controls.Add(DescriptionControl);

                foreach(CustomCheckboxListValue value in Values)
                {
                    cbl.addItem(value.Label, value.ID.ToString(), false);
                }

                return cbl;
            }
        }

        public override CustomField.TipoProprietà Tipo
        {
            get { return TipoProprietà.CheckboxList; }
        }

        public override List<VfGeneric> FormConfigurazione
        {
            get
            {
                if (_FormConfigurazione == null)
                {
                    _FormConfigurazione = new List<VfGeneric>();

                    // i vfcheckboxlist hanno il required a false sempre
                    _FormConfigurazione.AddRange(CommonFields.Where(x=>x.Key!="obbligatorio"));

                    VfCheckBox ripetiOrizzontalmente = new VfCheckBox("repeatHorizontal", "Ripeti Orizzontalmente", "RepeatHorizontal");
                    _FormConfigurazione.Add(ripetiOrizzontalmente);

                    VfCheckBox almenoUnoRichiesto = new VfCheckBox("atLeastOneRequired", "Almeno un item richiesto", "AtLeastOneRequired");
                    _FormConfigurazione.Add(almenoUnoRichiesto);
                }
                return _FormConfigurazione;
            }
        }
        private List<VfGeneric> _FormConfigurazione;

        public override string DBDataType
        {
            get { throw new NotImplementedException(); }
        }

        public override string TableName
        {
            get { return this.Name.Replace(' ', '_') + "_values"; } //"custom_field_checkboxlist_values"; }
        }

        public override string IdColumnName
        {
            get { return "IDcustom_" + this.Name.Replace(' ', '_') + "_value"; }
        }

        public override string EntityName
        {
            get { return "NetCms.Users.CustomAnagrafe.CustomCheckboxListValue"; }
        }

        public override List<GUI.ToolbarButton> AdditionalToolbar
        {
            get
            {
                List<GUI.ToolbarButton> toolbar = new List<GUI.ToolbarButton>();
                toolbar.Add(new GUI.ToolbarButton("campocustomcheckboxlist_gest.aspx?cf=" + this.ID, NetCms.GUI.Icons.Icons.Cog_Edit, "Gestione Opzioni CheckboxList"));
                return toolbar;
            }
        }

        public override CustomField Clone(CustomAnagrafe ca)
        {           
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomCheckboxListField, CustomCheckboxListField>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Anagrafe, o => o.Ignore())
                .ForMember(d => d.CustomGroup, o => o.Ignore())
                .ForMember(d => d.DependencyValuesCollection, o => o.Ignore());
            });
            config.AssertConfigurationIsValid();

            var newCustomCheckboxListField = new CustomCheckboxListField();
          //  Mapper.Map(this, newCustomCheckboxListField);

            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomCheckboxListField);

            newCustomCheckboxListField.Values = this.Values.Select(item => item.Clone(newCustomCheckboxListField)).ToList();

            newCustomCheckboxListField.DependencyValuesCollection = this.DependencyValuesCollection.Select(item => item.Clone(newCustomCheckboxListField)).ToList();

            newCustomCheckboxListField.Anagrafe = ca;
            //newCustomCheckboxListField.CustomGroup = this.CustomGroup.Clone(ca);
            return newCustomCheckboxListField;
        }
    }
}
