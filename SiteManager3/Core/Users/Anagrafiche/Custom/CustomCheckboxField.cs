﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomCheckboxField : CustomField
    {
        public CustomCheckboxField()
        {
            Value = "0";
        }

        public virtual string Value
        {
            get;
            set;
        }

        public override CustomField.TipoProprietà Tipo
        {
            get { return TipoProprietà.Checkbox; }
        }

        public override VfGeneric CampoInAnagrafe
        {
            get
            {
                VfCheckBox checkbox = new VfCheckBox(ControlId, Name, Name) { Required = this.Required, BindField = false };
                if (!string.IsNullOrEmpty(InfoDescription))
                    checkbox.InfoControl.Controls.Add(DescriptionControl);
                if(Value.CompareTo("0") == 0)
                    checkbox.DefaultValue = false;
                if (Value.CompareTo("1") == 0)
                    checkbox.DefaultValue = true; 
                return checkbox;
            }
        }

        public override List<VfGeneric> FormConfigurazione
        {
            get
            {
                if (_FormConfigurazione == null)
                {
                    _FormConfigurazione = new List<VfGeneric>();

                    _FormConfigurazione.AddRange(CommonFields);

                    VfDropDown defaultValue = new VfDropDown("preselezionata", "Preselezionata", "Value");
                    defaultValue.Options.Add("Si", "1");
                    defaultValue.Options.Add("No", "0");

                    _FormConfigurazione.Add(defaultValue);

                }
                return _FormConfigurazione;
            }
        }

        private List<VfGeneric> _FormConfigurazione;

        public override string DBDataType
        {
            get { return "tinyint(1)"; }
        }

        public override string SystemDataType
        {
            get { return "bool"; }
        }

        public override CustomField Clone(CustomAnagrafe ca)
        {           
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomCheckboxField, CustomCheckboxField>().ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Anagrafe, o => o.Ignore())
                .ForMember(d => d.CustomGroup, o => o.Ignore())
                .ForMember(d => d.DependencyValuesCollection, o => o.Ignore());
            });
            config.AssertConfigurationIsValid();
            var newCustomCheckBoxField = new CustomCheckboxField();
            
           // Mapper.Map(this, newCustomCheckBoxField);
            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomCheckBoxField);


            newCustomCheckBoxField.Anagrafe = ca;

            newCustomCheckBoxField.DependencyValuesCollection = this.DependencyValuesCollection.Select(item => item.Clone(newCustomCheckBoxField)).ToList();
            
            //newCustomCheckBoxField.CustomGroup = this.CustomGroup.Clone(ca);
            return newCustomCheckBoxField;
        }
    }
}
