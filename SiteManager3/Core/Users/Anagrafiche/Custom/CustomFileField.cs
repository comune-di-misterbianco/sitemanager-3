﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetService.Utility.ValidatedFields;
using System.Web.UI;
using AutoMapper;

namespace NetCms.Users.CustomAnagrafe
{
    public class CustomFileField : CustomOneToManyField
    {
        public virtual string FileType
        {
            get;
            set;
        }

        public virtual string FileTypeDesc
        {
            get;
            set;
        }

        public virtual string UploadButtonText
        {
            get;
            set;
        }

        public virtual string UploadFileHandler
        {
            get;
            set;
        }

        public virtual string DeleteFileHandler
        {
            get;
            set;
        }

        public virtual string UploadFolder
        {
            get;
            set;
        }

        public virtual string SwfObjectFilePath
        {
            get;
            set;
        }      
        public virtual string UploadifyScriptsFolderPath
        {
            get;
            set;
        }

        public virtual bool MultiUpload
        {
            get;
            set;
        }

        public virtual bool AutoUpload
        {
            get;
            set;
        }

        public CustomFileField()
        {
            Name = "Allegati";
        }

        public override CustomField.TipoProprietà Tipo
        {
            get { return TipoProprietà.File; }
        }

        protected internal override string ControlId
        {
            get
            {
                return Anagrafe.DatabaseName + "_Allegati";
            }
        }

        public override VfGeneric CampoInAnagrafe
        {
            get
            {
                VfUploadify file = new VfUploadify(ControlId, Name, Name, FileType, FileTypeDesc, UploadButtonText, UploadFileHandler, DeleteFileHandler, UploadFolder, SwfObjectFilePath, UploadifyScriptsFolderPath, MultiUpload, AutoUpload, NetService.Utility.ValidatedFields.VfUploadify.FieldMode.Database) { Required = this.Required, BindField = false };
                if (!string.IsNullOrEmpty(InfoDescription))
                    file.InfoControl.Controls.Add(DescriptionControl);
                return file;
            }
        }

        public override List<VfGeneric> FormConfigurazione
        {
            get
            {
                if (_FormConfigurazione == null)
                {
                    _FormConfigurazione = new List<VfGeneric>();
                    _FormConfigurazione.AddRange(CommonFields.Where(x => x.Key != "label"));

                    VfTextBox fileType = new VfTextBox("fileType", "Estensioni file accettati", "FileType", InputTypes.Text);
                    fileType.InfoControl.Controls.Add(new LiteralControl("Indicare le estensioni dei file accettati es: *.pdf; *.doc; "));
                    fileType.Required = true;
                    _FormConfigurazione.Add(fileType);

                    VfTextBox fileTypeDesc = new VfTextBox("fileTypeDesc", "Descrizione file accettati", "FileTypeDesc", InputTypes.Text);
                    fileTypeDesc.InfoControl.Controls.Add(new LiteralControl("Indicare la descrizione da mostrare per i file accettati es: File accettati *.pdf; *.doc; "));
                    fileTypeDesc.Required = true;
                    _FormConfigurazione.Add(fileTypeDesc);

                    VfTextBox uploadButtonText = new VfTextBox("uploadButtonText", "Etichetta bottone di trasferimento", "UploadButtonText", InputTypes.Text);
                    uploadButtonText.Required = true;
                    _FormConfigurazione.Add(uploadButtonText);

                    VfTextBox uploadFileHandler = new VfTextBox("uploadFileHandler", "Percorso relativo dell'handler di trasferimento ", "UploadFileHandler", InputTypes.Text);
                    uploadFileHandler.Required = true;
                    _FormConfigurazione.Add(uploadFileHandler);

                    VfTextBox deleteFileHandler = new VfTextBox("deleteFileHandler", "Percorso relativo dell'handler di eliminazione ", "DeleteFileHandler", InputTypes.Text);
                    deleteFileHandler.Required = true;
                    _FormConfigurazione.Add(deleteFileHandler);

                    VfTextBox uploadFolder = new VfTextBox("uploadFolder", "Percorso relativo della cartella temporanea per il trasferimento dei file", "UploadFolder", InputTypes.Text);
                    uploadFolder.Required = true;
                    _FormConfigurazione.Add(uploadFolder);

                    VfTextBox uploadSwfObjectFilePath = new VfTextBox("swfObjectFilePath", "Percorso relativo del file swfobject.js", "SwfObjectFilePath", InputTypes.Text);
                    uploadSwfObjectFilePath.Required = true;
                    _FormConfigurazione.Add(uploadSwfObjectFilePath);                    

                    VfTextBox uploadUploadifyScriptsFolderPath = new VfTextBox("uploadifyScriptsFolderPath", "Percorso relativo della cartella ", "UploadifyScriptsFolderPath", InputTypes.Text);
                    uploadUploadifyScriptsFolderPath.Required = true;
                    _FormConfigurazione.Add(uploadUploadifyScriptsFolderPath);

                    VfCheckBox multiUpload = new VfCheckBox("multiUpload", "Trasferimento multiplo", "MultiUpload");
                    _FormConfigurazione.Add(multiUpload);

                    VfCheckBox autoUpload = new VfCheckBox("autoUpload", "Trasferimento automatico", "AutoUpload");
                    _FormConfigurazione.Add(autoUpload);

                }
                return _FormConfigurazione;
            }
        }
        private List<VfGeneric> _FormConfigurazione;

        public override string SystemDataType
        {
            get { return "NetCms.Users.AllegatoUtente"; }
        }


        public override CustomField Clone(CustomAnagrafe ca)
        {           
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CustomFileField, CustomFileField>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Anagrafe, o => o.Ignore())
                .ForMember(d => d.CustomGroup, o => o.Ignore())
                .ForMember(d => d.DependencyValuesCollection, o => o.Ignore());
            });

            config.AssertConfigurationIsValid();

            var newCustomFileField = new CustomFileField();
            
            var mapper = config.CreateMapper();
            mapper.Map(this, newCustomFileField);

            newCustomFileField.DependencyValuesCollection = this.DependencyValuesCollection.Select(item => item.Clone(newCustomFileField)).ToList();
            newCustomFileField.Anagrafe = ca;
            //newCustomFileField.CustomGroup = this.CustomGroup.Clone(ca);
            return newCustomFileField;
        }
    }
}
