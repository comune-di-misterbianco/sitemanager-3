using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using NetCms.Users.CustomAnagrafe;
using System.Linq;
using NetService.Utility.ValidatedFields;
using NHibernate.Proxy;
using LabelsManager;
using NetService.Utility.UI;
using G2Core.Common;

/// <summary>
/// User Profile
/// </summary>

namespace NetCms.Users
{
    public class AnagraficaBase
    {

        #region Label

        /// <summary>
        /// Users Labels
        /// </summary>
        public static Labels Labels
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(UsersLabels)) as Labels;
            }
        }
        #endregion

        //public string ID
        //{
        //    get
        //    {
        //        return _ID;
        //    }
        //}
        //private string _ID;

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Email
        {
            get;
            set;
        }

        public AnagraficaBase()
        {        
        }

        private List<NetCms.Users.AnagraficaDetail> _DetailsCollection;
        public List<NetCms.Users.AnagraficaDetail> Details
        {
            get
            {
                if (_DetailsCollection == null)
                {
                    _DetailsCollection = new List<NetCms.Users.AnagraficaDetail>();

                    User associatedUser = UsersBusinessLogic.GetUserByAnagraficaId(this.ID);
                    Hashtable detailMap = UsersBusinessLogic.GetAnagraficaMapUser(associatedUser);

                    _DetailsCollection.Add(new AnagraficaDetail("Email", detailMap["Email"].ToString()));

                    NetCms.Users.CustomAnagrafe.CustomAnagrafe strutturaAnagrafe = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(associatedUser.AnagraficaName);
                    if (strutturaAnagrafe != null && strutturaAnagrafe.CustomFields.Count > 0)
                    {
                        if (strutturaAnagrafe.CustomGroups == null || strutturaAnagrafe.CustomGroups.Count == 0)
                        {
                            foreach (DictionaryEntry item in detailMap)
                            {
                                if (strutturaAnagrafe.CustomFields.Where(x => x.Name == item.Key.ToString()).Count() > 0)
                                    if (strutturaAnagrafe.CustomFields.Where(x => x.Name == item.Key.ToString()).First().ShowInDetails)
                                        BuildDetailEntry(strutturaAnagrafe, item);
                            }
                        }
                        else
                        {
                            foreach (CustomGroup group in strutturaAnagrafe.CustomGroups.OrderBy(x => x.Posizione))
                            {
                                foreach (CustomField field in group.CustomFields.OrderBy(x => x.PositionInGroup))
                                {
                                    if (detailMap.ContainsKey(field.Name) && field.ShowInDetails)
                                    {
                                        BuildDetailEntry(strutturaAnagrafe, new DictionaryEntry(field.Name, detailMap[field.Name]));
                                    }
                                }
                            }
                        }
                    }
                }

                return _DetailsCollection;
            }
        }

        private void BuildDetailEntry(NetCms.Users.CustomAnagrafe.CustomAnagrafe strutturaAnagrafe, DictionaryEntry item)
        {
            if (item.Value != null)
            {
                CustomField cf = strutturaAnagrafe.CustomFields.Where(x => x.Name == item.Key.ToString()).First();
                switch (cf.EffettoSuMapping)
                {
                    case CustomAnagrafe.CustomField.MappingEffect.ForeignKey:
                        {
                            Type tipo = CustomAnagrafeBusinessLogic.GetTypeFromTypeName(cf.SystemDataType);

                            Hashtable htoggetto = null;
                            if (item.Value is INHibernateProxy)
                            {
                                htoggetto = CustomAnagrafeBusinessLogic.GetCurrentMapSession().GetSessionImplementation().PersistenceContext.Unproxy(item.Value) as Hashtable;
                            }
                            else
                                htoggetto = (Hashtable)item.Value;

                            var oggetto = CustomAnagrafeBusinessLogic.GetObjectByIdAndType((int)htoggetto["ID"], tipo);

                            //le righe di codice sotto sono state inserite perch� quando all'interno della scheda di una anagrafica istituti si entrava nel dettaglio,
                            //i campi categoria,tiposcuola e sottotipo scuola avevano come valore "IstitutiCommon.model.'nomedellaclasse'". Questo perch� il metodo AddDetail
                            //passava item ed oggetto.tostring() questo faceva stampare "IstitutiCommon.model.'nomedellaclasse'" nella tabella. A noi non serve l'oggetto,
                            //ma il valore del propertyname che si trova nella classe CustomForeignKeyToMappedTableField che eredita da CustomField. 
                            //Quindi effettuando il casting, ed usando il propertyname nell'oggetto hashtable, posso passare il valore della propriet� 
                            //ed inserire il valore corretto nella tabella

                            if (cf.GetType() == typeof(CustomForeignKeyToMappedTableField))
                            {
                                CustomForeignKeyToMappedTableField cfm = (CustomForeignKeyToMappedTableField)cf;
                                string valore = htoggetto[cfm.PropertyName].ToString();

                                //AddDetail(item, oggetto.ToString());

                                AddDetail(item, valore);
                            }
                            else
                                AddDetail(item, oggetto.ToString());
                           
                            break;
                        }
                    case CustomAnagrafe.CustomField.MappingEffect.ManyToMany:
                        {
                            IList list = (IList)item.Value;
                            string value = "";
                            if (list.Count > 0)
                            {
                                

                                //value = (list[0] as Hashtable)["Label"].ToString();
                                for (int i = 0; i < list.Count; i++)
                                {
                                    string itemValue = (list[i] as Hashtable)["Value"].ToString();
                                    if ((cf as CustomCheckboxListField).Values.Count(x=>x.Value == itemValue) > 0)
                                        value += (list[i] as Hashtable)["Label"].ToString() + ", ";
                                }
                            }
                            if (value.Length>2)
                                value = value.Remove(value.Length-2,2);

                            AddDetail(item, value);

                            break;
                        }
                    case CustomAnagrafe.CustomField.MappingEffect.OneToMany:
                        {
                            IList list = (IList)item.Value;

                            if (cf.Tipo == CustomAnagrafe.CustomField.TipoPropriet�.File)
                            {
                                string value = "";
                                if (list.Count > 0)
                                {
                                    Hashtable firstFile = list[0] as Hashtable;
                                    value = "<a href=\"" + NetCms.Configurations.Paths.AbsoluteRoot + "/charts/UserAttach.aspx?id=" + firstFile["ID"].ToString() + "\">" + firstFile["FileName"].ToString() + "</a>";
                                    for (int i = 1; i < list.Count; i++)
                                    {
                                        Hashtable file = list[i] as Hashtable;
                                        value += "<br/><a href=\"" + NetCms.Configurations.Paths.AbsoluteRoot + "/charts/UserAttach.aspx?id=" + file["ID"].ToString() + "\">" + file["FileName"].ToString() + "</a>";
                                    }
                                }
                                AddLinkDetail(item, value);
                            }
                            else
                            {
                                string value = "";
                                if (list.Count > 0)
                                {
                                    value = (list[0] as Hashtable)["Label"].ToString();
                                    for (int i = 1; i < list.Count; i++)
                                    {
                                        value += "," + (list[i] as Hashtable)["Label"].ToString();
                                    }
                                }
                                AddDetail(item, value);
                            }

                            break;
                        }
                    case CustomAnagrafe.CustomField.MappingEffect.Property:
                        {
                            switch (cf.Tipo)
                            {
                                case CustomAnagrafe.CustomField.TipoPropriet�.Data:
                                    {
                                        CustomDateField cdf = (CustomDateField)cf;
                                        DateTime value = (DateTime)item.Value;
                                        switch (cdf.Formato)
                                        {
                                            case VfDate.DateFormat.DataeOra:
                                                AddDetail(item, value.ToString("g"));
                                                break;
                                            case VfDate.DateFormat.SoloData:
                                                AddDetail(item, value.ToShortDateString());
                                                break;
                                            case VfDate.DateFormat.SoloOra:
                                                AddDetail(item, value.ToShortTimeString());
                                                break;
                                        }
                                        break;
                                    }
                                case CustomAnagrafe.CustomField.TipoPropriet�.Checkbox:
                                    {
                                        bool value = (bool)item.Value;
                                        AddDetail(item, value == true ? "SI" : "NO");
                                        break;
                                    }
                                case CustomAnagrafe.CustomField.TipoPropriet�.Dropdown:
                                    {
                                        CustomDropdownField cdf = (CustomDropdownField)cf;
                                        CustomDropdownValue value = cdf.Values.FirstOrDefault(x => x.Value == item.Value.ToString());
                                        string label = value != null ? value.Label : "";
                                        AddDetail(item, label);
                                        break;
                                    }
                                default: AddDetail(item, item.Value.ToString());
                                    break;
                            }

                            break;
                        }
                }
            }
            else
                AddDetail(item, "");
        }

        private void AddDetail(DictionaryEntry item, string value)
        {
            _DetailsCollection.Add(new AnagraficaDetail(item.Key.ToString(), value));
        }

        private void AddLinkDetail(DictionaryEntry item, string value)
        {
            _DetailsCollection.Add(new AnagraficaLinkDetail(item.Key.ToString(), value));
        }        

        /// <summary>
        /// Ritorna il controllo web relativo al profilo utente
        /// </summary>
        /// <param name="urlPage">Indicare il path relativo della pagina web che espone il controllo web</param>
        /// <returns>WebControl</returns>
        public WebControl GetProfileDetails(string urlPage = "", WebControl deleteCtrl = null)
        {
            WebControl div = new WebControl(HtmlTextWriterTag.Div);
            div.CssClass = "page user-profile";

            WebControl wrapper = new WebControl(HtmlTextWriterTag.Div);
            wrapper.CssClass = "page-wrapper";
         
            if (!EliminationRequest)
            {
             
                WebControl profilecard = new WebControl(HtmlTextWriterTag.Div);
                profilecard.CssClass = "m-3 p-3 bg-white rounded shadow-sm";

                WebControl h1 = new WebControl(HtmlTextWriterTag.H1);
                h1.Controls.Add(new LiteralControl(Labels[UsersLabels.UsersLabelsList.UserProfileDetailPageTitle]));
                wrapper.Controls.Add(h1);

                WebControl container = new WebControl(HtmlTextWriterTag.Div);
                container.CssClass = "container-fluid";

                WebControl row = new WebControl(HtmlTextWriterTag.Div);
                row.CssClass = "row";

                WebControl avatar = new WebControl(HtmlTextWriterTag.Div);
                avatar.CssClass = "col-lg-4 col-md-5";

                string avatarCode = @"<div class=""profile-image text-center""><img class=""img-fluid""  src =""" + NetCms.Configurations.ThemeEngine.ThemesFolder + @"/system/assets/images/user_profile.svg"" alt=""Foto utente""/></div>";
                avatar.Controls.Add(new LiteralControl(avatarCode));
                row.Controls.Add(avatar);

                WebControl profileinfo = new WebControl(HtmlTextWriterTag.Div);
                profileinfo.CssClass = "col-lg-8 col-md-7";

                string profileCode = @"<h4 class=""text-left"">" + CurrentUser.NomeCompleto + "</h4>";
                foreach (KeyValuePair<string, string> data in ProfileData.Where(x => x.Value != "-1"))
                {
                    profileCode += @"<p class=""text-left""><span class=""font-weight-bold"">" + data.Key + "</span>: " + data.Value + "</p>";
                }

                profileinfo.Controls.Add(new LiteralControl(profileCode));

                // download dati in csv
                Button downloadCsv = new Button();
                downloadCsv.CssClass = "btn btn-primary pull-left download mr-2";
                downloadCsv.ID = "csvDownload";
                downloadCsv.Text = "Scarica";
                downloadCsv.ToolTip = "Scarica i dati in formato csv (Art.20 - Regolamento generale per la protezione dei dati personali n. 2016/679)";
                downloadCsv.Click += DownloadCsv_Click;
                profileinfo.Controls.Add(downloadCsv);

                if (deleteCtrl == null)
                {
                    Button cancellaProfilo = new Button();
                    cancellaProfilo.CssClass = "btn btn-warning pull-left download"; ;
                    cancellaProfilo.ID = "rmProfile";
                    cancellaProfilo.Text = "Cancella profilo";
                    cancellaProfilo.ToolTip = "Cancella definitivamente i tuoi dati dal sistema";
                    //cancellaProfilo.Click += CancellaProfilo_Click;
                    profileinfo.Controls.Add(cancellaProfilo);
                }
                else
                {                    
                    profileinfo.Controls.Add(deleteCtrl);
                }

                row.Controls.Add(profileinfo);
          
                container.Controls.Add(row);

                profilecard.Controls.Add(container);

                wrapper.Controls.Add(profilecard);
            }                
            else
            {
                EliminationConfirmControl delControl = DeleteControl("Conferma la cancellazione del proprio account utente", "Sei sicuro?", urlPage);
                delControl.ConfirmButton.Click += ConfirmButton_Click;
                wrapper.Controls.Add(delControl);
            }
            div.Controls.Add(wrapper);          

            return div;
        }      

        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            // notifica all'amministratore di sistema
            NetCms.Users.UsersBusinessLogic.AddSysNotify(NetCms.Users.AccountManager.CurrentAccount.ID, NetCms.Users.AccountManager.CurrentAccount.ID, string.Format(UserLabels[UsersLabels.UsersLabelsList.UserDeleteProfile], NetCms.Users.AccountManager.CurrentAccount.UserName), "");

            // invio email di informazione dell'attivit� di eliminazione/disattivazione profilo effettuata dalla pagina di dettaglio del profilo
            CurrentUser.SendUserProfileDisableMail();

            // disattivazione del profilo utente
            CurrentUser.DisableProfile();

            // logout dal sistema             
            NetCms.Users.AccountManager.DoLogOut();
            var currentPath = System.Web.HttpContext.Current.Request.Path;
          
            // redirect alla pagina principale del sistema
            if (currentPath.StartsWith("/cms"))
                Diagnostics.Diagnostics.Redirect("/cms/default.aspx");
            else
                Diagnostics.Diagnostics.Redirect("/default.aspx");
        }
   
        private void DownloadCsv_Click(object sender, EventArgs e)
        {
            string csvFileName = "data.csv";
            string csvFolderPath = "/repository/users/" + CurrentUser.ID + "/";
            csvFolderPath = System.Web.HttpContext.Current.Server.MapPath(csvFolderPath);

            try
            {
                

                if (!System.IO.Directory.Exists(csvFolderPath))
                    System.IO.Directory.CreateDirectory(csvFolderPath);

                using (var writer = new System.IO.StreamWriter(csvFolderPath + csvFileName))
                using (var csv = new CsvHelper.CsvWriter(writer, System.Globalization.CultureInfo.InvariantCulture))
                {
                    csv.Configuration.SanitizeForInjection = true;
                    csv.Configuration.AllowComments = true;
                    
                    csv.WriteComment("Documento generato il " + DateTime.Now.ToString());
                    csv.NextRecord();
                    
                    // salvo l'intestazione
                    foreach (KeyValuePair<string, string> data in ProfileData)
                    {
                        csv.WriteField(data.Key);
                    }
                    csv.NextRecord();
                    
                    // salvo i valori 
                    foreach (KeyValuePair<string, string> data in ProfileData)
                    {                        
                        csv.WriteField(data.Value);                        
                    }
                    csv.NextRecord();                    
                }

                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;

                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.ContentType = "text/csv";
                response.AppendHeader("Content-Disposition", "attachment; filename="+ csvFileName);
                response.TransmitFile(csvFolderPath + csvFileName);
                response.Flush();
                response.End();

            }
            catch (Exception ex) 
            {
                Diagnostics.Diagnostics.TraceException(ex, CurrentUser.ID.ToString(), CurrentUser.UserName, this.GetType(),null);
            }
            finally
            {
                System.IO.File.Delete(csvFolderPath + csvFileName);
            }
        }

        private User _CurrentUser;
        private User CurrentUser
        {
            get
            {
                if (_CurrentUser == null)
                _CurrentUser = NetCms.Users.UsersBusinessLogic.GetUserByAnagraficaId(this.ID);
                return _CurrentUser;
            }
        }

        public static Labels UserLabels
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(UsersLabels)) as Labels;
            }
        }

        private Dictionary<string, string> _ProfileData;
        private Dictionary<string,string> ProfileData
        {
            get
            {
                if (_ProfileData == null)
                {
                    _ProfileData = new Dictionary<string, string>();

                    _ProfileData.Add("Anagrafe", CurrentUser.AnagraficaName);
                    _ProfileData.Add("Nome utente", CurrentUser.UserName);
                    _ProfileData.Add("Data attivazione profilo", CurrentUser.CreationTime.ToString());
                    _ProfileData.Add("Data ultimo accesso", CurrentUser.LastAccess.ToString());
                    _ProfileData.Add("Data ultima modifica profilo", CurrentUser.LastUpdate.ToString());
                    _ProfileData.Add("Gruppi", (CurrentUser.Groups != null ? string.Join(", ", CurrentUser.Groups.Select(x => x.Name).ToArray()) : ""));
                   
                    foreach (NetCms.Users.AnagraficaDetail Detail in this.Details)
                    {
                        if (!string.IsNullOrEmpty(Detail.Value) && !_ProfileData.ContainsKey(Detail.Title))                            
                            _ProfileData.Add(Detail.Title, Detail.Value);                        
                    }
                }

                return _ProfileData;
            }

        }

        public WebControl UpdateEmailByConfirmationLink()
        {

            WebControl field = new WebControl(HtmlTextWriterTag.Fieldset);
            field.Controls.Add(new LiteralControl("<legend>Esito operazione</legend>"));

            string message = string.Empty;
                       
            const int not_executed = -3;
            const int invalid_data = -2;
            const int current_emailchanged = -1;
            const int error_saving = 0;
            const int updated = 1;

            bool update_user_profile = false;
            int resulttype = not_executed;
            ConfirmEmailUpdate updateEmailData = AccountManager.GetEmailUpdateDataFromQueryString();
            resulttype = invalid_data;
            if (updateEmailData != null)
            {
                if (NetCms.Users.AccountManager.CurrentAccount.ID.ToString() == updateEmailData.IDuser)
                    //se la email corrente coincide con quella visionata dall'utente al momento dell'aggiornamento procedi
                    if (true)//string.IsNullOrEmpty(updateEmailData.oldemail) || string.Compare(NetCms.Users.AccountManager.CurrentAccount.Anagrafica.Email, updateEmailData.oldemail) == 0)
                    {
                        bool result = UsersBusinessLogic.UpdateEmail(this.ID, NetCms.Users.AccountManager.CurrentAccount.AnagraficaType, updateEmailData.newemail); //UpdateEmail(updateEmailData.newemail);
                       
                        if (NetCms.Users.AccountManager.CurrentAccount.CustomAnagrafe.EmailAsUserName)
                        {
                            User usr = UsersBusinessLogic.GetById(NetCms.Users.AccountManager.CurrentAccount.ID);
                            usr.UserName = updateEmailData.newemail;
                            Profile prf = ProfileBusinessLogic.GetById(usr.Profile.ID);
                            prf.Nome = updateEmailData.newemail;
                            UsersBusinessLogic.SaveOrUpdateUser(usr);
                            ProfileBusinessLogic.SaveOrUpdateProfile(prf);
                            update_user_profile = true;
                        }

                        if (result)
                            resulttype = updated;
                        NetCms.Users.AccountManager.UpdateCurrentUser();
                    }
                    else
                        resulttype = current_emailchanged;
            }
            else
                resulttype = invalid_data;

            switch (resulttype)
            {
                case not_executed:
                    message = "Attenzione:Procedura non eseguita.";
                    break;
                case invalid_data:
                    message = "Attenzione:non � possibile procedere all'aggiornamento della sua email: il link non contiene elementi validi.";
                    break;
                case current_emailchanged:
                    message = "Attenzione:l'email correntemente impostata non coincide con quella da modificare " + updateEmailData.oldemail + ". Se si desidera modificare l'email corrente, riaccedi alla form di modifica dati.";
                    break;
                case error_saving:
                    message = "Attenzione:Si � verificato un errore durante l'aggiornamento dell'email.";
                    break;
                case updated:
                    message = "Aggiornamento dell'email effettuato con successo!";
                    if (update_user_profile)
                        message += "Aggiornamento dell'Username effettuato con successo. La nuova Username � uguale a '" + updateEmailData.newemail + "'";
                    break;
                default:
                    break;
            }

            field.Controls.Add(new LiteralControl("<p class=\"esito\">" + message + "</p>"));

            return (field);
        }
      
        protected EliminationConfirmControl DeleteControl(string titoloMessaggio, string messaggio, string urlBack)
        {
            EliminationConfirmControl control = new EliminationConfirmControl(titoloMessaggio, messaggio, urlBack);
            control.ConfirmButton.CssClass = "btn btn-primary mr-2";
            control.NegationButton.CssClass = "btn btn-secondary";
            return control;
        }

        public bool EliminationRequest
        {
            get
            {
                RequestVariable request = new RequestVariable("rmProfile", RequestVariable.RequestType.Form);
                RequestVariable confirm = new RequestVariable("confirm", RequestVariable.RequestType.Form);

                if ((request.IsValidString && request.StringValue == "Cancella profilo") || (confirm.IsValidString && confirm.StringValue == "Si"))
                    _EliminationRequest = true;
                return _EliminationRequest;
            }
        }
        private bool _EliminationRequest = false;
    }
    
    public class AnagraficaDetail
    {
        public string Title
        {
            get { return _Title; }
            private set { _Title = value; }
        }
        private string _Title;

        public string Value
        {
            get { return _Value; }
            private set { _Value = value; }
        }
        private string _Value;

        public AnagraficaDetail(string title, string value)
        {
            this.Title = title;
            this.Value = value;
        }
    }

    public class AnagraficaLinkDetail : AnagraficaDetail
    {
        public AnagraficaLinkDetail(string title, string value)
            : base(title, value)
        {

        }
    }
}