using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using NetCms.Connections;
using NetCms.Users.Search;
using NetService.Utility.ValidatedFields;
using NetCms.Users.CustomAnagrafe;
using System.Linq;
using NetCms.Users.CustomAnagrafe;
using NHibernate;
using GenericDAO.DAO;
using System.Collections;
using NetService.Utility.RecordsFinder;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users
{
    public class AnagraficaSearch
    {
       
        public virtual List<CustomSearchField> CustomSearchFields
        {
            get
            {
                List<NetCms.Users.Search.CustomSearchField> customSearchFields = new List<NetCms.Users.Search.CustomSearchField>();
                //Per il solo campo email visto che � il campo base
                VfTextBox fieldEmail = new VfTextBox("Email", "Email", "Email", InputTypes.Email);
                customSearchFields.Add(new NetCms.Users.Search.CustomSearchField(fieldEmail.Label, fieldEmail.Label, fieldEmail.SearchParameter));

                IList<NetCms.Users.CustomAnagrafe.CustomAnagrafe> struttureAnagrafe = CustomAnagrafeBusinessLogic.FindAllCustomAnagrafe();
                foreach (NetCms.Users.CustomAnagrafe.CustomAnagrafe anagrafica in struttureAnagrafe)
                { 
                    IList<CustomField> fieldsToAddInSearch = anagrafica.CustomFields.Where(x=>x.ShowInSearch).ToList();
                    foreach (CustomField field in fieldsToAddInSearch)
                    {
                        if (customSearchFields.Where(x=>x.Key.ToLower() == field.Name.ToLower()).Count() == 0)
                            customSearchFields.Add(new CustomSearchField(field.Name, field.Name, field.CampoInAnagrafe.SearchParameter,field.CampoInAnagrafe.ReturnObjectType));
                    }
                }
                
                return customSearchFields;
            }
        }

        public List<CustomSearchField> GetCustomSearchFieldsByAnagraficaType(string anagraficaType)
        {
            List<NetCms.Users.Search.CustomSearchField> customSearchFields = new List<NetCms.Users.Search.CustomSearchField>();
            //Per il solo campo email visto che � il campo base
            VfTextBox fieldEmail = new VfTextBox("Email", "Email", "Email", InputTypes.Email);
            customSearchFields.Add(new NetCms.Users.Search.CustomSearchField(fieldEmail.Label, fieldEmail.Label, fieldEmail.SearchParameter));

            NetCms.Users.CustomAnagrafe.CustomAnagrafe anagrafica = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(anagraficaType.Replace("Anagrafica", ""));

            IList<CustomField> fieldsToAddInSearch = anagrafica.CustomFields.Where(x => x.ShowInSearch).ToList();
            foreach (CustomField field in fieldsToAddInSearch)
            {
                if (customSearchFields.Where(x => x.Key.ToLower() == field.Name.ToLower()).Count() == 0)
                    customSearchFields.Add(new CustomSearchField(field.Name, field.Name, field.CampoInAnagrafe.SearchParameter, field.CampoInAnagrafe.ReturnObjectType));
            }

            return customSearchFields;
        }

        //public List<CustomSearchField> BasicSearchFields
        //{
        //    get
        //    {
        //        List<CustomSearchField> basicSearchFields = new List<CustomSearchField>();
        //        basicSearchFields.Add(new CustomSearchField("Nome Utente", "Nome Utente", "in_Name_User"));
        //        basicSearchFields.Add(new CustomSearchField("Nome Gruppo", "Nome Gruppo", "in_Name_Group"));
        //        basicSearchFields.Add(new CustomSearchField("Nome Macro Gruppo", "Nome Macro Gruppo", "in_Label_MacroGroup"));

        //        return basicSearchFields;
        //    }
        //}

        //NUOVA VERSIONE PER OTTIMIZZARE LE PERFORMANCE
        public virtual ICollection<User> SearchResults(NetCms.Users.User user, int pageStart, int rpp, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICollection<User> searchResult = new List<User>();

            //Estrazione dei custom search fields che sono stati valorizzati nella ricerca
            List<CustomSearchField> customFieldSelected = CustomSearchFields.Where(x => x.SearchRequest.IsValidString || x.SearchRequest.IsValidInteger).ToList();

            //Lettura di tutte le anagrafiche custom
            List<CustomAnagrafe.CustomAnagrafe> anagrafiche = CustomAnagrafeBusinessLogic.FindAllCustomAnagrafe().ToList();

            //Per ciascuna anagrafica custom creo un oggetto contenente il riferimento alla stessa e il numero di campi che compaiono tra i custom search field valorizzati
            var result = from anag in anagrafiche
                         select new
                         {
                             Anagrafica = anag,
                             NumeroCampi = anag.CustomFields.Count(x => customFieldSelected.Where(y => y.Key.ToLower() == x.Name.ToLower()).Count() > 0)
                         };

            //Estrazione delle anagrafiche il cui numero di campi coincide con il numero di custom search fields valorizzati eccetto l'email che � contenuta nell'anagrafica base
            var resultFiltered = from res in result
                                 where res.NumeroCampi == customFieldSelected.Count(x => x.Key != "Email")
                                 select res;

            //Collezione di utenti amministrati dall'utente corrente
            NetCms.Users.Search.UserRelated rels = new UserRelated(user);
            //ICollection<User> usersToTake = rels.UsersIAdmin(0, rels.UsersIAdminCount(additionalJoins, additionalConditions), additionalJoins, additionalConditions);

            //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
            //SearchParameter userIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
            //SearchParameter usersToTakeSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, userIdSP);

            //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
            List<SearchParameter> searchFieldSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
            //searchFieldSP.Add(usersToTakeSP);

            IGenericDAO<Hashtable> dao = new CriteriaNhibernateDAO<Hashtable>(UsersBusinessLogic.GetCurrentMapSession());

            IList<Hashtable> map = new List<Hashtable>();
            //Se � stato valorizzato il custom search field della mail, filtraggio per anagraficaBase
            if (customFieldSelected.Count(x => x.Key == "Email") == 1)
            {
                //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
                //SearchParameter userBaseIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
                //Codice inserito per filtrare solo gli utenti dell'anagrafica base ed evitare duplicati nella successiva ricerca all'interno delle custom anagrafi
                SearchParameter userAnagraficaTypeSP = new SearchParameter(null, "AnagraficaType", "AnagraficaBase", Finder.ComparisonCriteria.Equals, false);
                SearchParameter usersToTakeBaseSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { /*userBaseIdSP,*/ userAnagraficaTypeSP });

                //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
                List<SearchParameter> searchFieldBaseSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
                searchFieldBaseSP.Add(usersToTakeBaseSP);

                IList<Hashtable> founded = dao.FindByCriteria("AnagraficaBase", searchFieldBaseSP.ToArray());
                map = map.Concat(founded.Where(x => x["User"] != null)).ToList();
            }

            //Per ciascuna anagrafica custom trovata, estrazione dei record in base ai criteri selezionati
            foreach (var item in resultFiltered)
            {                
                IList<SearchParameter> searchFieldSPAdj = (from sf in searchFieldSP
                                                           where sf.Name != "Email"
                                                           let field = item.Anagrafica.CustomFields.Where(x => x.Name.ToLower() == sf.Name.ToLower()).First()
                                                           select new SearchParameter(field.CampoInAnagrafe.Label, field.Name, sf.Value, sf.ComparisonCriteria, sf.IsProducedByDefault)).ToList();

                if (searchFieldSP.Count(x => x.Name == "Email") > 0)
                    searchFieldSPAdj.Add(searchFieldSP.First(x => x.Name == "Email"));

                IList<Hashtable> founded = dao.FindByCriteria("Anagrafica" + item.Anagrafica.Nome, searchFieldSPAdj.ToArray());
                map = map.Concat(founded.Where(x => x["User"] != null)).ToList();
            }

            //Collezione degli ID di tutti gli utenti da visualizzare
            List<int> iDsToShow = new List<int>();
            foreach (Hashtable tab in map)
            {
                Hashtable userHashtable = (Hashtable)tab["User"];
                iDsToShow.Add(int.Parse(userHashtable["ID"].ToString()));
            }

            //Filtro della ricerca con gli id da mostrare, paginati in base ai parametri in ingresso
            //searchResult = usersToTake.Where(x => iDsToShow.Contains(x.ID)).Skip(pageStart * rpp).Take(rpp).ToList();

            if (additionalConditions != null)
                additionalConditions = additionalConditions.Concat(new SearchCondition[] { new SearchCondition("ID", iDsToShow, SearchCondition.ComparisonType.In, SearchCondition.CriteriaType.User) }).ToArray();
            else
                additionalConditions = new SearchCondition[] { new SearchCondition("ID", iDsToShow, SearchCondition.ComparisonType.In, SearchCondition.CriteriaType.User) };

            searchResult = rels.UsersIAdmin(pageStart, rpp, additionalJoins, additionalConditions); // usersToTake.Where(x => iDsToShow.Contains(x.ID)).Skip(pageStart * rpp).Take(rpp).ToList();

            return searchResult;
        }
        

        /* INIZIO COMMENTO METODO PRE OTTIMIZZAZIONE PERFORMANCE

        public virtual ICollection<User> SearchResults(NetCms.Users.User user, int pageStart, int rpp, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICollection<User> searchResult = new List<User>();

            //Estrazione dei custom search fields che sono stati valorizzati nella ricerca
            List<CustomSearchField> customFieldSelected = CustomSearchFields.Where(x => x.SearchRequest.IsValidString || x.SearchRequest.IsValidInteger).ToList();

            //Lettura di tutte le anagrafiche custom
            List<CustomAnagrafe.CustomAnagrafe> anagrafiche = CustomAnagrafeBusinessLogic.FindAllCustomAnagrafe().ToList();

            //Per ciascuna anagrafica custom creo un oggetto contenente il riferimento alla stessa e il numero di campi che compaiono tra i custom search field valorizzati
            var result = from anag in anagrafiche
                         select new
                         {
                             Anagrafica = anag,
                             NumeroCampi = anag.CustomFields.Count(x=> customFieldSelected.Where(y=>y.Key.ToLower() == x.Name.ToLower()).Count() > 0)
                         };

            //Estrazione delle anagrafiche il cui numero di campi coincide con il numero di custom search fields valorizzati eccetto l'email che � contenuta nell'anagrafica base
            var resultFiltered = from res in result
                                 where res.NumeroCampi == customFieldSelected.Count(x => x.Key != "Email")
                                 select res;

            //Collezione di utenti amministrati dall'utente corrente
            NetCms.Users.Search.UserRelated rels = new UserRelated(user);
            ICollection<User> usersToTake = rels.UsersIAdmin(0,rels.UsersIAdminCount(additionalJoins,additionalConditions),additionalJoins,additionalConditions);

            //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
            SearchParameter userIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
            SearchParameter usersToTakeSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, userIdSP);

            //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
            List<SearchParameter> searchFieldSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
            searchFieldSP.Add(usersToTakeSP);

            IGenericDAO<Hashtable> dao = new CriteriaNhibernateDAO<Hashtable>(UsersBusinessLogic.GetCurrentMapSession());

            IList<Hashtable> map = new List<Hashtable>();
            //Se � stato valorizzato il custom search field della mail, filtraggio per anagraficaBase
            if (customFieldSelected.Count(x => x.Key == "Email") == 1)
            {
                //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
                SearchParameter userBaseIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
                //Codice inserito per filtrare solo gli utenti dell'anagrafica base ed evitare duplicati nella successiva ricerca all'interno delle custom anagrafi
                SearchParameter userAnagraficaTypeSP = new SearchParameter(null, "AnagraficaType", "AnagraficaBase", Finder.ComparisonCriteria.Equals, false);
                SearchParameter usersToTakeBaseSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { userBaseIdSP, userAnagraficaTypeSP });

                //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
                List<SearchParameter> searchFieldBaseSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
                searchFieldBaseSP.Add(usersToTakeBaseSP);
                
                IList<Hashtable> founded = dao.FindByCriteria("AnagraficaBase", searchFieldBaseSP.ToArray());
                map = map.Concat(founded).ToList();
            }

            //Per ciascuna anagrafica custom trovata, estrazione dei record in base ai criteri selezionati
            foreach (var item in resultFiltered)
            {
                IList<Hashtable> founded = dao.FindByCriteria("Anagrafica" + item.Anagrafica.Nome, searchFieldSP.ToArray());
                map = map.Concat(founded).ToList();
            }

            //Collezione degli ID di tutti gli utenti da visualizzare
            List<int> iDsToShow = new List<int>();
            foreach (Hashtable tab in map)
            {
                Hashtable userHashtable = (Hashtable)tab["User"];
                iDsToShow.Add(int.Parse(userHashtable["ID"].ToString()));                
            }

            //Filtro della ricerca con gli id da mostrare, paginati in base ai parametri in ingresso
            searchResult = usersToTake.Where(x => iDsToShow.Contains(x.ID)).Skip(pageStart * rpp).Take(rpp).ToList();

            //foreach (Hashtable tab in map.Skip(pageStart*rpp).Take(rpp))
            //{
            //    Hashtable userHashtable = (Hashtable)tab["User"];
            //    //CONTROLLO NON NECESSARIO VISTO CHE VENGONO GIA' FILTRATI IN PRECEDENZA
            //    //if (usersToTake.Where(x => x.ID == int.Parse(userHashtable["ID"].ToString())).Count() > 0)
            //    //{
            //        User userToShow = usersToTake.Where(x => x.ID == int.Parse(userHashtable["ID"].ToString())).First();
            //        searchResult.Add(userToShow);
            //    //}
            //}
            //List<System.Data.Common.DbParameter> parameters = new List<System.Data.Common.DbParameter>();

            //NetCms.Users.Search.UserRelated rels = new UserRelated(user);
            //string mygroups = rels.MyGroupsConditions;
            //if (mygroups.StartsWith(" OR "))
            //{
            //    mygroups = mygroups.Substring(3, mygroups.Length - 3);
            //}
            //mygroups += " AND Deleted_User = 0 AND (AnagraficaType_User LIKE '" + AnagraficaType.FullName + "' )";
            ////parameters.Add(InnerConnection.CreateParameter("in_database", this.OuterConnection.DatabaseName, ParameterDirection.Input));
            //parameters.Add(InnerConnection.CreateParameter("in_groups_filter", mygroups, ParameterDirection.Input));

            //foreach (CustomSearchField field in this.BasicSearchFields)
            //{
            //    var par = field.GetParameter(this.InnerConnection);
            //    if (par != null) parameters.Add(par);
            //}

            //foreach (CustomSearchField field in this.CustomSearchFields)
            //{
            //    var par = field.GetParameter(this.InnerConnection);
            //    if (par != null) parameters.Add(par);
            //}
            //dt = InnerConnection.FillData(ProcudureName, CommandType.StoredProcedure, parameters);

            return searchResult;
        }
         
        FINE COMMENTO */

        //NUOVA VERSIONE PER OTTIMIZZARE LE PERFORMANCE
        public virtual ICollection<User> SearchResultsByAnagraficaType(NetCms.Users.User user, string anagraficaType, int pageStart, int rpp, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICollection<User> searchResult = new List<User>();

            //Estrazione dei custom search fields che sono stati valorizzati nella ricerca per anagraficaType
            List<CustomSearchField> customFieldSelected = GetCustomSearchFieldsByAnagraficaType(anagraficaType).Where(x => x.SearchRequest.IsValidString || x.SearchRequest.IsValidInteger).ToList();

            //Lettura della anagrafica custom
            CustomAnagrafe.CustomAnagrafe anagrafica = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(anagraficaType.Replace("Anagrafica", ""));

            //Per questa anagrafica custom creo un oggetto contenente il riferimento alla stessa e il numero di campi che compaiono tra i custom search field valorizzati
            var result = new
            {
                Anagrafica = anagrafica,
                NumeroCampi = anagrafica.CustomFields.Count(x => customFieldSelected.Where(y => y.Key.ToLower() == x.Name.ToLower()).Count() > 0)
            };

            //Collezione di utenti amministrati dall'utente corrente
            NetCms.Users.Search.UserRelated rels = new UserRelated(user);
            //ICollection<User> usersToTake = rels.UsersIAdminByAnagraficaType(0, rels.UsersIAdminCountByAnagraficaType(anagraficaType, additionalJoins, additionalConditions), anagraficaType, additionalJoins, additionalConditions);

            //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
            //SearchParameter userIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
            //SearchParameter usersToTakeSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, userIdSP);

            //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
            List<SearchParameter> searchFieldSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
            //searchFieldSP.Add(usersToTakeSP);

            IGenericDAO<Hashtable> dao = new CriteriaNhibernateDAO<Hashtable>(UsersBusinessLogic.GetCurrentMapSession());

            IList<Hashtable> map = new List<Hashtable>();
            IList<Hashtable> founded;
            //Se � stato valorizzato il custom search field della mail, filtraggio per anagraficaBase
            if (customFieldSelected.Count(x => x.Key == "Email") == 1)
            {
                //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
                //SearchParameter userBaseIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
                //Codice inserito per filtrare solo gli utenti dell'anagrafica base ed evitare duplicati nella successiva ricerca all'interno delle custom anagrafi
                SearchParameter userAnagraficaTypeSP = new SearchParameter(null, "AnagraficaType", "AnagraficaBase", Finder.ComparisonCriteria.Equals, false);
                SearchParameter usersToTakeBaseSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { /*userBaseIdSP,*/ userAnagraficaTypeSP });

                //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
                List<SearchParameter> searchFieldBaseSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
                searchFieldBaseSP.Add(usersToTakeBaseSP);

                founded = dao.FindByCriteria("AnagraficaBase", searchFieldBaseSP.ToArray());
                map = map.Concat(founded.Where(x => x["User"] != null)).ToList();
            }

            //Per l'anagrafica custom, estrazione dei record in base ai criteri selezionati

            founded = dao.FindByCriteria("Anagrafica" + result.Anagrafica.Nome, searchFieldSP.ToArray());
            map = map.Concat(founded.Where(x => x["User"] != null)).ToList();

            //Collezione degli ID di tutti gli utenti da visualizzare
            List<int> iDsToShow = new List<int>();
            foreach (Hashtable tab in map)
            {
                Hashtable userHashtable = (Hashtable)tab["User"];
                iDsToShow.Add(int.Parse(userHashtable["ID"].ToString()));
            }

            //Filtro della ricerca con gli id da mostrare, paginati in base ai parametri in ingresso
            //searchResult = usersToTake.Where(x => iDsToShow.Contains(x.ID)).Skip(pageStart * rpp).Take(rpp).ToList();

            if (additionalConditions != null)
                additionalConditions = additionalConditions.Concat(new SearchCondition[] { new SearchCondition("ID", iDsToShow, SearchCondition.ComparisonType.In, SearchCondition.CriteriaType.User) }).ToArray();
            else
                additionalConditions = new SearchCondition[] { new SearchCondition("ID", iDsToShow, SearchCondition.ComparisonType.In, SearchCondition.CriteriaType.User) };

            searchResult = rels.UsersIAdmin(pageStart, rpp, additionalJoins, additionalConditions); // usersToTake.Where(x => iDsToShow.Contains(x.ID)).Skip(pageStart * rpp).Take(rpp).ToList();


            return searchResult;
        }

        /* INIZIO COMMENTO METODO PRE OTTIMIZZAZIONE PERFORMANCE
        public virtual ICollection<User> SearchResultsByAnagraficaType(NetCms.Users.User user, string anagraficaType, int pageStart, int rpp, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICollection<User> searchResult = new List<User>();

            //Estrazione dei custom search fields che sono stati valorizzati nella ricerca per anagraficaType
            List<CustomSearchField> customFieldSelected = GetCustomSearchFieldsByAnagraficaType(anagraficaType).Where(x => x.SearchRequest.IsValidString || x.SearchRequest.IsValidInteger).ToList();

            //Lettura della anagrafica custom
            CustomAnagrafe.CustomAnagrafe anagrafica = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(anagraficaType.Replace("Anagrafica", ""));

            //Per questa anagrafica custom creo un oggetto contenente il riferimento alla stessa e il numero di campi che compaiono tra i custom search field valorizzati
            var result = new
                         {
                             Anagrafica = anagrafica,
                             NumeroCampi = anagrafica.CustomFields.Count(x => customFieldSelected.Where(y => y.Key.ToLower() == x.Name.ToLower()).Count() > 0)
                         };

            //Collezione di utenti amministrati dall'utente corrente
            NetCms.Users.Search.UserRelated rels = new UserRelated(user);
            ICollection<User> usersToTake = rels.UsersIAdminByAnagraficaType(0, rels.UsersIAdminCountByAnagraficaType(anagraficaType, additionalJoins, additionalConditions), anagraficaType, additionalJoins, additionalConditions);

            //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
            SearchParameter userIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
            SearchParameter usersToTakeSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, userIdSP);

            //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
            List<SearchParameter> searchFieldSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
            searchFieldSP.Add(usersToTakeSP);

            IGenericDAO<Hashtable> dao = new CriteriaNhibernateDAO<Hashtable>(UsersBusinessLogic.GetCurrentMapSession());

            IList<Hashtable> map = new List<Hashtable>();
            IList<Hashtable> founded;
            //Se � stato valorizzato il custom search field della mail, filtraggio per anagraficaBase
            if (customFieldSelected.Count(x => x.Key == "Email") == 1)
            {
                //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
                SearchParameter userBaseIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
                //Codice inserito per filtrare solo gli utenti dell'anagrafica base ed evitare duplicati nella successiva ricerca all'interno delle custom anagrafi
                SearchParameter userAnagraficaTypeSP = new SearchParameter(null, "AnagraficaType", "AnagraficaBase", Finder.ComparisonCriteria.Equals, false);
                SearchParameter usersToTakeBaseSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { userBaseIdSP, userAnagraficaTypeSP });

                //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
                List<SearchParameter> searchFieldBaseSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
                searchFieldBaseSP.Add(usersToTakeBaseSP);

                founded = dao.FindByCriteria("AnagraficaBase", searchFieldBaseSP.ToArray());
                map = map.Concat(founded).ToList();
            }

            //Per l'anagrafica custom, estrazione dei record in base ai criteri selezionati

            founded = dao.FindByCriteria("Anagrafica" + result.Anagrafica.Nome, searchFieldSP.ToArray());
            map = map.Concat(founded).ToList();

            //Collezione degli ID di tutti gli utenti da visualizzare
            List<int> iDsToShow = new List<int>();
            foreach (Hashtable tab in map)
            {
                Hashtable userHashtable = (Hashtable)tab["User"];
                iDsToShow.Add(int.Parse(userHashtable["ID"].ToString()));
            }

            //Filtro della ricerca con gli id da mostrare, paginati in base ai parametri in ingresso
            searchResult = usersToTake.Where(x => iDsToShow.Contains(x.ID)).Skip(pageStart * rpp).Take(rpp).ToList();

            return searchResult;
        }
        FINE COMMENTO */

        //NUOVA VERSIONE PER OTTIMIZZARE LE PERFORMANCE
        public virtual int SearchResultsCount(NetCms.Users.User user, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICollection<User> searchResult = new List<User>();

            //Estrazione dei custom search fields che sono stati valorizzati nella ricerca
            List<CustomSearchField> customFieldSelected = CustomSearchFields.Where(x => x.SearchRequest.IsValidString || x.SearchRequest.IsValidInteger).ToList();

            //Lettura di tutte le anagrafiche custom
            List<CustomAnagrafe.CustomAnagrafe> anagrafiche = CustomAnagrafeBusinessLogic.FindAllCustomAnagrafe().ToList();

            //Per ciascuna anagrafica custom creo un oggetto contenente il riferimento alla stessa e il numero di campi che compaiono tra i custom search field valorizzati
            var result = from anag in anagrafiche
                         select new
                         {
                             Anagrafica = anag,
                             NumeroCampi = anag.CustomFields.Count(x => customFieldSelected.Where(y => y.Key.ToLower() == x.Name.ToLower()).Count() > 0)
                         };

            //Estrazione delle anagrafiche il cui numero di campi coincide con il numero di custom search fields valorizzati eccetto l'email che � contenuta nell'anagrafica base
            var resultFiltered = from res in result
                                 where res.NumeroCampi == customFieldSelected.Count(x => x.Key != "Email")
                                 select res;

            //Collezione di utenti amministrati dall'utente corrente
            NetCms.Users.Search.UserRelated rels = new UserRelated(user);
            //ICollection<User> usersToTake = rels.UsersIAdmin(0, rels.UsersIAdminCount(additionalJoins, additionalConditions), additionalJoins, additionalConditions);

            //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
            //SearchParameter userIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
            //SearchParameter usersToTakeSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, userIdSP);

            //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
            List<SearchParameter> searchFieldSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
            //searchFieldSP.Add(usersToTakeSP);

            IGenericDAO<Hashtable> dao = new CriteriaNhibernateDAO<Hashtable>(UsersBusinessLogic.GetCurrentMapSession());

            IList<Hashtable> map = new List<Hashtable>();
            //Se � stato valorizzato il custom search field della mail, filtraggio per anagraficaBase
            if (customFieldSelected.Count(x => x.Key == "Email") == 1)
            {
                //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
                //SearchParameter userBaseIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
                //Codice inserito per filtrare solo gli utenti dell'anagrafica base ed evitare duplicati nella successiva ricerca all'interno delle custom anagrafi
                SearchParameter userAnagraficaTypeSP = new SearchParameter(null, "AnagraficaType", "AnagraficaBase", Finder.ComparisonCriteria.Equals, false);
                SearchParameter usersToTakeBaseSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { /*userBaseIdSP,*/ userAnagraficaTypeSP });

                //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
                List<SearchParameter> searchFieldBaseSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
                searchFieldBaseSP.Add(usersToTakeBaseSP);

                IList<Hashtable> founded = dao.FindByCriteria("AnagraficaBase", searchFieldBaseSP.ToArray());
                map = map.Concat(founded.Where(x => x["User"] != null)).ToList();
            }

            //Per ciascuna anagrafica custom trovata, estrazione dei record in base ai criteri selezionati
            foreach (var item in resultFiltered)
            {
                IList<SearchParameter> searchFieldSPAdj = (from sf in searchFieldSP
                                                           where sf.Name != "Email"
                                                           let field = item.Anagrafica.CustomFields.Where(x => x.Name.ToLower() == sf.Name.ToLower()).First()
                                                           select new SearchParameter(field.CampoInAnagrafe.Label, field.Name, sf.Value, sf.ComparisonCriteria, sf.IsProducedByDefault)).ToList();

                if (searchFieldSP.Count(x => x.Name == "Email") > 0)
                    searchFieldSPAdj.Add(searchFieldSP.First(x => x.Name == "Email"));

                IList<Hashtable> founded = dao.FindByCriteria("Anagrafica" + item.Anagrafica.Nome, searchFieldSPAdj.ToArray());
                map = map.Concat(founded.Where(x => x["User"] != null)).ToList();
            }

            //Collezione degli ID di tutti gli utenti da visualizzare
            List<int> iDsToShow = new List<int>();
            foreach (Hashtable tab in map)
            {
                Hashtable userHashtable = (Hashtable)tab["User"];
                iDsToShow.Add(int.Parse(userHashtable["ID"].ToString()));
            }

            //Filtro della ricerca con gli id da mostrare, paginati in base ai parametri in ingresso
            //searchResult = usersToTake.Where(x => iDsToShow.Contains(x.ID)).Skip(pageStart * rpp).Take(rpp).ToList();

            if (additionalConditions != null)
                additionalConditions = additionalConditions.Concat(new SearchCondition[] { new SearchCondition("ID", iDsToShow, SearchCondition.ComparisonType.In, SearchCondition.CriteriaType.User) }).ToArray();
            else
                additionalConditions = new SearchCondition[] { new SearchCondition("ID", iDsToShow, SearchCondition.ComparisonType.In, SearchCondition.CriteriaType.User) };

            return rels.UsersIAdminCount(additionalJoins, additionalConditions); // usersToTake.Where(x => iDsToShow.Contains(x.ID)).Skip(pageStart * rpp).Take(rpp).ToList();
        }

        /* INIZIO COMMENTO METODO PRE OTTIMIZZAZIONE PERFORMANCE

        public virtual int SearchResultsCount(NetCms.Users.User user, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICollection<User> searchResult = new List<User>();

            //Estrazione dei custom search fields che sono stati valorizzati nella ricerca
            List<CustomSearchField> customFieldSelected = CustomSearchFields.Where(x => x.SearchRequest.IsValidString || x.SearchRequest.IsValidInteger).ToList();

            //Lettura di tutte le anagrafiche custom
            List<CustomAnagrafe.CustomAnagrafe> anagrafiche = CustomAnagrafeBusinessLogic.FindAllCustomAnagrafe().ToList();

            //Per ciascuna anagrafica custom creo un oggetto contenente il riferimento alla stessa e il numero di campi che compaiono tra i custom search field valorizzati
            var result = from anag in anagrafiche
                         select new
                         {
                             Anagrafica = anag,
                             NumeroCampi = anag.CustomFields.Count(x => customFieldSelected.Where(y => y.Key == x.Name).Count() > 0)
                         };

            //Estrazione delle anagrafiche il cui numero di campi coincide con il numero di custom search fields valorizzati eccetto l'email che � contenuta nell'anagrafica base
            var resultFiltered = from res in result
                                 where res.NumeroCampi == customFieldSelected.Count(x => x.Key != "Email")
                                 select res;

            //Collezione di utenti amministrati dall'utente corrente
            NetCms.Users.Search.UserRelated rels = new UserRelated(user);
            ICollection<User> usersToTake = rels.UsersIAdmin(0, rels.UsersIAdminCount(additionalJoins, additionalConditions), additionalJoins, additionalConditions);

            //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
            SearchParameter userIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
            SearchParameter usersToTakeSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, userIdSP);

            //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
            List<SearchParameter> searchFieldSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
            searchFieldSP.Add(usersToTakeSP);

            IGenericDAO<Hashtable> dao = new CriteriaNhibernateDAO<Hashtable>(UsersBusinessLogic.GetCurrentMapSession());

            int Numero = 0;
            //Se � stato valorizzato il custom search field della mail, filtraggio per anagraficaBase
            if (customFieldSelected.Count(x => x.Key == "Email") == 1)
            {
                //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
                SearchParameter userBaseIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
                //Codice inserito per filtrare solo gli utenti dell'anagrafica base ed evitare duplicati nella successiva ricerca all'interno delle custom anagrafi
                SearchParameter userAnagraficaTypeSP = new SearchParameter(null, "AnagraficaType", "AnagraficaBase", Finder.ComparisonCriteria.Equals, false);
                SearchParameter usersToTakeBaseSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { userBaseIdSP, userAnagraficaTypeSP });

                //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
                List<SearchParameter> searchFieldBaseSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
                searchFieldBaseSP.Add(usersToTakeBaseSP);

                Numero += dao.FindByCriteriaCount("AnagraficaBase", searchFieldBaseSP.ToArray());
            }
            //Per ciascuna anagrafica custom trovata, estrazione dei record in base ai criteri selezionati
            foreach (var item in resultFiltered)
            {
                Numero += dao.FindByCriteriaCount("Anagrafica" + item.Anagrafica.Nome, searchFieldSP.ToArray());
            }

            return Numero;
        }
        FINE COMMENTO */

        //NUOVA VERSIONE PER OTTIMIZZARE LE PERFORMANCE
        public virtual int SearchResultsCountByAnagraficaType(NetCms.Users.User user, string anagraficaType, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICollection<User> searchResult = new List<User>();

            //Estrazione dei custom search fields che sono stati valorizzati nella ricerca per anagraficaType
            List<CustomSearchField> customFieldSelected = GetCustomSearchFieldsByAnagraficaType(anagraficaType).Where(x => x.SearchRequest.IsValidString || x.SearchRequest.IsValidInteger).ToList();

            //Lettura della anagrafica custom
            CustomAnagrafe.CustomAnagrafe anagrafica = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(anagraficaType.Replace("Anagrafica", ""));

            //Per questa anagrafica custom creo un oggetto contenente il riferimento alla stessa e il numero di campi che compaiono tra i custom search field valorizzati
            var result = new
            {
                Anagrafica = anagrafica,
                NumeroCampi = anagrafica.CustomFields.Count(x => customFieldSelected.Where(y => y.Key.ToLower() == x.Name.ToLower()).Count() > 0)
            };

            //Collezione di utenti amministrati dall'utente corrente
            NetCms.Users.Search.UserRelated rels = new UserRelated(user);
            //ICollection<User> usersToTake = rels.UsersIAdminByAnagraficaType(0, rels.UsersIAdminCountByAnagraficaType(anagraficaType, additionalJoins, additionalConditions), anagraficaType, additionalJoins, additionalConditions);

            //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
            //SearchParameter userIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
            //SearchParameter usersToTakeSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, userIdSP);

            //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
            List<SearchParameter> searchFieldSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
            //searchFieldSP.Add(usersToTakeSP);

            IGenericDAO<Hashtable> dao = new CriteriaNhibernateDAO<Hashtable>(UsersBusinessLogic.GetCurrentMapSession());

            IList<Hashtable> map = new List<Hashtable>();
            IList<Hashtable> founded;
            //Se � stato valorizzato il custom search field della mail, filtraggio per anagraficaBase
            if (customFieldSelected.Count(x => x.Key == "Email") == 1)
            {
                //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
                //SearchParameter userBaseIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
                //Codice inserito per filtrare solo gli utenti dell'anagrafica base ed evitare duplicati nella successiva ricerca all'interno delle custom anagrafi
                SearchParameter userAnagraficaTypeSP = new SearchParameter(null, "AnagraficaType", "AnagraficaBase", Finder.ComparisonCriteria.Equals, false);
                SearchParameter usersToTakeBaseSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { /*userBaseIdSP,*/ userAnagraficaTypeSP });

                //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
                List<SearchParameter> searchFieldBaseSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
                searchFieldBaseSP.Add(usersToTakeBaseSP);

                founded = dao.FindByCriteria("AnagraficaBase", searchFieldBaseSP.ToArray());
                map = map.Concat(founded.Where(x => x["User"] != null)).ToList();
            }

            //Per l'anagrafica custom, estrazione dei record in base ai criteri selezionati

            founded = dao.FindByCriteria("Anagrafica" + result.Anagrafica.Nome, searchFieldSP.ToArray());
            map = map.Concat(founded.Where(x => x["User"] != null)).ToList();

            //Collezione degli ID di tutti gli utenti da visualizzare
            List<int> iDsToShow = new List<int>();
            foreach (Hashtable tab in map)
            {
                Hashtable userHashtable = (Hashtable)tab["User"];
                iDsToShow.Add(int.Parse(userHashtable["ID"].ToString()));
            }

            //Filtro della ricerca con gli id da mostrare, paginati in base ai parametri in ingresso
            //searchResult = usersToTake.Where(x => iDsToShow.Contains(x.ID)).Skip(pageStart * rpp).Take(rpp).ToList();

            if (additionalConditions != null)
                additionalConditions = additionalConditions.Concat(new SearchCondition[] { new SearchCondition("ID", iDsToShow, SearchCondition.ComparisonType.In, SearchCondition.CriteriaType.User) }).ToArray();
            else
                additionalConditions = new SearchCondition[] { new SearchCondition("ID", iDsToShow, SearchCondition.ComparisonType.In, SearchCondition.CriteriaType.User) };

            return rels.UsersIAdminCount(additionalJoins, additionalConditions);
        }

        /* INIZIO COMMENTO METODO PRE OTTIMIZZAZIONE PERFORMANCE
        public virtual int SearchResultsCountByAnagraficaType(NetCms.Users.User user, string anagraficaType, SearchJoin[] additionalJoins, SearchCondition[] additionalConditions)
        {
            ICollection<User> searchResult = new List<User>();

            //Estrazione dei custom search fields che sono stati valorizzati nella ricerca per anagraficaType
            List<CustomSearchField> customFieldSelected = GetCustomSearchFieldsByAnagraficaType(anagraficaType).Where(x => x.SearchRequest.IsValidString || x.SearchRequest.IsValidInteger).ToList();

            //Lettura della anagrafica custom
            CustomAnagrafe.CustomAnagrafe anagrafica = CustomAnagrafeBusinessLogic.GetCustomAnagrafeByName(anagraficaType.Replace("Anagrafica", ""));

            //Per questa anagrafica custom creo un oggetto contenente il riferimento alla stessa e il numero di campi che compaiono tra i custom search field valorizzati
            var result = new
                         {
                             Anagrafica = anagrafica,
                             NumeroCampi = anagrafica.CustomFields.Count(x => customFieldSelected.Where(y => y.Key == x.Name).Count() > 0)
                         };

            //Collezione di utenti amministrati dall'utente corrente
            NetCms.Users.Search.UserRelated rels = new UserRelated(user);
            ICollection<User> usersToTake = rels.UsersIAdminByAnagraficaType(0, rels.UsersIAdminCountByAnagraficaType(anagraficaType, additionalJoins, additionalConditions), anagraficaType, additionalJoins, additionalConditions);

            //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
            SearchParameter userIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
            SearchParameter usersToTakeSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, userIdSP);

            //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
            List<SearchParameter> searchFieldSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
            searchFieldSP.Add(usersToTakeSP);

            IGenericDAO<Hashtable> dao = new CriteriaNhibernateDAO<Hashtable>(UsersBusinessLogic.GetCurrentMapSession());

            int Numero = 0;
            //Se � stato valorizzato il custom search field della mail, filtraggio per anagraficaBase
            if (customFieldSelected.Count(x => x.Key == "Email") == 1)
            {
                //SearchParameter per filtrare gli id utente in funzione di quelli amministrati
                SearchParameter userBaseIdSP = new SearchParameter(null, "ID", usersToTake.Select(x => x.ID).ToList<int>(), Finder.ComparisonCriteria.In, false);
                //Codice inserito per filtrare solo gli utenti dell'anagrafica base ed evitare duplicati nella successiva ricerca all'interno delle custom anagrafi
                SearchParameter userAnagraficaTypeSP = new SearchParameter(null, "AnagraficaType", "AnagraficaBase", Finder.ComparisonCriteria.Equals, false);
                SearchParameter usersToTakeBaseSP = new SearchParameter(null, "User", SearchParameter.RelationTypes.OneToMany, new SearchParameter[] { userBaseIdSP, userAnagraficaTypeSP });

                //Appesa del searchparameter precedente ai searchParameters dei custom search fields valorizzati
                List<SearchParameter> searchFieldBaseSP = customFieldSelected.Select(x => x.GetSearchParameter()).ToList();
                searchFieldBaseSP.Add(usersToTakeBaseSP);

                Numero += dao.FindByCriteriaCount("AnagraficaBase", searchFieldBaseSP.ToArray());
            }

            //Per l'anagrafica custom, estrazione dei record in base ai criteri selezionati

            Numero += dao.FindByCriteriaCount("Anagrafica" + result.Anagrafica.Nome, searchFieldSP.ToArray());

            return Numero;
        }
        FINE COMMENTO */
    }
}