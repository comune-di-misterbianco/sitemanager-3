﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace Users.Cookie
{
    /// <summary>
    /// To be able to access System.Web.Security.CookieProtectionHelper.
    /// CookieProtectionHelperWrapper uses reflection to obtain a reference to the underlying methods 
    /// and exposes the same methods as of the original class.
    /// </summary>
    public static class CookieProtectionHelperWrapper
    {
        private static MethodInfo _encode;
        private static MethodInfo _decode;

        static CookieProtectionHelperWrapper()
        {
            // obtaining a reference to System.Web assembly
            Assembly systemWeb = typeof(HttpContext).Assembly;
            if (systemWeb == null)
            {
                throw new InvalidOperationException(
                    "Unable to load System.Web.");
            }
            // obtaining a reference to the internal class CookieProtectionHelper
            Type cookieProtectionHelper = systemWeb.GetType(
                    "System.Web.Security.CookieProtectionHelper");
            if (cookieProtectionHelper == null)
            {
                throw new InvalidOperationException(
                    "Unable to get the internal class CookieProtectionHelper.");
            }
            // obtaining references to the methods of CookieProtectionHelper class
            _encode = cookieProtectionHelper.GetMethod(
                    "Encode", BindingFlags.NonPublic | BindingFlags.Static);
            _decode = cookieProtectionHelper.GetMethod(
                    "Decode", BindingFlags.NonPublic | BindingFlags.Static);

            if (_encode == null || _decode == null)
            {
                throw new InvalidOperationException(
                    "Unable to get the methods to invoke.");
            }
        }
        public static string Encode(CookieProtection cookieProtection,
                                byte[] buf, int count)
        {
            //return (string)_encode.Invoke(null,
            //        new object[] { cookieProtection, buf, count });

            var protectedMessage = System.Web.Security.MachineKey.Protect(buf, "JWT Authentication token");
            var protectedMessageAsBase64 = Convert.ToBase64String(protectedMessage);
            return protectedMessageAsBase64;
        }

        public static byte[] Decode(CookieProtection cookieProtection,
                                    string data)
        {
            //return (byte[])_decode.Invoke(null,
            //        new object[] { cookieProtection, data });
                  

            var convertFromBase64 = Convert.FromBase64String(data);
           
            var unprotectedMessage = System.Web.Security.MachineKey.Unprotect(convertFromBase64, "JWT Authentication token");
            var unprotectedMessageAsBase64 = Convert.ToBase64String(unprotectedMessage);
            return unprotectedMessage;
        }
    }
}
