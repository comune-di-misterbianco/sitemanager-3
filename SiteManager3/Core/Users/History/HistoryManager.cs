using System;
using System.Web;
using System.Data;
using System.Collections;
/// <summary>
/// Summary description for xNodeCollection
/// </summary>

namespace WebFileSystem.GUI.History
{
    public class HistoryManager
    {
        private const string HistorySessionKey = "HistoryManagerEntriesObject";
        public HistoryEntriesCollection Entries;

        public HistoryManager()
        {
            /*if (HttpContext.Current.Session[HistorySessionKey]!=null)
                Entries = (HistoryEntriesCollection)HttpContext.Current.Session[HistorySessionKey];
            else
            {
                Entries = new HistoryEntriesCollection();
                HttpContext.Current.Session[HistorySessionKey] = Entries;
            }*/
            Entries = new HistoryEntriesCollection();

            HistoryEntry entry = new HistoryEntry(System.Web.HttpContext.Current.Request.Url.ToString());
            Entries.Add(entry);
        }
        
        public string GetBackURL()
        {
            if(Entries.CurrentEntry!=null)
                return Entries.CurrentEntry.Href;
            return NetCms.Configurations.Paths.AbsoluteSiteManagerRoot+"/default.aspx";    
        }

        public void UpdateHistoryData()
        {
            if (Entries.LastEntry.Href.ToLower() != System.Web.HttpContext.Current.Request.Url.ToString().ToLower())
            {
                HistoryEntry entry = new HistoryEntry(System.Web.HttpContext.Current.Request.Url.ToString());
                Entries.Add(entry);
            }
        }
    }
}