using System;
using System.Data;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for FilesDocument
/// </summary>
namespace WebFileSystem.GUI.History
{
    public class HistoryEntry
    {
        private string _Href;
        public string Href
        {
            get { return _Href; }
        }
        public string Key
        {
            get { return "History" + this.GetHashCode(); }
        }
        private int _Index;
        public int Index
        {
            get { return Index; }
            set 
            {
                if (_Index == -1)
                    _Index = value;
                else
                {
                    //throw new Exception("Propriet� Index gi� impostata sull'istanza della classe HistoryEntry");

                    NetCms.LogAndTrance.ExceptionManager.ExceptionLogger.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, "Propriet� Index gi� impostata sull'istanza della classe HistoryEntry", this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error);
                }
            }
        }
        public HistoryEntry(string href)
        {
            _Href = href.ToLower();
            _Index = -1;
        }

    }
}
