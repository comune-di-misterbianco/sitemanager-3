using System;
using System.Data;
using System.Collections;
/// <summary>
/// Summary description for xNodeCollection
/// </summary>

namespace WebFileSystem.GUI.History
{
    public class HistoryEntriesCollection : IEnumerable
    {
        private G2Collection coll;


        public int Count 
        { get { return coll.Count; } }

        public HistoryEntriesCollection()
        {
            coll = new G2Collection();
        }

        public void Add(HistoryEntry entry)
        {
            bool add = true;
            if (this.Contains(entry))
                add = !RemoveDeadEntries(entry);
            if(add)
                coll.Add(entry.Key, entry);
        }

        public void Remove(string key)
        {
            coll.Remove(key);
        }

        public void Remove(int index)
        {
            coll.Remove(index);
        }

        private bool RemoveDeadEntries(HistoryEntry entry)
        {
            int i = LastIndexOf(entry);
            if (this.Count - i < 3)
            {
                for (int j = this.Count - 1; j > i; j--)
                    this.coll.Remove(j);
                return true;
            }

            return false;
        }

        public HistoryEntry this[int i]
        {
            get
            {
                HistoryEntry value = (HistoryEntry)coll[coll.Keys[i]];
                return value;
            }
        }
        /*public HistoryEntry this[HistoryEntry entry]
        {
            get
            {
                HistoryEntry value = (HistoryEntry)coll[entry.Key];
                return value;
            }
        }
        */
        public bool Contains(HistoryEntry entry)
        {
            foreach (HistoryEntry selectedEntry in this)
                if (entry.Href == selectedEntry.Href)
                    return true;
            return false;
        }
        public HistoryEntry LastEntry
        {
            get
            {
                if (this.Count > 0)
                    return this[Count - 1];
                else
                    return null;
            }
        }
        public HistoryEntry CurrentEntry
        {
            get
            {
                if (this.Count > 1)
                    return this[Count - 2];
                else
                    return null;
            }
        }

        public int LastIndexOf(HistoryEntry entry)
        {
            return LastIndexOf(entry.Href);
        }

        public int LastIndexOf(string url)
        {
            int i = this.Count;
            bool found = false;
            while (!found && --i >= 0)
                if (url.ToLower() == this[i].Href.ToLower())
                    found = true;

            return i;
        }
        
    #region Enumerator

    public IEnumerator GetEnumerator()
    {
        return new CollectionEnumerator(this);
    }

    private class CollectionEnumerator : IEnumerator
    {
        private int CurentPos = -1;
        private HistoryEntriesCollection Collection;
        public CollectionEnumerator(HistoryEntriesCollection coll)
        {
            Collection = coll;
        }
        public object Current
        {
            get
            {
                return Collection[CurentPos];
            }
        }
        public bool MoveNext()
        {
            if (CurentPos < Collection.Count - 1)
            {
                CurentPos++;
                return true;
            }
            else
                return false;
        }
        public void Reset()
        {
            CurentPos = -1;
        }
    }
    #endregion
    }
}