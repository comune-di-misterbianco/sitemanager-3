﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NHibernate.Collection.Generic;

namespace NetCms.Users.Mapping
{
    public class UserNetworkMapping : ClassMap<UserNetwork>
    {
        public UserNetworkMapping()
        {
            Table("users_networks");
            Id(x => x.ID).Column("id_UserNetwork");

            References(x => x.Utente)
                .ForeignKey("user_usersnetworks")
                 .Column("User_UserNetwork")
                 .Not.Nullable()
                 .Fetch.Select();

            References(x => x.Network)
                .ForeignKey("network_usersnetworks")
                 .Column("Network_UserNetwork")
                 .Not.Nullable()
                 .Fetch.Select()
                 .LazyLoad(Laziness.False);
        }
    }
}
