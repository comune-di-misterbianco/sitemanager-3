﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.Mapping
{
    public class MacroGroupMapping : ClassMap<MacroGroup>
    {
        public MacroGroupMapping()
        {
            Table("groups_macrogroups");
            Id(x => x.ID).Column("id_MacroGroup");
            Map(x => x.Label).Column("Label_MacroGroup");
            Map(x => x.Key).Column("Key_MacroGroup");

            HasMany(x => x.Groups)
                .KeyColumn("MacroGroup_Group")
               .Fetch.Select().AsSet()
               .Cascade.All().Inverse();
        }
    }
}

