﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NHibernate.Collection.Generic;
using System.Collections;

namespace NetCms.Users.Mapping
{
    public class UserMapping : ClassMap<User>
    {
        public UserMapping()
        {
            Table("users");
            Id(x => x.ID).Column("id_User");
            Map(x => x.UserName).Column("Name_User");
            Map(x => x.PreviewType).Column("PreviewType_User").Default("0");
            Map(x => x.Password).Column("Password_User");
            Map(x => x.State).Column("State_User").Default("2");// era impostato a default = . Adesso in fase di inserimento l'account viene settato a 2 (mai attivato) - cosi da distinguere gli utenti mai attivati e diagnosticare il motivo.
            Map(x => x.DeletedInt).Column("Deleted_User").Default("0");
            Map(x => x.FoldersGUI).Column("FoldersGUI_User").Default("0");
            Map(x => x.Editor).Column("Editor_User").Default("0");
            //Map(x => x.Type).Column("Type_User").Default("0");
            Map(x => x.AnagraficaType).Column("AnagraficaType_User").CustomSqlType("varchar(1024)").Default("0");
            Map(x => x.ActivationKey).Column("Key_User");
            Map(x => x.NuovaPassword).Column("NewPassword_User");
            Map(x => x.CreationTime).Column("CreationTime_User");
            Map(x => x.LastAccess).Column("LastAccess_User");
            Map(x => x.LastUpdate).Column("LastUpdate_User");
            Map(x => x.God).Column("God_User").Default("0");
            Map(x => x.Debug).Column("Debug_User").Default("0");
            Map(x => x.Registrato).Default("0");
            Map(x => x.CurrentNetworkPath);
            Map(x => x.OldID).Column("OldID_User").Default("-1");
            //Map(x => x.AnagraficaID).Column("Anagrafica_User").Not.Nullable();

            References(x => x.AnagraficaMapped, "Anagrafica_User").EntityName("AnagraficaBase")
              .ForeignKey("users_anagrafica").Unique().Cascade.All().LazyLoad(Laziness.False);

            References(x => x.Profile, "Profile_User")
                .ForeignKey("users_profile").Unique().Cascade.All().LazyLoad(Laziness.False);

            HasMany(x => x.AssociatedGroups)
                .KeyColumn("User_UserGroup")
                .Fetch.Select().AsSet()
                .Cascade.All()
                .Inverse();
                //.Not.LazyLoad();

            HasMany(x => x.Notifiche)
               .KeyColumn("Destinatario")
               .Fetch.Select().AsSet()
               .Cascade.All()
               .Inverse();
               //.Not.LazyLoad();

            HasMany(x => x.AssociatedNetworks)
                .KeyColumn("User_UserNetwork")
                .Fetch.Select().AsSet()
                .Cascade.All()
                .Inverse();

            HasManyToMany(x => x.FavoritesFolders)
                .ChildKeyColumn("Folder_Favorite")
                .ParentKeyColumn("User_Favorite")
                .Table("favorites")
                .Fetch.Select()
                .AsSet()
                .Cascade.All();
        }
    }
}

