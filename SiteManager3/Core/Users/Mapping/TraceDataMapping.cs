﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.Models;

namespace Users.Mapping
{
    public class TraceDataMapping : ClassMap<TraceData>
    {
        public TraceDataMapping()
        {
            Table("users_tracing");
            Id(x => x.ID);
          
            Map(x => x.PublicIp);
            Map(x => x.OS);
            Map(x => x.Browser);
            Map(x => x.Url);
            Map(x => x.Referer);
            Map(x => x.SessionID);
            Map(x => x.Cookies);
            Map(x => x.AcceptLanguage);
            Map(x => x.DateOra);

            Map(x => x.HypotheticUserType);

            Map(x => x.PositionStatus);
            Map(x => x.Latitudine);
            Map(x => x.Longitudine);            
        }
    }
}
