﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.Mapping
{
    public class ProfileMapping : ClassMap<Profile>
    {
        public ProfileMapping()
        {
            Table("profiles");
            Id(x => x.ID).Column("id_Profile");
            Map(x => x.Nome).Column("Name_Profile");
        }
    }
}

