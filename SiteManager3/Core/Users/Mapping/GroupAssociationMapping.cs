﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.Mapping
{
    public class GroupAssociationMapping : ClassMap<GroupAssociation>
    {
        public GroupAssociationMapping()
        {
            Table("usersgroups");
            CompositeId()
            .KeyReference(x => x.User, "User_UserGroup")
            .KeyReference(x => x.Group, "Group_UserGroup");

            Map(x => x.Type).Column("Type_UserGroup").Default("0");
            Map(x => x.PublishRole).Column("PublishRole_UserGroup").Default("0");

            References(x => x.User)
                .Column("User_UserGroup")
                .Fetch.Select()
                //.LazyLoad(Laziness.False)
                .Not.Insert();

            References(x => x.Group)
                .Column("Group_UserGroup")
                .Fetch.Select()
                .LazyLoad(Laziness.False)
                .Not.Insert();

        }
    }
}

