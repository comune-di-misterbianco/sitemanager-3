﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.Mapping
{
    public class GroupMapping : ClassMap<Group>
    {
        public GroupMapping()
        {
            //Table("groups");
            Table("`groups`");
            Id(x => x.ID).Column("id_Group");
            Map(x => x.Name).Column("Name_Group");
            Map(x => x.Tree).Column("Tree_Group");
            Map(x => x.Depth).Column("Depth_Group");
            Map(x => x.SystemName).Column("SystemKey_Group");

            References(x => x.Profile, "Profile_Group")
                .ForeignKey("groups_profile").Unique().Cascade.All().LazyLoad(Laziness.False);

            References(x => x.Parent)
                 .ForeignKey("groups_parent")
                 .Column("Parent_Group")
                 .Nullable()
                 .Fetch.Select()
                 .LazyLoad(Laziness.False);

            HasMany(x => x.Groups)
                .KeyColumn("Parent_Group")
               .Fetch.Select().AsSet()
               .Cascade.All().Inverse().Not.LazyLoad();

            References(x => x.MacroGroup)
                 .Column("MacroGroup_Group")
                 .Nullable()
                 .Fetch.Select();

            HasMany(x => x.AssociatedUsers)
                .KeyColumn("Group_UserGroup")
                .Fetch.Select().AsSet()
                .Cascade.All()
                .Inverse()
                .Not.LazyLoad();
        }
    }
}

