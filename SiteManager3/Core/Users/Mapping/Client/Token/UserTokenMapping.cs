﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Mapping.Client.Token
{
    public class UserTokenMapping : ClassMap<Users.Models.Token.UserTokens.UserToken>
    {
        public UserTokenMapping()
        {
            Table("user_tokens");
            Id(x => x.Id).Column("Id")/*.GeneratedBy.GuidComb()*/;

            Map(x => x.UserId).Column("UserId").Default(null);
            Map(x => x.LoginProvider).Column("LoginProvider").Default(null);

            Map(x => x.Name).Column("Name").Default(null);
            Map(x => x.Value).Column("Value").Default(null).CustomSqlType("mediumtext");
        }
    }
}
