﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Mapping.Client
{
    public class ClientMapping : ClassMap<Users.Models.Client.Client>
    {
        public ClientMapping()
        {
            Table("clients");
            Id(x => x.Id).Column("Id");
            Map(x => x.ClientId).Column("ClientId");
            Map(x => x.ClientSecret).Column("ClientSecret");

            Map(x => x.Disabled).Column("Disabled").Default("0");
            Map(x => x.DisableBy).Column("DisableBy");
            Map(x => x.DisableTimestamp).Column("DisableTimestamp").Default(null);
        }
    }
}
