﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users.Mapping.Client
{
    public class ClientClaimsMapping : ClassMap<Users.Models.Client.ClientClaim>
    {
        public ClientClaimsMapping()
        {
            Table("clientclaims");
            Id(x => x.Id).Column("Id"); 

            Map(x => x.ClaimType).Column("ClaimType")/*.Default(Users.Models.Client.ClientClaim.ClientClaimRole)*/;
            Map(x => x.ClaimValue).Column("ClaimValue");

            References(x => x.ClientRef, "ClientRef")
               .ForeignKey("clientref").Cascade.All();           
        }        
    }
}
