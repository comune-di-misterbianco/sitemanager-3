﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.Mapping
{
    public class NotificaSistemaMapping : SubclassMap<NotificaSistema>
    {
        public NotificaSistemaMapping()
        {
            Table("notifiche_system");
            KeyColumn("id_Notifica");
        }
    }
}

