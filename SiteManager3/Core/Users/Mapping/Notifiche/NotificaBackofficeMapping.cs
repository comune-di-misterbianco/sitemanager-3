﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.Mapping
{
    public class NotificaBackofficeMapping : SubclassMap<NotificaBackoffice>
    {
        public NotificaBackofficeMapping()
        {
            Table("notifiche_backoffice");
            KeyColumn("id_Notifica");
            Map(x => x.NotifyBackofficeType);
        }
    }
}

