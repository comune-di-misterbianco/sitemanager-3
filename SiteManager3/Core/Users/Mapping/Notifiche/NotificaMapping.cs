﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.Mapping
{
    public class NotificaMapping : ClassMap<Notifica>
    {
        public NotificaMapping()
        {
            Table("notifiche");
            Id(x => x.ID);
            Map(x => x.Titolo).CustomSqlType("text");
            Map(x => x.Testo).CustomSqlType("text");
            Map(x => x.Letta);
            Map(x => x.Cancellata).Default("false");
            Map(x => x.Link);
            Map(x => x.Data);

            References(x => x.Network)
                 .Column("Network")
                 .Fetch.Select();

            References(x => x.Destinatario)                 
                 .Column("Destinatario")
                 .Not.Nullable()      
                 .Fetch.Select();

            References(x => x.Responsabile)
                 .Column("Responsabile")
                 .Nullable()
                 .Fetch.Select();
        }
    }
}
