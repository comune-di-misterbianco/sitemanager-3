﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.Mapping
{
    public class NotificaFrontendMapping : SubclassMap<NotificaFrontend>
    {
        public NotificaFrontendMapping()
        {
            Table("notifiche_frontend");
            KeyColumn("id_Notifica");
        }
    }
}

