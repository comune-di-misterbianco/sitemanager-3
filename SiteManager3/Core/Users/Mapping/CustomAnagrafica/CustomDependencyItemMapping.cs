﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomDependencyItemMapping : ClassMap<CustomDependencyItem>
    {
        public CustomDependencyItemMapping()
        {
            Table("custom_field_dependency_item");
            Id(x => x.ID);
            Map(x => x.DependencyDDValue);
            Map(x => x.isRequiredForValue);

            References(x => x.DependentField)
                 .Column("IDCustomField")
                 .Nullable()
                 .Fetch.Select();

        }
    }
}