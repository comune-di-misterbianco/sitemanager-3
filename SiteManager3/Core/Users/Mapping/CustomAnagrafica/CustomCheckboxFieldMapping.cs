﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomCheckboxFieldMapping : SubclassMap<CustomCheckboxField>
    {
        public CustomCheckboxFieldMapping()
        {
            Table("custom_field_checkbox");
            KeyColumn("IDcustom_field_checkbox");
            Map(x => x.Value);
        }
    }
}
