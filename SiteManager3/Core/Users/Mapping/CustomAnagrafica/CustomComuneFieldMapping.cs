﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomComuneFieldMapping : SubclassMap<CustomComuneField>
    {
        public CustomComuneFieldMapping()
        {
            Table("custom_field_comune");
            KeyColumn("IDcustom_field_comune");
        }
    }
}
