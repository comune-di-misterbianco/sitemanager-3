﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomForeignKeyToMappedTableFieldMapping : SubclassMap<CustomForeignKeyToMappedTableField>
    {
        public CustomForeignKeyToMappedTableFieldMapping()
        {
            Table("custom_field_foreignKeyMapped");
            KeyColumn("IDcustom_field_foreignKeyMapped");
            Map(x => x.PropertyName);
            Map(x => x.MappedEntityName);
            Map(x => x.CustomId);
        }
    }
}
