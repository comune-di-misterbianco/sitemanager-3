﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomAnagrafeApplicazioniMapping : ClassMap<CustomAnagrafeApplicazioni>
    {
        public CustomAnagrafeApplicazioniMapping()
        {
            Table("custom_anagrafe_applicazioni");
            Id(x => x.ID);
            Map(x => x.IdApplicazione).Not.Nullable();

            References(x => x.Anagrafe)
                 .Column("IDCustomAnagrafe")
                 .Not.Nullable()
                 .Fetch.Select();

        }
    }
}
