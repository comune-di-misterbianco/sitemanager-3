﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class ProvinciaMapping : ClassMap<Provincia>
    {
        public ProvinciaMapping()
        {
            Table("province");
            Cache.ReadOnly();
            ReadOnly();
            Id(x => x.ID).Column("IDProvincia");
            Map(x => x.Nome).Column("provincia");
            Map(x => x.Sigla).Column("sigla_provincia").CustomSqlType("varchar(2)");
            Map(x => x.CodiceProvincia).Column("codice_provincia").CustomSqlType("varchar(3)").Unique().Not.Nullable().Index("codice_provincia");
            Map(x => x.Attivo).Column("attivo").Default("1");

            HasMany(x => x.Comuni)
                .KeyColumn("IDProvincia")
                .Fetch.Select().AsSet()
                .Cascade.All();

            References(x => x.Regione)
                 .Column("ID_Regione")
                 .ForeignKey("provincia_regione")
                 .Fetch.Select();
        }
    }
}
