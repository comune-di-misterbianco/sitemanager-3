﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class ComuneMapping : ClassMap<Comune>
    {
        public ComuneMapping()
        {
            Table("comuni");
            Cache.ReadOnly();
            ReadOnly();
            Id(x => x.ID).Column("IDcomune");
            Map(x => x.Nome).Column("comune").Not.Nullable();
            Map(x => x.CodiceComune).Column("codice_comune").CustomSqlType("varchar(6)").Not.Nullable().Unique().Index("codice_comune");
            Map(x => x.CodiceCatastale).Column("codice_catastale").CustomSqlType("varchar(20)");
            Map(x => x.Attivo).Column("attivo");

            References(x => x.Provincia)
                 .Column("IDProvincia")
                 .ForeignKey("comune_provincia")
                 .Fetch.Select();
        }
    }
}
