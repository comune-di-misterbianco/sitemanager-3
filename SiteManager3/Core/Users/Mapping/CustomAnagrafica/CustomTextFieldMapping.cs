﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomTextFieldMapping : SubclassMap<CustomTextField>
    {
        public CustomTextFieldMapping()
        {
            Table("custom_field_text");
            KeyColumn("IDcustom_field_text");
            Map(x => x.MaxLength);
            Map(x => x.MinLength);
            Map(x => x.Columns).Default("0");
            Map(x => x.isMultiline).Default("false");
            Map(x => x.Rows).Default("0");
            Map(x => x.InputType);
        }
    }
}
