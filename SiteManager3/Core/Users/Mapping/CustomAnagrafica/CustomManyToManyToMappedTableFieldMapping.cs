﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomManyToManyToMappedTableFieldMapping : SubclassMap<CustomManyToManyToMappedTableField>
    {
        public CustomManyToManyToMappedTableFieldMapping()
        {
            Table("custom_field_manyToManyMapped");
            KeyColumn("IDcustom_field_manyToManyMapped");
            Map(x => x.ControlloAssociato);
            Map(x => x.NomeCampoIdentificativo);
            Map(x => x.NomeCampoEtichetta);
            Map(x => x.MappedEntityName);
            Map(x => x.MappedTableName);
        }
    }
}
