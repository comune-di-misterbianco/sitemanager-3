﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomGroupMapping : ClassMap<CustomGroup>
    {
        public CustomGroupMapping()
        {
            Table("custom_group");
            Id(x => x.ID);
            Map(x => x.Nome);
            Map(x => x.Descrizione);
            Map(x => x.Posizione);
            Map(x => x.DependsOnAnotherField).Default("false");
            Map(x => x.DependencyCustomFieldName).Nullable();

            HasMany(x => x.CustomFields)
                .KeyColumn("IDCustomGroup")
                .Fetch.Select().AsSet()
                .Cascade.All()
                .Inverse().Not.LazyLoad();

            References(x => x.CustomAnagrafe)
                 .Column("IDCustomAnagrafe")
                 .Nullable()
                 .Fetch.Select();
        }
    }
}
