﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomFieldMapping : ClassMap<CustomField>
    {
        public CustomFieldMapping()
        {
            Table("custom_field");
            Id(x => x.ID);
            Map(x => x.Name);
            Map(x => x.InfoDescription);
            Map(x => x.Required);
            Map(x => x.ShowInDetails);
            Map(x => x.ShowInSearch);
            Map(x => x.Cancellato).Default("0");
            Map(x => x.IsUsername);
            Map(x => x.CriterioRicerca);
            Map(x => x.PositionInGroup).Default("0");
            Map(x => x.ColumnCssClass);
            Map(x => x.RowOrder);

            References(x => x.NewField)
                .Column("IDCampoCustomNuovo")
                .Nullable();

            References(x => x.Anagrafe)
                 .Column("IDCustomAnagrafe")
                 .Nullable()
                 .Fetch.Select();

            References(x => x.CustomGroup)
                 .Column("IDCustomGroup")
                 .Nullable()
                 .Fetch.Select();

            HasMany(x => x.DependencyValuesCollection)
               .KeyColumn("IDCustomField")
               .Fetch.Select().AsSet()
               .Cascade.AllDeleteOrphan()
               .Inverse().Not.LazyLoad();
        }
    }
}
