﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomDropdownValueMapping : ClassMap<CustomDropdownValue>
    {
        public CustomDropdownValueMapping()
        {
            Table("custom_field_dropdown_values");
            Id(x => x.ID);
            Map(x => x.Value);
            Map(x => x.Label);

            References(x => x.CustomField)
                 .Column("IDDropdownField")
                 .Fetch.Select();

        }
    }
}
