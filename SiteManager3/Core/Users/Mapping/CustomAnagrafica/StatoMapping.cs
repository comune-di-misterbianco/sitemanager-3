﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class StatoMapping : ClassMap<Stato>
    {
        public StatoMapping()
        {
            Table("stati_cittadinanza");
            Cache.ReadOnly();
            ReadOnly();
            Id(x => x.ID).Column("IDStato");
            Map(x => x.Nome).Column("stato");
            Map(x => x.MembroCEE).Column("membro_cee");
            Map(x => x.Cittadinanza).Column("cittadinanza");
            Map(x => x.CodiceStato).Column("cod_stato").CustomSqlType("varchar(20)");
        }
    }
}
