﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomCheckboxListFieldMapping : SubclassMap<CustomCheckboxListField>
    {
        public CustomCheckboxListFieldMapping()
        {
            Table("custom_field_checkboxlist");
            KeyColumn("IDcustom_field_checkboxlist");
            Map(x => x.AtLeastOneRequired);
            Map(x => x.RepeatHorizontal);

            HasMany(x => x.Values)
                .KeyColumn("IDCheckboxListField")
               .Fetch.Select().AsSet()
               .Cascade.AllDeleteOrphan()
               .Inverse().Not.LazyLoad();
        }
    }
}
