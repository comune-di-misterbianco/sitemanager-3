﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomDropdownFieldMapping : SubclassMap<CustomDropdownField>
    {
        public CustomDropdownFieldMapping()
        {
            Table("custom_field_dropdown");
            KeyColumn("IDcustom_field_dropdown");

            HasMany(x => x.Values)
                .KeyColumn("IDDropdownField")
               .Fetch.Select().AsSet()
               .Cascade.AllDeleteOrphan()
               .Inverse().Not.LazyLoad();

            
        }
    }
}
