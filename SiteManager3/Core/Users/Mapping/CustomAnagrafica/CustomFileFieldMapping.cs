﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomFileFieldMapping : SubclassMap<CustomFileField>
    {
        public CustomFileFieldMapping()
        {
            Table("custom_field_file");
            KeyColumn("IDcustom_field_file");
            Map(x => x.FileType);
            Map(x => x.FileTypeDesc);
            Map(x => x.UploadButtonText);
            Map(x => x.UploadFileHandler);
            Map(x => x.UploadFolder);
            Map(x => x.MultiUpload);
            Map(x => x.AutoUpload);
            Map(x => x.SwfObjectFilePath);            
            Map(x => x.UploadifyScriptsFolderPath);
        }
    }
}
