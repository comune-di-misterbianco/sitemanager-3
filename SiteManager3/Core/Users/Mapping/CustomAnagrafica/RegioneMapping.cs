﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class RegioneMapping : ClassMap<Regione>
    {
        public RegioneMapping()
        {
            Table("regioni");
            Cache.ReadOnly();
            ReadOnly();
            Id(x => x.ID).Column("ID_Regione");
            Map(x => x.Nome).Column("regione");
            Map(x => x.Attivo);

            HasMany(x => x.Province)
                .KeyColumn("ID_Regione")
                .Fetch.Select().AsSet()
                .Cascade.All();
        }
    }
}
