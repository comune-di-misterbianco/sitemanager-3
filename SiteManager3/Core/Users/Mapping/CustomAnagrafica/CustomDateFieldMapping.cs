﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomDateFieldMapping : SubclassMap<CustomDateField>
    {
        public CustomDateFieldMapping()
        {
            Table("custom_field_date");
            KeyColumn("IDcustom_field_date");
            Map(x => x.MaxDate);
            Map(x => x.MinDate);
            Map(x => x.Formato);
        }
    }
}
