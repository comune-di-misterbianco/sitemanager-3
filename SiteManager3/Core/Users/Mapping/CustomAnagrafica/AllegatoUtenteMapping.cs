﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class AllegatoUtenteMapping : ClassMap<AllegatoUtente>
    {
        public AllegatoUtenteMapping()
        {
            Table("anagrafica_attachments");
            Id(x => x.ID);
            Map(x => x.File).CustomSqlType("mediumblob");
            Map(x => x.FileName);
            Map(x => x.ContentType);
            References(x => x.AnagraficaMapped, "AnagraficaID")
                .EntityName("AnagraficaBase")
                .ForeignKey("attachments_anagrafica")                
                //.Cascade.All()
                .LazyLoad(Laziness.False);
        }
    }
}
