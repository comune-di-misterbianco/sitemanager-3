﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomCheckboxListValueMapping : ClassMap<CustomCheckboxListValue>
    {
        public CustomCheckboxListValueMapping()
        {
            Table("custom_field_checkboxlist_values");
            Id(x => x.ID);
            Map(x => x.Value);
            Map(x => x.Label);

            References(x => x.CustomField)
                 .Column("IDCheckboxListField")
                 .Fetch.Select();
        }
    }
}
