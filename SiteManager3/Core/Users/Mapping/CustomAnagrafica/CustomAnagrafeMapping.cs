﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomAnagrafeMapping : ClassMap<CustomAnagrafe>
    {
        public CustomAnagrafeMapping()
        {
            Table("custom_anagrafe");
            Id(x => x.ID);
            Map(x => x.Nome);
            Map(x => x.Installata);
            Map(x => x.TestoEmailAttivazione).CustomSqlType("TEXT");
            Map(x => x.TestoEmailRegistrazioneNegata).CustomSqlType("TEXT");
            Map(x => x.UseSpid);
            Map(x => x.RenderType);
            Map(x => x.VisibileInFrontend);
            Map(x => x.EmailAsUserName);
            Map(x => x.ValidaConCaptcha);
            Map(x => x.AggiungiInformativaPrivacy);
            Map(x => x.TestoInformativaPrivacy).CustomSqlType("TEXT"); 
            Map(x => x.LabelInformativaPrivacy);
            Map(x => x.DomandaAccettazioneInformativaPrivacy);
            Map(x => x.RichiediConfermaEmail);
            Map(x => x.NumeroRigheDaVisualizzareAreaInformativa);
            Map(x => x.NumeroCaratteriDaVisualizzareAreaInformativa);

            References(x => x.DefaultGroup)
                 .Nullable()
                 .Fetch.Select()
                 .Cascade.All();

            HasMany(x => x.Applicazioni)
                .KeyColumn("IDCustomAnagrafe")
               .Fetch.Select().AsSet()
               .Cascade.All()
               .Inverse().Not.LazyLoad();

            HasMany(x => x.CustomFields)
                .KeyColumn("IDCustomAnagrafe")
               .Fetch.Select().AsSet()
               .Cascade.All()
               .Inverse().Not.LazyLoad();

            HasMany(x => x.CustomGroups)
                .KeyColumn("IDCustomAnagrafe")
               .Fetch.Select().AsSet()
               .Cascade.All()
               .Inverse().Not.LazyLoad();

            HasMany(x => x.FrontendFields)
                .KeyColumn("IDFrontCustomAnagrafe")
               .Fetch.Select().AsSet()
               .Cascade.All().Not.LazyLoad();

            References(x => x.RegRequestNotifyGroup)
                 .Nullable()
                 .Fetch.Select()
                 .Cascade.All();
        }
    }
}
