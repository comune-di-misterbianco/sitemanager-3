﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.CustomAnagrafe.Mapping
{
    public class CustomDropdownProvinceFieldMapping : SubclassMap<CustomProvinciaField>
    {
        public CustomDropdownProvinceFieldMapping()
        {
            Table("custom_field_provincia");
            KeyColumn("IDcustom_field_provincia");
        }
    }
}
