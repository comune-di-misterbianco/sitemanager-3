﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.Mapping
{
    public class GroupProfileMapping : SubclassMap<GroupProfile>
    {
        public GroupProfileMapping()
        {
            Table("groups_profile");
            KeyColumn("id_Profile");

            HasOne(x => x.Group)
                 .PropertyRef(r => r.Profile)
                 .Cascade.All();
        }
    }
}

