﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Users.Mapping
{
    public class UserProfileMapping : SubclassMap<UserProfile>
    {
        public UserProfileMapping()
        {
            Table("users_profile");
            KeyColumn("id_Profile");

            HasOne(x => x.User)
                 .PropertyRef(r => r.Profile)
                 .Cascade.All();
        }
    }
}

