using System;
using System.Collections.Generic;

namespace NetCms.Users
{
    
    public interface Entity
    {
        //Profile Profile { get;}

        bool Abilitato { get; }
        int ID { get; }

        int Depth { get;}

        //GroupsCollection Groups { get;} 

        ICollection<Group> Groups { get; }
    }
}