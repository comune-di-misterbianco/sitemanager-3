//using System;
//using System.Collections;
///// <summary>
///// Summary description for xNodeCollection
///// </summary>

//namespace NetCms.Users
//{
//    public class GroupAssociationCollection : IEnumerable
//    {
//        private G2Collection coll;

//        public int Count { get { return coll.Count; } }

//        public GroupAssociationCollection()
//        {
//            coll = new G2Collection();
//        }

//        public void Add(GroupAssociation association)
//        {
//            coll.Add(association.ID.ToString(), association);
//        }

//        public GroupAssociation this[int i]
//        {
//            get
//            {
//                GroupAssociation value = (GroupAssociation)coll[coll.Keys[i]];
//                return value;
//            }
//        }

//        public GroupAssociation this[string key]
//        {
//            get
//            {
//                GroupAssociation field = (GroupAssociation)coll[key];
//                return field;
//            }
//        }
//    #region Enumerator

//    public IEnumerator GetEnumerator()
//    {
//        return new CollectionEnumerator(this);
//    }

//    private class CollectionEnumerator : IEnumerator
//    {
//        private int CurentPos = -1;
//        private GroupAssociationCollection Collection;
//        public CollectionEnumerator(GroupAssociationCollection coll)
//        {
//            Collection = coll;
//        }
//        public object Current
//        {
//            get
//            {
//                return Collection[CurentPos];
//            }
//        }
//        public bool MoveNext()
//        {
//            if (CurentPos < Collection.Count - 1)
//            {
//                CurentPos++;
//                return true;
//            }
//            else
//                return false;
//        }
//        public void Reset()
//        {
//            CurentPos = -1;
//        }
//    }
//    #endregion
//    }
//}