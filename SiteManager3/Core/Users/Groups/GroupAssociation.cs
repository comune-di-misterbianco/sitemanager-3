using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using NetCms.Connections;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users
{
    public class GroupAssociation
    {
        #region ProprietÓ da mappare

        public virtual Group Group
        {
            get;
            set;
        }

        public virtual User User
        {
            get;
            set;
        }

        public virtual int Type
        {
            get;
            set;
        }

        public virtual int PublishRole
        {
            get;
            set;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;
            hashCode = hashCode ^ User.ID.GetHashCode() ^ Group.ID.GetHashCode();
            return hashCode;
        }

        public override bool Equals(object obj)
        {
            var toCompare = obj as GroupAssociation;
            if (toCompare == null)
            {
                return false;
            }
            return (this.GetHashCode() == toCompare.GetHashCode());
        }

        #endregion

        //public Group Group
        //{
        //    get {
        //        if (_Group == null)
        //        {
        //            _Group = GroupsHandler.RootGroup.FindGroup(this.GroupID);
        //        }
        //        return _Group; 
        //    }
        //}
        //private Group _Group;

        //public string ID
        //{
        //    get { return _ID; }
        //}
        //private string _ID;

        public virtual bool IsAdministrator
        {
            get
            {
                return this.GrantsRole != GrantAdministrationTypes.User;
            }
        }

        public virtual GrantAdministrationTypes GrantsRole
        {
            get
            {
                switch (Type)
                {
                    case 1: return GrantAdministrationTypes.Administrator;
                    case 2: return GrantAdministrationTypes.SuperAdministrator;
                    default: return GrantAdministrationTypes.User;
                }
            }
        }

        public virtual ContentAdministrationTypes ContentsRole
        {
            get
            {
                switch (PublishRole)
                {
                    case 1: return ContentAdministrationTypes.Review;
                    case 2: return ContentAdministrationTypes.Publish;
                    default: return ContentAdministrationTypes.Write;
                }
            }
        }

        public virtual string ContentsRoleLabel
        {
            get
            {
                switch (ContentsRole)
                {
                    case ContentAdministrationTypes.Publish: return "Pubblicatore";
                    case ContentAdministrationTypes.Review: return "Revisionatore";
                    default: return "Redattore";
                }
            }
        }

        public virtual string GrantsRoleLabel
        {
            get
            {
                switch (GrantsRole)
                {
                    case GrantAdministrationTypes.Administrator: return "Amministratore";
                    case GrantAdministrationTypes.SuperAdministrator: return "Super Amministratore";
                    default: return "Utente";
                }
            }
        }

        //private User _User;
        //public User User
        //{
        //    get
        //    {
        //        if (_User == null)
        //            _User = new User(this.UserID);
        //        return _User;
        //    }
        //}

        //public string UserID
        //{
        //    get { return _UserID; }
        //    private set { _UserID = value; }
        //}
        //private string _UserID;

        //public string GroupID
        //{
        //    get { return _GroupID; }
        //    private set { _GroupID = value; }
        //}
        //private string _GroupID;

        public virtual int UserID
        {
            get
            {
                return User.ID;
            }
        }

        public virtual int GroupID
        {
            get
            {
                return Group.ID;
            }
        }

        public GroupAssociation()
        {
 
        }
			
        //public GroupAssociation(DataRow row)
        //    : this(row, null, null)
        //{

        //}

        //public GroupAssociation(DataRow row, Group group)
        //    : this(row, null, group)
        //{

        //}

        //public GroupAssociation(DataRow row, User user)
        //    : this(row,user,null)
        //{

        //}

        //public GroupAssociation(DataRow row, User user, Group group)
        //{
        //    _User = user;
        //    _Group = group;

        //    UserID = row["User_UserGroup"].ToString();
        //    GroupID = row["Group_UserGroup"].ToString();

        //    _ID = row["id_UserGroup"].ToString();
        //    switch (row["Type_UserGroup"].ToString())
        //    {
        //        case "1": _GrantsRole = GrantAdministrationTypes.Administrator; break;
        //        case "2": _GrantsRole = GrantAdministrationTypes.SuperAdministrator; break;
        //        default: _GrantsRole = GrantAdministrationTypes.User; break;
        //    }
        //    switch (row["PublishRole_UserGroup"].ToString())
        //    {
        //        case "1": _ContentsRole = ContentAdministrationTypes.Review; break;
        //        case "2": _ContentsRole = ContentAdministrationTypes.Publish; break;
        //        default: _ContentsRole = ContentAdministrationTypes.Write; break;
        //    }
        //}


        public virtual bool CanIHandleGroup(Users.Group userGroup)
        {
            return this.GrantsRole != GrantAdministrationTypes.User && (this.Group.ID == userGroup.ID || userGroup.IsMyParent(this.GroupID.ToString()));
        }
    }
}