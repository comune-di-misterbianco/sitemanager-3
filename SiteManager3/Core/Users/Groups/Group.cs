using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using NetCms.Connections;
using NHibernate;
using AutoMapper;

/// <summary>
/// Summary description for StructureGrants
/// </summary>

namespace NetCms.Users
{
    public class Group : Entity
    {
        #region Propriet� da mappare

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Name
        {
            get;
            set;
        }

        public virtual GroupProfile Profile
        {
            get;
            set;
        }

        public virtual int Depth
        {
            get;
            set;
        }

        public virtual string Tree
        {
            get;
            set;
        }

        public virtual string SystemName
        {
            get;
            set;
        }

        public virtual ICollection<GroupAssociation> AssociatedUsers
        {
            get;
            set;
        }

        public virtual Group Parent
        {
            get;
            set;
        }

        public virtual ICollection<Group> Groups
        {
            get;
            set;
        }

        public virtual MacroGroup MacroGroup
        {
            get;
            set;
        }

        #endregion

        //public const string GroupsSQL = "SELECT * FROM Groups INNER JOIN groups_macrogroups ON MacroGroup_Group = id_MacroGroup ORDER BY Tree_Group";

        private Connection _Conn;
        private Connection Conn
        {
            get
            {
                //if (_Conn == null)
                //    _Conn = ConnectionsManager.CommonConnection;
                //return _Conn;
                NetCms.Connections.ConnectionsManager ConnManager = new NetCms.Connections.ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }

        //public int ID
        //{
        //    get
        //    {
        //        return _ID;
        //    }
        //}
        //private int _ID;

        //public string MacroGroupLabel
        //{
        //    get
        //    {
        //        return _MacroGroupLabel;
        //    }
        //}
        //private string _MacroGroupLabel;

        //public string MacroGroupKey
        //{
        //    get
        //    {
        //        return _MacroGroupKey;
        //    }
        //}
        //private string _MacroGroupKey;

        //public string MacroGroupID
        //{
        //    get
        //    {
        //        return _MacroGroupID;
        //    }
        //}
        //private string _MacroGroupID;

        //private string _Name;
        //public string Name
        //{
        //    get
        //    {
        //        return _Name;
        //    }
        //}

        public virtual string ProfileID
        {
            get
            {
                return Profile.ID.ToString();
            }
        }

        public virtual string FullPathName
        {
            get
            {
                return _FullPathName ?? (_FullPathName = (this.Parent == null) ? this.Name : this.Parent.FullPathName + "/" + Name);
            }
        }
        private string _FullPathName;

        //public int Depth
        //{
        //    get { return _Depth; }
        //}
        //private int _Depth;

        //private int GrantValue = -1;

        //public string Tree
        //{
        //    get { return _Tree; }
        //}
        //private string _Tree;

        private bool _Stato;
        public virtual bool Stato
        {
            get { return _Stato; }
        }
        public virtual bool Abilitato { get { return Stato; } }

        //public string SystemName
        //{
        //    get { return _SystemName; }
        //    private set { _SystemName = value; }
        //}
        //private string _SystemName;

        public virtual bool IsSystem
        {
            get
            {
                return !string.IsNullOrEmpty(SystemName);
            }
        }

//        public UserCollection Users
//        {
//            get
//            {
//                if (_Users == null)
//                {

//                    DataRow[] rows = Connection.SqlQuery(@"SELECT * FROM Users 
//                                                        INNER JOIN usersgroups ON (users.id_User = usersgroups.User_UserGroup) 
//                                                        INNER JOIN groups ON (usersgroups.Group_UserGroup = groups.id_Group) 
//                                                        ORDER BY Depth_Group DESC,Tree_Group").Select(" Deleted_User = 0 AND Group_UserGroup = " + this.ID);
//                    _Users = new UserCollection();

//                    foreach (DataRow row in rows)
//                        _Users.Add(User.Fabricate(row));

//                }
//                return _Users;
//            }
//        }
//        private UserCollection _Users;

        private ICollection<User> _Users;
        public virtual ICollection<User> Users
        {
            get
            {
                if (_Users == null)
                {
                    _Users = AssociatedUsers.Select(x => x.User).ToList();
                }

                return _Users;
            }
        }

//        public List<GroupAssociation> AssociatedUsers
//        {
//            get
//            {
//                if (_AssociatedUsers == null)
//                {
//                    DataRow[] rows = Connection.SqlQuery(@"SELECT * FROM Users 
//                                                        INNER JOIN usersgroups ON (users.id_User = usersgroups.User_UserGroup) 
//                                                        INNER JOIN groups ON (usersgroups.Group_UserGroup = groups.id_Group) 
//                                                        ORDER BY Depth_Group DESC,Tree_Group").Select("Deleted_User = 0 AND Group_UserGroup = " + this.ID);
//                    _AssociatedUsers = new List<GroupAssociation>();

//                    foreach (DataRow row in rows)
//                        _AssociatedUsers.Add(new GroupAssociation(row, this));
//                }
//                return _AssociatedUsers;
//            }
//        }
//        private List<GroupAssociation> _AssociatedUsers;

        // NEI SUCCESSIVI UTILIZZI IN LETTURA VA USATO QUESTO, CHE � LA VERSIONE ORDINATA DELLA COLLEZIONE CARICATA DA DB (AssociatedUsers)
        private ICollection<GroupAssociation> _AssociatedUsersOrdered;
        public virtual ICollection<GroupAssociation> AssociatedUsersOrdered
        {
            get
            {
                if (_AssociatedUsersOrdered == null)
                {
                    _AssociatedUsersOrdered = AssociatedUsers.OrderByDescending(x => x.Group.Depth).ThenBy(x => x.Group.Tree).ToList();
                }

                return _AssociatedUsersOrdered;
            }

        }

        /// <summary>
        /// Restituisce gli Utenti di questo gruppo e dei suoi sottogruppi 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        //public UserCollection AllUsers
        //{
        //    get
        //    {
        //        UserCollection allUsers = new UserCollection();

        //        foreach (User user in Users)
        //            allUsers.Add(user);

        //        foreach (Group child in ChildGroups)
        //        {
        //            foreach (User user in child.AllUsers)
        //                allUsers.Add(user);
        //        }

        //        return allUsers;
        //    }
        //}

        public virtual List<User> AllUsers
        {
            get
            {
                List<User> allUsers = new List<User>();

                allUsers.AddRange(Users);

                foreach (Group child in Groups)
                {
                    allUsers.AddRange(child.AllUsers);
                }

                return allUsers;
            }
        }

        /// <summary>
        /// Restituisce gli Utenti di questo gruppo e dei suoi sottogruppi 
        /// per il quale l'utente passato per parametro � amministratore o super amministratore
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        //public List<User> AllMyUsers(User user)
        //{
        //    List<User> allMyUsers = new List<User>();
        //    if (user.AdministrationLevel != GrantAdministrationTypes.User)
        //    {
        //        foreach (User associatedUser in this.Users)
        //            if (user.AdministrationLevel <= associatedUser.AdministrationLevel)
        //                allMyUsers.Add(associatedUser);

        //        foreach (Group group in this.ChildGroups)
        //            foreach (User associatedUser in group.Users)
        //                if (user.AdministrationLevel <= associatedUser.AdministrationLevel)
        //                    allMyUsers.Add(associatedUser);
        //    }

        //    return allMyUsers;
        //}

        public virtual List<User> AllMyUsers(User user)
        {
            List<User> allMyUsers = new List<User>();
            if (user.AdministrationLevel != GrantAdministrationTypes.User)
            {
                allMyUsers.AddRange(this.Users.Where(x => x.AdministrationLevel > user.AdministrationLevel));

                foreach (Group group in this.Groups)
                    allMyUsers.AddRange(group.Users.Where(x => x.AdministrationLevel > user.AdministrationLevel));

            }

            return allMyUsers;
        }

        /*public IEnumerable<User> Revisors
        {
            get{
                if (_Revisors == null)
                    _Revisors = GetRevisors2();
                return _Revisors;
            }
        }
        public IEnumerable<User> _Revisors;
        */
        public virtual IEnumerable<User> GetRevisors()
        {
            var revisors = from associatedUser in AssociatedUsersOrdered
                           where associatedUser.ContentsRole != ContentAdministrationTypes.Write
                           select associatedUser.User;
            return revisors;

        }
        public virtual IEnumerable<GroupAssociation> GetRevisorsAssociations()
        {
            var revisors = from associatedUser in AssociatedUsersOrdered
                           where associatedUser.ContentsRole != ContentAdministrationTypes.Write
                           select associatedUser;
            return revisors;

        }
        public virtual IEnumerable<User> GetPublishers()
        {
            var revisors = from associatedUser in AssociatedUsersOrdered
                           where associatedUser.ContentsRole == ContentAdministrationTypes.Publish
                           select associatedUser.User;

            return revisors;
        }

        public virtual bool ImSuperAdmin(User user)
        {
            var admins = from associatedUser in AssociatedUsersOrdered
                         where (associatedUser.GrantsRole == GrantAdministrationTypes.SuperAdministrator) && (user.ID == associatedUser.UserID)
                         select associatedUser.User;
            if (admins.Count() > 0) return true;
            else if (this.Parent != null) return this.Parent.ImSuperAdmin(user);

            return false;
        }

        public virtual bool ImAdmin(User user)
        {
            var admins = from associatedUser in AssociatedUsersOrdered
                         where (associatedUser.GrantsRole != GrantAdministrationTypes.User) && (user.ID == associatedUser.UserID)
                         select associatedUser.User;
            if (admins.Count() > 0) return true;
            else if (this.Parent != null) return this.Parent.ImAdmin(user);

            return false;
        }


        // DA VERIFICARE IN QUANTO RITORNA SEMPRE TRUE
        public virtual bool NeedToShowInTrees(User user)
        {
            return true;

            if (ImAdmin(user)) return true;
            else
                foreach (Group group in this.Groups)
                    if (group.NeedToShowInTrees(user)) return true;

            return false;
        }

        //private Group _Parent;
        //public Group Parent
        //{
        //    get
        //    {
        //        return _Parent;
        //    }
        //    set
        //    {
        //        if (_Parent == null)
        //            _Parent = value;
        //        else
        //        {
        //            //throw new Exception("Gruppo genitore gi� impostato sull'istanza dell'oggetto");

        //            LogAndTrance.ExceptionManager.ExceptionLogger.SaveLog(NetCms.Users.AccountManager.CurrentAccount.ID, "Gruppo genitore gi� impostato sull'istanza dell'oggetto", this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error);
        //        }
        //    }
        //}

        public virtual bool HasChilds
        {
            get
            {
                return (Groups != null && Groups.Count > 0);
            }
        }

        //public GroupsCollection ChildGroups
        //{
        //    get
        //    {
        //        return Groups;
        //    }

        //}
        //public GroupsCollection _Groups;
        //public GroupsCollection Groups
        //{
        //    get
        //    {
        //        if (_Groups == null)
        //            InitChilds();
        //        return _Groups;
        //    }

        //}

        //public Group(string id)
        //{
        //    DataRow[] groupData = Connection.SqlQuery(GroupsSQL).Select("id_Group = " + id);
        //    InitData(groupData[0]);
        //}
        //public Group(DataRow data)
        //{
        //    InitData(data);
        //}

        public Group()
        {
            Groups = new HashSet<Group>();
            AssociatedUsers = new HashSet<GroupAssociation>();
        }

        //public User FindUser(int id)
        //{
        //    /*SitemanagerUserCollection Users = this.AllUsers[id.ToString()];
        //    Users.c
        //    for (int i = 0; i < Users.Count; i++)
        //    {
        //        if (id == Users[i].ID)
        //            return Users[i];
        //    }*/
        //    //return null;
        //    return ((Users.User)this.AllUsers[id.ToString()]);
        //}

        public virtual User FindUser(int id)
        {
            try
            {
                return this.AllUsers.First(x => x.ID == id);
            }
            catch
            {
                return null;
            }
        }

        //public Group FindGroup(int id) { return this.FindGroup(id.ToString()); }
        //public Group FindGroup(string id)
        //{
        //    if (this.ID.ToString() == id)
        //        return this;
        //    else
        //        return ChildGroups.FindGroup(id.ToString());
        //}

        public virtual Group FindGroup(int id)
        {
            return GroupsBusinessLogic.FindGroup(this, id.ToString(), GroupsBusinessLogic.SearchFor.ID);
        }

        public virtual Group FindGroupByKey(string key)
        {
            return GroupsBusinessLogic.FindGroup(this, key, GroupsBusinessLogic.SearchFor.Key);
        }

        public virtual bool IsMyParent(string key)
        {
            return Parent != null && (this.Parent.ID.ToString() == key || this.Parent.IsMyParent(key));
        }

        public virtual bool IsMyChild(string id)
        {
            return FindGroup(int.Parse(id)) != null;
        }

        public virtual bool IsMyChild(Group group)
        {
            return FindGroup(group.ID) != null;
        }

        //private void InitData(DataRow data)
        //{
        //    if (data.Table.Columns.Contains("id_MacroGroup"))
        //    {
        //        _MacroGroupID = data["id_MacroGroup"].ToString();
        //        _MacroGroupLabel = data["Label_MacroGroup"].ToString();
        //        _MacroGroupKey = data["Key_MacroGroup"].ToString();
        //    }
        //    _Name = data["Name_Group"].ToString();
        //    _ID = int.Parse(data["id_Group"].ToString());
        //    _Profile = new Profile(data["Profile_Group"].ToString());
        //    _Depth = int.Parse(data["Depth_Group"].ToString());
        //    _Tree = data["Tree_Group"].ToString(); ;

        //    this.SystemName = data["SystemKey_Group"].ToString();
        //    this.IsSystem = !string.IsNullOrEmpty(SystemName);

        //}

        //private void InitChilds()
        //{
        //    _Groups = new GroupsCollection(this);
        //    DataRow[] groupData = Connection.SqlQuery(GroupsSQL).Select("Parent_Group = " + this.ID, " Tree_Group");
        //    foreach (DataRow row in groupData)
        //    {
        //        this.Groups.Add(new Group(row));
        //    }
        //}

        //public void Update()
        //{
        //    DataRow[] groupData = Connection.SqlQuery(GroupsSQL).Select("id_Group = " + this.ID);
        //    InitData(groupData[0]);
        //}


        // NON SEMBRA ESSERE USATO
//        public virtual int getGroupUserLink(string GroupID, string GroupTree)
//        {
//            string[] parents = GroupTree.Split('.');
//            string where = "User_UserGroup = " + AccountManager.CurrentAccount.ID + " AND (";
//            for (int i = 1; i < parents.Length - 1; i++)
//            {
//                if (i != 1)
//                    where += " OR ";
//                where += "(Group_UserGroup = " + parents[i] + "AND Type_UserGroup = 2)";
//            }
//            where += ")  ";
//            DataTable tab = Connection.SqlQuery(@"SELECT * FROM Users 
//                                                        INNER JOIN usersgroups ON (users.id_User = usersgroups.User_UserGroup) 
//                                                        INNER JOIN groups ON (usersgroups.Group_UserGroup = groups.id_Group) 
//                                                        ORDER BY Depth_Group DESC,Tree_Group");
//            if (tab.Select(where).Length > 0)
//                return 2;//Superadmin
//            else
//            {
//                where = " (Group_UserGroup = " + GroupID + "AND Type_UserGroup = 1)";
//                if (tab.Select(where).Length > 0) return 1;
//            }
//            return 0;
//        }
        
        /// <summary>
        /// aggiunge un untente al gruppo
        /// </summary>
        /// <param name="userID">ID dell'tente da aggiungere</param>
        /// <param name="roleID">0=utenti, 1=adiministrators, 2=superadministrators</param>
        /// <param name="connection">connessione da usare</param>
        /// <returns></returns>
        //public bool AddUserToGroup(string userID, GrantAdministrationTypes grantAdministrationRole, ContentAdministrationTypes contentAdministrationRole)
        //{
        //    int contentRoleID;
        //    int grantsRoleD;

        //    switch (grantAdministrationRole)
        //    {
        //        case GrantAdministrationTypes.SuperAdministrator: grantsRoleD = 2; break;
        //        case GrantAdministrationTypes.Administrator: grantsRoleD = 1; break;
        //        case GrantAdministrationTypes.User: grantsRoleD = 0; break;
        //        default: grantsRoleD = 0; break;
        //    }

        //    switch (contentAdministrationRole)
        //    {
        //        case ContentAdministrationTypes.Publish: contentRoleID = 2; break;
        //        case ContentAdministrationTypes.Review: contentRoleID = 1; break;
        //        case ContentAdministrationTypes.Write: contentRoleID = 0; break;
        //        default: contentRoleID = 0; break;
        //    }

        //    //aggiungere controllo se l'utente vi appartiene gi�
        //    string sql = "INSERT INTO usersgroups (User_UserGroup,Group_UserGroup,Type_UserGroup,PublishRole_UserGroup) VALUES({0},{1},{2},{3})";
        //    sql = sql.FormatByParameters(userID, this.ID, grantsRoleD, contentRoleID);
        //    Connection.ExecuteInsert(sql);
        //    NetCms.Users.GroupsHandler.RebuildGroups();
        //    return true;
        //}

        public virtual bool AddUserToGroup(User user, GrantAdministrationTypes grantAdministrationRole, ContentAdministrationTypes contentAdministrationRole, ISession session = null)
        {
            int contentRoleID;
            int grantsRoleD;

            switch (grantAdministrationRole)
            {
                case GrantAdministrationTypes.SuperAdministrator: grantsRoleD = 2; break;
                case GrantAdministrationTypes.Administrator: grantsRoleD = 1; break;
                case GrantAdministrationTypes.User: grantsRoleD = 0; break;
                default: grantsRoleD = 0; break;
            }

            switch (contentAdministrationRole)
            {
                case ContentAdministrationTypes.Publish: contentRoleID = 2; break;
                case ContentAdministrationTypes.Review: contentRoleID = 1; break;
                case ContentAdministrationTypes.Write: contentRoleID = 0; break;
                default: contentRoleID = 0; break;
            }

            if (!this.Users.Contains(user))
            {
                GroupAssociation ga = new GroupAssociation();
                ga.Group = this;
                ga.User = user;
                ga.Type = grantsRoleD;
                ga.PublishRole = contentRoleID;

                try
                {
                    this.AssociatedUsers.Add(ga);
                    user.AssociatedGroups.Add(ga);
                    GroupsBusinessLogic.SaveOrUpdateGroupAssociation(ga,false,session);
                }
                catch
                {
                    return false;
                }
            }

            //NetCms.Users.GroupsHandler.RebuildGroups();
            return true;
        }


        //public bool RemoveUserFromGroup(string userID)
        //{
        //    string sql = "Delete from usersgroups Where User_UserGroup=" + userID + " and Group_UserGroup=" + this.ID;
        //    bool success = Connection.Execute(sql);
        //    NetCms.Users.GroupsHandler.RebuildGroups();
        //    return (success);
        //}

        public virtual bool RemoveUserFromGroup(User user)
        {
            GroupAssociation ga = this.AssociatedUsers.First(x=>x.User == user);

            this.AssociatedUsers = this.AssociatedUsers.Where(x => x.User != user).ToList();
            user.AssociatedGroups = user.AssociatedGroups.Where(x => x.Group != this).ToList();

            bool success = GroupsBusinessLogic.DeleteGroupAssociation(ga);
            
            NetCms.Users.GroupsHandler.RebuildGroups();
            return success;
        }

        /// <summary>
        /// Rimuove l'utente dal gruppo e dai relativi sottogruppi
        /// </summary>
        /// <param name="userID">Id dell'utente da rimuovere</param>
        /// <returns></returns>
//        public bool RemoveUserFromGroupTree(string userID)
//        {
//            //string sql = "Delete from usersgroups Where User_UserGroup="+userID+" and Group_UserGroup=" + this.ID;            
//            string sql = @"Delete from usersgroups Where 
//                        User_UserGroup=" + userID + @" and Group_UserGroup in 
//                        (SELECT groups.id_Group FROM groups Where groups.Tree_Group like '" + this.Tree + "%')";
//            bool success = Connection.Execute(sql);
//            NetCms.Users.GroupsHandler.RebuildGroups();
//            return (success);
//        }

        public virtual bool RemoveUserFromGroupTree(User user)
        {
            bool success = GroupsBusinessLogic.RemoveUserFromGroupTree(this, user);
            NetCms.Users.GroupsHandler.RebuildGroups();
            return (success);
        }

        public virtual Group FindParentByMacroGroup(string macroGroupKey)
        {
            if (this.MacroGroup.Key.ToString() == macroGroupKey)
                return this;
            if (Parent != null)
                return Parent.FindParentByMacroGroup(macroGroupKey);

            return null;
        }

        /// <summary>
        /// Crea un nuovo gruppo figlio di quello attuale.
        /// </summary>
        /// <param name="groupName">Nome del nuovo gruppo</param>
        /// <param name="macroGroupID">ID del macrogruppo di appartenenza (0 = nessun macrogruppo)</param>
        /// <returns></returns>
        //public int CreateChildGroup(string groupName, string systemName, string macroGroupKey)
        //{
        //    int mgid = 0;
        //    DataTable rdr = this.Connection.SqlQuery("SELECT * FROM groups_macrogroups WHERE Key_MacroGroup = '" + macroGroupKey + "' ORDER BY Label_MacroGroup");
        //    if (rdr.Rows.Count == 1) mgid = (int)rdr.Rows[0]["id_MacroGroup"];
        //    return CreateChildGroup(groupName, systemName, mgid);
        //}

        public virtual int CreateChildGroup(string groupName, string systemName, string macroGroupKey, ISession session = null)
        {
            if (session == null)
                session = GroupsBusinessLogic.GetCurrentSession();

            MacroGroup macroGroup = GroupsBusinessLogic.GetMacroGroupByKey(macroGroupKey, session);
            return CreateChildGroup(groupName, systemName, macroGroup, session);
        }

        /// <summary>
        /// Crea un nuovo gruppo figlio di quello attuale.
        /// </summary>
        /// <param name="groupName">Nome del nuovo gruppo</param>
        /// <param name="groupName">Chiave di sistema del nuovo gruppo</param>
        /// <param name="macroGroupID">ID del macrogruppo di appartenenza (0 = nessun macrogruppo)</param>
        /// <returns></returns>
        //public int CreateChildGroup(string groupName, string systemName, int macroGroupID)
        //{
        //    //Controllo se il gruppo esiste gi�, in tal caso estituisco l'id del gruppo trovato su db
        //    DataTable rdr = this.Connection.SqlQuery("SELECT id_Group, Name_Group FROM Groups WHERE Name_Group LIKE '" + groupName + "' AND Parent_Group = " + ID + "");
        //    if (rdr.Rows.Count == 1) return (int)rdr.Rows[0]["id_Group"];

        //    //Se non esiste eseguo la procedura per crearlo
        //    string sql = "INSERT INTO Groups (Name_Group,MacroGroup_Group,SystemKey_Group)";
        //    sql += " Values ('" + groupName + "','" + macroGroupID + "','" + G2Core.Common.Utility.CleanKey(systemName) + "')";
        //    int GroupID = this.Connection.ExecuteInsert(sql);

        //    sql = "INSERT INTO Profiles (Name_Profile,Type_Profile)";
        //    sql += " Values ('" + groupName + GroupID + "',1)";
        //    int ProfileID = this.Connection.ExecuteInsert(sql);
        //    int Depth = (this.Depth + 1);

        //    sql = "UPDATE Groups SET ";
        //    sql += " Tree_Group = '" + this.Tree + NetUtility.TreeUtility.FormatNumber(GroupID.ToString()) + ".'";
        //    sql += ", Depth_Group = " + Depth.ToString() + "";
        //    sql += ", Parent_Group = " + ID + "";
        //    sql += ", Profile_Group = " + ProfileID + "";
        //    sql += "  WHERE id_Group = " + GroupID;
        //    this.Connection.Execute(sql);
        //    NetCms.Users.GroupsHandler.RebuildGroups();
        //    this.InitChilds();
        //    return GroupID;
        //}

        public virtual int CreateChildGroup(string groupName, string systemName, MacroGroup macroGroup, ISession session = null)
        {
            //Controllo se il gruppo esiste gi�, in tal caso restituisco l'id del gruppo trovato su db

            int childId = -1;

            if (session == null)
                session = GroupsBusinessLogic.GetCurrentSession();

            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    Group child = GroupsBusinessLogic.GetGroupByNameAndParent(groupName, this, sess);

                    if (child != null)
                        childId = child.ID;
                    else
                    {

                        child = new Group();
                        child.Name = groupName;
                        child.MacroGroup = macroGroup;
                        child.SystemName = G2Core.Common.Utility.CleanKey(systemName);

                        child = GroupsBusinessLogic.SaveOrUpdateGroup(child, sess);

                        GroupProfile profile = new GroupProfile();
                        profile.Nome = groupName + child.ID;

                        profile = ProfileBusinessLogic.SaveOrUpdateProfile(profile, sess) as GroupProfile;

                        child.Tree = this.Tree + NetUtility.TreeUtility.FormatNumber(child.ID.ToString()) + ".";
                        child.Depth = this.Depth + 1;
                        child.Parent = this;
                        child.Profile = profile;
                        profile.Group = child;

                        this.Groups.Add(child);
                        GroupsBusinessLogic.SaveOrUpdateGroup(child, sess);
                        childId = child.ID;
                    }
                    tx.Commit();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
                return childId;

            }
            //GroupsBusinessLogic.SaveOrUpdateGroup(this);

            //NetCms.Users.GroupsHandler.RebuildGroups();
        }

        public virtual bool Move(Group parentGroup)
        {
            bool ok = MoveRecursive(parentGroup);
            if (ok)
            {
                GroupsHandler.RebuildGroups();
                G2Core.Caching.Cache cache = new G2Core.Caching.Cache();
                cache.RemoveAll("");
            }
            return ok;
        }

        //private bool MoveRecursive(Group parentGroup)
        //{
        //    string newTree = parentGroup.Tree + G2Core.Common.Utility.FormatNumber(this.ID.ToString()) + ".";
        //    string sql = "UPDATE Groups SET Depth_Group = " + (parentGroup.Depth + 1) + ", Parent_Group = " + parentGroup.ID + ", Tree_Group = '" + newTree + "' WHERE id_Group = " + this.ID;
        //    this._Tree = newTree;
        //    this._Depth = (parentGroup.Depth + 1);
        //    bool ok = this.Connection.Execute(sql);

        //    if (ok)
        //    {
        //        foreach (Group group in this.ChildGroups)
        //            group.MoveRecursive(this);
        //    }
        //    return ok;
        //}

        private bool MoveRecursive(Group parentGroup)
        {
            string newTree = parentGroup.Tree + G2Core.Common.Utility.FormatNumber(this.ID.ToString()) + ".";

            this.Depth = parentGroup.Depth + 1;
            this.Parent = parentGroup;
            this.Tree = newTree;

            bool ok = GroupsBusinessLogic.SaveOrUpdateGroup(this) != null;

            if (ok)
            {
                foreach (Group group in this.Groups)
                    group.MoveRecursive(this);
            }
            return ok;
        }

        //public virtual Group Clone(Group parentGroup)
        //{
        //    Mapper.Reset();
        //    Mapper.CreateMap<Group, Group>().ForMember(d => d.ID, o => o.Ignore());
        //    var newGroup = new Group();
        //    Mapper.Map(this, newGroup);
        //    newGroup.Profile = this.Profile.Clone(newGroup);
        //    newGroup.Parent = parentGroup;
        //    return newGroup;
        //}

        public virtual Group Clone(Group parentGroup = null)
        {
           
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Group, Group>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Groups, o => o.Ignore())
                .ForMember(d => d.AllUsers, o => o.Ignore())
                .ForMember(d => d.AssociatedUsers, o => o.Ignore())
                .ForMember(d => d.AssociatedUsersOrdered, o => o.Ignore())
                .ForMember(d => d.Users, o => o.Ignore())
                .ForMember(d => d.MacroGroup, o => o.Ignore());
            });
            
            config.AssertConfigurationIsValid();

            var newGroup = new Group();                      

            var mapper = config.CreateMapper();
            mapper.Map(this, newGroup);

            newGroup.Profile = this.Profile.Clone(newGroup);
            newGroup.Parent = parentGroup;

            if (this.HasChilds)
                newGroup.Groups = this.Groups.Select(item => item.Clone(newGroup)).ToList();
            
            return newGroup;
        }
    }
}