﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;

namespace NetCms.Users
{
    public class MacroGroup
    {
        public MacroGroup()
        {
            Groups = new HashSet<Group>();
        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Label
        {
            get;
            set;
        }

        public virtual string Key
        {
            get;
            set;
        }

        public virtual ICollection<Group> Groups
        {
            get;
            set;
        }

        public virtual MacroGroup Clone()
        {        
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MacroGroup, MacroGroup>()
                .ForMember(d => d.ID, o => o.Ignore())
                .ForMember(d => d.Groups, o => o.Ignore());

            });

            config.AssertConfigurationIsValid();

            var newMacroGroup = new MacroGroup();

            var mapper = config.CreateMapper();
            mapper.Map(this, newMacroGroup);

            return newMacroGroup;
        }
    }
}
