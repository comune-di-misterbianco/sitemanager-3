//using System;
//using System.Collections;
///// <summary>
///// Summary description for xNodeCollection
///// </summary>

//namespace NetCms.Users
//{
//    public class GroupsCollection : IEnumerable
//    {
//        private G2Collection coll;
//        private Group Owner;

//        public int Count { get { return coll.Count; } }

//        public GroupsCollection(Group ownerGroup)
//        {
//            coll = new G2Collection();
//            Owner = ownerGroup;
//        }

//        public GroupsCollection()
//        {
//            coll = new G2Collection();
//        }
//        public void Add(Group group)
//        {
//            coll.Add(group.ID.ToString(), group);
//            if (Owner != null)
//                group.Parent = Owner;            
//        }
//        public void Add(string key, Group group)
//        {
//            coll.Add(group.ID.ToString(), group);
//            if (Owner != null)
//                group.Parent = Owner;
//        }       

//        public Group this[int i]
//        {
//            get
//            {
//                Group value = (Group)coll[coll.Keys[i]];
//                return value;
//            }
//        }
//        public Group this[string key]
//        {
//            get
//            {
//                Group field = (Group)coll[key];
//                return field;
//            }
//        }


//        public Group FindGroup(string key)
//        {
//            Group node = null;

//            if (this.Owner != null && (this.Owner.ID.ToString() == key || this.Owner.SystemName.ToString() == key))
//                node = this.Owner;

//            if (node == null)
//                node = this[key];

//            int i = 0;
//            while (node == null && i < this.Count)
//                node = this[i++].ChildGroups.FindGroup(key);

//            return node;
//        }
//        public bool Contains(string key)
//        {
//            return this[key] != null;
//        }
//        #region Enumerator

//        public IEnumerator GetEnumerator()
//        {
//            return new CollectionEnumerator(this);
//        }

//        private class CollectionEnumerator : IEnumerator
//        {
//            private int CurentPos = -1;
//            private GroupsCollection Collection;
//            public CollectionEnumerator(GroupsCollection coll)
//            {
//                Collection = coll;
//            }
//            public object Current
//            {
//                get
//                {
//                    return Collection[CurentPos];
//                }
//            }
//            public bool MoveNext()
//            {
//                if (CurentPos < Collection.Count - 1)
//                {
//                    CurentPos++;
//                    return true;
//                }
//                else
//                    return false;
//            }
//            public void Reset()
//            {
//                CurentPos = -1;
//            }
//        }
//        #endregion
//    }
//}