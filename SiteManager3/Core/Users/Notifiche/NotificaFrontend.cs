﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Networks;
using NHibernate;

namespace NetCms.Users
{
    public class NotificaFrontend : Notifica
    {
        public override Notifica.TipoNotifica Tipologia
        {
            get { return TipoNotifica.Frontend; }
        }

        /// <summary>
        /// Metodo per la creazione delle notifiche di frontend. Da usare ovunque si voglia creare una notifica.
        /// </summary>
        /// <param name="Title"></param>
        /// <param name="Text"></param>
        /// <param name="DestProfileID"></param>
        /// <param name="SenderProfileID"></param>
        /// <param name="url"></param>
        /// <param name="NetworkID"></param>
        public static void CreateNewNotificaFrontend(string Title, string Text, int DestProfileID, int SenderProfileID, string url, int NetworkID, ISession session = null)
        {
            if (session == null)
                session = UsersBusinessLogic.GetCurrentSession();

            NotificaFrontend notifica = new NotificaFrontend();
            notifica.Titolo = Title;
            notifica.Testo = Text;
            notifica.Data = DateTime.Now;
            notifica.Letta = false;
            notifica.Cancellata = false;
            notifica.Link = url;
            User Destinatario = (ProfileBusinessLogic.GetById(DestProfileID,session) as UserProfile).User;
            notifica.Destinatario = Destinatario;
            notifica.Responsabile = (ProfileBusinessLogic.GetById(SenderProfileID,session) as UserProfile).User;
            Destinatario.Notifiche.Add(notifica);

            if (NetworkID != null)
            {
                notifica.Network = NetworksBusinessLogic.GetById(NetworkID,session);
            }

            UsersBusinessLogic.SaveOrUpdateNotifica(notifica,session);
        }
    }
}
