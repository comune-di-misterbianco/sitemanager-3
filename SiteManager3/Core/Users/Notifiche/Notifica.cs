﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Networks;
using NHibernate;

namespace NetCms.Users
{
    public abstract class Notifica
    {
        public Notifica()
        {

        }

        public virtual int ID
        {
            get;
            set;
        }

        public virtual string Titolo
        {
            get;
            set;
        }

        public virtual string Testo
        {
            get;
            set;
        }

        public virtual string Link
        {
            get;
            set;
        }

        public virtual DateTime Data
        {
            get;
            set;
        }

        public virtual bool Letta
        {
            get;
            set;
        }

        public virtual bool Cancellata
        {
            get;
            set;
        }

        public virtual User Destinatario
        {
            get;
            set;
        }

        public virtual User Responsabile
        {
            get;
            set;
        }

        public virtual BasicNetwork Network
        {
            get;
            set;
        }

        public enum Columns
        {   
            ID,
            Titolo,
            Testo,
            Link,
            Data,
            Letta,
            Cancellata,
            Destinatario,
            Responsabile,
            Network,
            Tipologia,
        }

        public enum TipoNotifica
        {
            Frontend,
            Backoffice,
            System
        }

        public abstract TipoNotifica Tipologia
        {
            get;
        }

        /// <summary>
        /// Meotodo per settare a "Letta" la notifica.
        /// </summary>
        /// <param name="activityID"></param>
        public static void SetNotificaDone(string activityID, ISession session = null)
        {
            if (session == null)
                session = UsersBusinessLogic.GetCurrentSession();

            Notifica notifica = UsersBusinessLogic.GetNotificaById(int.Parse(activityID),session);
            notifica.Letta = true;
            UsersBusinessLogic.SaveOrUpdateNotifica(notifica,session);
        }

        /// <summary>
        /// Metodo per settare a "Cancellata" la notifica.
        /// </summary>
        /// <param name="activityID"></param>
        public static void SetNotificaDeleted(string activityID, ISession session = null)
        {
            if (session == null)
                session = UsersBusinessLogic.GetCurrentSession();

            Notifica notifica = UsersBusinessLogic.GetNotificaById(int.Parse(activityID),session);
            notifica.Cancellata = true;
            UsersBusinessLogic.SaveOrUpdateNotifica(notifica,session);
        }
    
    
    }
}
