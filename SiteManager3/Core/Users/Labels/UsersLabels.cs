﻿using LabelsManager;
using System;

namespace NetCms.Users
{
    public class UsersLabels : Labels
    {
        public const String ApplicationKey = "users";

        public UsersLabels() : base(ApplicationKey)
        {
        }

        public enum UsersLabelsList
        {
            UserProfileEditDescription,
            UserProfileUpdateError,
            UserProfileUpdatePageTitle,
            UserProfileDetailPageTitle,
            UserPasswordUpdateTitle,
            UserPasswordUpdateBtn,
            UserPasswordUpdateDescription,
            UserProfileUpdateErrorBtn,
            // messaggi per l'audit dell'utente
            UserLoginMessage,
            UserLogoutMessage,
            UserPasswordRecoveryMessage,
            UserPasswordRecoverySaveMessage,
            UserPasswordRecoveryPageOpened,
            UserPasswordEditMessage,
            UserProfileView,
            UserProfileFormView,
            UserConfirmEmailChange,
            UserProfileEditView,
            UserViewNotifyList,
            UserDeleteProfile,
            UserDifferentPassword,
            UserPasswordUpdated,
            UsernameUsed
        }
    }
}