﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Networks
{
    public static class NetworkTimings
    {
        /// <summary>
        /// Esprime il tempo ,dall'ultimo accesso, dopo il quale i singoli oggetti Network vengono scaricati dalla cache.
        /// </summary>
        public static int NetworkExpireTiming
        {
            get
            {
                return 20;
            }
        }

        /// <summary>
        /// Esprime il tempo ,dall'ultimo accesso, dopo il quale i singoli oggetti Folders vengono scaricati dalla cache.
        /// </summary>
        public static int FoldersExpireTiming
        {
            get
            {
                return 20;
            }
        }
        
        /// <summary>
        /// Esprime il tempo ,dall'ultimo accesso, dopo il quale i singoli oggetti State relativi ai FoldersGrants vengono scaricati dalla cache.
        /// </summary>
        public static int FoldersGrantsStateExpireTiming
        {
            get
            {
                return 20;
            }
        }

        /// <summary>
        /// Esprime il tempo, dall'ultimo accesso, dopo il quale i singoli oggetti State relativi ai Folders vengono scaricati dalla cache.
        /// </summary>
        public static int FoldersStateExpireTiming
        {
            get
            {
                return 20;
            }
        }

        /// <summary>
        /// Esprime il tempo ,dall'ultimo accesso, dopo il quale i singoli oggetti Documents vengono scaricati dalla cache.
        /// </summary>
        public static int DocumentsExpireTiming
        {
            get
            {
                return 20;
            }
        }

        /// <summary>
        /// Esprime il tempo ,dall'ultimo accesso, dopo il quale i singoli oggetti State relativi ai DocumentsGrants  vengono scaricati dalla cache.
        /// </summary>
        public static int DocumentsGrantsStateExpireTiming
        {
            get
            {
                return 20;
            }
        }

        /// <summary>
        /// Esprime il tempo ,dall'ultimo accesso, dopo il quale i singoli oggetti State relativi ai Documents vengono scaricati dalla cache.
        /// </summary>
        public static int DocumentsStateExpireTiming
        {
            get
            {
                return 20;
            }
        }
    }
}
