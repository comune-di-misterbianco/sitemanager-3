﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Networks
{
    public interface INetwork
    {
        int ID { get; }
        string SystemName { get; }
        string Label { get; }
        bool HasFrontend { get; }
        bool Enabled { get; }
        string RootPath { get; }
        int DefaultLang { get; }
        bool Default { get; }
        NetworkCmsStates CmsStatus { get; }
        NetworkPaths Paths { get; }
        Faces Face { get; }

        void EnableNetwork();
        void DisableNetwork();
        void DisableCms();
        void EnableCms();
    }
}
