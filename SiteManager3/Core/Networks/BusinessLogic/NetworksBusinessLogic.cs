﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NetCms.DBSessionProvider;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using System.Xml;
using NetCms.Networks.WebFS;
using System.Reflection;

namespace NetCms.Networks
{
    public static class NetworksBusinessLogic
    {
        public static ISession GetCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static BasicNetwork GetById(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<BasicNetwork> dao = new CriteriaNhibernateDAO<BasicNetwork>(innerSession);
            return dao.GetById(id);
        }

        public static BasicNetwork GetByName(string name, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<BasicNetwork> dao = new CriteriaNhibernateDAO<BasicNetwork>(innerSession);
            SearchParameter networkName = new SearchParameter(null, "SystemName", name, Finder.ComparisonCriteria.Like, false);
            return dao.GetByCriteria(new SearchParameter[] { networkName });
        }

        public static IList<BasicNetwork> FindAllNetworksPlusFiltersFromConfig(bool onlyEnabled, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<BasicNetwork> dao = new CriteriaNhibernateDAO<BasicNetwork>(innerSession);

            List<SearchParameter> networkFilters = new List<SearchParameter>();

            if (onlyEnabled)
            {
                SearchParameter networkEnabledSP = new SearchParameter(null, "Enabled", true, Finder.ComparisonCriteria.Equals, false);
                networkFilters.Add(networkEnabledSP);
            }


            #region MyRegion
            /*
                if (Configurations.XmlConfig.GetNodeContent("/Portal/networks-filters") != null)
                {

                    bool hideall = Configurations.XmlConfig.GetBooleanAttribute("/Portal/networks-filters", "hideall");
                    XmlNodeList forcedToSee = Configurations.XmlConfig.GetNodes("/Portal/networks-filters/force-show-network");
                    XmlNodeList forcedToHide = Configurations.XmlConfig.GetNodes("/Portal/networks-filters/force-hide-network");
                    if (hideall && forcedToSee.Count > 0)
                    {
                        //sql += " AND (";
                        //bool first = true;
                        List<int> netIdsToSee = new List<int>();
                        foreach (XmlNode netID in forcedToSee)
                        {
                            netIdsToSee.Add(int.Parse(netID.InnerXml));
                            //sql += (!first ? " OR " : "") + " id_Network = " + netID.InnerXml;
                            //first = false;
                        }
                        SearchParameter networkToSeeSP = new SearchParameter(null, "ID", netIdsToSee, Finder.ComparisonCriteria.In, false);
                        networkFilters.Add(networkToSeeSP);
                        //sql += ") ";
                    }

                    if (forcedToHide.Count > 0)
                    {
                        //sql += " AND (";
                        //bool first = true;
                        //List<int> netIdsToHide = new List<int>();
                        foreach (XmlNode netID in forcedToHide)
                        {
                            //netIdsToHide.Add(int.Parse(netID.InnerXml));
                            SearchParameter networkToHideSP = new SearchParameter(null, "ID", int.Parse(netID.InnerXml), Finder.ComparisonCriteria.Not, false);
                            networkFilters.Add(networkToHideSP);
                            //sql += (!first ? " OR " : "") + " id_Network <> " + netID.InnerXml;
                            //first = false;
                        }

                        //sql += ") ";
                    }
                }

                */ 
            #endregion


            return dao.FindByCriteria(networkFilters.ToArray());
        }

        public static bool CheckExistNetworkFolderByPath(string path, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<NetworkFolder> dao = new CriteriaNhibernateDAO<NetworkFolder>(innerSession);
            SearchParameter networkPath = new SearchParameter(null, "Path", path, Finder.ComparisonCriteria.Equals, false);
            return dao.GetByCriteria(new SearchParameter[] { networkPath }) != null;
        }

        public static BasicNetwork SaveOrUpdateNetwork(BasicNetwork net, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<BasicNetwork> dao = new CriteriaNhibernateDAO<BasicNetwork>(innerSession);
            return dao.SaveOrUpdate(net);
        }

        public static void DeleteNetwork(BasicNetwork net, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetCurrentSession();

            IGenericDAO<BasicNetwork> dao = new CriteriaNhibernateDAO<BasicNetwork>(innerSession);
            dao.Delete(net);
        }

        public static Type GetTypeFromTypeName(string typeName)
        {
            foreach (Assembly currentassembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                Type t = currentassembly.GetType(typeName, false, true);
                if (t != null) { return t; }
            }

            return null;
        }
    }
}
