﻿using System;
using System.Web;
using System.Web.Caching;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using NetCms.Connections;
using G2Core.Caching;
using G2Core.Caching.Visibility;

namespace NetCms.Networks
{
    /// <summary>
    /// Evitare di creare più istanze di questa Lista ma usare i metodi forniti in Networks.NetworksManager.GlobalIndexer
    /// </summary>
    public class NetworksData : List<BasicNetwork>
    {
        //public bool OnlyEnabled
        //{
        //    get { return _OnlyEnabled; }
        //    private set { _OnlyEnabled = value; }
        //}
        //private bool _OnlyEnabled = true;

        //public string NetworksQuery
        //{
        //    get
        //    {
        //        if (_NetworksQuery == null)
        //        {
        //            string sql = "SELECT * FROM Networks WHERE "+(OnlyEnabled?" Enabled_Network = 1":"1=1");

        //            if (Configurations.XmlConfig.GetNodeContent("/Portal/networks-filters") != null)
        //            {

        //                bool hideall = Configurations.XmlConfig.GetBooleanAttribute("/Portal/networks-filters", "hideall");
        //                XmlNodeList forcedToSee = Configurations.XmlConfig.GetNodes("/Portal/networks-filters/force-show-network");
        //                XmlNodeList forcedToHide = Configurations.XmlConfig.GetNodes("/Portal/networks-filters/force-hide-network");
        //                if (hideall && forcedToSee.Count > 0)
        //                {
        //                    sql += " AND (";
        //                    bool first = true;
        //                    foreach (XmlNode netID in forcedToSee)
        //                    {
        //                        sql += (!first ? " OR " : "") + " id_Network = " + netID.InnerXml;
        //                        first = false;
        //                    }
        //                    sql += ") ";
        //                }

        //                if (forcedToHide.Count > 0)
        //                {
        //                    sql += " AND (";
        //                    bool first = true;
        //                    foreach (XmlNode netID in forcedToHide)
        //                    {
        //                        sql += (!first ? " OR " : "") + " id_Network <> " + netID.InnerXml;
        //                        first = false;
        //                    }
        //                    sql += ") ";
        //                }
        //            }
        //            _NetworksQuery = sql;
        //        }
        //        return _NetworksQuery;
        //    }
        //}
        //private string _NetworksQuery;

        //private NetCms.Connections.Connection _Connection;
        //private NetCms.Connections.Connection Connection
        //{
        //    get
        //    {
        //        if (_Connection == null)
        //            _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
        //        return _Connection;
        //    }
        //}

        public NetworksData():this(true)
        {
        }

        public NetworksData(bool listOnlyEnabledNetworks)
        {
            //OnlyEnabled = listOnlyEnabledNetworks;
            this.Clear();
            this.AddRange(NetworksBusinessLogic.FindAllNetworksPlusFiltersFromConfig(listOnlyEnabledNetworks));
            //FillTable();
        }
        //public void FillTable()
        //{
        //    this.Connection.FillDataTable(NetworksQuery, this);
        //}

        public bool Contains(int networkID)
        {
            return this.Exists(x=>x.ID == networkID);
        }
    }
}