//using System;
//using System.Web;
//using System.Collections;
//using System.Collections.Generic;
//using System.Data;
//using NetCms.Connections;

///// <summary>
///// Summary description for xNodeCollection
///// </summary>

//namespace NetCms.Networks.WebFS
//{
//    public abstract class NetworkFoldersCollection : G2Core.Caching.CacheableCollection, IEnumerable
//    {
//        public NetCms.Connections.Connection Connection { get; private set; }
//        public override int ItemsCachingTime
//        {
//            get
//            {
//                return Networks.NetworkTimings.FoldersExpireTiming;
//            }
//            set
//            {
//            }
//        }
//        public NetworkFoldersCollection(G2Core.Caching.Visibility.CachingVisibility Visibility, NetCms.Connections.Connection conn)
//            : base(Visibility,true)
//        {
//            this.Connection = conn;
//            this.ItemsCachingTime = 2;
//            this.TableCachingTime = 10;
//            this.SourceTableVisibility = G2Core.Caching.Visibility.CachingVisibility.Global;
//        }

//        public override string PrimaryKey
//        {
//            get { return "id_Folder"; }
//        }
//        public override string ElementsType
//        {
//            get { return Type.GetType("NetCms.Networks.WebFS.NetworkFolder").ToString(); }
//        }

        
//        public new Networks.WebFS.NetworkFolder this[int i]
//        {
//            get
//            {
//                return (NetworkFolder)base[i];
//            }
//        }

//        public new Networks.WebFS.NetworkFolder this[string key]
//        {
//            get
//            {
//                return (NetworkFolder)base[key];
//            }
//        }

//        public NetworkFolder FindFolder(string key)
//        {
//            NetworkFolder result = null;

//            result = this[key];

//            int i = 0;

//            while (result == null && i < this.Count)
//            {
//                if (result == null && this[i].Path.ToLower() == key.ToLower())
//                    result = this[i];

//                if (result == null && this[i].Nome.ToLower() == key.ToLower())
//                    result = this[i];

//                i++;
//            }
//            i = 0;
//            while (result == null && i < this.Count)
//            {
//                if (result == null)
//                    result = this[i].Folders.FindFolder(key);

//                i++;
//            }

//            return result;
//        }
        
//        #region Enumerator

//        public IEnumerator GetEnumerator()
//        {
//            return new CollectionEnumerator(this);
//        }

//        private class CollectionEnumerator : IEnumerator
//        {
//            private int CurentPos = -1;
//            private NetworkFoldersCollection Collection;
//            public CollectionEnumerator(NetworkFoldersCollection coll)
//            {
//                Collection = coll;
//            }
//            public object Current
//            {
//                get
//                {
//                    return Collection[CurentPos];
//                }
//            }
//            public bool MoveNext()
//            {
//                if (CurentPos < Collection.Count - 1)
//                {
//                    CurentPos++;
//                    return true;
//                }
//                else
//                    return false;
//            }
//            public void Reset()
//            {
//                CurentPos = -1;
//            }
//        }
//        #endregion

//        public override string BuildCommandAssociatedKey(string commandText, CommandType commandType, List<System.Data.Common.DbParameter> parameters)
//        {
//            return this.Connection.BuildCommandAssociatedKey(commandText, commandType, parameters);
//        }
//        public override DataTable FillData(string commandText, CommandType commandType, List<System.Data.Common.DbParameter> parameters)
//        {
//            return this.Connection.FillData(commandText, commandType, parameters);
//        }
//    }
//}