﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetCms.Networks.WebFS
{
    public abstract class NetworkFolder : CmsWebFSObject, IFolder
    {
        public NetworkFolder()
        {
            Folders = new HashSet<NetworkFolder>();
            Documents = new HashSet<NetworkDocument>();
        }


        public virtual ICollection<NetworkFolder> Folders { get; set; }

        public virtual ICollection<NetworkDocument> Documents { get; set; }

        public virtual NetworkFolder Parent
        {
            get;
            set;
        }

        public virtual string Path
        {
            get;
            set;
        }

        public virtual int Sistema
        {
            get;
            set;
        }

        public virtual bool IsSystemFolder
        {
            get
            {
                return Sistema == 1;
            }
        }

        public virtual bool Hidden
        {
            get
            {
                return Sistema == 2;
            }
        }

        //public virtual bool HasSubFolders
        //{
        //    get 
        //    { 
        //        return Folders.Count > 0; 
        //    }
        //}

        //public virtual bool HasDocuments
        //{
        //    get { return Documents.Count > 0; }
        //}

        public virtual string NetworkSystemName { get { return Network.SystemName; } }

        public virtual int NetworkID { get { return Network.ID; } }
//        /// <summary>
//        /// Collezione contenente le sottocartelle di questo oggetto Folder
//        /// </summary>
//        public abstract NetworkFoldersCollection Folders { get; }


//        public override DataTable CriteriaTable
//        {
//            get
//            {
//                if (_CriteriaTable == null)
//                    _CriteriaTable = this.Connection.SqlQuery(@"SELECT Key_Criterio,Tipo_Criterio,id_FolderProfile AS id_ObjectProfile,Folder_FolderProfile AS Object_ObjectProfile,Profile_FolderProfile AS Profile_ObjectProfile,Value_FolderProfileCriteria AS Value_ObjectProfileCriteria FROM folderprofilescriteria
//                     INNER JOIN folderprofiles ON id_FolderProfile = FolderProfile_FolderProfileCriteria
//                     INNER JOIN criteri ON id_Criterio = Criteria_FolderProfileCriteria");

//                return _CriteriaTable;
//            }
//        }
//        private DataTable _CriteriaTable;


//        public override DataTable GetGrantsTable(string AssociationID)
//        {
//            DataTable _GrantsTable = this.Connection.SqlQuery("SELECT * FROM folderprofilescriteria WHERE FolderProfile_FolderProfileCriteria = " + AssociationID);
//            return _GrantsTable;
//        }

//        private List<CmsWebFSObject> _Childs;
//        public override List<CmsWebFSObject> Childs
//        {
//            get { if (_Childs == null) InitChilds(); return _Childs; }
//        }
//        private void InitChilds()
//        {
//            _Childs = new List<CmsWebFSObject>();

//            foreach (CmsWebFSObject obj in this.Folders)
//                _Childs.Add(obj);
//            foreach (CmsWebFSObject obj in this.Documents)
//                _Childs.Add(obj);
//        }

//        public override CmsWebFSObject ParentObject
//        {
//            get { return this.Parent; }
//        }

//        public bool HasSubFolders
//        {
//            get { return Folders.Count > 0; }
//        }
//        public bool HasDocuments
//        {
//            get { return Documents.Count > 0; }
//        }

//        public string NetworkSystemName { get { return Network.SystemName; } }

//        public int FolderLang
//        {
//            get { return _FolderLang; }
//        }
//        protected int _FolderLang;

//        public string Path
//        {
//            get { return _Path; }
//        }
//        protected string _Path;

//        public override WebFSObjectTypes ObjectType
//        {
//            get { return WebFSObjectTypes.Folder; }
//        }

//        public override NetCms.Connections.Connection Connection
//        {
//            get
//            {
//                return this.Network.Connection;
//            }
//        }

//        private string _Nome;
//        public override string Nome
//        {
//            get
//            {
//                /*if (_Nome == null)
//                    if (Record != null)
//                        this._Nome = new LanguageDatabaseField("ID", "id_Prodotto", Record);
//                    else
//                        this._Nome = new DatabaseField("ID", "id_Prodotto", DatabaseFieldTypeData.DatabaseFieldTypes.String);
//                */
//                return _Nome;
//            }
//            protected set { _Nome = value; }
//        }

//        public string Label
//        {
//            get { return _Label; }
//        }
//        protected string _Label;

//        public string Tree
//        {
//            get { return _Tree; }
//        }
//        protected string _Tree;

//        public int Type
//        {
//            get { return _Type; }
//        }
//        protected int _Type;

//        private bool _IsSystemFolder;
//        public bool IsSystemFolder
//        {
//            get
//            {
//                return _IsSystemFolder;
//            }
//        }

//        public bool IsRootFolder
//        {
//            get
//            {
//                return this.Depth == 1 && Parent == null;
//            }
//        }

//        private string _Descrizione;
//        public string Descrizione
//        {
//            get
//            {
//                return _Descrizione;
//            }
//        }

//        private bool _Hidden;
//        public bool Hidden
//        {
//            get
//            {
//                return _Hidden;
//            }
//        }

//        public string CssClass
//        {
//            get { return _CssClass; }
//        }
//        protected string _CssClass;

//        public override string QueryString4URLs
//        {
//            get { return "folder=" + this.ID; }
//        }
//        private int _Stato;
//        public int StateValue
//        {
//            get
//            {
//                if (Parent != null)
//                    return _Stato >= Parent.StateValue ? _Stato : Parent.StateValue;
//                else
//                    return _Stato;
//            }
//        }

//        public virtual string TypeName
//        {
//            get
//            {
//                if (_TypeName == null)
//                {

//                    /*DataRow[] rows = Applicativi.Select("id_Applicativo = " + Type);
//                    if (rows.Length > 0)
//                        _TypeName = rows[0]["Nome_Applicativo"].ToString();
//                    else
//                        _TypeName = "";*/
//                }
//                return _TypeName;
//            }
//        }
//        protected string _TypeName;

//        public bool Restricted
//        {
//            get
//            {
//                if (Parent != null)
//                    return _Restricted || Parent.Restricted;
//                return _Restricted;
//            }
//        }
//        protected bool _Restricted;

//        public int RestrictedArea
//        {
//            get
//            {
//                if (_RestrictedArea == 0 && Parent != null)
//                    return Parent.RestrictedArea;
//                return _RestrictedArea;
//            }
//        }
//        protected int _RestrictedArea;

//        public NetworkFolder Parent
//        {
//            get
//            {
//                return this.OwnerCollection == null || this.OwnerCollection.OwnerItem == null ? null : (NetworkFolder)this.OwnerCollection.OwnerItem.CachedObject;
//            }
//        }

        public virtual Networks.INetwork Network
        {
            //get
            //{
            //    return NetworksManager.GetNetwork(NetworkKey);
            //}
            get;
            set;
        }

        private NetworkKey _NetworkKey;
        public virtual NetworkKey NetworkKey
        {
            get
            {
                return _NetworkKey;
            }
            protected internal set { _NetworkKey = value; }
        }

//        public int Depth
//        {
//            get { return _Depth; }
//            protected set { _Depth = value; }
//        }
//        protected int _Depth = 0;

//        private RssStatus _RssValue;
//        public RssStatus RssValue
//        {
//            get
//            {
//                return _RssValue;
//            }
//        }
//        public enum RssStatus { SystemError, Inheriths, Disabled, Enabled }

//        public NetworkDocumentsCollection Documents
//        {
//            get
//            {
//                if (_Documents == null)
//                    _Documents = BuildDocumentsCollection();

//                return _Documents;
//            }
//            protected set { _Documents = value; }
//        }
//        private NetworkDocumentsCollection _Documents;

        public abstract Networks.WebFS.NetworkDocument CurrentDocument { get; }
        public abstract string TranslatedPath { get; }

        //        public bool EnableRss
        //        {
        //            get
        //            {
        //                switch (RssValue)
        //                {
        //                    case RssStatus.Disabled: return false;
        //                    case RssStatus.Enabled: return true;
        //                    case RssStatus.Inheriths: return Parent != null ? Parent.EnableRss : false;
        //                    case RssStatus.SystemError: return false;
        //                    default: return false;
        //                }
        //            }
        //        }

        //        protected abstract NetworkDocumentsCollection BuildDocumentsCollection();

        public NetworkFolder(NetworkKey networkKey)
        {
            NetworkKey = networkKey;
            Folders = new HashSet<NetworkFolder>();
            Documents = new HashSet<NetworkDocument>();
            //InitData(Row);
        }

        //public virtual void InitData(DataRow Data)
        //{
        //    ID = Data["id_Folder"].ToString();
        //    _Nome = Data["Name_Folder"].ToString();
        //    _Path = Data["Path_Folder"].ToString();
        //    SourceRecordHash = Data.GetHashCode().ToString();

        //    _IsSystemFolder = Data["System_Folder"].ToString() == "1";
        //    _Hidden = Data["System_Folder"].ToString() == "2";
        //    _Path = Data["Path_Folder"].ToString();
        //    _Stato = int.Parse(Data["Stato_Folder"].ToString());
        //    _Nome = Data["Name_Folder"].ToString();
        //    _Label = Data["Label_FolderLang"].ToString();
        //    _FolderLang = int.Parse(Data["id_FolderLang"].ToString());
        //    if (Data.Table.Columns.Contains("Rss_Folder"))
        //    {
        //        switch (Data["Rss_Folder"].ToString())
        //        {
        //            case "0": _RssValue = RssStatus.Inheriths; break;
        //            case "1": _RssValue = RssStatus.Enabled; break;
        //            case "2": _RssValue = RssStatus.Disabled; break;
        //        }
        //    }
        //    else
        //        _RssValue = RssStatus.SystemError;
        //    _Depth = int.Parse(Data["Depth_Folder"].ToString());
        //    ID = Data["id_Folder"].ToString();
        //    _Tree = Data["Tree_Folder"].ToString();
        //    _Restricted = Data["Restricted_Folder"].ToString() != "0";
        //    _RestrictedArea = int.Parse(Data["Restricted_Folder"].ToString());
        //    _Descrizione = Data["Desc_FolderLang"].ToString();
        //    _CssClass = Data["CssClass_Folder"].ToString();
        //    _Type = int.Parse(Data["Tipo_Folder"].ToString());
        //}

        public abstract NetworkFolder FindFolderByID(int id);

        public abstract NetworkFolder FindFolderByName(string name);

        public abstract NetworkFolder FindFolderByPath(string path);

        public abstract NetworkFolder GetFolderByNetworkAndParentsPath(string folderPath, int networkId, List<string> paths);

        public abstract NetworkDocument FindDocByID(int id);

        // usato per ricerca ricorsiva nel sottoalbero
        public abstract NetworkDocument FindDocByPhysicalName(string name);

        //public NetworkDocument FindDoc(string key)
        //{
        //    int id = 0;
        //    return int.TryParse(key, out id) ? FindDocumentByID(key) : FindDocumentByName(key);
        //}


        public abstract NetworkDocument GetDocumentByPhysicalName(string name);

        public abstract string TranslatePath(int langId, int parentLangId);

        //public string StatusLabel
        //{
        //    get
        //    {
        //        return StatusLabels(this.StateValue); ;
        //    }
        //}
        //public static string StatusLabels(int status)
        //{
        //    string statlabel = "";
        //    switch (status)
        //    {
        //        case 0:
        //            statlabel = "Menu + Raggiungibile";
        //            break;
        //        case 1:
        //            statlabel = "Raggiungibile";
        //            break;
        //        case 2:
        //            statlabel = "Non Raggiungibile";
        //            break;
        //        case 3:
        //            statlabel = "Nascosto";
        //            break;
        //    }
        //    return statlabel;
        //}
        //public FolderStates Status
        //{
        //    get
        //    {
        //        FolderStates stat = FolderStates.NotReachable;

        //        switch (StateValue)
        //        {
        //            case 0:
        //                stat = FolderStates.Menu;
        //                break;
        //            case 1:
        //                stat = FolderStates.Reachable;
        //                break;
        //            case 2:
        //                stat = FolderStates.NotReachable;
        //                break;
        //        }
        //        return stat;
        //    }
        //}
        //public enum FolderStates
        //{
        //    Menu,
        //    Reachable,
        //    NotReachable
        //}
    }
}

