﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Networks.WebFS
{
    public interface IFolder
    {
        int ID { get; }
        string Path { get; }
        string NetworkSystemName { get; }
        int NetworkID { get; }
    }
}
