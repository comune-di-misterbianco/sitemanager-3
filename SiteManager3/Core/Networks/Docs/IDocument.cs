﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Networks.WebFS
{
    public interface IDocument
    {
        int ID { get; }
        string NetworkSystemName { get; }
        int NetworkID { get; }
    }
}
