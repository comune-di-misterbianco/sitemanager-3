﻿using System;
using System.Web;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using NetCms.Connections;

namespace NetCms.Networks.WebFS
{
    public abstract class NetworkDocument : CmsWebFSObject, IDocument
    {

        public NetworkDocument()
        { }

        public override Networks.INetwork Network
        {
            get
            {
                return NetworksManager.GetNetwork(NetworkKey);
            }

        }

        public virtual NetworkFolder Folder
        {
            get;
            set;
        }       

        public virtual string NetworkSystemName { get { return Network.SystemName; } }

        public virtual int NetworkID { get { return Network.ID; } }

        //private NetworkKey _NetworkKey;
        protected internal virtual NetworkKey NetworkKey
        {
            get
            {
                return Folder.NetworkKey;
                //return _NetworkKey;
            }
            //private set { _NetworkKey = value; }
        }

        public abstract string TranslatedName { get; }

        //        public override string QueryString4URLs
        //        {
        //            get { return "folder=" + this.Folder.ID + "&doc=" + this.ID; }
        //        }

        //        public override DataTable CriteriaTable
        //        {
        //            get
        //            {
        //                if (_CriteriaTable == null)
        //                    _CriteriaTable = this.Connection.SqlQuery(@"SELECT Key_Criterio,Tipo_Criterio,id_DocProfile AS id_ObjectProfile,doc_docProfile AS Object_ObjectProfile,Profile_docProfile AS Profile_ObjectProfile,Value_docProfileCriteria AS Value_ObjectProfileCriteria FROM docprofilescriteria
        //                     INNER JOIN docprofiles ON id_DocProfile = docProfile_docProfileCriteria
        //                     INNER JOIN criteri ON id_Criterio = Criteria_docProfileCriteria");

        //                return _CriteriaTable;
        //            }
        //        }
        //        private DataTable _CriteriaTable;

        //        public string NetworkSystemName { get { return Network.SystemName; } }

        //        public override DataTable GetGrantsTable(string AssociationID)
        //        {
        //                 DataTable   _GrantsTable = this.Connection.SqlQuery("SELECT * FROM folderprofilescriteria WHERE FolderProfile_FolderProfileCriteria = " + AssociationID);
        //                return _GrantsTable;
        //        }

        //        public override WebFSObjectTypes ObjectType
        //        {
        //            get { return WebFSObjectTypes.Document; }
        //        }

        //        public override List<CmsWebFSObject> Childs
        //        {
        //            get { return null; }
        //        }

        //        public override Connection Connection
        //        {
        //            get { return this.Network.Connection; }
        //        }

        //        public override CmsWebFSObject ParentObject
        //        {
        //            get { return this.Folder; }
        //        }

        //        public virtual string FullName
        //        {
        //            get { return Folder.Path + "/" + Name; }
        //        }

        //        private int _HasNext;
        //        public bool HasNext
        //        {
        //            get
        //            {
        //                if (_HasNext == 0)
        //                {
        //                    _HasNext = 2;

        //                    bool found = false;
        //                    for (int i = 0; i < Folder.Documents.Count && !found; i++)
        //                    {
        //                        if (Folder.Documents[i] == this)
        //                        {
        //                            _HasNext = i + 1 < Folder.Documents.Count ? 1 : 2;
        //                            found = true;
        //                        }
        //                    }

        //                }
        //                return _HasNext == 1;
        //            }
        //        }

        //        public abstract string Link
        //        {
        //            get;
        //        }

        //        public virtual int Content
        //        {
        //            get
        //            {
        //                return _Content;
        //            }
        //        }
        //        public int _Content = -1;

        //        public virtual int Stato
        //        {
        //            get
        //            {
        //                return _Stato;
        //            }
        //        }
        //        public int _Stato = -1;

        //        private string _Name;
        //        public virtual string Name
        //        {
        //            get
        //            {
        //                return _Name;
        //            }
        //        }

        //        public override string Nome
        //        {
        //            get
        //            {
        //                return this.Label;
        //            }
        //            protected set { }
        //        }

        //        private string _FolderID;
        //        private string FolderID
        //        {
        //            get
        //            {
        //                return _FolderID;
        //            }
        //        }

        //        private string _Descrizione;
        //        public string Descrizione
        //        {
        //            get
        //            {
        //                return _Descrizione;
        //            }
        //        }

        //        private string _Label;
        //        public string Label
        //        {
        //            get
        //            {
        //                return _Label;
        //            }
        //        }

        //        public NetworkFolder Folder
        //        {
        //            get 
        //            {
        //                if (_Folder == null)
        //                {
        //                    _Folder = Network.RootFolder.FindFolder(this.FolderID);
        //                }
        //                return _Folder; 
        //            }
        //        }
        //        protected NetworkFolder _Folder;

        //        public int _Lang;
        //        public virtual int Lang
        //        {
        //            get
        //            {
        //                return _Lang;
        //            }
        //        }

        //        private string _Extension;
        //        public virtual string Extension
        //        {
        //            get
        //            {
        //                if (_Extension == null)
        //                {
        //                    string[] exts = Name.ToLower().Split('.');
        //                    if (exts.Length > 0)
        //                        _Extension = exts[exts.Length - 1];
        //                    else
        //                        _Extension = "";
        //                }
        //                return _Extension;
        //            }
        //        }

        //        private string _CssClass;
        //        public string CssClass
        //        {
        //            get
        //            {
        //                return _CssClass;
        //            }
        //        }

        //        private int _Application;
        //        public int Application
        //        {
        //            get
        //            {
        //                return _Application;
        //            }
        //        }

        //        private string _Date;
        //        public virtual string Date
        //        {
        //            get
        //            {
        //                return _Date;
        //            }
        //        }

        //        private int _TitleOffset = -1;
        //        public bool AddKeywordsToTitle
        //        {
        //            get
        //            {
        //                return _TitleOffset == 1;
        //            }
        //        }

        //        protected string _LastModify;
        //        public virtual string LastModify
        //        {
        //            get
        //            {
        //                return _LastModify;
        //            }
        //        }

        //        public G2Core.Common.StringCollection DocLangsContents
        //        {
        //            get
        //            {
        //                if (_DocLangsContents == null)
        //                {
        //                    _DocLangsContents = new G2Core.Common.StringCollection();

        //                    DataTable tab = this.Network.Connection.SqlQuery("SELECT * FROM Docs_Lang WHERE Doc_DocLang = " + this.ID);
        //                    foreach(DataRow row in tab.Rows)
        //                        _DocLangsContents.Add(row["id_DocLang"].ToString(), row["Lang_DocLang"].ToString());
        //                }
        //                return _DocLangsContents;
        //            }
        //        }
        //        private G2Core.Common.StringCollection _DocLangsContents;

        //        #region Stato del Documento

        //        public string StatusLabel
        //        {
        //            get
        //            {
        //                return StatusLabels(Stato);
        //            }
        //        }
        //        public static string StatusLabels(int status)
        //        {
        //            string statlabel = "";
        //            switch (status)
        //            {
        //                case 0:
        //                    statlabel = "Menu + Raggiungibile";
        //                    break;
        //                case 1:
        //                    statlabel = "Raggiungibile";
        //                    break;
        //                case 2:
        //                    statlabel = "Non Raggiungibile";
        //                    break;
        //                case 3:
        //                    statlabel = "Nascosto";
        //                    break;
        //            }
        //            return statlabel;
        //        }
        //        public DocumentStates Status
        //        {
        //            get
        //            {
        //                DocumentStates stat = DocumentStates.NotReachable;

        //                switch (Stato)
        //                {
        //                    case 0:
        //                        stat = DocumentStates.Menu;
        //                        break;
        //                    case 1:
        //                        stat = DocumentStates.Reachable;
        //                        break;
        //                    case 2:
        //                        stat = DocumentStates.NotReachable;
        //                        break;
        //                }
        //                return stat;
        //            }
        //        }
        //        public enum DocumentStates
        //        {
        //            Menu = 0,
        //            Reachable = 1,
        //            NotReachable = 2
        //        }

        //        #endregion

        public NetworkDocument(/*DataRow Row, */NetworkKey networkKey)
        {
            //NetworkKey = networkKey;
            //InitData(Row);
        }

//        public void InitData(DataRow Data)
//        {
//            _Lang = int.Parse(Data["id_DocLang"].ToString());
//            _Stato = int.Parse(Data["Stato_Doc"].ToString());
//            ID = Data["id_Doc"].ToString();
//            _Name = Data["Name_Doc"].ToString();
//            _FolderID = Data["Folder_Doc"].ToString();
//            SourceRecordHash = Data.GetHashCode().ToString();
//            _Label = Data["Label_DocLang"].ToString();
//            _Descrizione = Data["Desc_DocLang"].ToString();
//            int.TryParse(Data["Content_DocLang"].ToString(), out _Content);
//            int.TryParse(Data["Stato_Doc"].ToString(), out _Stato);
//            int.TryParse(Data["TitleOffset_Doc"].ToString(), out _TitleOffset);
//            _Date = Data["Data_Doc"].ToString();
//            if (_Date.Length > 10)
//                _Date = _Date.Remove(10);
//            _Application = int.Parse(Data["Application_Doc"].ToString());
//            _CssClass = Data["CssClass_Doc"].ToString();
//            _LastModify = Data["Modify_Doc"].ToString();
//            if (_LastModify.Length > 10)
//                _LastModify = _LastModify.Remove(10);
//        }
    }
}
