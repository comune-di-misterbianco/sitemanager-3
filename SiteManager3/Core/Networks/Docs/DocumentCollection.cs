//using System;
//using System.Web;
//using System.Collections;
//using System.Collections.Generic;
//using System.Data;
//using NetCms.Connections;

//namespace NetCms.Networks.WebFS
//{
//    public abstract class NetworkDocumentsCollection : G2Core.Caching.CacheableCollection, IEnumerable
//    {
//        public NetCms.Connections.Connection Connection { get; private set; }

//        public override int ItemsCachingTime
//        {
//            get
//            {
//                return Networks.NetworkTimings.DocumentsExpireTiming;
//            }
//            set
//            {
//            }
//        }
//        public NetworkDocumentsCollection(G2Core.Caching.Visibility.CachingVisibility Visibility, NetCms.Connections.Connection conn)
//            : base(Visibility, true)
//        {
//            this.Connection = conn;
//            this.ItemsCachingTime = 2;
//            this.TableCachingTime = 10;
//            this.SourceTableVisibility = G2Core.Caching.Visibility.CachingVisibility.Global;
//        }

//        public override string PrimaryKey
//        {
//            get { return "id_Doc"; }
//        }
//        public override string ElementsType
//        {
//            get { return Type.GetType("NetCms.Networks.WebFS.NetworkDocument").ToString(); }
//        }

//        public new NetworkDocument this[int i]
//        {
//            get
//            {
//                return (NetworkDocument)base[i];
//            }
//        }

//        public new NetworkDocument this[string key]
//        {
//            get
//            {
//                return (NetworkDocument)base[key];
//            }
//        }

//        #region Enumerator

//        public IEnumerator GetEnumerator()
//        {
//            return new CollectionEnumerator(this);
//        }

//        private class CollectionEnumerator : IEnumerator
//        {
//            private int CurentPos = -1;
//            private NetworkDocumentsCollection Collection;
//            public CollectionEnumerator(NetworkDocumentsCollection coll)
//            {
//                Collection = coll;
//            }
//            public object Current
//            {
//                get
//                {
//                    return Collection[CurentPos];
//                }
//            }
//            public bool MoveNext()
//            {
//                if (CurentPos < Collection.Count - 1)
//                {
//                    CurentPos++;
//                    return true;
//                }
//                else
//                    return false;
//            }
//            public void Reset()
//            {
//                CurentPos = -1;
//            }
//        }
//        #endregion

//        public override string BuildCommandAssociatedKey(string commandText, CommandType commandType, List<System.Data.Common.DbParameter> parameters)
//        {
//            return this.Connection.BuildCommandAssociatedKey(commandText, commandType, parameters);
//        }

//        public override DataTable FillData(string commandText, CommandType commandType, List<System.Data.Common.DbParameter> parameters)
//        {
//            return this.Connection.FillData(commandText, commandType, parameters);
//        }
//    }
//}