﻿using System.ComponentModel;

namespace NetCms.Front
{
    public enum FrontLayouts
    {
        [Description("Errore di sistema")]
        SystemError = -1,
        [Description("Eredita layout")]
        Inherits = 0,
        [Description("Mostra tutti i contenitori ")]
        All = 8,
        [Description("Mostra solo colonna sinistra e destra")]
        LeftRight = 1,
        [Description("Mostra solo colonna sinistra e footer")]
        LeftFooter = 2,
        [Description("Mostra solo colonna destra e footer")]
        RightFooter = 3,
        [Description("Mostra solo colonna sinistra")]
        Left = 4,
        [Description("Mostra solo colonna destra")]
        Right = 5,
        [Description("Mostra solo footer")]
        Footer = 6,
        [Description("Nascondi tutti i contenitori")]
        None = 7
    }  
}