using System;
using System.Web;
using System.Web.Caching;
using System.Collections.Generic;
using System.Data;
using System.Text;
using NetCms.Connections;
using G2Core.Caching;
using G2Core.Caching.Visibility;
using NetCms.Networks.WebFS;
using NHibernate;

namespace NetCms.Networks
{
    public enum NetworkCmsStates { NotCreated, Enabled, Disabled }
    public class BasicNetwork : INetwork
    {
        //DA COMMENTARE
        //private NetCms.Connections.Connection _Connection;
        public virtual NetCms.Connections.Connection Connection
        {
            get
            {
                NetCms.Connections.ConnectionsManager ConnManager = new ConnectionsManager();
                return ConnManager.CommonConnection;

                //if (_Connection == null)
                //    _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
                //return _Connection;
            }
        }

        #region Propriet� da mappare

        public virtual int ID
        {
            get;
            set;
        }
        
        public virtual string SystemName
        {
            get;
            set; // { _SystemName = value.ToLower(); }
        }

        public virtual string Label
        {
            get;
            set;
        }

        public virtual bool HasFrontend
        {
            get;
            set;
        }

        public virtual bool Enabled
        {
            get;
            set;
        }

        public virtual string RootPath
        {
            get;
            set;
        }
        
        public virtual int DefaultLang
        {
            get;
            set;
        }

        public virtual bool Predefinita
        {
            get;
            set;
        }

        public virtual int StatoCMS
        { 
            get; 
            set; 
        }


        public virtual ICollection<NetworkFolder> NetworkFolders
        {
            get;
            set;
        }
        #endregion

        public virtual bool Default
        {
            get
            { return Predefinita && NetCms.Configurations.Paths.UseDefaultNetwork; }
        }

        //Non pu� essere ricavata a questo livello
        //public string RootFolderID
        //{
        //    get
        //    {
        //        if (_RootFolderID == null)
        //        {
        //            DataTable table = Connection.SqlQuery("SELECT id_Folder FROM folders WHERE Depth_Folder = 1 AND Network_Folder = " + this.ID);
        //            _RootFolderID = table.Rows[0]["id_Folder"].ToString();
        //        }
        //        return _RootFolderID;
        //    }
        //}
        //private string _RootFolderID;

        

        public virtual NetworkPaths Paths
        {
            get
            {
                if (_Paths == null)
                    _Paths = new NetworkPaths(this);
                return _Paths;
            }
        }
        private NetworkPaths _Paths;

        public virtual Faces Face { get { return Faces.Admin; } }

        public virtual NetworkCmsStates CmsStatus
        {
            get {
                switch (StatoCMS)
                {
                    case 1: _CmsStatus = NetworkCmsStates.Enabled; break;
                    case 2: _CmsStatus = NetworkCmsStates.Disabled; break;
                    default: _CmsStatus = NetworkCmsStates.NotCreated; break;
                }
                return _CmsStatus; 
            }
        }
        private NetworkCmsStates _CmsStatus;


        public BasicNetwork()
        {
            NetworkFolders = new HashSet<NetworkFolder>();
        }


        //public BasicNetwork(int id)
        //{
        //    ID = id.ToString();

        //    DataTable networksTable = new NetworksData(false);
        //    DataRow NetworkRecord = networksTable.Select("id_Network = " + this.ID)[0];
        //    Init(NetworkRecord);
        //}
        //public BasicNetwork(DataRow row)
        //{
        //    Init(row);
        //}
        //public BasicNetwork(string name)
        //{
        //    this.SystemName = name;

        //    DataTable networksTable = new NetworksData(false);
        //    DataRow NetworkRecord = networksTable.Select("Nome_Network LIKE '" + this.SystemName + "'")[0];
        //    Init(NetworkRecord);
        //}

        //private void Init(DataRow NetworkRecord)
        //{
        //    ID = NetworkRecord["id_Network"].ToString();
        //    SystemName = NetworkRecord["Nome_Network"].ToString();
        //    Label = NetworkRecord["Label_Network"].ToString();
        //    HasFrontend = NetworkRecord["Frontend_Network"].ToString() == "1";
        //    Enabled = NetworkRecord["Enabled_Network"].ToString() == "1";
        //    RootPath = NetworkRecord["Webroot_Network"].ToString();
        //    DefaultLang = int.Parse(NetworkRecord["DefaultLang_Network"].ToString());

        //    //La network viene usata come default, solo se nel file di configurazione � impostato l'uso della network di default.
        //    Default = NetworkRecord["Default_Network"].ToString() == "1" && NetCms.Configurations.Paths.UseDefaultNetwork;
            
        //    switch(NetworkRecord["Cms_Network"].ToString())
        //    {
        //        case "1": CmsStatus = NetworkCmsStates.Enabled; break;
        //        case "2": CmsStatus = NetworkCmsStates.Disabled; break;
        //        default: CmsStatus = NetworkCmsStates.NotCreated; break;
        //    }
        //}

        public virtual void EnableNetwork()
        {
            //string sql = "UPDATE Networks SET Enabled_Network = 1 WHERE id_Network = " + this.ID;
            //Connection.Execute(sql);

            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    BasicNetwork net = NetworksBusinessLogic.GetById(this.ID, sess);
                    net.Enabled = true;
                    NetworksBusinessLogic.SaveOrUpdateNetwork(net,sess);
                    tx.Commit();
                    NetCms.Networks.NetworksManager.GlobalIndexer.Update();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }            
        }

        public virtual void DisableNetwork()
        {
            //string sql = "UPDATE Networks SET Enabled_Network = 0 WHERE id_Network = " + this.ID;
            //Connection.Execute(sql);

            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    BasicNetwork net = NetworksBusinessLogic.GetById(this.ID, sess);
                    net.Enabled = false;
                    NetworksBusinessLogic.SaveOrUpdateNetwork(net, sess);
                    tx.Commit();
                    NetCms.Networks.NetworksManager.GlobalIndexer.Update();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }  
        }

        public virtual void DisableCms()
        {
            //string sql = "UPDATE Networks SET Cms_Network = 2 WHERE id_Network = " + this.ID;
            //Connection.Execute(sql);

            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    BasicNetwork net = NetworksBusinessLogic.GetById(this.ID, sess);
                    net.StatoCMS = 2;
                    NetworksBusinessLogic.SaveOrUpdateNetwork(net, sess);
                    tx.Commit();
                    NetCms.Networks.NetworksManager.GlobalIndexer.Update();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            }            
        }

        public virtual void EnableCms()
        {
            //string sql = "UPDATE Networks SET Cms_Network = 1 WHERE id_Network = " + this.ID;
            //Connection.Execute(sql);

            using (ISession sess = DBSessionProvider.FluentSessionProvider.Instance.OpenInnerSession())
            using (ITransaction tx = sess.BeginTransaction())
            {
                try
                {
                    BasicNetwork net = NetworksBusinessLogic.GetById(this.ID, sess);
                    net.StatoCMS = 1;
                    NetworksBusinessLogic.SaveOrUpdateNetwork(net, sess);
                    tx.Commit();
                    NetCms.Networks.NetworksManager.GlobalIndexer.Update();
                }
                catch (Exception ex)
                {
                    tx.Rollback();
                }
            } 
        }
    }

    public class NetworkPaths
    {
        public BasicNetwork Network { get; private set; }
        public string AbsoluteFrontRoot { get; private set; }
        public string AbsoluteAdminRoot { get; private set; }
        public string AbsoluteConfigRoot { get; private set; }
        public string AbsoluteCssRoot { get; private set; }

        public NetworkPaths(BasicNetwork network)
        {
            Network = network;

            this.AbsoluteFrontRoot = NetCms.Configurations.Paths.AbsoluteRoot;
            if (network.RootPath.Length > 0 && !this.Network.Default)
                this.AbsoluteFrontRoot += "/" + network.RootPath.ToLower();

            //Aggiunto (network.RootPath.Length>0?network.RootPath:network.SystemName) per prevedere il caso in cui Webroot_Network � vuoto quando si usa la default network sul path principale
            this.AbsoluteCssRoot = NetCms.Configurations.Paths.AbsoluteRoot + "/css/" + (network.RootPath.Length>0?network.RootPath:network.SystemName);
            this.AbsoluteAdminRoot = NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/" + (network.RootPath.Length > 0 ? network.RootPath : network.SystemName);
            this.AbsoluteConfigRoot = NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\" + (network.RootPath.Length > 0 ? network.RootPath : network.SystemName);
        }
    }
}