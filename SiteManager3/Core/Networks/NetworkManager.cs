﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace NetCms.Networks
{
    public enum NetworksStates { OutsideNetworks, InsideNetwork }
    public static class NetworksManager
    {
        public const string NetworkStatusPageItemsKey = "SM2_NetworkStatus";
        public const string NetworkFaceItemsKey = "SM2_CurrentFace";
        public const string CurrentNetworkItemsKey = "SM2_CurrentNetwork";
        public const string CurrentNetworkSystemNameItemsKey = "SM2_CurrentNetworkSystemName";

        public static INetwork GetNetwork(NetworkKey key)
        {
            return GlobalIndexer.AllNetworks[GlobalIndexer.NetworkKeys[key.NetworkID]];
            //return (Network)new G2Core.Caching.Cache()[key.ToString()];
        }
        public static void RemoveNetworkKey(Network network)
        {
            AvailableNetworksKey.Remove(network.CacheKey.ToString());
        }

        public static Faces CurrentFace
        {
            get
            {
                return (Faces)(HttpContext.Current == null ? Faces.Front : HttpContext.Current.Items[NetworkFaceItemsKey]);
            }
        }
        public static NetworksStates NetworkStatus
        {
            get
            {
                if (HttpContext.Current.Items.Contains(NetworkStatusPageItemsKey))
                    return (NetworksStates)HttpContext.Current.Items[NetworkStatusPageItemsKey];
                else return  NetworksStates.OutsideNetworks;
            }
        }

        public static Network CurrentActiveNetwork
        {
            get
            {
                return (Network)HttpContext.Current.Items[NetworksManager.CurrentNetworkItemsKey];
            }
        }

        public static void SetCurrentActiveNetwork(Network Network)
        {
            HttpContext.Current.Items[NetworksManager.CurrentNetworkSystemNameItemsKey] = Network.SystemName;
            HttpContext.Current.Items[NetworksManager.CurrentNetworkItemsKey] = Network;
        }

        private static G2Core.Common.StringCollection availableNetworksKey;
        public static G2Core.Common.StringCollection AvailableNetworksKey
        {
            get
            {
                if (availableNetworksKey == null)
                    availableNetworksKey = new G2Core.Common.StringCollection();

                return availableNetworksKey;
            }
        }

        public static string CurrentFakeNetwork
        {
            get
            {
                if (CurrentActiveNetwork != null)
                {
                    Uri currentUri = HttpContext.Current.Request.Url;
                    string currentURL = currentUri.AbsolutePath.ToLower();

                    //DA TESTARE: Dovrebbe correggere un bug nel caso in cui AbsoluteFrontRoot è vuoto perchè siamo nella network di default.
                    string splitKey = CurrentActiveNetwork.Paths.AbsoluteFrontRoot.Length == 0 ? "/" : CurrentActiveNetwork.Paths.AbsoluteFrontRoot.ToLower();

                    string[] split = { splitKey };
                    string currentNetPath = currentURL.Split(split, StringSplitOptions.None)[1];

                    if (currentNetPath.LastIndexOf("/") > 0)
                    {
                        string fakeNetwork = currentNetPath.Split('/')[1];
                        if (GlobalIndexer.AllNetworks.ContainsKey(fakeNetwork.ToLower()))
                            return fakeNetwork;
                    }

                    return CurrentActiveNetwork.SystemName;
                }
                return null;
            }
        }

        public static NetworkKey CurrentFakeNetworkKey
        {
            get
            {
                var currentFakeNetwork = CurrentFakeNetwork;
                if (CurrentFakeNetwork != null)
                {
                    return new NetworkKey(NetCms.Networks.NetworksManager.GlobalIndexer.GetNetworkBySystemName(currentFakeNetwork).ID);
                }
                return null;
            }
        }

        public static int CurrentFakeNetworkID
        {
            get
            {
                var currentFakeNetwork = CurrentFakeNetwork;
                if (CurrentFakeNetwork != null)
                {
                    return NetCms.Networks.NetworksManager.GlobalIndexer.GetNetworkBySystemName(currentFakeNetwork).ID;
                }
                return 0;
            }
        }

        public static NetworksIndexer GlobalIndexer
        {
            get
            {
                if (_GlobalIndexer == null)
                    _GlobalIndexer = new NetworksIndexer();
                return _GlobalIndexer;
            }
        }
        private static NetworksIndexer _GlobalIndexer;
    }

    public class NetworkKey
    {
        public string  KeyOffset
        {
            get { return _KeyOffset; }
            private set { _KeyOffset = value; }
        }
        private string  _KeyOffset;

        public int  NetworkID
        {
            get { return _NetworkID; }
            private set { _NetworkID = value; }
        }
        private int  _NetworkID;

        public string BaseKey
        {
            get
            {
                if (_BaseKey == null)
                    _BaseKey = "[Network][" + this.NetworkID + "]";
                return _BaseKey;
            }
        }
        private string _BaseKey;

        public G2Core.Caching.Visibility.CachingVisibility Visibility
        {
            get { return _Visibility; }
            private set { _Visibility = value; }
        }
        private G2Core.Caching.Visibility.CachingVisibility _Visibility;

        public NetworkKey(int NetworkID)
            :this(NetworkID,G2Core.Caching.Visibility.CachingVisibility .User,""){}
        public NetworkKey(int NetworkID, G2Core.Caching.Visibility.CachingVisibility Visibility)
            :this(NetworkID, Visibility, "") { }
        public NetworkKey(int NetworkID, G2Core.Caching.Visibility.CachingVisibility Visibility, string KeyOffset)
        {
            this.KeyOffset = KeyOffset;
            this.NetworkID = NetworkID;
            this.Visibility = Visibility;
        }

        public string GetKey()
        {
            string key = G2Core.Caching.Visibility.CachingVisibilityUtility.BuildVisibilityKey(this.Visibility);
            key += this.BaseKey;
            return key;
        }
        public override string ToString()
        {
            return GetKey(); 
        }
    }
}
