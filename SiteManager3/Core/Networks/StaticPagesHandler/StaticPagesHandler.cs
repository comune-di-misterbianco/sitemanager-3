using System;
using System.Web;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
//using DynamicUrl;

namespace NetCms.Front.Static
{
    public class PagesClassesCollection : Dictionary<string, AttributeTypeCouple> { }

    public static class StaticPagesCollector
    {
        private const string AssociationsKey = "NetCms.Front.Static.InvalidationHandler";

        public static PagesClassesCollection ClassesKeysAssociations
        {
            get
            {
                if (_ClassesKeysAssociations == null)
                {
                    if (HttpContext.Current.Application[AssociationsKey] != null)
                        _ClassesKeysAssociations = (PagesClassesCollection)HttpContext.Current.Application[AssociationsKey];
                    else
                    {
                        HttpContext.Current.Application[AssociationsKey] = _ClassesKeysAssociations = new PagesClassesCollection();
                    }
                }

                return _ClassesKeysAssociations;
            }
        }
        private static PagesClassesCollection _ClassesKeysAssociations;

        public static void ParseType(Type type)
        {
            var attributes = System.Attribute.GetCustomAttributes(type);
            if (attributes != null)
            {
                foreach(var couple in attributes.Where(x => x is FrontendStaticPage).Select(x=> new AttributeTypeCouple(x as FrontendStaticPage, type)))
                {
                    foreach(var pageAddress in couple.Attribute.Addresses)
                    {
                        if (ClassesKeysAssociations.Keys.Contains(pageAddress))
                            throw new NetCms.Exceptions.ServerError500Exception("L'elenco delle pagine statiche contiene gi� una pagina con paths '" + couple.Attribute.Addresses + "'");
                        if (type.HinheritsFrom(typeof(System.Web.UI.Control).FullName))
                            ClassesKeysAssociations.Add(pageAddress, couple);
                    }
                    
                }
            }
        }
        
        public static bool HinheritsFrom(this Type childType, string parentType)
        {
            var type = childType;

            while (type != null)
            {
                if (type.FullName == parentType) return true;
                type = type.BaseType;
            }

            return false;
        }

        private static G2Core.Caching.PersistentCacheObject<AttributeTypeCouple> CurrentStaticPageData
        {
            get
            {
                return _CurrentStaticPageData ?? (_CurrentStaticPageData = new G2Core.Caching.PersistentCacheObject<AttributeTypeCouple>("SM2_CurrentStaticPage", G2Core.Caching.StoreTypes.Page) { AutoInitObjectInstance = false });
            }
        }
        private static G2Core.Caching.PersistentCacheObject<AttributeTypeCouple> _CurrentStaticPageData;

        public static AttributeTypeCouple CurrentStaticPage
        {
            get
            {
                return CurrentStaticPageData.Instance;
            }
            set
            {
                CurrentStaticPageData.SetObjectInstance(value);
                ApplicationContext.SetCurrentComponent(value.Type.Assembly);
            }
        }
        public static bool HasCurrentStaticPage
        {
            get
            {
                return CurrentStaticPageData.Instance !=null;
            }
        }
    }

    public class AttributeTypeCouple
    {
        public FrontendStaticPage Attribute { get; private set; } 
        public Type Type { get; private set; }

        public AttributeTypeCouple(FrontendStaticPage attribute, Type type)
        {
            Attribute = attribute;
            Type = type;
        }
    }
}