﻿namespace NetCms.Networks.WebFS
{
    public abstract class WebFSObject /*: G2Core.Caching.CacheableObject*/
    {
        // aggiunto l'ID

        public virtual int ID { get; set; }
        public virtual string Nome { get; set; }
        public abstract string QueryString4URLs { get; }
        public virtual INetwork Network { get; set; }

        public WebFSObject() { }
    }
}

