//using System;
//using System.Data;
//using System.Web;
//using System.Collections;
//using System.Collections.Generic;
////using NetCms.Structure.Grants;

///// <summary>
///// Summary description for StructureGrants
///// </summary>

//namespace NetCms.Networks.WebFS
//{
//    public enum SpecifyTypes { Inherits, Allow, Deny, NotAssociated }
//    public class Specify
//    {
//        public int Depth
//        {
//            get { return Profile.Depth; }
//        }

//        public string SpecifyURL
//        {
//            get
//            {
//                return this.WfsObject.Network.Paths.AbsoluteAdminRoot + "/cms/grants/grants.aspx?"+this.WfsObject.QueryString4URLs;
//            }
//        }

//        public SpecifyTypes Value
//        {
//            get { return _Value; }
//            private set { _Value = value; }
//        }
//        private SpecifyTypes _Value;

//        public bool Inherits { get { return Value == SpecifyTypes.Inherits; } }
//        public bool Allow { get { return Value == SpecifyTypes.Allow; } }
//        public bool Deny { get { return Value == SpecifyTypes.Deny; } }
//        public bool Found { get { return Value != SpecifyTypes.Inherits; } }
        
//        public NetCms.Users.Profile Profile
//        {
//            get
//            {
//                return _Profile;
//            }
//        }
//        private NetCms.Users.Profile _Profile;

//        public CmsWebFSObject WfsObject
//        {
//            get { return _Object; }
//            private set { _Object = value; }
//        }
//        private CmsWebFSObject _Object;

//        public Specify(SpecifyTypes value, NetCms.Users.Profile profile, CmsWebFSObject wfsObject)
//        {
//            this.WfsObject = wfsObject;
//            Value = value;
//            _Profile = profile;
//        }

//        public Specify(int value, NetCms.Users.Profile profile, CmsWebFSObject wfsObject)
//        {
//            this.WfsObject = wfsObject;
//            Value = Parse(value);
//            _Profile = profile;
//        }

//        public static SpecifyTypes Parse(int value)
//        {
//            switch (value)
//            {
//                case 0: return SpecifyTypes.Inherits;
//                case 1: return SpecifyTypes.Allow;
//                case 2: return SpecifyTypes.Deny;
//                default: return SpecifyTypes.NotAssociated;
//            }
//        }
//    }
//}