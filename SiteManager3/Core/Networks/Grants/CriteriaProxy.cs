//using System;
//using System.Data;
//using System.Collections;
//using System.Collections.Generic;

///// <summary>
///// Summary description for StructureGrants
///// </summary>

//namespace NetCms.Networks.WebFS
//{
//    public class CriteriaCollector
//    {
//        public Dictionary<string,FullCriteria> Permessi
//        {
//            get
//            {
//                if (_Permessi == null)
//                {
//                    _Permessi = new Dictionary<string, FullCriteria>();
//                }
//                return _Permessi;
//            }
//        }
//        private Dictionary<string, FullCriteria> _Permessi;

//        private CmsWebFSObject _Folder;
//        public CmsWebFSObject Folder
//        {
//            get { return _Folder; }
//        }

//        public CriteriaCollector(CmsWebFSObject folder)
//        {
//            _Folder = folder;
//        }

//        public FullCriteria this[NetCms.Users.Profile profile]
//        {
//            get
//            {
//                if (!Permessi.ContainsKey(profile.ID.ToString()))
//                    this.Permessi.Add(profile.ID.ToString(), new FullCriteria(profile, this.Folder));
//                return Permessi[profile.ID.ToString()];
//            }
//        }
//    }
//}