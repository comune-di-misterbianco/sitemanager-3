﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using NetCms.Users;
//using NetCms.Structure.Grants;

namespace NetCms.Networks.WebFS
{
    public enum WebFSObjectTypes { Folder, Document }

    public abstract class CmsWebFSObject : WebFSObject
    {
        public virtual bool HasParent
        { get { return ParentObject != null; } }
        //public abstract DataTable CriteriaTable { get; }

        public abstract WebFSObjectTypes ObjectType { get; }
        public abstract List<CmsWebFSObject> Childs { get; }

        public abstract NetCms.Connections.Connection Connection { get; }
        
        public abstract CmsWebFSObject ParentObject { get; }

        //public virtual FullCriteria Criteria
        //{
        //    get
        //    {
        //        return this.CriteriaProxy[NetCms.Users.AccountManager.CurrentAccount.Profile];
        //    }
        //}

        //public abstract DataTable GetGrantsTable(string AssociationID);

        ////public virtual CriteriaCollector CriteriaProxy
        ////{
        ////    get
        ////    {
        ////        if (_CriteriaProxy == null)
        ////            _CriteriaProxy = new CriteriaCollector(this);

        ////        return _CriteriaProxy;
        ////    }
        ////}
        ////private CriteriaCollector _CriteriaProxy;

        //public virtual bool Grant
        //{
        //    get
        //    {
        //        return this.Criteria[CriteriaTypes.Show];
        //    }
        //}

        //#region NeedToShow_Loaded -> Specifica se la cartella deve essere visualizzata a causa di un figlio che ha bisogno di essere visualizzato.

        //public virtual bool NeedToShow
        //{
        //    get
        //    {
        //        return this.Criteria.NeedToShow;
        //    }
        //}

        //#endregion

        public CmsWebFSObject() { }

        //I seguenti metodi sembra non siano utilizzato nel cms.
        /*
        public virtual System.Web.UI.WebControls.WebControl BuildNode(Profile profile)
        {
            string classe = this.CriteriaProxy[profile].NeedToShow ? "blue" : "red";

            System.Web.UI.WebControls.WebControl node = new System.Web.UI.WebControls.WebControl(System.Web.UI.HtmlTextWriterTag.Li);
            node.Controls.Add(new System.Web.UI.LiteralControl("<span style=\"color:" + classe + "\">" + this.Nome + " (" + this.ID + ")</span>"));

            System.Web.UI.WebControls.WebControl Lista = new System.Web.UI.WebControls.WebControl(System.Web.UI.HtmlTextWriterTag.Ul);

            if (Childs != null)
                foreach (CmsWebFSObject obj in this.Childs)
                    Lista.Controls.Add(obj.BuildNode(profile));

            if (Lista.Controls.Count > 0)
                node.Controls.Add(Lista);

            return node;
        }

        //public virtual Dictionary<string, User> GetRevisors(int workflowStep, GroupsCollection groups)
        //{
        //    Dictionary<string, User> revisors = new Dictionary<string, User>();

        //    foreach (Group group in groups)
        //        GetRevisorsFromGroup(workflowStep, group, revisors);

        //    return revisors;
        //}

        public virtual Dictionary<string, User> GetRevisors(int workflowStep, ICollection<Group> groups)
        {
            Dictionary<string, User> revisors = new Dictionary<string, User>();

            foreach (Group group in groups)
                GetRevisorsFromGroup(workflowStep, group, revisors);

            return revisors;
        }

        protected virtual void GetRevisorsFromGroup(int workflowStep, Group group, Dictionary<string, User> revisors)
        {
            foreach (User user in group.GetRevisors())
            {
                bool granted = this.CriteriaProxy[user.Profile][CriteriaTypes.Redactor];
                if (granted && !revisors.ContainsKey(user.ID.ToString()))
                {
                    if (workflowStep == 0)
                        revisors.Add(user.ID.ToString(), user);

                    // Se ho trovato almeno 1 revisore e sono alla ricerca del passo corrente di workflow, interrompo il ciclo 
                    //e passo alla chiamata successiva
                    else break;
                }
            }

            // Se sono alla ricerca del passo corrente di workflow
            bool revisorFinded = revisors.Count > 0;
            if ((workflowStep > 0 || !revisorFinded) && group.Parent != null)
            {
                int newWorkflowStep = revisorFinded ? workflowStep - 1 : workflowStep;
                GetRevisorsFromGroup(newWorkflowStep, group.Parent, revisors);
            }
        }

        public virtual bool ImRevisor(int workflowStep, User revisor, User author)
        {
            var Revisors = this.GetRevisors(workflowStep, author.Groups);
            return Revisors.ContainsKey(revisor.ID.ToString());
        }
        */
        //public abstract void DeleteGrantsForAssociation(string AssociationID);
        //public abstract void AddGrant(string AssociationID, string grantValue, string idCriterio, string creatorUserID);
    }
}

