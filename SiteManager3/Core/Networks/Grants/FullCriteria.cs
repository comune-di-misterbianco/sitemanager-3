//using System;
//using System.Data;
//using System.Web;
//using System.Collections;
//using System.Collections.Generic;
//using NetCms.Structure.Grants;

///// <summary>
///// Summary description for StructureGrants
///// </summary>

//namespace NetCms.Networks.WebFS
//{
//    public class FullCriteria
//    {
//        private SpecifyTypes MostValuableSpecify;
        
//        public string ObjectProfileID
//        {
//            get { return _ObjectProfileID; }
//            private set { _ObjectProfileID = value; }
//        }
//        private string _ObjectProfileID = "0";

//        public DataTable ApplicationsTable
//        {
//            get
//            {
//                if (_ApplicationsTable == null)
//                    _ApplicationsTable = this.Object.Connection.SqlQuery(@"SELECT * FROM applicazioni");

//                return _ApplicationsTable;
//            }
//        }
//        private static DataTable _ApplicationsTable;

//        private CmsWebFSObject _Object;
//        public CmsWebFSObject Object
//        {
//            get { return _Object; }
//        }
         
//        public NetCms.Users.Profile Profile
//        {
//            get { return _Profile; }
//            private set { _Profile = value; }
//        }
//        private NetCms.Users.Profile _Profile;

//        private Dictionary<string, int> Values;

//        public FullCriteria(NetCms.Users.Profile profile, CmsWebFSObject obj)
//        {
//            MostValuableSpecify = Configurations.Generics.MostValuableGrant ? SpecifyTypes.Allow : SpecifyTypes.Deny;
//            _Object = obj;
//            this.Profile = profile;

//            Values = new Dictionary<string, int>();
//            DataRow[] rows = Object.CriteriaTable.Select("Object_ObjectProfile = " + Object.ID + " AND Profile_ObjectProfile = " + Profile);
//            foreach (DataRow row in rows)
//            {
//                if (ObjectProfileID == null) ObjectProfileID = row["id_ObjectProfile"].ToString();
//                Values.Add(
//                                (row["Key_Criterio"].ToString()),
//                                int.Parse(row["Value_ObjectProfileCriteria"].ToString())
//                           );

//            }
//        }

//        public static string CriteriaSwitch(CriteriaTypes type)
//        {
//            switch (type)
//            {
//                case CriteriaTypes.Advanced:
//                    return "advanced";
//                case CriteriaTypes.Create:
//                    return "create";
//                case CriteriaTypes.Delete:
//                    return "delete";
//                case CriteriaTypes.Grants:
//                    return "grants";
//                case CriteriaTypes.NewFolder:
//                    return "newfolder";
//                case CriteriaTypes.Redactor:
//                    return "redactor";
//                case CriteriaTypes.Publisher:
//                    return "publisher";
//                case CriteriaTypes.Bandi:
//                    return "bandi";
//                case CriteriaTypes.Show:
//                    return "show";
//                case CriteriaTypes.Modify:
//                    return "modify";
//                case CriteriaTypes.FoldersVisibility:
//                    return "foldersvisibility";
//                case CriteriaTypes.Frontend:
//                    return "frontend";
//            }

//            return null;
//        }

//        public static CriteriaTypes CriteriaSwitch(string type)
//        {
//            switch (type)
//            {
//                case "advanced":
//                    return CriteriaTypes.Advanced;
//                case "create":
//                    return CriteriaTypes.Create;
//                case "delete":
//                    return CriteriaTypes.Delete;
//                case "grants":
//                    return CriteriaTypes.Grants;
//                case "newfolder":
//                    return CriteriaTypes.NewFolder;
//                case "redactor":
//                    return CriteriaTypes.Redactor;
//                case "publisher":
//                    return CriteriaTypes.Publisher;
//                case "bandi":
//                    return CriteriaTypes.Bandi;
//                case "show":
//                    return CriteriaTypes.Show;
//                case "modify":
//                    return CriteriaTypes.Modify;
//                case "foldersvisibility":
//                    return CriteriaTypes.FoldersVisibility;
//                case "frontend":
//                    return CriteriaTypes.Frontend;
//            }

//            throw new Exception("Criterio non esistente");
//        }

//        public bool this[string type]
//        {
//            get
//            {
//                return SearchPermissionByInheritage(type).Allow;
//            }
//        }

//        public bool this[CriteriaTypes type]
//        {
//            get
//            {
//                return SearchPermissionByInheritage(CriteriaSwitch(type)).Allow;
//            }
//        }

//        public bool Application(CriteriaTypes type, string applicationID)
//        {
//            _ApplicationsTable = null;
//            string appKey = ApplicationsTable.Select("id_Applicazione = " + applicationID)[0]["SystemName_Applicazione"].ToString();
//            string criterio = CriteriaSwitch(type) + "_" + appKey;
//            return this[criterio];
//        }
//        /*public bool Application(CriteriaTypes type, int index)
//        {
//            return true;
//        }*/
//        public Specify GetRawPermission(string type)
//        {
//            Specify currentValue = new Specify(this.Values.ContainsKey(type) ? this.Values[type] : 0, Profile, this.Object);
//            return currentValue;
//        }

//        public Specify SearchPermissionByInheritage(string type, bool SkipDownToTopSearch = false)
//        {
//            Specify value = this.SearchPermissionValueByFolderInheritage(type,SkipDownToTopSearch);
//            return value;
//        }
//        public Specify SearchPermissionValueByFolderInheritage(string type, bool SkipDownToTopSearch = false)
//        {
//            Specify value = this.SearchPermissionValueByGroupInheritage(type);

//            CmsWebFSObject currentObject = this.Object;
//            while (!value.Found && currentObject.HasParent && !SkipDownToTopSearch)
//            {
//                currentObject = currentObject.ParentObject;
//                value = currentObject.CriteriaProxy[this.Profile].SearchPermissionValueByFolderInheritage(type);
//            }

//            return value;
//        }
//        public Specify SearchPermissionValueByGroupInheritage(string type)
//        {
//            Specify currentValue = new Specify(this.Values.ContainsKey(type) ? this.Values[type] : 0, Profile, this.Object);

//            if (currentValue.Inherits && this.Profile.HasParent)
//            {
//                foreach (NetCms.Users.Group group in this.Profile.ParentsGroups)
//                {
//                    Specify tmpValue = this.Object.CriteriaProxy[group.Profile].SearchPermissionValueByGroupInheritage(type);
//                    //Se il valore trovato NON E' EREDITA controllo le condizioni per il quale devo sovrasvrivere il valore corrente
//                    if (tmpValue.Found)
//                    {
//                        //Se il vecchio valore � EREDITA, sovrascrivo il vecchio valore col nuovo
//                        if (currentValue.Inherits)
//                            currentValue = tmpValue;
//                        else
//                            //Se il nuovo valore ha un peso maggiore di quello precendete
//                            if (currentValue.Depth < tmpValue.Depth)
//                                currentValue = tmpValue;
//                            else
//                                //Se il nuovo valore ha un peso maggiore uguale a quello precendete,
//                                //ma la specifica � concorde con la specifica di maggior importanza impostata da setup di sistema
//                                if (currentValue.Depth == tmpValue.Depth && tmpValue.Value == MostValuableSpecify)
//                                    currentValue = tmpValue;
//                    }
//                }
//            }

//            return currentValue;
//        }


//        public bool SearchPermissionTopDown(string type)
//        {
//            int DatabaseValue = this.Values.ContainsKey(type) ? this.Values[type] : 0;
//            if (DatabaseValue > 0)
//                return DatabaseValue == 1;
//            else
//            {
//                if (Object.Childs != null && Object.Childs.Count > 0)
//                    foreach (CmsWebFSObject obj in this.Object.Childs)
//                        if (obj.CriteriaProxy[Profile].SearchPermissionAllDirections(type,true)) return true;
//            }

//            return false;
//        }

//        public bool SearchPermissionAllDirections(string type, bool SkipDownToTopSearch = false)
//        {
//            return SearchPermissionByInheritage(type,SkipDownToTopSearch).Allow || SearchPermissionTopDown(type);
//        }

//        #region NeedToShow_Loaded -> Specifica se la cartella deve essere visualizzata a causa di un figlio che ha bisogno di essere visualizzato.

//        private bool NeedToShow_Loaded = false;//variabile che indica se la propriet� show � stata gi� letta.
//        private bool NeedToShow_Backstore;//variabile che contiene il valore restituito dalla propriet� Show
//        public bool NeedToShow
//        {
//            get
//            {
//                if (!NeedToShow_Loaded)
//                {
//                    NeedToShow_Backstore = this.SearchPermissionAllDirections("show");
//                    NeedToShow_Loaded = true;
//                }

//                return NeedToShow_Backstore;
//            }
//        }

//        #endregion
//    }
 
//}
//namespace NetCms.Structure.Grants
//{
//    public enum CriteriaTypes
//    {
//        Show,
//        Create,
//        Modify,
//        Delete,
//        Grants,
//        NewFolder,
//        Redactor,
//        Publisher,
//        Bandi,
//        Advanced,
//        FoldersVisibility,
//        Frontend
//    }
//}