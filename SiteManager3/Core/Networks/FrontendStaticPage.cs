﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web;

namespace NetCms.Front.Static
{
    [System.AttributeUsage(System.AttributeTargets.Class, AllowMultiple = true)]
    public class FrontendStaticPage : System.Attribute
    {
        /// <summary>
        /// Indirizzo di reperimento della pagina
        /// Es: /default.aspx , /users/default.aspx , /users/edit/edit.aspx
        /// </summary>
        /// <param name="address">Stringa contenente uno o più path al quale la pagina è reperibile, separati da virgola (,)</param>
        /// <param name="pageTitle">Stringa contenente il Titolo della Pagina</param>
        /// <param name="whereParts">Una stringa formattata con questo schema: [Key,Value][Key,Value][Key,Value][Key,Value][Key,Value][Key,Value]. Contenente coppie Label-Href, utilizzato per impostare delle Bread Crumbs personalizzate.</param>
        /// <param name="meta">Una stringa formattata con questo schema: [Key,Value][Key,Value][Key,Value][Key,Value][Key,Value][Key,Value]. contenente coppie NomeMeta-ValoreMeta, utilizzato per impostare dei meta personalizzati per la pagina.</param>
        public FrontendStaticPage(string[] addresses, string[] pageTitles, FrontLayouts pageLayout, string whereParts, string meta)
        {
            this.Addresses = addresses.Select(s => s.ToLower()).ToArray();
            this.PageTitles = pageTitles;
            this.PageLayout = pageLayout;
            AddBreadcrumbs = true;
            if (!string.IsNullOrEmpty(meta))
            {
                var split1 = meta.Split(new[] { "][" }, StringSplitOptions.RemoveEmptyEntries);
                Meta = split1.ToDictionary(v => v.Split(',')[0], v => v.Split(',')[1]);
            }
            if (!string.IsNullOrEmpty(whereParts))
            {
                var split1 = whereParts.Split(new[] { "][" }, StringSplitOptions.RemoveEmptyEntries);
                WhereParts = split1.ToDictionary(v => v.Split(',')[0], v => v.Split(',')[1]);
            }
        }

        /// <summary>
        /// Indirizzo di reperimento della pagina
        /// Es: /default.aspx , /users/default.aspx , /users/edit/edit.aspx
        /// </summary>
        /// <param name="address">Stringa contenente uno o più path al quale la pagina è reperibile, separati da virgola (,)</param>
        /// <param name="pageTitle">Stringa contenente il Titolo della Pagina</param>
        /// <param name="whereParts">Una stringa formattata con questo schema: [Key,Value][Key,Value][Key,Value][Key,Value][Key,Value][Key,Value]. Contenente coppie Label-Href, utilizzato per impostare delle Bread Crumbs personalizzate.(opzionale)</param>
        public FrontendStaticPage(string[] addresses, string[] pageTitles, FrontLayouts pageLayout, string whereParts)
            : this(addresses, pageTitles, pageLayout, whereParts, "")
        {
        }

        /// <summary>
        /// Indirizzo di reperimento della pagina
        /// Es: /default.aspx , /users/default.aspx , /users/edit/edit.aspx
        /// </summary>
        /// <param name="address">Stringa contenente uno o più path al quale la pagina è reperibile, separati da virgola (,)</param>
        /// <param name="pageTitle">Stringa contenente il Titolo della Pagina</param>
        public FrontendStaticPage(string[] addresses, string[] pageTitles, FrontLayouts pageLayout)
            : this(addresses, pageTitles, pageLayout, "", "")
        {
        }

        /// <summary>
        /// Indirizzo di reperimento della pagina
        /// Es: /default.aspx , /users/default.aspx , /users/edit/edit.aspx
        /// </summary>
        /// <param name="addresses">Stringa contenente l'indirizzo</param>
        /// <param name="pageTitle">Stringa contenente il Titolo della Pagina</param>
        public FrontendStaticPage(string[] addresses, string[] pageTitles)
            : this(addresses, pageTitles, FrontLayouts.All, "", "")
        {
        }

        /// <summary>
        /// Indirizzo di reperimento della pagina
        /// Es: /default.aspx , /users/default.aspx , /users/edit/edit.aspx
        /// </summary>
        /// <param name="addresses">Stringa contenente uno o più path al quale la pagina è reperibile, separati da virgola (,)</param>
        /// <param name="pageTitle">Stringa contenente il Titolo della Pagina</param>
        /// <param name="whereParts">Una stringa formattata con questo schema: [Key,Value][Key,Value][Key,Value][Key,Value][Key,Value][Key,Value]. Contenente coppie Label-Href, utilizzato per impostare delle Bread Crumbs personalizzate.</param>
        /// <param name="meta">Una stringa formattata con questo schema: [Key,Value][Key,Value][Key,Value][Key,Value][Key,Value][Key,Value]. contenente coppie NomeMeta-ValoreMeta, utilizzato per impostare dei meta personalizzati per la pagina.</param>
        /// <param name="label">Label utilizzata per selezionare il titolo corretto nella lingua selezionata. Se impostata viene considerata, altrimenti si utilizza pageTitle</param>
        public FrontendStaticPage(string[] addresses, string[] pageTitles, FrontLayouts pageLayout, string whereParts, string meta, StaticPagesLabelsList label)
            :this(addresses, pageTitles, pageLayout, whereParts, meta)
        {
            this.PageTitleLabel = label;
        }


        /// <summary>
        /// L'array Addresses rappresenta un array di indirizzi della pag statica
        /// ogni elemento corrisponde ad una lingua differente
        /// la sequenza segue l'ordine di Id del db (0 ITA, 1 EN, etc)
        /// </summary>
        public string[] Addresses { get; private set; }

        /// <summary>
        /// L'array PageTitles rappresenta un array di titoli di pagina
        /// ogni elemento corrisponde ad una lingua differente
        /// la sequenza segue l'ordine di Id del db (0 ITA, 1 EN, etc)
        /// Se il multilang è abilitato verrà prelevato da questo array il page title
        /// corrispondente a alla lingua selezionata
        /// Altrimenti di default verrà utilizzato il primo elemento.
        /// </summary>
        public string[] PageTitles { get; private set; }

        public bool AddBreadcrumbs { get; set; }
        public bool ShowInFullScreen { get; set; }
        public bool OnlyLoggedUsers { get; set; }
        public FrontLayouts PageLayout { get; private set; }
        /// <summary>
        /// Parametri da passare al costruttore al momentoi di istanziare la Classe associata all'attributo
        /// </summary>
        public object[] CostructorParameters { get; set; }
        public Dictionary<string, string> Meta { get; private set; }
        public Dictionary<string, string> WhereParts { get; private set; }

        public static void AddCssIfNotExists(string cssFullHref, HtmlHead PageHeader)
        {
            try
            {
                if (HttpContext.Current != null && HttpContext.Current.Server != null)
                {
                    string path = HttpContext.Current.Server.MapPath(cssFullHref);
                    if (System.IO.File.Exists(path))
                    {
                        bool add = true;
                        try
                        {
                            var htmlLinks = PageHeader.Controls.Cast<object>().Where(x => x is HtmlLink).Cast<HtmlLink>();
                            if (add && htmlLinks.Count(x => x.Attributes["href"] != null && x.Attributes["href"].ToLower().Contains(cssFullHref.ToLower())) > 0)
                                add = false;
                        }
                        catch
                        {
                            add = true;
                        }

                        try
                        {
                            var literalControls = PageHeader.Controls.Cast<object>().Where(x => x is LiteralControl).Cast<LiteralControl>();
                            if (add && literalControls.Count(x => x.Text != null && x.Text.ToLower().Contains(cssFullHref.ToLower())) > 0)
                                add = false;
                        }
                        catch
                        {
                            add = true;
                        }

                        if (add)
                        {
                            string cssLink = @"<link rel=""stylesheet"" type=""text/css"" href=""" + cssFullHref + @""" />";
                            PageHeader.Controls.Add(new System.Web.UI.LiteralControl(cssLink));
                        }
                    }
                }
            }
            catch { }
        }


        public StaticPagesLabelsList PageTitleLabel { get; private set; }
        public enum StaticPagesLabelsList
        {        
            None,
            ImpostazioniAccount,
            ProfiloLabelLink,
            EditProfiloLabelLink,
            EditPasswordLabelLink,
            ChiarimentiLabelLink,
            NotificheLabelLink            
        }
    }
}

