﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Networks.Mapping
{
    public class BasicNetworkMapping : ClassMap<BasicNetwork>
    {
        public BasicNetworkMapping()
        {
            Table("networks");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_Network");
            Map(x => x.SystemName).Column("Nome_Network");
            Map(x => x.Predefinita).Column("Default_Network");
            Map(x => x.RootPath).Column("Webroot_Network");
            Map(x => x.HasFrontend).Column("Frontend_Network");
            Map(x => x.DefaultLang).Column("DefaultLang_Network");
            Map(x => x.Enabled).Column("Enabled_Network");
            Map(x => x.Label).Column("Label_Network");
            Map(x => x.StatoCMS).Column("Cms_Network");

            HasMany(x => x.NetworkFolders)
                .KeyColumn("Network_Folder")
                .Fetch.Select().AsSet()
                .Cascade.All().Inverse();
        }
    }
}
