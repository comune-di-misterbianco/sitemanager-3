﻿using System;
using System.Web;
using System.Collections.Generic;
using NetCms.Connections;
using G2Core.Caching.Visibility;

namespace NetCms
{
    namespace Networks
    {
        /// <summary>
        /// La classe network è adesso diventata un decoratore
        /// </summary>
        public abstract class Network : INetwork //: BasicNetwork
        {
            protected INetwork decoratedNetwork;

            //DA ELIMINARE
            private NetCms.Connections.Connection _Conn;
            public NetCms.Connections.Connection Connection
            {
                get
                {
                    //if (_Connection == null)
                    //    _Connection = NetCms.Connections.ConnectionsManager.GetNetworkConnection(this.SystemName);
                    //return _Connection;

                    ConnectionsManager ConnManager = new ConnectionsManager();
                    return ConnManager.CommonConnection;
                }
            }

            //public G2Core.Caching.Cache Cache
            //{
            //    get
            //    {
            //        if (_Cache == null)
            //            _Cache = new G2Core.Caching.Cache();
            //        return _Cache;
            //    }
            //}
            //private G2Core.Caching.Cache _Cache;

            #region Root Folder

            //public CacheableItem RootFolderCacheableItem
            //{
            //    get
            //    {
            //        if (_RootFolderCacheableItem == null)
            //        {
            //            Networks.WebFS.NetworkFolder rootFolder = BuildRootFolder();
            //            _RootFolderCacheableItem = rootFolder.OwnerCollection.GetCacheableItem(rootFolder.ID);
            //        }
            //        return _RootFolderCacheableItem;
            //    }
            //}
            //private CacheableItem _RootFolderCacheableItem;

            public int RootFolderID
            {
                get
                {
                    if (_RootFolderID == 0)
                    {
                        _RootFolderID = RootFolder.ID;
                    }
                    return _RootFolderID;
                }
            }
            private int _RootFolderID;

            public Networks.WebFS.NetworkFolder RootFolder
            {
                get
                {
                    return BuildRootFolder();//(Networks.WebFS.NetworkFolder)RootFolderCacheableItem.CachedObject;
                }
            }

            public abstract Networks.WebFS.NetworkFolder BuildRootFolder();

            #endregion

            public virtual Networks.WebFS.NetworkFolder CurrentFolder
            {
                get
                {
                    if (Networks.NetworksManager.NetworkStatus == NetworksStates.OutsideNetworks)
                    {
                        Exception ex = new Exception("Impossibile ottenere la cartella cartella corrente quando non si è all'interno di una Network");
                        NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                        ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);
                    }
                    if (this.Face == Faces.Admin && HttpContext.Current.Items["Networks.WebFS.NetworkFolder.CurrentFolder"] == null)
                    {
                        Networks.WebFS.NetworkFolder folder = null;
                        
                        G2Core.Common.RequestVariable folderRequest = new G2Core.Common.RequestVariable("folder", G2Core.Common.RequestVariable.RequestType.QueryString);
                        
                        if (folderRequest.IntValue > 0)
                            folder = this.RootFolder.FindFolderByID(folderRequest.IntValue); //cerco la cartella richiesta nell'albero, in caso che non venisse trovata il metodo restituisce null
                        
                        if (folder == null)
                            folder = this.RootFolder;//nel caso in cui non viene trovata la cartella richiesta restituisco la cartella radice.

                        HttpContext.Current.Items["Networks.WebFS.NetworkFolder.CurrentFolder"] = folder;
                    }

                    if (this.Face == Faces.Front && HttpContext.Current.Items["Networks.WebFS.NetworkFolder.CurrentFolder"] == null)
                    {
                        Networks.WebFS.NetworkFolder folder = null;

                        folder = this.RootFolder.FindFolderByPath(CurrentFolderPath); //cerco la cartella richiesta nell'albero, in caso che non venisse trovata il metodo restituisce null
                        if (folder == null)
                        {
                            //VERIFICARE CHE SIA EQUIVALENTE
                            folder = this.RootFolder.GetFolderByNetworkAndParentsPath(CurrentFolderPath.Replace("'", "''"), NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID, ParentsFolderSQL(CurrentFolderPath));

                            //string sql = ParentsFolderSQL(CurrentFolderPath);

                            //DataTable table = this.Connection.SqlQuery(sql);
                            //if (table.Rows.Count == 1)
                                //folder = this.RootFolder.FindFolderByPath(table.Rows[0]["Path_Folder"].ToString()); //cerco la cartella richiesta nell'albero, in caso che non venisse trovata il metodo restituisce null
                        }
                        HttpContext.Current.Items["Networks.WebFS.NetworkFolder.CurrentFolder"] = folder;
                    }

                    return (Networks.WebFS.NetworkFolder)HttpContext.Current.Items["Networks.WebFS.NetworkFolder.CurrentFolder"];
                }
            }
            
            //protected string ParentsFolderSQL(string folderpath)
            //{
            //    string folders = "SELECT * Path_Folder FROM (Folders INNER JOIN network_folders ON id_Folder = id_NetworkFolder) WHERE"
            //        + " Network_Folder = " + NetCms.Networks.NetworksManager.CurrentActiveNetwork.ID 
            //        + " AND HomeType_Folder>0 AND System_Folder = 5 AND(";

            //    string[] array = folderpath.Split('/');
            //    string pathtot = "";
            //    bool first = true;
            //    for (int i = 0; i < array.Length; i++)
            //    {
            //        if (array[i].Length > 0)
            //        {
            //            if (!first)
            //                folders += " OR ";
            //            else
            //                first = false;

            //            pathtot += "/" + array[i].Replace("'", "''");

            //            folders += " Path_Folder = '" + pathtot + "'";
            //        }
            //    }

            //    folders += ")";
            //    return folders;
            //}

            protected List<string> ParentsFolderSQL(string folderpath)
            {
                List<string> paths = new List<string>();

                string[] array = folderpath.Split('/');
                string pathtot = "";

                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i].Length > 0)
                    {

                        pathtot += "/" + array[i].Replace("'", "''");

                        paths.Add(pathtot);
                    }
                }

                return paths;
            }

            public const string FrontCurrentFolderPathItemKey = "FrontCurrentFolderPathKey";

            public string CurrentFolderPath
            {
                get
                {
                    string _CurrentFolderPath;
                    if (HttpContext.Current.Items.Contains(FrontCurrentFolderPathItemKey))
                        _CurrentFolderPath = (string)HttpContext.Current.Items[FrontCurrentFolderPathItemKey];
                    else
                        HttpContext.Current.Items[FrontCurrentFolderPathItemKey] = _CurrentFolderPath = BuildCurrentFolderPath();

                    return _CurrentFolderPath;
                }
            }

            private string BuildCurrentFolderPath()
            {
                string currentFolderPath ="";
                G2Core.Common.QueryStringRequestVariable path = new G2Core.Common.QueryStringRequestVariable("path");
                if (path.IsValidString) currentFolderPath = path.StringValue;
                else
                {
                    string sPath = System.Web.HttpContext.Current.Request.Url.LocalPath;
                    System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
                    string sRet = oInfo.Name;
                    string pagename = sRet.ToLower();

                    currentFolderPath = HttpContext.Current.Request.Url.LocalPath.ToLower().Replace(pagename.ToLower(), "");

                    if (this.Paths.AbsoluteFrontRoot.Length > 0)
                        currentFolderPath = currentFolderPath.Replace(this.Paths.AbsoluteFrontRoot.ToLower(), "");
                }
                while (currentFolderPath.EndsWith("/"))
                    currentFolderPath = currentFolderPath.Substring(0, currentFolderPath.Length - 1);

                bool found = false;
                do
                {
                    //VERIFICARE CHE FUNZIONI
                    //DataTable folders = Connection.SqlQuery("SELECT * FROM (Folders INNER JOIN network_folders ON id_Folder = id_NetworkFolder) WHERE Path_Folder = '" + currentFolderPath + "'");
                    found = NetworksBusinessLogic.CheckExistNetworkFolderByPath(currentFolderPath);
                    //if (folders.Rows.Count == 1)
                    //{
                    //    found = true;
                    //}
                    if (!found)
                    {
                        currentFolderPath = currentFolderPath.Remove(currentFolderPath.LastIndexOf('/'));
                    }
                } while (!found && currentFolderPath.Length > 1);

                return currentFolderPath;
            }

            private string _eMail;
            public string eMail
            {
                get
                {
                    if (_eMail == null)
                        _eMail = Configurations.XmlConfig.GetAttribute("/Portal/configs/Networks/" + this.SystemName + "/frontend", "email");

                    return _eMail;
                }
            }

            private string _Website;
            public string Website
            {
                get
                {
                    if (_Website == null)
                    {
                        _Website = "http://" + HttpContext.Current.Request.Url.Authority;
                        //_Website = Configurations.XmlConfig.GetAttribute("/Portal/configs/Networks/" + this.SystemName + "/frontend", "website");
                    }
                    return _Website;
                }
            }

            private string _DisclaimerBandi;
            public string DisclaimerBandi
            {
                get
                {
                    if (_DisclaimerBandi == null)
                    {
                        _DisclaimerBandi = Configurations.XmlConfig.GetNodeXml("/Portal/configs/Networks/" + this.SystemName + "/frontend/externalapplications/DisclaimerBandi");
                    }

                    return _DisclaimerBandi;
                }
            }

            private string _DisclaimerRegistrazione;
            public string DisclaimerRegistrazione
            {
                get
                {
                    if (_DisclaimerRegistrazione == null)
                        _DisclaimerRegistrazione = Configurations.XmlConfig.GetNodeXml("/Portal/configs/Networks/" + this.SystemName + "/frontend/externalapplications/DisclaimerRegistrazione");

                    return _DisclaimerRegistrazione;
                }
            }

            public int FolderNameMaxLenght
            {
                get
                {
                    if (_FolderNameMaxLenght == null)
                        _FolderNameMaxLenght = NetCms.Configurations.XmlConfig.GetIntegerAttribute("/Portal/Networks/" + SystemName, "foldernamemaxlenght");

                    return _FolderNameMaxLenght.Value;
                }
            }
            private int? _FolderNameMaxLenght;


            public string _ThemeFolder = string.Empty;
            public string ThemeFolder
            {
                get { return _ThemeFolder; }
                set { _ThemeFolder = value; }            
            }


            public string CssFolder
            {
                get
                {
                    // return this.SystemName + (!string.IsNullOrEmpty(ThemeFolder)? "/" + ThemeFolder : "");
                    if (!string.IsNullOrEmpty(ThemeFolder))
                        return "/" + NetCms.Configurations.ThemeEngine.ThemesFolder + "/" + ThemeFolder + "/css";
                    return "/css/" + this.SystemName;
                }
            }

            public string Path
            {
                get
                {
                    return RootPath;
                }
            }

            private NetworkKey _CacheKey;
            public virtual NetworkKey CacheKey
            {
                get
                {
                    if (_CacheKey == null)
                        _CacheKey = new NetworkKey(this.ID,Face == Faces.Admin? CachingVisibility.User :  CachingVisibility.Global);

                    return _CacheKey;
                }
            }

            public Network(INetwork baseNetwork)
            {
                decoratedNetwork = baseNetwork;
            }

            public Network(int id)//:base(id)
            {
                this.decoratedNetwork = Networks.NetworksManager.GlobalIndexer.GetNetworkByID(id); //NetworksBusinessLogic.GetById(id);
                //AddToCache();
            }
            public Network(string name)//:base(name)
            {
                this.decoratedNetwork = Networks.NetworksManager.GlobalIndexer.GetNetworkBySystemName(name);//NetworksBusinessLogic.GetByName(name);
                //AddToCache();
            }
            public Network(NetworkKey networkKey)//:base(int.Parse(networkKey.NetworkID))
            {
                this.decoratedNetwork = Networks.NetworksManager.GlobalIndexer.GetNetworkByID(networkKey.NetworkID);
                _CacheKey = networkKey;
                //AddToCache();
            }

            //private void AddToCache()
            //{
            //    Cache.Insert(CacheKey.ToString(),//La chiave univoca di accesso all'oggetto in cache
            //         this,//Il valore dell'oggetto in cache
            //         NetworkTimings.NetworkExpireTiming//L'oggetto viene rimosso dalla cache NetworkExpireTiming minuti dopo l'ultimo utilizzo
            //         );
            //}
            //public void RemovedCallback(String k, Object v, CacheItemRemovedReason r)
            //{
            //    NetworksManager.RemoveNetworkKey(this);
            //}

            #region Implementazione Interfaccia INetwork
            
            public int ID
            {
                get { return decoratedNetwork.ID; }
            }

            public string SystemName
            {
                get { return decoratedNetwork.SystemName; }
            }

            public string Label
            {
                get { return decoratedNetwork.Label; }
            }

            public bool HasFrontend
            {
                get { return decoratedNetwork.HasFrontend; }
            }

            public bool Enabled
            {
                get { return decoratedNetwork.Enabled; }
            }

            public string RootPath
            {
                get { return decoratedNetwork.RootPath; }
            }

            public int DefaultLang
            {
                get { return decoratedNetwork.DefaultLang; }
            }

            public bool Default
            {
                get { return decoratedNetwork.Default; }
            }

            public NetworkCmsStates CmsStatus
            {
                get { return decoratedNetwork.CmsStatus; }
            }

            public NetworkPaths Paths
            {
                get { return decoratedNetwork.Paths; }
            }

            public virtual Faces Face
            {
                get { return decoratedNetwork.Face; }
            }

            public void EnableNetwork()
            {
                decoratedNetwork.EnableNetwork();
            }

            public void DisableNetwork()
            {
                decoratedNetwork.DisableNetwork();
            }

            public void DisableCms()
            {
                decoratedNetwork.DisableCms();
            }

            public void EnableCms()
            {
                decoratedNetwork.EnableCms();
            }

            #endregion
        }
    }
}