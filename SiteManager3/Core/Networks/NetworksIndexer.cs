﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace NetCms.Networks
{
    public class NetworksIndexer
    {
        /// <summary>
        /// Un dizionario contenente coppie del tipo Network.SystemName / BasicNetwork
        /// </summary>
        public Dictionary<string, BasicNetwork> AllNetworks
        {
            get
            {
                if (_AllNetworks == null)
                    _AllNetworks = new Dictionary<string, BasicNetwork>();
                return _AllNetworks;
            }
        }
        private Dictionary<string,BasicNetwork> _AllNetworks;

        //public G2Core.Caching.Visibility.CachingVisibility Visibility
        //{
        //    get
        //    {
        //        if (_Visibility == null)
        //            _Visibility = new G2Core.Caching.Visibility.CachingVisibility();
        //        return _Visibility;
        //    }
        //}
        //private G2Core.Caching.Visibility.CachingVisibility _Visibility;

        /// <summary>
        /// Un dizionario contenente coppie del tipo Network.SystemName / BasicNetwork
        /// </summary>
        public Dictionary<string, BasicNetwork> WithoutCms
        {
            get
            {
                if (_WithoutCms == null)
                    _WithoutCms = new Dictionary<string, BasicNetwork>();
                return _WithoutCms;
            }
        }
        private Dictionary<string, BasicNetwork> _WithoutCms;

        /// <summary>
        /// Un dizionario contenente coppie del tipo Network.SystemName / BasicNetwork
        /// </summary>
        public Dictionary<string, BasicNetwork> CmsEnabled
        {
            get
            {
                if (_CmsEnabled == null)
                    _CmsEnabled = new Dictionary<string, BasicNetwork>();
                return _CmsEnabled;
            }
        }
        private Dictionary<string, BasicNetwork> _CmsEnabled;

        /// <summary>
        /// Un dizionario contenente coppie del tipo Network.SystemName / BasicNetwork
        /// </summary>
        public Dictionary<string, BasicNetwork> WithCms
        {
            get
            {
                if (_WithCms == null)
                    _WithCms = new Dictionary<string, BasicNetwork>();
                return _WithCms;
            }
        }
        private Dictionary<string, BasicNetwork> _WithCms;

        /// <summary>
        /// Un dizionario contenente coppie del tipo Network.ID / Network.SystemName
        /// </summary>
        public Dictionary<int, string> NetworkKeys
        {
            get
            {
                if (_NetworkKeys == null)
                    _NetworkKeys = new Dictionary<int, string>();
                return _NetworkKeys;
            }
        }
        private Dictionary<int, string> _NetworkKeys;

        public NetworksIndexer()
        {
            //_Visibility = G2Core.Caching.Visibility.CachingVisibility.Global;
            Update();
        }

        //public NetworksIndexer(G2Core.Caching.Visibility.CachingVisibility visibility)
        //{
        //    _Visibility = visibility;
        //    Update();
        //}

        public BasicNetwork GetNetworkByID(int id)
        {
            return AllNetworks[NetworkKeys[id]];
        }

        public BasicNetwork GetNetworkBySystemName(string systemName)
        {
            return AllNetworks[systemName];
        }

        public void Update()
        {
            IList<BasicNetwork> netList = new NetworksData(false);
            WithoutCms.Clear();
            AllNetworks.Clear();
            WithCms.Clear();
            CmsEnabled.Clear();
            NetworkKeys.Clear();

            this._AllNetworks = netList.ToDictionary(x => x.SystemName);

            this._CmsEnabled = netList.Where(x => x.CmsStatus == NetworkCmsStates.Enabled).ToDictionary(x => x.SystemName);
            
            this._WithCms = netList.Where(x => x.CmsStatus == NetworkCmsStates.Enabled).ToDictionary(x => x.SystemName);
            var concat = this._WithCms.Concat(netList.Where(x => x.CmsStatus == NetworkCmsStates.Disabled).ToDictionary(x => x.SystemName));
            this._WithCms = concat.ToDictionary(x => x.Value.SystemName, x=>x.Value);

            this._WithoutCms = netList.Where(x => x.CmsStatus == NetworkCmsStates.NotCreated).ToDictionary(x => x.SystemName);

            foreach(BasicNetwork net in netList)
                this.NetworkKeys.Add(net.ID, net.SystemName);

            //foreach (DataRow row in table.Rows)
            //{
            //    BasicNetwork bn = new BasicNetwork(row);
            //    this.AllNetworks.Add(bn.SystemName, bn);
            //    this.NetworkKeys.Add(bn.ID, bn.SystemName);

            //    if (bn.CmsStatus == NetworkCmsStates.Enabled)
            //    {
            //        this.CmsEnabled.Add(bn.SystemName, bn);
            //        this.WithCms.Add(bn.SystemName, bn);
            //    }

            //    if (bn.CmsStatus == NetworkCmsStates.NotCreated)
            //        this.WithoutCms.Add(bn.SystemName, bn);

            //    if (bn.CmsStatus == NetworkCmsStates.Disabled)
            //        this.WithCms.Add(bn.SystemName, bn);
            //}
        }
    }
}
