﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Cfg;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using System.Reflection;
using System.IO;

namespace NetCms.DBSessionProvider
{
    public class FluentSessionHelper
    {
        private static ISessionFactory sessionFactory;
        private static ISession session;

        public static ISessionFactory GetSessionFactory(string database_name, string server, string username, string password, List<Assembly> assembliesToWorkWith)
        {
            sessionFactory = Fluently.Configure()
                    .Database(MySQLConfiguration.Standard.ConnectionString(cs => cs.Server(server).Database(database_name).Username(username).Password(password)))
                    .Mappings(x =>
                    {
                        x.FluentMappings.Conventions.Setup(m => m.Add(AutoImport.Never()));

                        foreach (Assembly assembly in assembliesToWorkWith)
                            x.FluentMappings.AddFromAssembly(assembly);
                    })

                    .ExposeConfiguration(x => x.SetProperty("current_session_context_class", "thread_static"))
                    .ExposeConfiguration(x => x.SetProperty("hbm2ddl.keywords", "none"))
                    .ExposeConfiguration(AddAnagraficaConfig)
                    .BuildSessionFactory();
         
            return sessionFactory;
        }
       
        public static ISessionFactory GetSessionFactory(string database_name, string server, string tcpport, string username, string password, List<Assembly> assembliesToWorkWith)
        {
            sessionFactory = Fluently.Configure()
                    .Database(MySQLConfiguration.Standard.ConnectionString(cs => cs.Is("Server=" + server + "; Port=" + tcpport + ";Database=" + database_name + "; Uid=" + username + "; Pwd=" + password + ";")))
                    .Mappings(x =>
                    {
                        x.FluentMappings.Conventions.Setup(m => m.Add(AutoImport.Never()));

                        foreach (Assembly assembly in assembliesToWorkWith)
                            x.FluentMappings.AddFromAssembly(assembly);
                    })

                    .ExposeConfiguration(x => x.SetProperty("current_session_context_class", "thread_static"))
                    .ExposeConfiguration(x => x.SetProperty("hbm2ddl.keywords", "none"))
                    .ExposeConfiguration(AddAnagraficaConfig)
                    .BuildSessionFactory();

            return sessionFactory;
        }

        private static void AddAnagraficaConfig(Configuration config)
        {            
            FileInfo anagrafiche = new FileInfo(NetCms.Configurations.Paths.PhysicalRoot + "\\App_Data\\AnagraficaBase.hbm.xml");
            if (anagrafiche.Exists)
            {
                config.AddFile(anagrafiche);
            }
        }

        public static ISession GetSession()
        {
            if (sessionFactory == null) return null;

            session = sessionFactory.OpenSession();
            return session;
        }

        public static void CloseSessionFactory()
        {
            if (sessionFactory != null)
            {
                sessionFactory.Close();
            }
        }
    }
}
