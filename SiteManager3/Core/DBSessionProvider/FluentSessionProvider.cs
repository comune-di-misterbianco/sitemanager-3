﻿using NHibernate;
using FluentNHibernate.Cfg;
using System.Web;
using NHibernate.Context;
using FluentNHibernate.Cfg.Db;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Cfg;
using System.Reflection;
using System.Collections.Generic;
using FluentNHibernate.Conventions.Helpers;
using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using NHibernate.Dialect;

namespace NetCms.DBSessionProvider
{
    public class FluentSessionProvider
    {
        private const string FluentAssembliesKey = "NetCms.DBSessionProvider.FluentAssemblies";

        private FluentSessionProvider()
        {

        }

        private static FluentSessionProvider instance;

        public static FluentSessionProvider Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new FluentSessionProvider();
                }
                return instance;
            }
        }

        private ISessionFactory sessionFactory;

        public ISessionFactory GetSessionFactory()
        {
            return sessionFactory;
        }

        public void CreateSessionFactory()
        {
            BuildSessionFactoryFromAssemblies();
        }

        public void UpdateSessionFactory()
        {
            sessionFactory.Close();

            DeleteFileConfiguration();

            BuildSessionFactoryFromAssemblies();
        }

        public void UpdateSessionFactoryAdd(Assembly assemblyToAdd)
        {
            if (!FluentAssemblies.Contains(assemblyToAdd))
            {
                FluentAssemblies.Add(assemblyToAdd);

                // testare se è necessario
                sessionFactory.Close();

                DeleteFileConfiguration();

                BuildSessionFactoryFromAssemblies();
            }
        }

        public void UpdateSessionFactoryAddRange(ICollection<Assembly> assemblyToAdd, bool isInsidePackage = false)
        {
            foreach (Assembly ass in assemblyToAdd)
            {
                if (!FluentAssemblies.Contains(ass))
                {
                    FluentAssemblies.Add(ass);
                }
            }

            if (!isInsidePackage)
            {
                // testare se è necessario
                sessionFactory.Close();

                //_configuration = null;
                DeleteFileConfiguration();

                BuildSessionFactoryFromAssemblies();
            }
        }

        public void UpdateSessionFactoryRemove(Assembly assemblyToRemove)
        {
            if (FluentAssemblies.Contains(assemblyToRemove))
            {
                FluentAssemblies.Remove(assemblyToRemove);

                // testare se è necessario
                sessionFactory.Close();

                DeleteFileConfiguration();

                BuildSessionFactoryFromAssemblies();
            }
        }

        private void BuildSessionFactoryFromAssemblies()
        {
            sessionFactory = GetConfiguration().BuildSessionFactory();
        }

        public static IList<Assembly> FluentAssemblies
        {
            get
            {
                if (_FluentAssemblies == null)
                {
                    if (HttpContext.Current.Application[FluentAssembliesKey] != null)
                        _FluentAssemblies = (List<Assembly>)HttpContext.Current.Application[FluentAssembliesKey];
                    else
                        HttpContext.Current.Application[FluentAssembliesKey] = _FluentAssemblies = new List<Assembly>();

                }

                return _FluentAssemblies;
            }
        }
        private static IList<Assembly> _FluentAssemblies;

        public ISession GetSession()
        {
            //Aggiungo la verifica del bind tra contesto corrente e sessione
            if (HttpContext.Current != null && ManagedWebSessionContext.HasBind(HttpContext.Current, sessionFactory))
            {
                return sessionFactory.GetCurrentSession();
            }
            return null;
        }

        public ISession OpenInnerSession()
        {
            ISession session = sessionFactory.OpenSession();
            return session;
        }

        public ISession OpenSession()
        {
            ISession session = sessionFactory.OpenSession();
            ManagedWebSessionContext.Bind(HttpContext.Current, session);
            return session;
        }

        public void CloseSession()
        {
            ISession session = ManagedWebSessionContext.Unbind(HttpContext.Current, sessionFactory);
            if (session != null)
            {
                if (session.Transaction != null && session.Transaction.IsActive)
                {
                    session.Transaction.Rollback();
                }
                else
                {
                    session.Flush();
                }
                session.Close();
            }
        }

        public IStatelessSession OpenStatelessSession()
        {
            IStatelessSession session = sessionFactory.OpenStatelessSession();
            return session;
        }

        public void CloseStatelessSession(IStatelessSession session)
        {
            if (session != null)
            {
                if (session.Transaction != null && session.Transaction.IsActive)
                {
                    session.Transaction.Rollback();
                }
                session.Close();
            }
        }

        private static void BuildSchema(Configuration config)
        {            
            FileInfo anagrafiche = new FileInfo(NetCms.Configurations.Paths.PhysicalRoot + "\\App_Data\\AnagraficaBase.hbm.xml");
            if (anagrafiche.Exists)
            {
                config.AddFile(anagrafiche);
            }

            #region ContattiCustom           
            FileInfo contatti = new FileInfo(NetCms.Configurations.Paths.PhysicalRoot + "\\App_Data\\ContattiBase.hbm.xml");
            if (contatti.Exists)
            {
                config.AddFile(contatti);
            }
            #endregion

            var schemaUpdate = new SchemaUpdate(config);

            schemaUpdate.Execute(false, true);
           
            IList<Exception> exceptions = schemaUpdate.Exceptions;

            //TODO: aggiungere un log per le eventuali exception avvenute durante lo schema update
            using (StreamWriter w = File.AppendText(NetCms.Configurations.Paths.PhysicalRoot + "\\App_Data\\fluentnhibernate_exception.txt"))
            {
                foreach (Exception e in exceptions)
                {
                    w.Write("\r\nLog Entry : ");
                    w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
                    w.WriteLine(e.Message + System.Environment.NewLine);
                    w.WriteLine(e.StackTrace + System.Environment.NewLine);
                    w.WriteLine("-------------------------------");                    
                }
            }                           
        }


        //********************************************************************************************************
        //    Aggiunta per il salvataggio e il caricamento della configurazione del mapping di nhibernate
        //********************************************************************************************************
        //private string ConfigFile = NetCms.Configurations.Paths.AbsoluteConfigRoot + "\\applications-data\\fluentConfig.bin";
        private string ConfigFile = NetCms.Configurations.Paths.PhysicalRoot + "\\App_Data\\fluentConfig.bin";

        public Configuration GetConfiguration() 
        {
            Configuration configuration = null;

            if (File.Exists(ConfigFile) && IsConfigurationFileValid())
            {
                configuration = LoadConfigurationFromFile();
                if (configuration != null)
                {
                    sessionFactory = configuration.BuildSessionFactory();
                }
            }
            else 
            {
                configuration = Fluently.Configure()
                        .Database(MySQLConfiguration.Standard.Dialect<MySQL5Dialect>()
                                 .ConnectionString(cs => cs.FromConnectionStringWithKey("ConnectionString"))
                                 .AdoNetBatchSize(1)
                                 .UseReflectionOptimizer()
                                 .UseOuterJoin()                                 
                                 .DoNot.ShowSql())                        
                    .Cache(c => c.UseQueryCache()
                                 .UseSecondLevelCache()                                 
                                 .ProviderClass<NHibernate.Caches.SysCache2.SysCacheProvider>()                                 
                                 .UseMinimalPuts()
                                 )
                    .Mappings(x =>
                    {
                        x.FluentMappings.Conventions.Setup(m => m.Add(AutoImport.Never()));

                        foreach (Assembly assembly in FluentAssemblies)
                        {
                            x.FluentMappings.AddFromAssembly(assembly);
                        }
                        x.FluentMappings.ExportTo(NetCms.Configurations.Paths.PhysicalRoot + "\\App_Data\\");

                    })

                    .ExposeConfiguration(x => x.SetProperty("current_session_context_class", "managed_web"))
                    //.ExposeConfiguration(x => x.SetProperty("hbm2ddl.keywords", "auto-quote")) 
                    //.ExposeConfiguration(x => x.SetProperty("expiration", "30"))
                    .ExposeConfiguration(x => x.SetProperty("hbm2ddl.keywords", "none")) 
                    .ExposeConfiguration(x => x.SetProperty("generate_statistics", "false"))                    
                    .ExposeConfiguration(BuildSchema)

                    .BuildConfiguration();

                SaveConfigurationToFile(configuration);
            }
            return configuration;
        }

        private Boolean IsConfigurationFileValid()
        {
            string ForcedConfigFileValid = System.Web.Configuration.WebConfigurationManager.AppSettings["NhConfigFileValid"];

            if (ForcedConfigFileValid == "false")
            {

                foreach (Assembly assembly in FluentAssemblies)
                {
                    if (assembly.Location == null)
                        return false;
                    var configInfo = new FileInfo(ConfigFile);
                    var assInfo = new FileInfo(assembly.Location);
                    if (configInfo.LastWriteTime < assInfo.LastWriteTime)
                        return false;
                }
            }
            return true;
        }
     
        private void SaveConfigurationToFile(Configuration configuration)
        {
            var file = File.Open(ConfigFile, FileMode.Create);
            var bf = new BinaryFormatter();
            bf.Serialize(file, configuration);
            file.Close();
        }

        private Configuration LoadConfigurationFromFile()
        {
            try
            {
                var file = File.Open(ConfigFile, FileMode.Open);
                var bf = new BinaryFormatter();
                var config = bf.Deserialize(file) as Configuration;
                file.Close();
                return config;
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        private void DeleteFileConfiguration()
        {
            File.Delete(ConfigFile);
        }     
    }
}
