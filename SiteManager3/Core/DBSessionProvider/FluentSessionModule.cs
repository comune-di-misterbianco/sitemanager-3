﻿using System;
using System.Web;

namespace NetCms.DBSessionProvider
{
    public class FluentSessionModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.BeginRequest += OpenSession;
            context.EndRequest += CloseSession;
        }

        private void OpenSession(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;
            if (CheckEstension(application))
                FluentSessionProvider.Instance.OpenSession();

        }

        private void CloseSession(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;
            if (CheckEstension(application))
            {
                FluentSessionProvider.Instance.CloseSession();
            }
        }

        public void Dispose()
        {

        }

        private bool CheckEstension(HttpApplication application)
        {
            Uri Url = application.Context.Request.Url;
            if (Url != null && (Url.LocalPath.ToLower().Contains(".aspx") || Url.LocalPath.ToLower().Contains("/api/")))
                return true;
            else return false;
        }
    }
}
