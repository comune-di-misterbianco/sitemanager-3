﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Reflection;
using System.Collections.Generic;

namespace NetCms.WebAPI
{
    public static class WebAPIControllerPool
    {
        private const string AssembliesApplicationKey = "NetCms.WebAPI.WebAPIControllerPool";

        public static Dictionary<string, AbstractBaseController> Assemblies
        {
            get
            {
                if (_Assemblies == null)
                {
                    if (HttpContext.Current.Application[AssembliesApplicationKey] != null)
                        _Assemblies = (Dictionary<string, AbstractBaseController>)HttpContext.Current.Application[AssembliesApplicationKey];
                    else
                        HttpContext.Current.Application[AssembliesApplicationKey] = _Assemblies = new Dictionary<string, AbstractBaseController>();
                        
                }

                return _Assemblies;
            }
        }
        private static Dictionary<string, AbstractBaseController> _Assemblies;

        
        public static void ParseType(Type type, Assembly asm)
        {
            if (type.BaseType == Type.GetType("NetCms.WebAPI.AbstractBaseController"))
            {
                Object[] paramsArray = {};
                AbstractBaseController ControllersData = (AbstractBaseController)Activator.CreateInstance(type, paramsArray);

                Assemblies.Add(type.Name,ControllersData);
            }
        }
    }
}