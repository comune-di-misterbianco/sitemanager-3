﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.WebAPI
{
    public class BaseInfo
    {
        public BaseInfo()
        {
        }

        public string Titolo { get; set; }

        public string Descrizione { get; set; }

        public string Data { get; set; }

        public string Link { get; set; }
    }
}
