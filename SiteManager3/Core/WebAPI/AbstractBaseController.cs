﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Net.Http;
using System.Net.Http.Formatting;

namespace NetCms.WebAPI
{
    public abstract class AbstractBaseController : ApiController
    {
        //public abstract BaseInfo GetById(int id);

        public abstract IEnumerable<BaseInfo> GetAll(string net);

        public abstract IEnumerable<BaseInfo> GetAllByFolder(string net, int id);

        
    }
}
