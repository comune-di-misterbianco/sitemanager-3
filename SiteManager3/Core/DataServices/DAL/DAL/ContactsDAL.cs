﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NetFrontend.DAL
{
    public class ContactsDAL
    {
        public static DataRow[] ListUfficiUtentiMembri(string sqlConditions, int page = 0, int recordCount = 0)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseUfficiUtentiMembriSQL, sqlConditions, page, recordCount, "Cognome_Anagrafica,Nome_Anagrafica");
        }

        public static DataRow[] ListUfficiGruppiMembri(string sqlConditions, int page = 0, int recordCount = 0)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseUfficiGruppiMembriSQL, sqlConditions, page, recordCount, "Name_Group");
        }

        public static DataRow[] ListUffici(string sqlConditions = "", int page = 0, int recordCount = 0)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseUfficiSQL, sqlConditions, page, recordCount, "Nome_Ufficio");
        }

        public static DataRow[] ListDipendenti(string sqlConditions, int page = 0, int recordCount = 0)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseDipendentiSQL, sqlConditions, page, recordCount, "Cognome_Anagrafica,Nome_Anagrafica");
        }
    }
}
