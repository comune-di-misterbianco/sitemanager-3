﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NetFrontend.DAL
{
    public class HomepageDAL
    {
        public static DataRow GetModuloCircolareByID(int ID)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseModuliCircolariSQL, "Modulo_ModuloCircolare = " + ID, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }

        public static DataRow GetModuloByID(int ID)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseModuliSQL, "id_Modulo = " + ID, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }

        public static DataRow GetModuloApplicativoByID(int ID)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseModuliApplicativiSQL, "Modulo_ModuloApplicativo = " + ID, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }
        
        public static DataRow GetModuloMenu(int ID)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseModuliMenuSQL, "Modulo_ModuloMenu = " + ID, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }
        public static DataRow GetModuloMenu(string sqlConditions)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseModuliMenuSQL, sqlConditions, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }
        
        public static DataRow GetModuloStaticoByID(int ID)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseModuliStaticiSQL, "rif = " + ID, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }

        public static DataRow GetModuloStaticoBySqlConditions(string sqlConditions)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseModuliStaticiSQL, sqlConditions, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }

        public static DataRow GetModuloRssSqlByConditions(string sqlConditions)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseModuliRssSQL, sqlConditions, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }


        /// <summary>
        /// Restituisce un set di records corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina è rappresentata dallo 0</param>
        /// <param name="recordCount">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>
        /// <returns></returns>
        public static DataRow[] ListModuliCircolari(string sqlConditions, int page = 0, int recordCount = 0)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseModuliCircolariSQL, sqlConditions, page, recordCount);
        }

        /// <summary>
        /// Restituisce un set di records corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina è rappresentata dallo 0</param>
        /// <param name="recordCount">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>
        /// <returns></returns>
        public static DataRow[] ListModuli(string sqlConditions, int page = 0, int recordCount = 0, string order = null)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseModuliSQL, sqlConditions, page, recordCount);
        }

        /// <summary>
        /// Restituisce un set di records corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina è rappresentata dallo 0</param>
        /// <param name="recordCount">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>
        /// <returns></returns>
        public static DataRow[] ListModuliApplicativi(string sqlConditions, int page = 0, int recordCount = 0)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseModuliApplicativiSQL, sqlConditions, page, recordCount);
        }

        /// <summary>
        /// Restituisce un set di records corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina è rappresentata dallo 0</param>
        /// <param name="recordCount">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>
        /// <returns></returns>
        public static DataRow[] ListContenitori(string sqlConditions, int page = 0, int recordCount = 0, string order = null)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseContenitoriSQL, sqlConditions, page, recordCount);
        }
    }
}
