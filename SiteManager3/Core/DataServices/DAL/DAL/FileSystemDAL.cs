﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NetCms.Connections;

namespace NetFrontend.DAL
{
    public class FileSystemDAL
    {
        public static DataRow GetDocumentRecordByID(int ID)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseDocumentsSQL, "id_Doc = " + ID, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }

        public static bool IsFolderRestricted(int ID)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(string.Format(DataServices.DAL.Resources.SqlQueries.BaseFolderIsRestrictedSQL, ID), string.Empty, 0, 1);
            return rows.Length > 0;
        }

        public static bool IsDocumentRestricted(int ID)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(string.Format(DataServices.DAL.Resources.SqlQueries.BaseDocumentIsRestrictedSQL, ID), string.Empty, 0, 1);
            return rows.Length > 0;
        }

        public static DataRow GetFolderRecordByID(int ID)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseFoldersSQL, "id_Folder = " + ID, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }

        public static DataRow GetFolderRecordBySqlConditions(string sql)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseFoldersSQL, sql, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }

        /// <summary>
        /// Restituisce il documento corrispondente ai paramentri richiesti.
        /// La funzione restituisce un oggetto DataRow solo se la query da 1 solo risultato.
        /// </summary>
        /// <param name="sql">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <returns></returns>
        public static DataRow GetDocumentRecordBySqlConditions(string sql)
        {
            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseDocumentsSQL, sql, 0, 1);
            return rows.Length > 0 ? rows.First() : null;
        }

        /// <summary>
        /// Restituisce un set di documenti corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina è rappresentata dallo 0</param>
        /// <param name="recordCount">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>
        /// <param name="order">Una string contenente le clausule da inserire nel ORDER BY della query es. "miocampo, altrocampo"</param>
        /// <returns></returns>
        public static DataRow[] ListDocumentsRecordsBySqlConditions(string sqlConditions, int page = 0, int recordCount = 0, string order = null)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseDocumentsSQL, sqlConditions, page, recordCount, order);
        }

        /// <summary>
        /// Restituisce un set di documenti corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina è rappresentata dallo 0</param>
        /// <param name="recordCount">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>
        /// <returns></returns>
        public static DataRow[] ListDocumentsMetaRecordsBySqlConditions(string sqlConditions, int page = 0, int recordCount = 0)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseDocumentsMetaSQL, sqlConditions, page, recordCount);
        }

        /// <summary>
        /// Restituisce un set di documenti corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina è rappresentata dallo 0</param>
        /// <param name="recordCount">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>
        /// <returns></returns>
        public static DataRow[] ListDocumentsTagsRecordsBySqlConditions(string sqlConditions, int page = 0, int recordCount = 0)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseDocumentsTagsSQL, sqlConditions, page, recordCount);
        }

        /// <summary>
        /// Restituisce un set di cartelle corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina è rappresentata dallo 0</param>
        /// <param name="recordCount">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>
        /// <param name="order">Una string contenente le clausule da inserire nel ORDER BY della query es. "miocampo, altrocampo"</param>
        /// <returns></returns>
        public static DataRow[] ListFoldersRecordsBySqlConditions(string sqlConditions, int page = 0, int recordCount = 0, string order = null)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseFoldersSQL, sqlConditions, page, recordCount, order);
        }

        /// <summary>
        /// Restituisce un set di cartelle corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina è rappresentata dallo 0</param>
        /// <param name="recordCount">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>       
        /// <param name="order">Una string contenente le clausule da inserire nel ORDER BY della query es. "miocampo, altrocampo"</param>
        /// <returns></returns>
        public static DataRow[] ListFoldersMetaRecordsBySqlConditions(string sqlConditions, int page = 0, int recordCount = 0, string order = null)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseFoldersMetaSQL, sqlConditions, page, recordCount, order);
        }


        /// <summary>
        /// Restituisce il numero di record corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina è rappresentata dallo 0</param>
        /// <param name="recordCount">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>
        /// <param name="order">Una string contenente le clausule da inserire nel ORDER BY della query es. "miocampo, altrocampo"</param>
        /// <returns></returns>
        public static int CountDocumentsRecordsBySqlConditions(string sqlConditions)
        {
            return GenericDAL.CountRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseDocumentsSQL, sqlConditions);
        }
        /// <summary>
        /// Restituisce il numero di record corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina è rappresentata dallo 0</param>
        /// <param name="recordCount">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>
        /// <param name="order">Una string contenente le clausule da inserire nel ORDER BY della query es. "miocampo, altrocampo"</param>
        /// <returns></returns>
        public static int CountFoldersRecordsBySqlConditions(string sqlConditions)
        {
            return GenericDAL.CountRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseFoldersSQL, sqlConditions);
        }
    }
}
