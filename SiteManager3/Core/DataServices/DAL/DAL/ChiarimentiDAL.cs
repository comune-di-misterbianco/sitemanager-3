﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NetFrontend.DAL
{
    public class ChiarimentiDAL
    {
        public static DataRow[] ListUfficiCompetenze(string sqlConditions, int page = 0, int recordCount = 0)
        {
            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseUfficiCompetenzeSQL, sqlConditions, page, recordCount);
        }
    }
}
