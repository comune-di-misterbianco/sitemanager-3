﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Data;

//namespace NetFrontend.DAL
//{
//    public class NewsletterDAL
//    {
//        public static int CountGuestsBySqlConditions(string sql)
//        {
//            return GenericDAL.CountRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseGuestsNewsletterSQL,sql);
//        }

//        /// <summary>
//        /// Restituisce il documento corrispondente ai paramentri richiesti.
//        /// La funzione restituisce un oggetto DataRow solo se la query da 1 solo risultato.
//        /// </summary>
//        /// <param name="sql">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
//        /// <returns></returns>
//        public static DataRow GetGuestsBySqlConditions(string sql)
//        {
//            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseGuestsNewsletterSQL, sql);
//            return rows.Length > 0 ? rows.First() : null;
//        }

//        /// <summary>
//        /// Restituisce il documento corrispondente ai paramentri richiesti.
//        /// La funzione restituisce un oggetto DataRow solo se la query da 1 solo risultato.
//        /// </summary>
//        /// <param name="sql">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
//        /// <returns></returns>
//        public static DataRow GetSottoscrizioneBySqlConditions(string sql)
//        {
//            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseSottoscrizioniNewsletterSQL, sql);
//            return rows.Length > 0 ? rows.First() : null;
//        }

//        /// <summary>
//        /// Restituisce il documento corrispondente ai paramentri richiesti.
//        /// La funzione restituisce un oggetto DataRow solo se la query da 1 solo risultato.
//        /// </summary>
//        /// <param name="sql">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
//        /// <returns></returns>
//        public static DataRow GetCanaleBySqlConditions(string sql)
//        {
//            var rows = GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseCanaliNewsletterSQL, sql,orderby:"nome_canale");
//            return rows.Length > 0 ? rows.First() : null;
//        }

//        /// <summary>
//        /// Restituisce un set di cartelle corrispondenti ai paramentri richiesti.
//        /// </summary>
//        /// <param name="sql">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
//        /// <returns></returns>
//        public static DataRow[] ListCanaliRecordsBySqlConditions(string sql = null, string order = null)
//        {
//            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseCanaliNewsletterSQL, sql, orderby: order);
//        }

//        /// <summary>
//        /// Restituisce un set di cartelle corrispondenti ai paramentri richiesti.
//        /// </summary>
//        /// <param name="sql">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
//        /// <returns></returns>
//        public static DataRow[] ListCategorieRecordsBySqlConditions(string sql = null, string order = null)
//        {
//            return GenericDAL.ListRecordsBySqlConditions(DataServices.DAL.Resources.SqlQueries.BaseCategorieNewsletterSQL, sql, orderby: order);
//        }
//    }
//}
