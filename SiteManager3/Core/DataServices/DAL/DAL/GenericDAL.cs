﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using NetCms.Connections;

namespace NetFrontend.DAL
{
    public class GenericDAL
    {
        private const string BaseQueryFormat = "{0} {1} {2} {3}";

        //questa variabile serve qualora si voglia utilizzare il GenericDal non per connettersi al database principale settato dentro la sezione <connectionStrings> del web.config
        // con il nome name="ConnectionString" ma ad un altro database esterno.é stato introdotto per permettere all'applicativo ROL di accedere al database dell'ecommerce per ricavarne
        //le statistiche di vendita ed utilizzo da parte dei vari attori.
        public static string externalDBAddress = "";

        public static ConnectionsManager ConnManager
        {
            get
            {
                if (string.IsNullOrEmpty(externalDBAddress))
                {
                    NetCms.Connections.ConnectionsManager ConnManager = new NetCms.Connections.ConnectionsManager();
                    return ConnManager;
                }
                else
                {
                    NetCms.Connections.ConnectionsManager ConnManager = new NetCms.Connections.ConnectionsManager(externalDBAddress);
                    return ConnManager;
                }
            }
        }
        
        /// <summary>
        /// Restituisce un set di cartelle corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sql">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <returns></returns>
        public static bool Execute(string sql)
        {
            return ConnManager.CommonConnection.Execute(sql);
        }

        /// <summary>
        /// Restituisce un set di cartelle corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sql">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <returns></returns>
        public static int ExecuteInsert(string sql)
        {
            return ConnManager.CommonConnection.ExecuteInsert(sql);
        }

        /// <summary>
        /// Restituisce un set di records corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlBaseQuery">Una string contenente la query base che verrà eseguita</param>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <param name="page">Un intero che rappresenta la pagina di database da restituire in caso di paginazione. La prima pagina è rappresentata dallo 0</param>
        /// <param name="pageSize">Un intero che rappresenta la il numero di record massimo da restituire per ogni pagina</param>
        /// <param name="orderby">Una stringa contenente le clausule da inserire nel ORDER BY della query es. "miocampo, altrocampo"</param>
        /// <returns></returns>
        public static DataRow[] ListRecordsBySqlConditions(string sqlBaseQuery, string sqlConditions = null, int page = 0, int pageSize = 0, string orderby = null)
        {
            //DalDebugLogger.PageLogger.SaveTime();

            string sqlOrder = !string.IsNullOrEmpty(orderby) ? " ORDER BY " + orderby : string.Empty;
            string sqlLimit = BuildLimit(page, pageSize);
            string sqlQuery = string.Format(BaseQueryFormat, sqlBaseQuery, (!string.IsNullOrEmpty(sqlConditions)?(" AND " + sqlConditions):""), sqlOrder, sqlLimit).Trim();

            DataTable table;

            table = ConnManager.CommonConnection.SqlQuery(sqlQuery);

            table.TableName = sqlQuery;

            var result = table != null ? table.Rows.Cast<DataRow>().ToArray() : null;

            //DalDebugLogger.PageLogger.DebugLog("SELECT -> " + sqlQuery);

            return result;
        }

        /// <summary>
        /// Restituisce il numero di record corrispondenti ai paramentri richiesti.
        /// </summary>
        /// <param name="sqlBaseQuery">Una string contenente la query base che verrà eseguita</param>
        /// <param name="sqlConditions">Una string contenente le clausule da inserire nel WHERE della query es. "miocampo = 10 AND altrocampo = 13"</param>
        /// <returns></returns>
        public static int CountRecordsBySqlConditions(string sqlBaseQuery, string sqlConditions = null)
        {
            //DalDebugLogger.PageLogger.SaveTime();
            //SqlParser.SqlParser myParser = new SqlParser.SqlParser(SqlParser.SqlParser.QueryTypes.Select);

            string sqlQuery = string.Format(BaseQueryFormat, sqlBaseQuery, (!string.IsNullOrEmpty(sqlConditions) ? (" AND " + sqlConditions) : ""), "", "").Trim();
            //myParser.Parse(sqlQuery);

            int fromStartIndex = sqlQuery.IndexOf("FROM");
            string sqlQueryFrom = sqlQuery.Substring(fromStartIndex + 5);

            int count;

            NetCms.Front.Data.PagedQuery pq = new NetCms.Front.Data.PagedQuery(ConnManager.CommonConnection)
                {
                //Select = myParser.Select,
                //Where = myParser.WhereClause.Replace("< >", "<>"),
                //From = myParser.From,
                From = sqlQueryFrom
                };
                count = pq.GetCount();

            //DalDebugLogger.PageLogger.DebugLog("COUNT -> " + sqlQuery);

            return count;
        }

        public static string BuildLimit(int page, int pageSize)
        {
            if (pageSize > 0)
            {
                int offset = page * pageSize;
                return string.Format(" LIMIT {0},{1}", offset, pageSize);
            }
            else return string.Empty;
        }
    }
}
