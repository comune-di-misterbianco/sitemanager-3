﻿using System;
using log4net;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.Web.Security;

public class DalDebugLogger
{
    private static readonly ILog logCount = LogManager.GetLogger("LoggerCountDAL");
    private static readonly ILog logIndex = LogManager.GetLogger("LoggerIndexDAL");

    private DalDebugLogger()
    {
    }

    private DateTime LastSaveTime { get; set; }
    public DateTime FirstSaveTime { get; private set; }

    public void SaveTime()
    {
        if (FirstSaveTime == DateTime.MinValue)
            FirstSaveTime = DateTime.Now;
        LastSaveTime = DateTime.Now;
    }

    public void DebugLog(string message)
    {
        TimeSpan time = DateTime.Now - LastSaveTime;
        string timeString = time.Minutes + ":" + time.Seconds + ":" + G2Core.Common.Utility.FormatNumber(time.Milliseconds.ToString(), 3);

        if (logCount.IsDebugEnabled)
        {
            QueriesTimings.Add(message + " [" + Guid.NewGuid().ToString() + "]", time);
            AddAggregateQueryData(message, time);
        }
    }

    public static Dictionary<string, TimeSpan> QueriesTimings
    {
        get
        {
            if (!HttpContext.Current.Items.Contains("PageDalLoggerQueriesTimings"))
            {
                HttpContext.Current.Items["PageDalLoggerQueriesTimings"] = new Dictionary<string, TimeSpan>();
            }
            return (Dictionary<string, TimeSpan>)HttpContext.Current.Items["PageDalLoggerQueriesTimings"];
        }
    }

    private static Dictionary<string, AggregateQueryData> QueriesAggregator
    {
        get
        {
            if (!HttpContext.Current.Items.Contains("PageDalLoggerQueriesAggregator"))
            {
                HttpContext.Current.Items["PageDalLoggerQueriesAggregator"] = new Dictionary<string, AggregateQueryData>();
            }
            return (Dictionary<string, AggregateQueryData>)HttpContext.Current.Items["PageDalLoggerQueriesAggregator"];
        }
    }

    public static void AddAggregateQueryData(string query, TimeSpan time)
    {
        if (!QueriesAggregator.ContainsKey(query))
        {
            AggregateQueryData qd = new AggregateQueryData(query, time);
            QueriesAggregator.Add(query, qd);
        }
        else
        {
            AggregateQueryData qd = QueriesAggregator[query];
            qd.Count++;
        }
    }

    public static void SaveQueriesReport()
    {
        if (NetCms.Configurations.Debug.FrontTimingsEnabled)
        {
            int i = 1;
            foreach (var entry in QueriesTimings)
            {
                string timeString = entry.Value.Minutes + ":" + entry.Value.Seconds + ":" + G2Core.Common.Utility.FormatNumber(entry.Value.Milliseconds.ToString(), 3);
                logIndex.Debug(i + ") " + timeString + " - " + entry.Key);
                i++;
            }
            int c = 1;
            foreach (var entry in QueriesAggregator.Values)
            {
                string timeString = entry.Time.Minutes + ":" + entry.Time.Seconds + ":" + G2Core.Common.Utility.FormatNumber(entry.Time.Milliseconds.ToString(), 3);
                logCount.Debug(c + ") x" + entry.Count + " - " + timeString + " - " + entry.Query);
                c++;
            }
            TimeSpan total = QueriesAggregator.Values.Select(x=>x.Time).Aggregate(TimeSpan.Zero, (subtotal, t) => subtotal.Add(t));
            logCount.Debug("Totale " + (i - 1) + " query eseguite in " + total.Seconds + "." + total.Milliseconds + " secondi");

        }
    }
    public static DalDebugLogger PageLogger
    {
        get
        {
            if (!HttpContext.Current.Items.Contains("DebugPageLogger"))
            {
                HttpContext.Current.Items["DebugPageLogger"] = new DalDebugLogger();
            }
            return (DalDebugLogger)HttpContext.Current.Items["DebugPageLogger"];
        }
    }

    private class AggregateQueryData
    {
        public string Query { get; private set; }
        public string Guid { get; private set; }
        public TimeSpan Time { get; private set; }
        public int Count { get; set; }

        public AggregateQueryData(string query, TimeSpan time)
        {
            Time = time;
            Guid = new Guid().ToString();
            Count = 1;
            Query = query;
        }
    }
}