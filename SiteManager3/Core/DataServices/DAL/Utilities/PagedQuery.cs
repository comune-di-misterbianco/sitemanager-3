﻿using System;
using System.Linq;
using System.Data;
using NetCms.Connections;

namespace NetCms.Front.Data
{
    public class PagedQuery
    {
        /// <summary>
        /// Questa propietà contiene i paramentri per costruire la parte di SELECT della query.
        /// es: Se la query da eseguire è "SELECT * FROM Users WHERE id=1" la proprietà dovrà contenere "SELECT *"
        /// </summary>
        public string Select
        {
            get
            {
                return _Select;
            }
            set
            {
                _Select = value;
            }
        }
        private string _Select;

        /// <summary>
        /// Questa propietà contiene i paramentri per costruire la parte di FROM della query.
        /// es: Se la query da eseguire è "SELECT * FROM Users WHERE id=1" la proprietà dovrà contenere "FROM Users"
        /// </summary>
        public string From
        {
            get
            {
                return _From;
            }
            set
            {
                _From = value;
            }
        }
        private string _From;

        /// <summary>
        /// Questa propietà contiene i paramentri per costruire la parte di WHERE della query.
        /// es: Se la query da eseguire è "SELECT * FROM Users WHERE id=1" la proprietà dovrà contenere "WHERE id=1"
        /// </summary>
        public string Where
        {
            get
            {
                return _Where;
            }
            set
            {
                _Where = value;
            }
        }
        private string _Where;

        /// <summary>
        /// Questa propietà contiene i paramentri per costruire la parte di WHERE della query.
        /// es: Se la query da eseguire è "SELECT id FROM Users GROUP BY id" la proprietà dovrà contenere "GROUP BY id"
        /// </summary>
        public string GroupBy
        {
            get
            {
                return _GroupBy;
            }
            set
            {
                _GroupBy = value;
            }
        }
        private string _GroupBy;

        public int RecordsPerPage
        {
            get
            {
                return _RecordsPerPage;
            }
            set
            {
                _RecordsPerPage = value;
            }
        }
        private int _RecordsPerPage;

        public NetCms.Connections.Connection Connection
        {
            get
            {
                return _Connection;
            }
            private set
            {
                _Connection = value;
            }
        }
        private NetCms.Connections.Connection _Connection; 

        public PagedQuery(NetCms.Connections.Connection conn)
        {
            this.Connection = conn;
        }

        private string BuildQuery()
        {
            string query = "";

            if (Select != null) query += "SELECT " + Select;
            else throw new Exception("Proprietà Select non impostata sull'istanza di PagedQuery.");
            if (From != null) query += " FROM " + From;
            else throw new Exception("Proprietà From non impostata sull'istanza di PagedQuery.");
            if (Where != null) query += " WHERE " + Where;
            if (GroupBy != null) query += " GROUP BY " + GroupBy;

            return query;
        }

        private string CountQuery()
        {
            string query = "";

            query += "SELECT COUNT(0)";

            if (From != null) query += " FROM " + From;
            else throw new Exception("Proprietà From non impostata sull'istanza di PagedQuery.");
            if (Where != null) query += " WHERE " + Where;
            if (GroupBy != null) query += " GROUP BY" + GroupBy;

            return query;
        }

        public PagedQueryResult GetResults(int page)
        {
            string baseQuery = BuildQuery();

            int firstRecordToFetch = ((page + 1) * (RecordsPerPage)) - RecordsPerPage;
            string resultsQuery = baseQuery + " LIMIT " + firstRecordToFetch + "," + RecordsPerPage;

            DataTable resultTable = this.Connection.SqlQuery(resultsQuery);

            int count = GetCount();
            if (count>0)
                return new PagedQueryResult(resultTable.Rows.Cast<DataRow>().ToArray(), count);
            else
                return null;
        }

        public int GetCount()
        {
            string countQuery = CountQuery();
            
            DataTable countTable = this.Connection.SqlQuery(countQuery);

            int count = -1;
            if (countTable != null && countTable.Rows.Count == 1)
                int.TryParse(countTable.Rows[0][0].ToString(), out count);

            return count;
        }
    }

    public class PagedQueryResult
    {
        public DataRow[] Records
        {
            get
            {
                return _Records;
            }
            private set
            {
                _Records = value;
            }
        }
        private DataRow[] _Records;

        public int TotalRecordsCount
        {
            get
            {
                return _TotalRecordsCount;
            }
            private set
            {
                _TotalRecordsCount = value;
            }
        }
        private int _TotalRecordsCount;

        private Connection Connection { get; set; }

        public PagedQueryResult(DataRow[] result, int totalRecordsCount)
        {
            this.Records = result;
            this.TotalRecordsCount = totalRecordsCount;
        }
    }
}
