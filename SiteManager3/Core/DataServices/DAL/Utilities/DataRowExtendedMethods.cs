﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace NetFrontend
{
    public static class DataRowExtendedMethods
    {
        public static bool IsNotNull(this DataRow row)
        {
            return row != null;
        }

        public static int IntValue(this DataRow row, string column)
        {
            int value = 0;
            int.TryParse(row[column].ToString(), out value);
            return value;
        }
    }
}
