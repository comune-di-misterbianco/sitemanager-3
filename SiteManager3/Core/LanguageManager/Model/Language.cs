﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LanguageManager.Model
{
    public class Language
    {
        public virtual int ID
        {
            get;
            set;
        }
        /// <summary>
        /// Nome Linguaggio
        /// </summary>
        public virtual string Nome_Lang
        {
            get;
            set; 
        }
        /// <summary>
        /// Valore per settare il linguaggio di Default sul db
        /// ATTENZIONE: Per questioni di retroccompatibilità è un int e non un booleano
        /// </summary>
        public virtual int Default_Lang
        {
            get;
            set;
        }
        /// <summary>
        /// Culture Code.
        /// Il formato è xx-YY. xx = lingua e YY = cultura.ES. en-US (inglese-StatiUniti)
        /// </summary>
        public virtual string CultureCode_Lang
        {
            get;
            set;
        }
        /// <summary>
        /// Parent_Lang.
        /// Intero mantenuto per retrocompatibilità.
        /// 
        /// </summary>
        public virtual int Parent_Lang
        {
            get;
            set; 
        }
        /// <summary>
        /// Enabled.
        /// Booleano che indica se il linguaggio è abilitato o meno.
        /// </summary>
        public virtual bool Enabled
        {
            get;
            set;

        }

        /// <summary>
        /// Two Letter Lang Identification
        /// </summary>
        public virtual string LangTwoLetterCode { get; set; }

        public enum Columns
        { 
            id_Lang,
            Nome_Lang,
            Default_Lang,
            CultureCode_Lang,
            Parent_Lang,
            Enabled,
            LangTwoLetterCode


        }
    }
}
