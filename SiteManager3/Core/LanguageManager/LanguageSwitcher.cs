﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.IO;
using NetCms.Vertical.Configuration;
using System.Web;
using GenericDAO.DAO.Utility;
using NetCms.Diagnostics;
using LanguageManager.BusinessLogic;

namespace LanguageManager
{
    public  class LanguageSwitcher : WebControl
    {

        IList<LanguageManager.Model.Language> languages;

        LanguageManager.Model.Language defaultLanguage = LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage;        
       
        public string toHtml()
        {
           
            TextWriter tw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(tw);
            this.RenderControl(hw);

            return tw.ToString();        
        }

        public List<object> GetDataForTpl()
        {
            return Langs;
        }

   
        public LanguageSwitcher(
            Dictionary<string,string> translatedPathsCollection = null, 
            Dictionary<string, string> translatedPageNameCollection = null, 
            bool useTpl = false, Dictionary<string, 
            string[]> staticPageAssociation = null
            ) 
            : base (HtmlTextWriterTag.Div)
        {
            Initialize(translatedPathsCollection, translatedPageNameCollection, useTpl, staticPageAssociation);         
        }

        protected override void OnPreRender(EventArgs e)
        {           
            base.OnPreRender(e);
        }

        private List<object> _Langs;
        private List<object> Langs {
            get
            {
                if (_Langs == null) 
                _Langs = new List<object>();
                return _Langs;
            }
        }

        public void Initialize(
            Dictionary<string, string> translatedPathsCollection, 
            Dictionary<string, string> translatedPageNameCollection, 
            bool useTpl = false,
            Dictionary<string, string[]> staticPageAssociation = null
            ) 
        {
           
            languages = new List<LanguageManager.Model.Language>();
            
            defaultLanguage = LanguageBusinessLogic.GetLanguageFromUrl(HttpContext.Current.Request.Path.ToString());

            languages = LanguageManager.BusinessLogic.LanguageBusinessLogic.GetEnabledLanguages();
            
            string [] lang;
            try
            {
                if (!useTpl)
                {

                    if ((LanguageManager.Utility.Config.languageNumber) <= languages.Count())
                    {
                        //WebControl div = new WebControl(HtmlTextWriterTag.Div);
                        this.CssClass = "languageContainer";

                        WebControl ul = new WebControl(HtmlTextWriterTag.Ul);
                        ul.CssClass = "language";

                        WebControl defaultLan = new WebControl(HtmlTextWriterTag.Li);

                        WebControl dl = new WebControl(HtmlTextWriterTag.Span);
                        dl.Controls.Add(new LiteralControl((defaultLanguage.Nome_Lang)));

                        defaultLan.ID = "defaultLanguage";
                        defaultLan.CssClass = "icon " + defaultLanguage.CultureCode_Lang;
                        defaultLan.Controls.Add(dl);
                        ul.Controls.Add(defaultLan);

                        WebControl li2 = new WebControl(HtmlTextWriterTag.Li);
                        li2.CssClass = "languageControl";

                        WebControl span = new WebControl(HtmlTextWriterTag.Span);
                        span.CssClass = "arrowSwitch";
                        li2.Controls.Add(span);

                        WebControl ul2 = new WebControl(HtmlTextWriterTag.Ul);
                        ul2.CssClass = "languageList";

                        foreach (LanguageManager.Model.Language l in languages)
                        {
                            if (l.ID != defaultLanguage.ID)
                            {
                                WebControl li = new WebControl(HtmlTextWriterTag.Li);

                                lang = l.CultureCode_Lang.Split('-');
                                li.CssClass = "icon " + l.CultureCode_Lang;
                                WebControl a = new WebControl(HtmlTextWriterTag.A);

                                string path = HttpContext.Current.Request.Path.ToString();

                                if (translatedPathsCollection != null && translatedPathsCollection.Count(x => x.Key == l.LangTwoLetterCode) > 0)
                                {

                                    path = translatedPathsCollection[l.LangTwoLetterCode] + (translatedPageNameCollection.Count > 0 && translatedPageNameCollection[l.LangTwoLetterCode] != null ? (translatedPathsCollection[l.LangTwoLetterCode].EndsWith("/") ? "" : "/") + translatedPageNameCollection[l.LangTwoLetterCode] : "");
                                }

                                string query = HttpContext.Current.Request.QueryString.ToString();

                                if (path.StartsWith("/" + defaultLanguage.LangTwoLetterCode + "/"))
                                {
                                    if (l.Default_Lang > 0)
                                        a.Attributes.Add("href", path.Replace("/" + defaultLanguage.LangTwoLetterCode + "/", "/") + (query.Length > 0 ? "?" + query : ""));
                                    else
                                        a.Attributes.Add("href", "/" + l.LangTwoLetterCode + path.Replace("/" + defaultLanguage.LangTwoLetterCode + "/", "/") + (query.Length > 0 ? "?" + query : ""));
                                }
                                else
                                {
                                    if (l.Default_Lang > 0)
                                        a.Attributes.Add("href", path + (query.Length > 0 ? "?" + query : ""));
                                    else
                                        a.Attributes.Add("href", "/" + l.LangTwoLetterCode + path + (query.Length > 0 ? "?" + query : ""));
                                }
                                a.Controls.Add(new LiteralControl(l.Nome_Lang));

                                li.Controls.Add(a);
                                ul2.Controls.Add(li);
                            }
                        }
                        li2.Controls.Add(ul2);
                        ul.Controls.Add(li2);
                        //div.Controls.Add(ul);
                        this.Controls.Add(ul);
                    }
                    else
                    {
                        //WebControl div = new WebControl(HtmlTextWriterTag.Div);
                        this.CssClass = "languageContainer type2";

                        foreach (LanguageManager.Model.Language l in languages)
                        {
                            WebControl a2 = new WebControl(HtmlTextWriterTag.A);
                            a2.CssClass = "icon " + l.CultureCode_Lang;
                            string path = HttpContext.Current.Request.Path.ToString();
                            string query = HttpContext.Current.Request.QueryString.ToString();
                            if (l.ID != defaultLanguage.ID)
                            {
                                if (path.StartsWith("/" + defaultLanguage.LangTwoLetterCode + "/"))
                                {
                                    if (l.Default_Lang > 0)
                                        a2.Attributes.Add("href", path.Replace("/" + defaultLanguage.LangTwoLetterCode + "/", "/") + "?" + query);
                                    else
                                        a2.Attributes.Add("href", "/" + l.LangTwoLetterCode + path.Replace("/" + defaultLanguage.LangTwoLetterCode + "/", "/") + "?" + query);
                                }
                                else
                                {
                                    if (l.Default_Lang > 0)
                                        a2.Attributes.Add("href", path + "?" + query);
                                    else
                                        a2.Attributes.Add("href", "/" + l.LangTwoLetterCode + path + "?" + query);
                                }
                            }
                            if (l.ID == defaultLanguage.ID)
                                a2.ID = "defaultLanguageB";

                            this.Controls.Add(a2);

                        }
                        //this.Controls.Add(div);
                    }
                }
                else
                {
                    foreach (LanguageManager.Model.Language language in languages)
                    {
                        if (language.ID != defaultLanguage.ID)
                        {
                            
                            string path = HttpContext.Current.Request.Path.ToString();
                            string urlCalcolato = string.Empty;

                            /// controllo se si tratta di una pagina statica
                            /// 
                            string keyStaticPage = string.Empty;
                            if (staticPageAssociation!= null)
                            {
                                keyStaticPage = (from sp in staticPageAssociation
                                          select
                                            sp.Value.Contains(path) ? sp.Key : null
                                          ).FirstOrDefault(x => !string.IsNullOrEmpty(x));
                            }
                            if (!string.IsNullOrEmpty(keyStaticPage))
                            {
                                string[] staticsUrl = null;
                                if (staticPageAssociation.TryGetValue(keyStaticPage, out staticsUrl))
                                {
                                    //int langIndex = language.ID - 1;
                                    if (staticsUrl.Length >= language.Parent_Lang)
                                        urlCalcolato = staticsUrl[language.Parent_Lang];
                                    else
                                        urlCalcolato = staticsUrl.FirstOrDefault();
                                }
                            }
                            else
                            {
                                if (translatedPathsCollection != null && translatedPathsCollection.Count(x => x.Key == language.LangTwoLetterCode) > 0)
                                    path = translatedPathsCollection[language.LangTwoLetterCode] + (translatedPageNameCollection.Count > 0 && translatedPageNameCollection[language.LangTwoLetterCode] != null ? (translatedPathsCollection[language.LangTwoLetterCode].EndsWith("/") ? "" : "/") + translatedPageNameCollection[language.LangTwoLetterCode] : "");

                                string query = HttpContext.Current.Request.QueryString.ToString();

                                if (path.StartsWith("/" + defaultLanguage.LangTwoLetterCode + "/"))
                                {
                                    if (language.Default_Lang > 0)
                                        urlCalcolato = path.Replace("/" + defaultLanguage.LangTwoLetterCode + "/", "/") + (query.Length > 0 ? "?" + query : "");
                                    else
                                        urlCalcolato = "/" + language.LangTwoLetterCode + path.Replace("/" + defaultLanguage.LangTwoLetterCode + "/", "/") + (query.Length > 0 ? "?" + query : "");
                                }
                                else
                                {
                                    if (language.Default_Lang > 0)
                                        urlCalcolato = path + (query.Length > 0 ? "?" + query : "");
                                    else
                                        urlCalcolato = "/" + language.LangTwoLetterCode + path + (query.Length > 0 ? "?" + query : "");
                                }
                            }

                            var langObj = new { label = language.Nome_Lang,
                                                url = urlCalcolato,
                                                culturecode = language.CultureCode_Lang,
                                                isdefault = language.Default_Lang,
                                                twolettercode = language.LangTwoLetterCode };
                            Langs.Add(langObj);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                Diagnostics.TraceException(ex, "", "", this);
            }
        }

    }
}
