﻿using NetCms.Vertical.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LanguageManager.Utility
{
    public static class Config
    {

        public static Configuration Configuration
        {

            get
            {
                return _Configuration ?? (_Configuration = new Configuration("languagemanager"));
            
            }
            
        }
        public static Configuration ConfigurationApp
        {
            get
            { 
                return _ConfigurationApp ?? (_ConfigurationApp = new Configuration("appSettings"));
            
            }
        
        
        }
        
        /// <summary>
        /// stringa che il valore del web.config 'defaultLanguage'.
        /// Se settato a 'db', la priorità del linguaggio di default è data al db, altrimenti viene data al browser
        /// </summary>
        public static string defaultLanguage
        {
            get
            {
                if (Configuration["defaultLanguage"] != null)
                    return Configuration["defaultLanguage"].ToString();
                return "";
            }
    
    
        }
        /// <summary>
        /// int che indica il numero di lingue oltre il quale avviene lo switch grafico del WebControl del multilingua.
        /// Per un numero minore a quello settato, verranno visualizzate solo le icone delle bandiere, altrimenti verrà visualizzato un menù
        /// stile dropdown
        /// </summary>
        public static int languageNumber
        {
            get 
            {
                if (Configuration["languageNumber"] != null)
                    return  Int32.Parse(Configuration["languageNumber"]);
                return 0;
            
            }
        
        
        }
        /// <summary>
        /// booleano che abilita o meno tutte le funzionalità legate al multilingua.
        /// </summary>
        public static bool EnableMultiLanguage
        {
            get
            {
                if (ConfigurationApp["enableMultiLanguage"] != null)
                    return Convert.ToBoolean(ConfigurationApp["enableMultiLanguage"]);
                return false;
            }
               
        }


        private static Configuration _Configuration;
        private static Configuration _ConfigurationApp;
    }
}
