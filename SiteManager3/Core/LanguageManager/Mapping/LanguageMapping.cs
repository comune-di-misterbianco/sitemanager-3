﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using LanguageManager.Model;

namespace LanguageManager.Mapping
{
    public class LanguageMapping : ClassMap<Language>
    {

        public LanguageMapping()
        {

            Table("lang");
            Id(x => x.ID)
                .Column("id_Lang");
                
            Map(x => x.Nome_Lang);
            Map(x => x.Default_Lang);
            Map(x => x.CultureCode_Lang);
            Map(x => x.Parent_Lang);
            Map(x => x.Enabled);
            Map(x => x.LangTwoLetterCode).Length(6);

        }


    }
}
