﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.DBSessionProvider;
using LanguageManager.Model;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using NetCms.Vertical.Configuration;
using System.Globalization;
using System.Web;

namespace LanguageManager.BusinessLogic
{
    public static  class LanguageBusinessLogic
    {

        public static ISession GetCurrentSession()
        {

            return FluentSessionProvider.Instance.GetSession();
        
        }

        public static bool HasCurrentSession()
        {
            return FluentSessionProvider.Instance.GetSession() != null;
        }
     
        /// <summary>
        /// Get del linguaggio dalla variabile di sessione.
        /// </summary>
        public static Language CurrentLanguage
        {

            get 
            {

                if (HttpContext.Current.Session != null && HttpContext.Current.Session["currentLanguage"] != null)
                {
                    _CurrentLanguage = (Language)HttpContext.Current.Session["currentLanguage"];
                }
                else if (HttpContext.Current.Items != null && HttpContext.Current.Items["currentLanguage"] != null)
                {
                    _CurrentLanguage = (Language)HttpContext.Current.Items["currentLanguage"];
                }
                else
                {
                    // _CurrentLanguage = GetLanguageByCultureCode(System.Threading.Thread.CurrentThread.CurrentCulture.ToString());
                    _CurrentLanguage = GetDefaultLanguage();
                }
                
                return _CurrentLanguage;
            
             }
            set
            {
                if (HttpContext.Current.Session != null)
                    HttpContext.Current.Session["currentLanguage"] = value;
                else
                    HttpContext.Current.Items["currentLanguage"] = value;
                var culture = new System.Globalization.CultureInfo(value.CultureCode_Lang.ToString());
                System.Threading.Thread.CurrentThread.CurrentCulture = culture;
                System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
                _CurrentLanguage = value;
            }
        
        }
        /// <summary>
        /// Get dei linguaggi abilitati.
        /// </summary>
        /// <param name="session"></param>
        /// <returns>Lista di oggetti Language</returns>
        public static IList<Language> GetEnabledLanguages(ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();
            SearchParameter sp = new SearchParameter(null,Language.Columns.Enabled.ToString(),true,Finder.ComparisonCriteria.Equals,false);

            IGenericDAO<Language> dao = new CriteriaNhibernateDAO<Language>(session);

            return dao.FindByCriteria(new SearchParameter[]{ sp });
        
        }


        public static IList<Language> GetLanguages(ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();

            IGenericDAO<Language> dao = new CriteriaNhibernateDAO<Language>(session);

            return dao.FindAll();
        }
        
        public static Language GetLanguageByCultureCode(string culture,ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();
            SearchParameter sp = new SearchParameter(null, Language.Columns.CultureCode_Lang.ToString(),culture, Finder.ComparisonCriteria.Equals, false);
            SearchParameter enabled = new SearchParameter(null, Language.Columns.Enabled.ToString(), true, Finder.ComparisonCriteria.Equals, false);
            IGenericDAO<Language> dao = new CriteriaNhibernateDAO<Language>(session);
            Language temp; 
            temp = dao.GetByCriteria(new SearchParameter[] { sp,enabled });

             if (temp == null)
                    temp = GetDefaultLanguage();
             return temp; 
            
        
        }


        public static Language GetLanguageById(int id, ISession session = null)
        {
            if (session == null)
                session = GetCurrentSession();
            
            IGenericDAO<Language> dao = new CriteriaNhibernateDAO<Language>(session);
            
            return dao.GetById(id);


        }


        private static Language _CurrentLanguage;

        /// <summary>
        /// Get linguaggio di default. 
        /// Se il parametro 'defaultLanguage' nel web.config è valorizzato 'db', allora viene data priorità 
        /// al linguaggio di default del database. Altrimenti viene data la priorità al linguaggio del browser.
        /// N.B. Si da per scontato che il linguaggio di default sia uno e che abbia il valore Enabled a true. 
        /// </summary>
        /// <param name="db">Valore di default a false.Se viene passato 'true' il metodo bypassa il parametro del web.config e cerca 
        /// il linguaggio direttamente nel db</param>
        /// <param name="session"></param>
        /// <returns>Oggetto di tipo Language</returns>
        public static Language GetDefaultLanguage(bool db = false, ISession session = null) 
        {
            Language currentLang;

            try
            {
                if (session == null)
                    session = GetCurrentSession();

                string config = LanguageManager.Utility.Config.defaultLanguage;
                int i = 1;
                //string lang = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
                string lang = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();

                /*if (lang == "en")
                {
                    lang = lang + "-" + "US";

                }
                else
                {
                    lang = lang + '-' + lang.ToUpper();
                }*/                

                SearchParameter cultureSp = new SearchParameter(null, Language.Columns.CultureCode_Lang.ToString(), lang, Finder.ComparisonCriteria.Equals, false);
                SearchParameter enabledSp = new SearchParameter(null, Language.Columns.Enabled.ToString(), true, Finder.ComparisonCriteria.Equals, false);
                SearchParameter defaultSp = new SearchParameter(null, Language.Columns.Default_Lang.ToString(), i, Finder.ComparisonCriteria.Equals, false);
                IList<SearchParameter> sp = new List<SearchParameter>();
                IGenericDAO<Language> dao = new CriteriaNhibernateDAO<Language>(session);
                sp.Add(enabledSp);


                if (!db)
                {
                    if (config == "db")
                    {
                        sp.Add(defaultSp);
                        currentLang = dao.GetByCriteria(sp.ToArray());
                    }
                    else
                    {

                        sp.Add(cultureSp);

                        currentLang = dao.GetByCriteria(sp.ToArray());
                    }
                }
                else
                {
                    sp.Add(defaultSp);
                    currentLang = dao.GetByCriteria(sp.ToArray());
                }
                if (currentLang == null)
                {
                    sp = new List<SearchParameter>();

                    sp.Add(defaultSp);
                    sp.Add(enabledSp);
                    currentLang = dao.GetByCriteria(sp.ToArray());
                }
            }
            catch(Exception ex)
            {
                // restituisco il default language
                currentLang = new Language();
                currentLang.CultureCode_Lang = "it-IT";
                currentLang.Default_Lang = 1;
                currentLang.Enabled = true;
                currentLang.Parent_Lang = 0;
                currentLang.Nome_Lang = "Italiano";
                currentLang.ID = 1;
                currentLang.LangTwoLetterCode = "it";
            }

            return currentLang;
        
        
        }

        /// <summary>
        /// Metodo che dato un URL in ingresso restituisce lo stesso, filtrato della componente che contiene la lingua
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetUrlWithoutLanguage(string url)
        {
            string urlToParse = url;
            IList<Language> languages = GetEnabledLanguages();
            if (languages != null)
            {
                foreach (Language lang in languages)
                {
                    if (urlToParse.Length == 3)
                        urlToParse = urlToParse.Replace("/" + lang.LangTwoLetterCode,"");
                    else
                        urlToParse = urlToParse.Replace("/" + lang.LangTwoLetterCode + "/", "/");
                }
            }
            return urlToParse;
        }

        /// <summary>
        /// Metodo che identifica il linguaggio attraverso l'URL
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static Language GetLanguageFromUrl(string url)
        {
            string urlToParse = url;
            IList<Language> languages = GetEnabledLanguages();
            if (languages != null)
            {
                foreach (Language lang in languages)
                {
                    if (urlToParse.Contains("/" + lang.LangTwoLetterCode + "/")) return lang;
                }
            }
            return GetDefaultLanguage(true);
        }

        public static bool ChangeLanguageHappened(Language current, string urlReferrer)
        {
            return CurrentLanguage != GetLanguageFromUrl(urlReferrer);
        }

        /// <summary>
        /// Metodo per rilevare se l'informazione del linguaggio è già presente nell'URL
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static bool UrlHasLanguageInfo(string url)
        {
            string urlToParse = url;
            using (ISession sess = FluentSessionProvider.Instance.OpenInnerSession())
            {
                IList<Language> languages = GetEnabledLanguages(sess);
                if (languages != null)
                {
                    foreach (Language lang in languages)
                    {
                        if (urlToParse.Contains("/" + lang.LangTwoLetterCode + "/")) return true;
                    }
                }
            }
            return false;
        }      
    }
}
