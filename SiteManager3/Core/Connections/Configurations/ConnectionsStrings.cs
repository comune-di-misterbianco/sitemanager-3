using System;
using System.Configuration;
using System.Web;
using System.Xml;


/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Configurations
{
    public static class ConnectionsStrings
    {

        /// <summary>
        /// Specifica la stringa di connessione al database per la connessione principale.
        /// </summary>
        private static string _MainDB;
        public static string MainDB
        {
            get
            { 
                if(_MainDB == null)
                {
                    if (System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnectionString"] == null)
                    {
                        Exception ex = new Exception("Stringa di connessione al database non specificata nel file 'Web.config'");
                        
                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, ex.Message, "", "", "72");
                    }
                    else
                    {
                        string strConn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
                        //if (NetCms.Configurations.Generics.PasswordCripted)
                        //{
                        //    NetCms.Diagnostics.DecodePass decoder = new NetCms.Diagnostics.DecodePass();
                        //    strConn = decoder.DecodeStringConn(strConn);
                            
                        //    if (strConn.Length == 0)
                        //    {
                        //        Exception ex = new Exception("La password della stringa di connessione \"ConnectionString\", presente all'interno del file 'Web.config', non � stata criptata.");
                        //        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, ex.Message, "", "", "143");
                        //    }
                        //}
                        _MainDB = strConn;
                    }
                }
                return _MainDB;
            }
        }

    }
}