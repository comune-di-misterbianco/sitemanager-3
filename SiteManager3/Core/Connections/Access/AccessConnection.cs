using System;
using System.Data;
using System.IO;
using System.Data.Common;
using System.Data.OleDb;
using System.Configuration;



namespace NetCms.Connections
{
    /// <summary>
    /// Deriva la classe base <see cref="NetCms.Connections.Connection"></see> implementando i metodi e proprietÓ
    /// per gestire la connessione e comandi per un database di tipo MS Access
    /// </summary>  
    /// <example>
    /// Segue un esempio di creazione di un'istanza della classe.
    /// <code>
    /// private NetCms.Connections.Connection _Conn;
    /// public NetCms.Connections.Connection Conn
    /// {
    ///     get
    ///     {
    ///        if (_Conn == null)
    ///        {
    ///             string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["eComm-DB"].ConnectionString;
    ///             _Conn = new NetCms.Connections.AccessConnection(connstr);
    ///        }
    ///        return _Conn;
    ///     }
    /// }
    /// </code>
    /// </example>
    public class AccessConnection : Connection
    {
        /// <summary>
        /// Implementa la logica di connessione ad un database MS Access restituendo un oggetto
        /// di connessione specifico System.Data.OleDb.OleDbConnection.
        /// </summary>
        /// <returns>connessione al database MySQL</returns>
        protected override DbConnection BuildConnection()
        {
            return new System.Data.OleDb.OleDbConnection(this.ConnectionString);
        }
        /// <summary>
        /// Implementa la logica per costruire un oggetto specifico di tipo System.Data.Common.DbDataAdapter
        /// in grado di eseguire delle istruzioni nei confronti di un database Accesss.
        /// </summary>
        /// <returns>L'oggetto di tipo System.Data.OleDb.OleDbDataAdapter creato.</returns>
        protected override DbDataAdapter BuildDataAdapter(DbCommand SelectCommand)
        {
            OleDbDataAdapter DataAdapterObj = new OleDbDataAdapter();
            DataAdapterObj.SelectCommand = (OleDbCommand)SelectCommand;
            return DataAdapterObj;
        }

        /// <summary>
        /// Implementa la logica per costruire un comando ad un database Access.
        /// </summary>
        /// <returns>Un'istruzione da eseguire su un database.</returns>
        protected override DbCommand BuildSelectCommand(DbConnection Conn)
        {
            OleDbCommand SelectCommandObj = new OleDbCommand();
            SelectCommandObj.Connection = (System.Data.OleDb.OleDbConnection)Conn;
            return SelectCommandObj;
        }

        /// <summary>
        /// Implementa la logica per creare un parametro di un oggetto DbCommand 
        /// per un database Access.
        /// </summary>
        /// <param name="parameterName">Nome del parametro.</param>
        /// <param name="value">Valore da assegnare al parametro.</param>
        /// <param name="direction">Direzione del parametro.</param>
        /// <returns>Il parametro creato</returns>
        public override DbParameter CreateParameter(string parameterName, object value, ParameterDirection direction)
        {
            throw new NotImplementedException();
        }

        public override DbParameter CreateParameter(string parameterName, object value, ParameterDirection direction, DbType tipo)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Inizializza una instanza della classe.
        /// </summary>
        /// <param name="connection_string">stringa di connessione ad un database MS Access</param>
        public AccessConnection(string connection_string)
            : base(connection_string)
        {
        }

        /// <summary>
        /// Implementa la logica per determinare l'ID del record inserito nell'ultima operazione 
        /// di insert.
        /// </summary>
        /// <returns>ID del record inserito nel database dall'ultima operazione di insert.</returns>
        public override int GetLastInsertID(DbConnection Connection)
        {
            int lastInsert = 0;
            //OleDbDataReader rdr;
            //if (!this.ConnectionOpen)
            //    Open();

            using (DbConnection conn = Connection)
            using (DbCommand SelectCommand = BuildSelectCommand(conn))
            {

                SelectCommand.CommandText = "SELECT LAST_INSERT_ID();";
                using (var rdr = (OleDbDataReader)SelectCommand.ExecuteReader())
                {

                    if (rdr.Read())
                        lastInsert = int.Parse(rdr[0].ToString());
                    else
                        lastInsert = 0;
                }
            }
            //rdr.Close();
            //rdr.Dispose();

            return lastInsert;
        }


        /// <summary>
        /// Restituisce una stringa rappresentante un dato di tipo DateTime conforme ai database 
        /// MS Access.    
        /// </summary>
        /// <param name="date">stringa da formattare</param>
        /// <returns>stringa formattata</returns>
        public override string FilterDate(DateTime date)
        {

            string day = date.Day.ToString();
            if (day.Length == 1) day = "0" + day;
            string month = date.Month.ToString();
            if (month.Length == 1) month = "0" + month;
            string year = date.Year.ToString();
            return "'" + date.Year.ToString() + month + day + "'";
        }



        /// <summary>
        /// Restituisce una stringa rappresentante un dato di tipo DateTime conforme ai database 
        /// MS Access. 
        /// </summary>
        /// <param name="date">stringa da formattare</param>
        /// <returns>stringa formattata</returns>   
        public override string FilterDateTime(DateTime date)
        {

            string day = date.Day.ToString();
            if (day.Length == 1) day = "0" + day;

            string month = date.Month.ToString();
            if (month.Length == 1) month = "0" + month;

            string year = date.Year.ToString();
            return "'" + date.Year.ToString() + month + day + " " + date.Hour + ":" + date.Minute + ":" + date.Second + "'";
        }

        public override bool CheckConnection()
        {
            throw new NotImplementedException();
        }
    }
}


//using System;
//using System.Data;
//using System.IO;
//using System.Data.Common;
//using System.Data.OleDb;
//using System.Configuration;



//namespace NetCms.Connections
//{
//    /// <summary>
//    /// Deriva la classe base <see cref="NetCms.Connections.Connection"></see> implementando i metodi e proprietÓ
//    /// per gestire la connessione e comandi per un database di tipo MS Access
//    /// </summary>  
//    /// <example>
//    /// Segue un esempio di creazione di un'istanza della classe.
//    /// <code>
//    /// private NetCms.Connections.Connection _Conn;
//    /// public NetCms.Connections.Connection Conn
//    /// {
//    ///     get
//    ///     {
//    ///        if (_Conn == null)
//    ///        {
//    ///             string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["eComm-DB"].ConnectionString;
//    ///             _Conn = new NetCms.Connections.AccessConnection(connstr);
//    ///        }
//    ///        return _Conn;
//    ///     }
//    /// }
//    /// </code>
//    /// </example>
//    public class AccessConnection : Connection
//    {
//        /// <summary>
//        /// Implementa la logica di connessione ad un database MS Access restituendo un oggetto
//        /// di connessione specifico System.Data.OleDb.OleDbConnection.
//        /// </summary>
//        /// <returns>connessione al database MySQL</returns>
//        protected override DbConnection BuildConnection()
//        {
//            return new System.Data.OleDb.OleDbConnection(this.ConnectionString);
//        }
//        /// <summary>
//        /// Implementa la logica per costruire un oggetto specifico di tipo System.Data.Common.DbDataAdapter
//        /// in grado di eseguire delle istruzioni nei confronti di un database Accesss.
//        /// </summary>
//        /// <returns>L'oggetto di tipo System.Data.OleDb.OleDbDataAdapter creato.</returns>
//        protected override DbDataAdapter BuildDataAdapter()
//        {
//            OleDbDataAdapter DataAdapterObj = new OleDbDataAdapter();
//            DataAdapterObj.SelectCommand = (OleDbCommand)SelectCommand;
//            return DataAdapterObj;
//        }

//        /// <summary>
//        /// Implementa la logica per costruire un comando ad un database Access.
//        /// </summary>
//        /// <returns>Un'istruzione da eseguire su un database.</returns>
//        protected override DbCommand BuildSelectCommand()
//        {
//            OleDbCommand SelectCommandObj = new OleDbCommand();
//            SelectCommandObj.Connection = (System.Data.OleDb.OleDbConnection)Conn;
//            return SelectCommandObj;
//        }

//        /// <summary>
//        /// Implementa la logica per creare un parametro di un oggetto DbCommand 
//        /// per un database Access.
//        /// </summary>
//        /// <param name="parameterName">Nome del parametro.</param>
//        /// <param name="value">Valore da assegnare al parametro.</param>
//        /// <param name="direction">Direzione del parametro.</param>
//        /// <returns>Il parametro creato</returns>
//        public override DbParameter  CreateParameter(string parameterName, object value, ParameterDirection direction)
//        {
//            throw new NotImplementedException();
//        }

//        /// <summary>
//        /// Inizializza una instanza della classe.
//        /// </summary>
//        /// <param name="connection_string">stringa di connessione ad un database MS Access</param>
//        public AccessConnection(string connection_string)
//            : base(connection_string)
//        {
//        }

//        /// <summary>
//        /// Implementa la logica per determinare l'ID del record inserito nell'ultima operazione 
//        /// di insert.
//        /// </summary>
//        /// <returns>ID del record inserito nel database dall'ultima operazione di insert.</returns>
//        public override int GetLastInsertID()
//        {
//            int lastInsert = 0;
//            OleDbDataReader rdr;
//            if (!this.ConnectionOpen)
//                Open();

//            SelectCommand.CommandText = "SELECT LAST_INSERT_ID();";
//            rdr = (OleDbDataReader)SelectCommand.ExecuteReader();


//            if (rdr.Read())
//                lastInsert = int.Parse(rdr[0].ToString());
//            else
//                lastInsert = 0;

//            rdr.Close();
//            rdr.Dispose();

//            return lastInsert;
//        }


//        /// <summary>
//        /// Restituisce una stringa rappresentante un dato di tipo DateTime conforme ai database 
//        /// MS Access.    
//        /// </summary>
//        /// <param name="date">stringa da formattare</param>
//        /// <returns>stringa formattata</returns>
//        public override string FilterDate(DateTime date)
//        {

//            string day = date.Day.ToString();
//            if (day.Length == 1) day = "0" + day;
//            string month = date.Month.ToString();
//            if (month.Length == 1) month = "0" + month;
//            string year = date.Year.ToString();
//            return "'" + date.Year.ToString() + month + day + "'";
//        }



//        /// <summary>
//        /// Restituisce una stringa rappresentante un dato di tipo DateTime conforme ai database 
//        /// MS Access. 
//        /// </summary>
//        /// <param name="date">stringa da formattare</param>
//        /// <returns>stringa formattata</returns>   
//        public override string FilterDateTime(DateTime date)
//        {

//            string day = date.Day.ToString();
//            if (day.Length == 1) day = "0" + day;

//            string month = date.Month.ToString();
//            if (month.Length == 1) month = "0" + month;

//            string year = date.Year.ToString();
//            return "'" + date.Year.ToString() + month + day + " " + date.Hour + ":" + date.Minute + ":" + date.Second + "'";
//        }
//    }
//}