﻿using System;
using System.Web;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Web.Security;
using System.Configuration;
using System.Collections.Generic;
using System.Diagnostics;

namespace NetCms.Connections
{
    /// <summary>    
    /// Classe astratta con proprietà e metodi di base per accedere ad una sorgente dati ed 
    /// eseguire delle operazioni di lettura e scrittura
    /// </summary>
    /// <remarks>
    /// Questa classe va derivata per ogni tipologia di sorgenti dati utilizzata
    /// </remarks>
    /// </summary>
    public abstract class Connection : IDisposable
    {
        public abstract bool CheckConnection();

        /// <summary>
        /// Controllo della connessione al database
        /// </summary>
        /// <returns>True if no connection, False connection is ok</returns>
        public static bool Check()
        {
            NetCms.Connections.ConnectionsManager ConnManager = new NetCms.Connections.ConnectionsManager();            
            return ConnManager.CommonConnection.CheckConnection();
        }

        #region Cache

        protected NetCms.Connections.Cache.Cache Cache
        {
            get
            {
                if (CacheObj == null)
                {
                    CacheObj = new Cache.Cache(CacheSize);
                }
                return CacheObj;
            }
        }
        private NetCms.Connections.Cache.Cache CacheObj;


        public bool ISWebApp { get; set; }

        private int _CacheSize;
        public int CacheSize
        {
            get { return _CacheSize; }
            set { _CacheSize = value; CacheObj = null; }
        }

        protected bool _CacheEnabled = true;
        public bool CacheEnabled
        {
            get { return _CacheEnabled && HttpContext.Current != null; }
            set { _CacheEnabled = value; }
        }
        #endregion

        /// <summary>
        /// Ottiene la stringa di connessione alla sorgente dati.
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return _ConnectionString;
            }
        }
        private string _ConnectionString;

        /// <summary>
        /// Restituisce la versione crittografata della stringa di connessione utilizzando  
        /// l'algoritmo MD5.        
        /// </summary>
        public string ConnectionStringMD5
        {
            get
            {
                if (_ConnectionStringMD5 == null)
                    _ConnectionStringMD5 = FormsAuthentication.HashPasswordForStoringInConfigFile(ConnectionString, "MD5").ToString();
                return _ConnectionStringMD5;
            }
        }
        private string _ConnectionStringMD5;

        //System.Data.Common.DbTransaction transaction = null;
        ///// <summary>
        ///// Indica se la corrente connessione al database è aperta.
        ///// </summary>
        //public virtual bool ConnectionOpen
        //{
        //    get
        //    {
        //        //return ConnectionOpen;
        //        return Conn.State.ToString().ToLower() == "open";
        //    }
        //}

        /// <summary>
        /// Implementa la logica di connessione al database per una tipologia specifica di sorgente dati.
        /// </summary>
        /// <returns>connessione al database</returns>
        protected abstract DbConnection BuildConnection();
        //private DbConnection ObjConn;
        //protected DbConnection Conn
        //{
        //    get
        //    {
        //        if (ObjConn == null)
        //        {
        //            ObjConn = BuildConnection();
        //        }
        //        return ObjConn;
        //    }
        //}

        public string DatabaseName
        {
            get
            {
                if (_DatabaseName == null)
                {
                    string[] splitter = { "db=", "database=" };
                    string[] splitted = this.ConnectionString.ToLower().Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                    splitted = splitted[1].Split(';');

                    _DatabaseName = splitted[0];
                }
                return _DatabaseName;
            }
        }
        private string _DatabaseName;

        #region DataAdapter
        /// <summary>
        /// Implementa la logica per costruire un comando SQL.
        /// </summary>
        /// <returns>Un'istruzione SQL o una stored procedure da eseguire su un database.</returns>
        protected abstract DbCommand BuildSelectCommand(DbConnection Connection);
        //private DbCommand _SelectCommand;
        //protected DbCommand SelectCommand
        //{
        //    get
        //    {
        //        if (_SelectCommand == null) _SelectCommand = BuildSelectCommand();
        //        return _SelectCommand;
        //    }
        //}
        #endregion

        #region SelectCommand
        /// <summary>
        /// Implementa la logica per costruire un oggetto di tipo System.Data.Common.DbDataAdapter
        /// in grado di eseguire delle istruzioni nei confronti del database associato.
        /// </summary>
        /// <returns>L'oggetto di tipo System.Data.Common.DbDataAdapter creato.</returns>
        protected abstract DbDataAdapter BuildDataAdapter(DbCommand Command);
        //private DbDataAdapter _DataAdapter;
        //protected DbDataAdapter DataAdapter
        //{
        //    get
        //    {
        //        if (_DataAdapter == null) _DataAdapter = BuildDataAdapter();
        //        return _DataAdapter;
        //    }
        //}

        #endregion

        //private bool _LeaveAlive = false;
        ///// <summary>
        ///// Imposta o ottiene il valore per indicare se lasciare aperta la connessione.
        ///// </summary>
        //public bool LeaveAlive
        //{
        //    get
        //    {
        //        return _LeaveAlive;
        //    }
        //    set
        //    {
        //        _LeaveAlive = value;
        //    }
        //}

        /// <summary>
        /// Implementa la logica per creare un parametro di un oggetto DbCommand 
        /// per un specifico tipo di database.
        /// </summary>
        /// <param name="parameterName">Nome del parametro.</param>
        /// <param name="value">Valore da assegnare al parametro.</param>
        /// <param name="direction">Direzione del parametro.</param>
        /// <returns>Il parametro creato</returns>
        public abstract DbParameter CreateParameter(string parameterName, object value, ParameterDirection direction);

        public abstract DbParameter CreateParameter(string parameterName, object value, ParameterDirection direction,DbType tipo);

        /// <summary>
        /// Metodo, con opzione di overload, che implementa la logica per eseguire dei comandi SQL di lettura dati dal database connesso
        /// e restituirli in un oggetto System.Data.DataTable. 
        /// Tale metodo può essere 
        /// </summary>
        /// <param name="commandText">Il comando da eseguire</param>
        /// <param name="commandType">il tipo di comando</param>
        /// <param name="parameters">Eventuali parametri da associare al comado</param>
        /// <returns>Oggetto System.Data.DataTable con i dati restituiti.</returns>
        public virtual DataTable FillData(string commandText, CommandType commandType, List<System.Data.Common.DbParameter> parameters)
        {
            DataTable table = null;
            int tryCount = 0;
            while (tryCount < 3 && table == null)
            {
                tryCount++;
                try
                {
                    table = FillDataWorker(commandText, commandType, parameters);
                }
                catch (Exception e)
                {
                    if (!(e is TimeoutException))
                        tryCount = int.MaxValue;
                    else
                    {
                        if (e is NoDBException || NetCms.Connections.Configurations.Debug.SqlExceptionsEnabled)
                            throw e;
                    }
                }
            }

            return table;
        }

        private DataTable FillDataWorker(string commandText, CommandType commandType, List<System.Data.Common.DbParameter> parameters)
        {
            string debugStringView = commandText;
            try
            {
                using(DbConnection conn = BuildConnection())
                using (DbCommand SelectCommand = BuildSelectCommand(conn))
                using(DbDataAdapter DataAdapter = BuildDataAdapter(SelectCommand))
                {
                    conn.Open();

                    SelectCommand.CommandText = commandText;
                    SelectCommand.CommandType = commandType;


                    if (parameters != null)
                    {
                        SelectCommand.Parameters.Clear();
                        foreach (System.Data.Common.DbParameter parameter in parameters)
                        {
                            SelectCommand.Parameters.Add(parameter);
                            if (debugStringView.Contains("?"))
                            {
                                int index = debugStringView.IndexOf("?");
                                debugStringView = debugStringView.Substring(0, index) +
                                                  parameter.Value +
                                                  debugStringView.Substring(index + 1, (debugStringView.Length) - (index + 1));
                            }
                        }
                    }
                    DataTable dt = new DataTable();
                    DataAdapter.Fill(dt);

                    #region Genero il nome univoco della DataTable

                    dt.TableName = this.BuildCommandAssociatedKey(commandText, commandType, parameters);

                    #endregion

                    return dt;
                }
            }
            catch (Exception e)
            {
                if (e is NoDBException || NetCms.Connections.Configurations.Debug.SqlExceptionsEnabled)
                    throw e;

                string message = "La query '" + commandText + "' ha restituito un errore del tipo: " + e.Message;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, message, "", "", "66");

                if (e.Message.ToLower().Contains("timeout"))
                    throw new TimeoutException();
            }
            return null;
        }

        /// <summary>
        /// Metodo per eseguire un comando SQL verso il database connnesso. 
        /// </summary>
        /// <param name="commandText">Il comando da eseguire</param>
        /// <param name="commandType">il tipo di comando</param>
        /// <param name="parameters">Eventuali parametri da associare al comado</param>        
        /// <param name="traceLog">(Opzionale) Indica se deve essere prodotto un og in caso di esecuzione effettuata con successo</param>        
        /// <returns>Indica se il commando è stato eseguito con successo.</returns>
        public virtual bool ExecuteCommand(string commandText, CommandType commandType, List<System.Data.Common.DbParameter> parameters, bool traceLog = true)
        {
            string sqlquery_log = "";
            bool success = false;
            try
            {

                using(DbConnection conn = BuildConnection())
                using (DbCommand SelectCommand = BuildSelectCommand(conn))
                {
                    conn.Open();

                        SelectCommand.CommandText = commandText;
                        SelectCommand.CommandType = commandType;
                        if (parameters != null)
                        {
                            SelectCommand.Parameters.Clear();
                            foreach (System.Data.Common.DbParameter parameter in parameters)
                            {
                                SelectCommand.Parameters.Add(parameter);
                                sqlquery_log += parameter.ParameterName + ":" + parameter.Value + ";";
                            }
                        }
                        SelectCommand.ExecuteNonQuery();
                        success = true;
                        if (ISWebApp)
                            this.ClearCache();

                        sqlquery_log = commandText + "  " + sqlquery_log;

                        if (traceLog)
                            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Info, sqlquery_log, "", "", "");
                    }
                    return success;
            }
            catch (Exception e)
            {
                if (e is NoDBException || NetCms.Connections.Configurations.Debug.SqlExceptionsEnabled)
                    throw e;

                if (e.Message.Contains("Prepared statement needs to be re-prepared"))
                    throw new NoDBException("");

                string g = e.Message;
                string message = "La query '" + commandText + "' ha restituito un errore del tipo: " + g;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, message, "", "", "67");
                return success;
            }
        }

        public virtual int ExecuteInsertCommand(string commandText, CommandType commandType, List<System.Data.Common.DbParameter> parameters)
        {
            string sqlquery_log = "";
            
            int success = -1;
            try
            {

                using(DbConnection conn = BuildConnection())
                using (DbCommand SelectCommand = BuildSelectCommand(conn))
                {
                    conn.Open();

                        SelectCommand.CommandText = commandText;
                        SelectCommand.CommandType = commandType;
                        if (parameters != null)
                        {
                            SelectCommand.Parameters.Clear();
                            foreach (System.Data.Common.DbParameter parameter in parameters)
                            {
                                SelectCommand.Parameters.Add(parameter);
                                sqlquery_log += parameter.ParameterName + ":" + parameter.Value + ";";
                            }
                        }
                        SelectCommand.ExecuteNonQuery();
                        success = 1;
                        success = this.GetLastInsertID(conn);
                        if (ISWebApp)
                            this.ClearCache();

                        sqlquery_log = commandText + "  " + sqlquery_log;
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Info, sqlquery_log, "", "", "");
                    }
                    return success;
            }
            catch (Exception e)
            {
                if (e is NoDBException || NetCms.Connections.Configurations.Debug.SqlExceptionsEnabled)
                    throw e;

                string g = e.Message;
                string message = "La query '" + commandText + "' ha restituito un errore del tipo: " + g;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, message, "", "", "67");
                return success;
            }
        }

        //public void StartTransaction()
        //{
        //    DisposeTransaction();
        //    transaction = null;
        //    if (!this.ConnectionOpen)
        //        Open();
        //    transaction = this.Conn.BeginTransaction();
        //}

        //public void CommitTransaction()
        //{
        //    if (transaction != null)
        //        transaction.Commit();
        //}

        //public void RollbackTransaction()
        //{
        //    if (transaction != null)
        //        transaction.Rollback();
        //}

        //public void DisposeTransaction()
        //{
        //    if (transaction != null)
        //        transaction.Dispose();
        //    Close();
        //}

        /// <summary>
        /// Costrtuisce una chiave per identificare in modo univoco la combinazione di 
        /// commandText,commandType parameters.
        /// </summary>
        /// <param name="commandText">La stringa di comando.</param>
        /// <param name="commandType">Il tipo di commando.</param>
        /// <param name="parameters">La lista di parametri associati al comando</param>
        /// <returns>chiave che identifica la combinazione dei parametri di ingresso</returns>
        public virtual string BuildCommandAssociatedKey(string commandText, CommandType commandType, List<System.Data.Common.DbParameter> parameters)
        {
            string pars = "";
            if (parameters != null)
                foreach (System.Data.Common.DbParameter par in parameters)
                    pars += (pars.Length > 0 ? "|" : "") + par.ParameterName + "-" + par.Value + "-" + par.Direction;

            return "[" + this.ConnectionStringMD5 + "][" + commandText + "][" + commandType + "]{" + pars + "}";
        }

        /// <summary>
        /// Inizializza una istanza della classe
        /// </summary>
        /// <param name="connectionString">stringa di connessione al database</param>
        public Connection(string connectionString)
        {
            _ConnectionString = connectionString;
            _CacheSize = 7;
            ISWebApp = true;
            /*
            StackTrace st;
            StackFrame sf;
            st = new StackTrace(true);
            sf = st.GetFrame(1);

            string calls = "";
            foreach (StackFrame frame in st.GetFrames())
                calls += "<li>" + frame.ToString() + "</li>";

            calls = "<ul>" + calls + "</ul>";

            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Connessione Creata (" + this.ConnectionString + ") da - " + DateTime.Now.ToString() + "", "0", "0", "1001");
            //NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Connessione Creata da Connection", "0", "0", "1001");*/
        }

        //public virtual bool ConnectionOpen(DbConnection Conn)
        //{
        //    return Conn.State.ToString().ToLower() == "open";
        //}


        ///// <summary>
        ///// Metodo con opzione di overload per aprire una connessione.
        ///// </summary>
        //public virtual void Open(DbConnection Conn)
        //{
        //    if (!ConnectionOpen(Conn)) try
        //        {
        //            Conn.Open();
        //        }
        //        catch (Exception ex)
        //        {
        //            string g = ex.Message;
        //            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Impossibile aprire la connessione (" + g + ")", "0", "0", "1000");
        //            throw new NoDBException("Database non disponibile (" + this.ConnectionString + ")");
        //        }
        //}

        /// <summary>
        /// Metodo con opzione di overload per chiudere una connessione.
        /// </summary>
        //public virtual void Close()
        //{
        //    if (ConnectionOpen)
        //    {
        //        Conn.Close();
        //        Conn.Dispose();
        //        DataAdapter.Dispose();
        //        _DataAdapter = null;
        //        SelectCommand.Dispose();
        //        _SelectCommand = null;
        //        ObjConn = null;
        //    }
        //}

        /// <summary>
        /// Metodo con opzione di overload per eseguire una query di lettura dati.
        /// </summary>
        /// <param name="sqlQueryString">query da eseguire</param>
        /// <returns>Oggetto System.Data.DataTable con i dati restituiti.</returns>
        public virtual DataTable SqlQuery(string sqlQueryString)
        {
            if (CacheEnabled) lock (Cache)
                {
                    if (Cache.Contains(sqlQueryString))
                        return Cache[sqlQueryString];
                }

            DataTable dt = new DataTable(sqlQueryString);

            bool ok = this.FillDataTable(sqlQueryString, dt);

            if (ok && this.CacheEnabled)
            {
                lock (Cache)
                {
                    Cache.Add(dt, sqlQueryString);
                }
            }

            return dt;
        }

        /// <summary>
        /// Metodo con opzione di overload per eseguire una query di lettura dati.
        /// </summary>
        /// <param name="sqlQueryString">query da eseguire</param>
        /// <returns>Oggetto System.Data.DataTable con i dati restituiti.</returns>
        public virtual bool FillDataTable(string sqlQueryString, DataTable dt)
        {
            bool status = false;

            if (dt == null)
            {
                string message = "La query '" + sqlQueryString + "' ha restituito un errore del tipo: " + "La DataTable di ingresso non può essere null";
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, message, "", "");
                throw new Exception(message);
            }
            try
            {
                using (DbConnection conn = BuildConnection())
                using (DbCommand SelectCommand = BuildSelectCommand(conn))
                using (DbDataAdapter DataAdapter = BuildDataAdapter(SelectCommand))
                {
                    conn.Open();
                    SelectCommand.CommandText = @sqlQueryString;
                    SelectCommand.CommandType = CommandType.Text;
                    DataAdapter.Fill(dt);
                    status = true;
                }
            }
            catch (Exception ex)
            {
                string timeout = "Timeout expired.  The timeout period elapsed prior to completion of the operation or the server is not responding.";               

                if (ex is NoDBException || NetCms.Connections.Configurations.Debug.SqlExceptionsEnabled)
                    throw ex;

                if (ex.InnerException != null && ex.InnerException.Message == timeout)
                    throw new NoDBException("La query '{0}' ha restituito un errore del tipo: {1}".FormatByParameters(sqlQueryString, timeout));

                string g = ex.Message;
                string message = "La query '" + sqlQueryString + "' ha restituito un errore del tipo: " + g;


                // Il sottostante trace genera un errore in fase di boot del cms in caso di non disponibilità/errore del db
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, message, "", "", "68");


                throw ex;
            }
            //return false;
            return status;
        }

        /// <summary>
        /// Metodo con opzione di overload per eseguire un comando sql.
        /// </summary>
        /// <param name="sqlQueryString">comando da eseguire</param>
        /// <returns>indica se il comdando è stato eseguito con successo.</returns>
        public virtual bool Execute(string sqlQueryString, bool raiseException = false)
        {
            return RealExecute(sqlQueryString, false, raiseException) > 0;
        }

        /// <summary>
        /// Metodo con opzione di overload per eseguire un comando sql di insert.
        /// </summary>
        /// <param name="sqlQueryString">comando da eseguire</param>
        /// <returns>Se >0 rappresenta l'ID del record inserito. Se -1 indica un errore durante l'esecuzione del comando.</returns>
        public virtual int ExecuteInsert(string sqlQueryString, bool raiseException = false)
        {
            return RealExecute(sqlQueryString, true, raiseException);
        }

        protected virtual int RealExecute(string sqlQueryString, bool IsInsertQuery, bool raiseException = false)
        {
            int success = -1;
            try
            {
                using (DbConnection conn = BuildConnection())
                using (DbCommand SelectCommand = BuildSelectCommand(conn))
                {
                    conn.Open();
                    SelectCommand.CommandText = sqlQueryString;
                    SelectCommand.ExecuteNonQuery();
                    //success = 1; - l'ho commentato in quanto dava problemi in caso le query sopra andavano in errore causando l'inserimento o l'update del record con id = 1 - l'avevo sistemato molto tempo fa ma si è ripresentato.
                    this.ClearCache();
                    if (IsInsertQuery)
                        success = this.GetLastInsertID(conn);

                    //if (!keepConnectionAlive && !LeaveAlive)
                    //    Close();
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Info, sqlQueryString, "", "", "");
                }
            }

            catch (Exception e)
            {
                if (e is NoDBException || NetCms.Connections.Configurations.Debug.SqlExceptionsEnabled || raiseException)
                    throw e;

                string g = e.Message;
                string message = "La query '" + sqlQueryString + "' ha restituito un errore del tipo: " + g;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, message, "", "", "172");
                return success;
            }
            return success;
        }

        public DbDataReader GetReader(string sqlQueryString, bool raiseException = false)
        {
            
            try
            {

                using(DbConnection conn = BuildConnection())
                using (DbCommand SelectCommand = BuildSelectCommand(conn))
                {
                    conn.Open();
                    SelectCommand.CommandText = @sqlQueryString;
                    SelectCommand.CommandType = CommandType.Text;
                    return SelectCommand.ExecuteReader();
                }
            }
            catch (Exception ex)
            {
                if (ex is NoDBException || NetCms.Connections.Configurations.Debug.SqlExceptionsEnabled)
                    throw ex;

                string g = ex.Message;
                string message = "La query '" + sqlQueryString + "' ha restituito un errore del tipo: " + g;
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, message, "", "", "68");
            }
            return null;
        }

        /// <summary>
        /// Implementa la logica per determinare l'ID del record inserito nell'ultima operazione 
        /// di insert.
        /// </summary>
        /// <returns>ID del record inserito nel database dall'ultima operazione di insert.</returns>
        public abstract int GetLastInsertID(DbConnection Connection);

        /// <summary>
        /// Rimuove il contenuto dell'oggetto cache. 
        /// </summary>
        public void ClearCache()
        {
            if (CacheEnabled && Cache != null)
                Cache.Clear();
        }

        /// <summary>
        /// Restituisce una stringa formattata come Date. 
        /// </summary>
        /// <param name="date">stringa da formattare</param>
        /// <returns>stringa formattata come data.</returns>
        public string FilterDate(string date)
        {
            DateTime dateTime;

            if (DateTime.TryParse(date, out dateTime))
            {
                return FilterDate(dateTime);
            }
            else return "";
        }

        /// <summary>
        /// Restituisce una stringa formattata come DateTime. 
        /// </summary>
        /// <param name="date">stringa da formattare</param>
        /// <returns>stringa formattata</returns>
        public string FilterDateTime(string date)
        {
            DateTime dateTime;

            if (DateTime.TryParse(date, out dateTime))
            {
                return FilterDateTime(dateTime);
            }
            else return "";
        }

        /// <summary>
        /// Restituisce una stringa rappresentante un dato di tipo DateTime conforme al tipo di database
        /// di riferimento per la classe derivata        
        /// </summary>
        /// <param name="date">stringa da formattare</param>
        /// <returns>stringa formattata</returns>
        public abstract string FilterDate(DateTime date);

        /// <summary>
        /// Restituisce una stringa rappresentante un dato di tipo DateTime conforme al tipo di database
        /// di riferimento per la classe derivata        
        /// </summary>
        /// <param name="date">stringa da formattare</param>
        /// <returns>stringa formattata</returns>   
        public abstract string FilterDateTime(DateTime date);

        public static string FilterString(string value)
        {
            string strValue = value;
            strValue = strValue.Replace("'", "''"); // Most important one! This line alone can prevent most injection attacks
            strValue = strValue.Replace("--", "");
            strValue = strValue.Replace("[", "[[]");
            strValue = strValue.Replace("%", "[%]");
            return strValue;
        }

        public void Dispose()
        {
            
        }
    }
}
