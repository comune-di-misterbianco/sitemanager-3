using System;
using System.Web;
using System.Collections;
using System.Collections.Generic; 
using System.Data;
using NetCms.Connections;

namespace NetCms.Connections
{
    public class ConnectionsManager : IDisposable
    {
        private const string CurrentNetworkSystemNameItemsKey = "SM2_CurrentNetworkSystemName";
        private const string CurrentGlobalConnectionKey = "SM2_CurrentGlobalConnection";
        private const string CurrentMySqlGlobalConnectionKey = "SM2_CurrentMySqlGlobalConnection";
        private string _externalAddressToConnect = "";
        public ConnectionsManager()
        { 
        
        }
        //questo costruttore � stato aggiunto per permettere di poter eseguire delle operazione su un database specificato e passato al costruttore che sia diverso dal db principale
        public ConnectionsManager(string externalAddressToConnect)
        {
            this._externalAddressToConnect = externalAddressToConnect;
        }

        //Common Connection
        public Connection CommonConnection
        {
            get
            {

                //if (HttpContext.Current != null && HttpContext.Current.Items != null)
                //{
                //    var Store = HttpContext.Current.Items;
                //    if (Store[CurrentGlobalConnectionKey] == null)
                //    {
                //        Connection Connection = new MySqlConnection(NetCms.Configurations.ConnectionsStrings.MainDB);
                //        Store[CurrentGlobalConnectionKey] = Connection;
                //    }
                //    return (Connection)Store[CurrentGlobalConnectionKey];
                //}

                //se la propriet� privata non viene valorizzata il comportamento rimane invariato e la connection viene eseguita sul mainDB. 
                //Se invece ha un valore, la connection sar� eseguita sul db passato come parametro
                if(string.IsNullOrEmpty(_externalAddressToConnect))
                    return new MySqlConnection(NetCms.Configurations.ConnectionsStrings.MainDB);
                else
                    return new MySqlConnection(_externalAddressToConnect);
            }
        }


        public void Dispose()
        {
            //throw new NotImplementedException();
            
        }
    }
}