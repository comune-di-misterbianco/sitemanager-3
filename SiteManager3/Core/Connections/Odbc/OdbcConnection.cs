//using System;
//using System.Data;
//using System.IO;
//using System.Data.Common;
//using System.Data.Odbc;
//using System.Configuration;


/////Classe deprecata. Non usiamo pi� connessioni ODBC
//namespace NetCms.Connections
//{
//    /// <summary>
//    /// Deriva la classe base <see cref="NetCms.Connections.Connection"></see> implementando i metodi e propriet�
//    /// per gestire la connessione e comandi per una sorgente dati ODBC.
//    /// </summary>     
//    public class OdbcConnection : Connection
//    {
//        /// <summary>
//        /// Implementa la logica di connessione ad una sorgente dati ODBC restituendo un oggetto
//        /// di connessione specifico System.Data.Odbc.OdbcConnection.
//        /// </summary>
//        /// <returns>connessione alla sorgente dati ODBC</returns>
//        protected override DbConnection BuildConnection()
//        { 
//            return new System.Data.Odbc.OdbcConnection(this.ConnectionString);
//        }

//        /// <summary>
//        /// Implementa la logica per costruire un oggetto specifico di tipo System.Data.Common.DbDataAdapter
//        /// in grado di eseguire delle istruzioni nei confronti di una sorgente dati ODBC.
//        /// </summary>
//        /// <returns>L'oggetto di tipo System.Data.ODBC.OdbcDataAdapter creato.</returns>
//        protected override DbDataAdapter BuildDataAdapter()
//        {
//            OdbcDataAdapter DataAdapterObj = new OdbcDataAdapter();
//            DataAdapterObj.SelectCommand = (OdbcCommand)SelectCommand;
//            return DataAdapterObj;
//        }

//        /// <summary>
//        /// Implementa la logica per creare un parametro di un oggetto DbCommand 
//        /// per una sorgente dati ODBC.
//        /// </summary>
//        /// <param name="parameterName">Nome del parametro.</param>
//        /// <param name="value">Valore da assegnare al parametro.</param>
//        /// <param name="direction">Direzione del parametro.</param>
//        /// <returns>Il parametro creato</returns>
//        public override DbParameter CreateParameter(string parameterName, object value, ParameterDirection direction)
//        {
//            OdbcParameter parameter = new OdbcParameter();
//            parameter.ParameterName = parameterName;
//            parameter.Value = value;
//            parameter.Direction = direction;
//            if (value == null)
//            {
//                parameter.Value = DBNull.Value;
//            }
//            return parameter;
//        }

//        /// <summary>
//        /// Implementa la logica per costruire un comando ad una sorgente dati ODBC.
//        /// </summary>
//        /// <returns>Un'istruzione da eseguire sulla sorgente dati.</returns>
//        protected override DbCommand BuildSelectCommand()
//        {
//            OdbcCommand SelectCommandObj = new OdbcCommand();
//            SelectCommandObj.Connection = (System.Data.Odbc.OdbcConnection)Conn;
//            return SelectCommandObj;
//        }

//        /// <summary>
//        /// Inizializza una instanza della classe.
//        /// </summary>
//        /// <param name="connection_string">stringa di connessione ad una sorgente dati ODBC.</param>
//        public OdbcConnection(string connection_string)
//            : base(connection_string)
//        {
//        }

//        /// <summary>
//        /// Implementa la logica per determinare l'ID del record inserito nell'ultima operazione 
//        /// di insert.
//        /// </summary>
//        /// <returns>ID del record inserito nel database dall'ultima operazione di insert.</returns>
//        public override int GetLastInsertID()
//        {
//            int lastInsert = 0;
//            bool keepConnectionAlive = false;
//            OdbcDataReader rdr;
//            if (!this.ConnectionOpen)
//                Open();
//            else
//                keepConnectionAlive = true;
//            SelectCommand.CommandText = "SELECT LAST_INSERT_ID();";
//            rdr = (OdbcDataReader)SelectCommand.ExecuteReader();


//            if (rdr.Read())
//                lastInsert = int.Parse(rdr[0].ToString());
//            else
//                lastInsert = 0;

//            rdr.Close();
//            rdr.Dispose();

//            if (!keepConnectionAlive) Close();

//            return lastInsert;
//        }

//        /// <summary>
//        /// Restituisce una stringa rappresentante un dato di tipo DateTime conforme a sorgenti dati ODBC.
//        /// </summary>
//        /// <param name="date">stringa da formattare</param>
//        /// <returns>stringa formattata</returns>
//        public override string FilterDate(DateTime date)
//        {
//            return OdbcFilterDate(date);
//        }

//        /// <summary>
//        /// Restituisce una stringa rappresentante un dato di tipo DateTime conforme a sorgenti dati ODBC.
//        /// </summary>
//        /// <param name="date">stringa da formattare</param>
//        /// <returns>stringa formattata</returns>   
//        public override string FilterDateTime(DateTime date)
//        {
//            return OdbcFilterDateTime(date);
//        }

//        /// <summary>
//        /// Restituisce una stringa rappresentante un dato di tipo DateTime conforme a sorgenti dati ODBC.
//        /// </summary>
//        /// <param name="date">stringa da formattare</param>
//        /// <returns>stringa formattata</returns>
//        public static string OdbcFilterDate(DateTime date)
//        {

//            string day = date.Day.ToString();
//            if (day.Length == 1) day = "0" + day;
//            string month = date.Month.ToString();
//            if (month.Length == 1) month = "0" + month;
//            string year = date.Year.ToString();
//            return "'" + date.Year.ToString() + month + day + "'";
//        }

//        /// <summary>
//        /// Restituisce una stringa rappresentante un dato di tipo DateTime conforme a sorgenti dati ODBC.
//        /// </summary>
//        /// <param name="date">stringa da formattare</param>
//        /// <returns>stringa formattata</returns>   
//        public static string OdbcFilterDateTime(DateTime date)
//        {
//            string day = date.Day.ToString();
//            if (day.Length == 1) day = "0" + day;

//            string month = date.Month.ToString();
//            if (month.Length == 1) month = "0" + month;

//            string year = date.Year.ToString();
//            return "'" + date.Year.ToString() + "/" + month + "/" + day + " " + date.Hour + ":" + date.Minute + ":" + date.Second + "'";
//        }
//    }
//}