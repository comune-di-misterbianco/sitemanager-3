﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetCms.Connections
{
    public class TimeoutException : System.Exception
    {
        public TimeoutException()
        { }

        public TimeoutException(string message)
            : base(message)
        { }

        public void TraceError(Exception ex, string username="", string userid="")
        {
            System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);
            System.Diagnostics.StackFrame sf = st.GetFrame(0);

            NetCms.Diagnostics.Diagnostics.TraceUnhandledError(sf, ex.Message, username, userid, ex);
        }
    }
}
