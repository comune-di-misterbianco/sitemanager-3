using System;
using System.Data;
using System.Collections;
using NetCms.Connections;



namespace NetCms.Connections.Cache
{   
    
    /// <summary>
    /// Implementa funzionalit� di gestione di un pool di cache
    /// </summary>
    public partial class CachePool
    {
        /// <summary>
        /// Fornisce funzionalit� standard per la creazione e la gestione di un insieme di oggetti oggetti <see cref="System.Data.DataTable"></see> 
        /// ordinato. 
        /// </summary>
        public class DataTableCollection : IDisposable
        {
            private NsvGenericCollection coll;
            /// <summary>
            /// Ritorna il numero di oggetti nella collezione 
            /// </summary>
            public int Count { get { return coll.Count; } }

            /// <summary>
            /// Inizializza una instanza della classe
            /// </summary>
            public DataTableCollection()
            {
                //
                // TODO: Add constructor logic here
                //
                coll = new NsvGenericCollection();
            }
            /// <summary>
            /// Rimuove tutti gli oggetti della collezione, liberando anche le risorse degli oggetti DataTable.
            /// </summary>
            public void Dispose()
            {
                for (int i = 0; i < this.Count; i++)
                {
                    DataTable table = this[i];
                    this.Remove(table.TableName);
                    table.Dispose();
                }
            }

            /// <summary>
            /// Aggiunge alla lista un nuovo oggetto di tipo <see cref="System.Data.DataTable"> </see>
            /// </summary>
            /// <param name="table">Oggetto da aggiungere alla lista</param>
            /// <param name="key">Chiave legata all'oggetto</param>
            public void Add(DataTable table, string key)
            {
                coll.Add(key, table);
            }


            /// <summary>
            /// Aggiunge alla lista un nuovo oggetto di tipo <see cref="System.Data.DataTable"> </see>
            /// </summary>
            /// <param name="table">Oggetto da aggiungere alla lista</param>          
            public void Add(DataTable table)
            {
                coll.Add(table.TableName, table);
            }

            /// <summary>
            /// Rimuove l'oggetto dalla lista relativo all'indice specificato.
            /// </summary>
            /// <param name="pos">Indice dell'oggetto da rimuovere.</param>
            public void Remove(int pos)
            {
                coll.Remove(this[pos].TableName);
            }

            /// <summary>
            /// Rimuove un oggetto dalla lista relativo alla chiave specificata.
            /// </summary>
            /// <param name="TableName">Chiave per accedere all'oggetto da rimuovere.</param>
            public void Remove(string TableName)
            {
                coll.Remove(TableName);
            }


            /// <summary>
            /// Rimuove tutti gli oggetti  dalla collezione
            /// </summary>
            public void Clear()
            {
                coll.Clear();
            }

            /// <summary>
            /// Permette di accedere ad un oggetto della collezione tramite un indice.
            /// </summary>
            /// <param name="i">indice della lista in base zero dell'oggetto da restituire.</param>
            /// <returns>Oggetto contenuto alla posizione specificata della lista.</returns>
            public DataTable this[int i]
            {
                get
                {
                    DataTable obj = (DataTable)coll[coll.Keys[i]];
                    return obj;
                }
            }

            /// <summary>
            /// Permette di accedere ad un oggetto della collezione tramite la chiave associata 
            /// all'oggetto.
            /// </summary>
            /// <param name="str">Chiave dell'oggetto da restiture.</param>
            /// <returns>Oggetto associato alla chiave specificata.</returns>
            public DataTable this[string TableName]
            {
                get
                {
                    DataTable obj = (DataTable)coll[TableName];
                    return obj;
                }
            }

            /// <summary>
            /// Indica se la collezione contiene un oggetto con la chiave specificata.
            /// </summary>
            /// <param name="key">Chiave dell'oggetto</param>
            /// <returns>Vero se la collezione contiene l'oggetto, falso altrimenti.</returns>
            public bool Contains(string TableName)
            {
                return coll[TableName] != null;
            }
            #region Enumerator


            /// <summary>
            /// Restituisce un enumeratore che scorre un insieme.
            /// </summary>
            /// <returns>Oggetto IEnumerator che pu� essere utilizzato per scorrere l'insieme.</returns>
            public IEnumerator GetEnumerator()
            {
                return new CollectionEnumerator(this);
            }

            private class CollectionEnumerator : IEnumerator
            {
                private int CurentPos = -1;
                private DataTableCollection Collection;
                public CollectionEnumerator(DataTableCollection coll)
                {
                    Collection = coll;
                }
                public object Current
                {
                    get
                    {
                        return Collection[CurentPos];
                    }
                }
                public bool MoveNext()
                {
                    if (CurentPos < Collection.Count - 1)
                    {
                        CurentPos++;
                        return true;
                    }
                    else
                        return false;
                }
                public void Reset()
                {
                    CurentPos = -1;
                }
            }
            #endregion
        }
    }
}