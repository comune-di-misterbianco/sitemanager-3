using System;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Configuration;


namespace NetCms.Connections.Cache
{
    public class Cache : IDisposable
    {
        private CachePool DirtyTables;
        private CachePool CleanTables;

        private int CacheSize;

        public Cache(int Size)
        {
            CacheSize = Size;
            initComponents();
        }

        public void initComponents()
        {
            DirtyTables = new CachePool(CacheSize);
            CleanTables = new CachePool(0);
        }

        public void Dispose()
        {
            CleanTables.Dispose();
            CleanTables = null;
            DirtyTables.Dispose();
            DirtyTables = null;
        }

        public void Add(DataTable table, string sqlQuery)
        {
            bool clear = !sqlQuery.ToLower().Contains("where");

            if (clear)
                CleanTables.Add(table, sqlQuery);
            else
                DirtyTables.Add(table, sqlQuery);
        }

        public void Clear()
        {
            DirtyTables.Dispose();
            CleanTables.Dispose();
            initComponents();
        }

        public bool Contains(string sqlQuery)
        {
            if(CleanTables.Contains(sqlQuery))
                return true;

            if (this.DirtyTables.Contains(sqlQuery))
                return true;

            return false;
        }

        public DataTable this[string sqlQuery]
        {
            get
            {
                if (this.CleanTables.Contains(sqlQuery))
                    return (DataTable)CleanTables[sqlQuery];

                if (this.DirtyTables.Contains(sqlQuery))
                    return (DataTable)this.DirtyTables[sqlQuery];
                
                return null;
            }
        }
        /*
        public string HtmlCacheOverview()
        {
            string overview = "<strong>Cache Hit = " + HitCount + "</strong><br />";
            overview += "<strong>Clean Hit Tot. = " + (HitCount + CleanTables.Tables.Count) + "</strong><br /><br />";
            overview += "<strong>Clean Tables</strong><ol>";

            foreach (DataTable table in CleanTables.Tables)
                overview += "<li>"+table.TableName+"</li>";
               
            overview += "</ol>";

            overview += "<strong>Dirty Tables</strong><ol>";

            foreach (DataTable table in Tables)
                if(table!=null)
                    overview += "<li>" + table.TableName + "</li>";

            overview += "</ol>";

            return overview;

        }*/
    }
}