using System;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Configuration;


/// <summary>
/// Summary description for AccessConnection
/// </summary>
/// 
namespace NetCms.Connections.Cache
{
    /// <summary>
    /// Implementa funzionalit� di gestione di un pool di cache
    /// </summary>
    public partial class CachePool : IDisposable
    {
        /// <summary>
        /// Collezione di oggetti <see cref="System.Data.DataTable"></see> 
        /// </summary>
        private G2Core.Caching.Cache Tables;

        private int _HitCount;
        /// <summary>
        /// Ritorna il numero di volte che un oggetto della collezione 
        /// � stato acceduto.
        /// </summary>
        public int HitCount
        {
            get { return _HitCount; }
        }
	
        private int CacheSize;
        private int Pos
        {
            get
            {
                return _Pos;
            }
            set
            {
                _Pos = CacheSize<1||value < CacheSize ? value : value % CacheSize;
            }
        }
        private int _Pos;

        /// <summary>
        /// Inizializza una nuova istanza della classe indicando la misura della cache.
        /// </summary>
        /// <param name="Size"></param>
        public CachePool(int Size)
        {
            CacheSize = Size;
            initComponents();
        }

        /// <summary>
        /// Inilializza gli elementi della classe;
        /// </summary>
        public void initComponents()
        {
            Tables = new G2Core.Caching.Cache();
            Pos = 0;
        }

        /// <summary>
        /// Rimuove gli elementi della collezione Tables e rilascia le risorse ad esse associate.
        /// </summary>
        public void Dispose()
        {
            Tables.RemoveAll("key LIKE '%ConnectionCache%'");
        }

        
        /// <summary>
        /// Aggiunge alla lista un nuovo oggetto indicando la chiave con cui riferenziarlo
        /// e l'oggetto da inserire di tipo <see cref="System.Data.DataTable"> </see>
        /// </summary>
        /// <param name="table">Oggetto da aggiungere</param>
        /// <param name="sqlQuery">Chiave legata all'oggetto</param>
        public void Add(DataTable table, string sqlQuery)
        {

            if (CacheSize > 0 && Pos == CacheSize)
                Tables.Remove(0);

            table.TableName = sqlQuery;
            Tables.Insert(BuildTableKey(sqlQuery), table, 20);

            Pos++;
        }

        /// <summary>
        /// Rimuove tutti gli oggetti  dalla collezione
        /// </summary>
        public void Clear()
        {
            Tables.RemoveAll("");
            Tables = null;
            initComponents();
        }


        /// <summary>
        /// Permette di accedere ad un oggetto della collezione tramite la chiave associata 
        /// all'oggetto.
        /// </summary>
        /// <param name="sqlQuery">Chiave dell'oggetto da restiture.</param>
        /// <returns>Oggetto associato alla chiave specificata.</returns>
        public DataTable this[string sqlQuery]
        {
            get
            {
                string key = BuildTableKey(sqlQuery);
                if (Tables.Contains(key))
                {
                    _HitCount++;
                    return (DataTable)Tables[key];
                }
                
                return null;
            }
        }

        private string BuildTableKey(string sqlQuery)
        {
            string key = "";

            key += CacheKeyHeader + "[" + sqlQuery.Replace("'", "").Replace(".", "") + "]";

            return key;
        }

        /// <summary>
        /// Indica se la collezione contiene un oggetto con la chiave specificata.
        /// </summary>
        /// <param name="sqlQuery">Chiave dell'oggetto che corrisponde ad una query</param>
        /// <returns>Vero se la collezione contiene l'oggetto, falso altrimenti.</returns>
        public bool Contains(string sqlQuery)
        {
            return (Tables.Contains(BuildTableKey(sqlQuery)));
        }

        public string CacheKeyHeader
        {
            get
            {
                if (_CacheKeyHeader == null)
                    _CacheKeyHeader = G2Core.Caching.Visibility.CachingVisibilityUtility.BuildVisibilityKey(G2Core.Caching.Visibility.CachingVisibility.Global) + "[ConnectionCache]";
                return _CacheKeyHeader;
            }
        }
        private string _CacheKeyHeader;

			
			
    }
}