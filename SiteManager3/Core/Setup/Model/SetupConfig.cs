﻿using NetCms.Diagnostics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Setup.BusinessLogic;

namespace Setup.Model
{
    public static class SetupConfig
    {
        public static string PortalName { get; set; }
        public static string PortalEmail { get; set; }
        public static string PortalRootPath { get; set; }

        public static string AbsoluteCMSFolderPath {
            get
            {
                string applicationFolder = HttpRuntime.AppDomainAppPath;
                DirectoryInfo currentFolder = new DirectoryInfo(applicationFolder);
                return currentFolder.FullName.ToString();                
            }
        }

        public static string AbsoluteCMSParentFolderPath
        {
            get
            {
                string applicationFolder = HttpRuntime.AppDomainAppPath;
                DirectoryInfo currentFolder = new DirectoryInfo(applicationFolder);
                return currentFolder.Parent.FullName.ToString();
            }
        }

        public static string AbsoluteLogsFolderPath { get; set; }

        public static string SmtpHost { get; set; }
        public static string SmtpPort { get; set; }
        public static string SmtpUser { get; set; }
        public static string SmtpPw { get; set; }
        public static string SmtpSsl { get; set; }


        public static bool ConfigXMLParsed { get; set; }
        public static bool WebConfigParsed { get; set; }
        public static bool DbImported { get; set; }

        public static bool ConfigXmlFileExist { get; set; }
        public static bool WebConfigFileExist { get; set; }
             
        public static CmsStatus Stato
        {
            get
            {
                if (FileParser.WebConfigIsParsed())
                {
                    if (DbUtils.GetDBTableCount() > 150)
                        return CmsStatus.Installed;
                }   
                return CmsStatus.NotInstalled;
                //}
                //else
                //{
                //    return CmsStatus.NotInstalled;
                //}

            }
        }

        public enum CmsStatus
        {
            Installed,
            NotInstalled            
        }

    }
}
