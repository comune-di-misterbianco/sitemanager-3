﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Setup.Model
{
    public static class DBConfig
    {
        public static string DbName { get; set; }
        public static string DbHost { get; set; }
        public static string DbUser { get; set; }
        public static string DbPw { get; set; }
        public static string DbPort { get; set; }

        private static string _DBConn;
        public static string SetupDBConn
        {
            get
            {
                if (_DBConn == null)
                {
                    if (System.Web.Configuration.WebConfigurationManager.ConnectionStrings["CmsStringModel"] == null)
                    {
                        throw new Exception("Stringa di connessione al database non specificata nel file 'Web.config'");                       
                    }
                    else
                    {
                        string strConn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["CmsStringModel"].ToString();
                        _DBConn = strConn;
                    }
                }
                return _DBConn;
            }
        }

        private static string _CmsDBConn;
        public static string CmsDBConn
        {
            get
            {
                if (_CmsDBConn == null)
                {
                    if (System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnectionString"] == null)
                    {
                        Exception ex = new Exception("Stringa di connessione al database non specificata nel file 'Web.config'");                        
                    }
                    else
                    {
                        string strConn = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
                        _CmsDBConn = strConn;
                    }
                }
                return _CmsDBConn;
            }
        }    

        public static string GetStringConn()
        {
            string strConn = SetupDBConn.Replace("{#**DbName**#}", DbName);
            strConn = strConn.Replace("{#**DbHost**#}", DbHost);
            strConn = strConn.Replace("{#**DbUser**#}", DbUser);
            strConn = strConn.Replace("{#**DbPw**#}", DbPw);
            strConn = strConn.Replace("{#**DbPort**#}", DbPort);

            return strConn;
        }

        public enum Column
        {
            DbName, DbHost, DbUser, DbPw, DbPort
        }
    }




}
