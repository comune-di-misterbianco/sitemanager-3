﻿using NetService.Utility.ValidatedFields;
using Setup.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Setup.Forms
{
    public class DbSettings : VfManager
    {
        public DbSettings(string controlId) : base (controlId)
        {
            this.Fields.Add(DbHost);
            this.Fields.Add(DbPort);
            this.Fields.Add(DbName);
            this.Fields.Add(DbUser);
            this.Fields.Add(DbPassword);
        }

        private VfTextBox DbHost
        {
            get
            {
                if (_DbHost == null)
                {
                    _DbHost = new VfTextBox(DBConfig.Column.DbHost.ToString(), "Indirizzo database server", DBConfig.Column.DbHost.ToString(), InputTypes.Text);
                    _DbHost.Required = true;
                    _DbHost.MaxLenght = 50;
                }
                return _DbHost;
            }
        }
        private VfTextBox _DbHost;

        private VfTextBox DbPort
        {
            get
            {
                if (_DbPort == null)
                {
                    _DbPort = new VfTextBox(DBConfig.Column.DbPort.ToString(), "Porta TCP", DBConfig.Column.DbPort.ToString(), InputTypes.Number);
                    _DbPort.Required = true;
                    _DbPort.MaxLenght = 5;
                }
                return _DbPort;
            }
        }
        private VfTextBox _DbPort;

        private VfTextBox DbName
        {
            get
            {
                if (_DbName == null)
                {
                    _DbName = new VfTextBox(DBConfig.Column.DbName.ToString(), "Nome database", DBConfig.Column.DbName.ToString(), InputTypes.Text);
                    _DbName.Required = true;
                    _DbName.MaxLenght = 50;
                }
                return _DbName;
            }
        }
        private VfTextBox _DbName;

        private VfTextBox DbUser
        {
            get
            {
                if (_DbUser == null)
                {
                    _DbUser = new VfTextBox(DBConfig.Column.DbUser.ToString(), "Nome utente", DBConfig.Column.DbUser.ToString(), InputTypes.Text);
                    _DbUser.Required = true;
                    _DbUser.MaxLenght = 25;
                }
                return _DbUser;
            }
        }
        private VfTextBox _DbUser;

        private VfTextBox DbPassword
        {
            get
            {
                if (_DbPassword == null)
                {
                    _DbPassword = new VfTextBox(DBConfig.Column.DbPw.ToString(), "Password", DBConfig.Column.DbPw.ToString(), InputTypes.Text);
                    _DbPassword.Required = true;
                    _DbPassword.MaxLenght = 25;
                }
                return _DbPassword;
            }
        }
        private VfTextBox _DbPassword;
    }
}
