﻿using NetCms.Diagnostics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Setup.Model;

namespace Setup.BusinessLogic
{
    public static class DbUtils
    {

        public static bool ImportDBFromFile(string scriptRelativePath)
        {
            bool esito = false;

            MySql.Data.MySqlClient.MySqlConnection MySqlConn = new MySql.Data.MySqlClient.MySqlConnection(DBConfig.GetStringConn());
            try
            {
                string dbFilePath = HttpContext.Current.Server.MapPath(scriptRelativePath);
                MySql.Data.MySqlClient.MySqlScript script = new MySql.Data.MySqlClient.MySqlScript(MySqlConn, File.ReadAllText(dbFilePath));
                script.Execute();
                esito = true;
            }
            catch (Exception ex)
            {
                Diagnostics.TraceMessage(TraceLevel.Error, ex.Message);
            }

            return esito;
        }

        public static bool CheckDbConn()
        {
            bool esito = false;
            MySql.Data.MySqlClient.MySqlConnection mySqlConn = new MySql.Data.MySqlClient.MySqlConnection(DBConfig.GetStringConn());

            try
            {
                mySqlConn.Open();
                esito = mySqlConn.Ping();
                mySqlConn.Close();
                //ConnStatus.Text = esito ? "Connessione eseguita con successo" : "Connessione fallita";
            }
            catch (Exception ex)
            {
                Diagnostics.TraceMessage(TraceLevel.Error, "Connessione al database " + Model.DBConfig.DbName + " fallita" + ex.Message);
            }
            return esito;
        }

        public static int GetDBTableCount()
        {
            int tableCount = -1;
            string results = string.Empty;
            

            MySql.Data.MySqlClient.MySqlConnection mySqlConn = new MySql.Data.MySqlClient.MySqlConnection(DBConfig.CmsDBConn);
            try
            {
                string sql = "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '" + mySqlConn.Database + "'";

                mySqlConn.Open();

                MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(sql, mySqlConn);
                object res = cmd.ExecuteScalar();
                if (res != null)
                {
                    tableCount = Convert.ToInt32(res);
                }
                
                mySqlConn.Close();
            }
            catch(Exception ex)
            {
                Diagnostics.TraceMessage(TraceLevel.Error, "Errore durante il conteggio delle tabelle del database " + Model.DBConfig.DbName + ex.Message);
            }

            return tableCount;
        }
    }
}
