﻿using NetCms.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Setup.Model;

namespace Setup.BusinessLogic
{
    public static class FileParser
    {
        private static string configFolderPath
        {
            get
            {
                return SetupConfig.AbsoluteCMSFolderPath + "\\Configs";
            }
        }

        public static string webconfigFilePath
        {
            get
            {
                return SetupConfig.AbsoluteCMSFolderPath + "\\web.config";
            }
        }

        public static string configFilePath
        {
            get
            {
                return configFolderPath + "\\config.xml";
            }
        }

        public static string ParseWebConfigFile()
        {
            string esito = "";
            try
            {
                string cmsWebConfig = SetupConfig.AbsoluteCMSParentFolderPath + "\\web.config";
                var fileContents = System.IO.File.ReadAllText(cmsWebConfig);

                System.IO.File.WriteAllText(SetupConfig.AbsoluteCMSParentFolderPath + "\\" + "webconfig.bak", fileContents);

                fileContents = fileContents.Replace("{#**PortalName**#}", SetupConfig.PortalName);
                fileContents = fileContents.Replace("{#**PortalEmail**#}", SetupConfig.PortalEmail);
                fileContents = fileContents.Replace("{#**PortalRootPath**#}", SetupConfig.PortalRootPath);

                fileContents = fileContents.Replace("{#**AbsoluteCMSFolderPath**#}", SetupConfig.AbsoluteCMSParentFolderPath);
                fileContents = fileContents.Replace("{#**AbsoluteLogsFolderPath**#}", SetupConfig.AbsoluteLogsFolderPath);

                fileContents = fileContents.Replace("{#**DbName**#}", DBConfig.DbName);
                fileContents = fileContents.Replace("{#**DbHost**#}", DBConfig.DbHost);
                fileContents = fileContents.Replace("{#**DbUser**#}", DBConfig.DbUser);
                fileContents = fileContents.Replace("{#**DbPw**#}", DBConfig.DbPw);
                fileContents = fileContents.Replace("{#**DbPort**#}", DBConfig.DbPort);

                fileContents = fileContents.Replace("{#**SmtpHost**#}", SetupConfig.SmtpHost);
                fileContents = fileContents.Replace("{#**SmtpPort**#}", SetupConfig.SmtpPort);
                fileContents = fileContents.Replace("{#**SmtpUser**#}", SetupConfig.SmtpUser);
                fileContents = fileContents.Replace("{#**SmtpPw**#}", SetupConfig.SmtpPw);
                fileContents = fileContents.Replace("{#**SmtpSsl**#}", SetupConfig.SmtpSsl);

                System.IO.File.WriteAllText(cmsWebConfig, fileContents);

                SetupConfig.WebConfigParsed = true;

                esito = "<div class=\"alert alert-success\">Web.Config processato correttamente!</div>";
            }
            catch (Exception ex)
            {
                SetupConfig.WebConfigParsed = false;
                esito = "<div class=\"alert alert-danger\">Errore durante il processamento del file Web.Config! " + ex.Message + "</div>";
            }
            return esito;
        }

        public static string ParseConfigFile()
        {
            string esito = "";
            try
            {
                string cmsConfigXml = SetupConfig.AbsoluteCMSParentFolderPath + "\\configs\\config.xml";
                var fileContents = System.IO.File.ReadAllText(cmsConfigXml);
                System.IO.File.WriteAllText(configFolderPath + "\\" + "config.xml.bak", fileContents);

                fileContents = fileContents.Replace("{#**PortalName**#}", SetupConfig.PortalName);
                fileContents = fileContents.Replace("{#**PortalEmail**#}", SetupConfig.PortalEmail);
                fileContents = fileContents.Replace("{#**PortalRootPath**#}", SetupConfig.PortalRootPath);

              //  fileContents = fileContents.Replace("{#**AbsoluteCMSFolderPath**#}", SetupConfig.AbsoluteCMSFolderPath);

                //fileContents = fileContents.Replace("{#**DbName**#}", Cfg.DBConfig.DbName);
                //fileContents = fileContents.Replace("{#**DbHost**#}", Cfg.DBConfig.DbHost);
                //fileContents = fileContents.Replace("{#**DbUser**#}", Cfg.DBConfig.DbUser);
                //fileContents = fileContents.Replace("{#**DbPw**#}", Cfg.DBConfig.DbPw);
                //fileContents = fileContents.Replace("{#**DbPort**#}", Cfg.DBConfig.DbPort);

                fileContents = fileContents.Replace("{#**SmtpHost**#}", SetupConfig.SmtpHost);
                fileContents = fileContents.Replace("{#**SmtpPort**#}", SetupConfig.SmtpPort);
                fileContents = fileContents.Replace("{#**SmtpUser**#}", SetupConfig.SmtpUser);
                fileContents = fileContents.Replace("{#**SmtpPw**#}", SetupConfig.SmtpPw);
                fileContents = fileContents.Replace("{#**SmtpSsl**#}", SetupConfig.SmtpSsl);

                System.IO.File.WriteAllText(cmsConfigXml, fileContents);

                SetupConfig.ConfigXMLParsed = true;

                esito = "<div class=\"alert alert-success\">Config.xml processato correttamente!</div>";
            }
            catch (Exception ex)
            {
                Diagnostics.TraceMessage(TraceLevel.Error, ex.Message);
                SetupConfig.ConfigXMLParsed = false;
                esito = "<div class=\"alert alert-danger\">Errore durante il processamento del file Config.xml! " + ex.Message + "</div>";
            }
            return esito;
        }

        public static bool WebConfigIsParsed()
        {            
            bool esito = false;

            var fileContents = System.IO.File.ReadAllText(webconfigFilePath);
            if (!fileContents.Contains("{#**DbName**#}"))
                return true;

            return esito;

        }

        public static bool ConfigIsParsed()
        {
            bool esito = false;

            return esito;

        }

    }
}
