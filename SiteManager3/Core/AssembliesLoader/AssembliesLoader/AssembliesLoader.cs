﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetCms.DBSessionProvider;

namespace NetCms.Assemblies
{
    public static class AssembliesLoader
    { 
        private static List<Assembly> ActiveRecordAssemblies = new List<Assembly>();
        public static void LoadAssemblies()
        {
            DirectoryInfo binFolder = new DirectoryInfo(HttpContext.Current.Server.MapPath(Configurations.Paths.AbsoluteRoot + "/bin/"));

            foreach (FileInfo file in binFolder.GetFiles("*.dll")
                .Where(x=>x.Name.ToLower() != "SolrNet.dll".ToLower()
                && x.Name.ToLower() != "tidy.dll".ToLower()
                && x.Name.ToLower() != "tidy.x86.dll".ToLower()
                && x.Name.ToLower() != "tidy.x64.dll".ToLower()
                && x.Name.ToLower() != "System.Threading.Tasks.Extensions.dll".ToLower())
                )
            {
                Assembly asm = Assembly.LoadFrom(file.FullName);
                try
                {
                    Type[] types = asm.GetTypes();
                    foreach (Type type in types.Where(x => x.BaseType != null && x.BaseType.FullName != null))
                        ParseType(type, asm);
                }
                catch(Exception ex)
                {
                    throw ex;
                }                
            }
            if (NetCms.Configurations.Debug.GenericEnabled)
            {
                string[] listaPa = NetCms.Front.Static.StaticPagesCollector.ClassesKeysAssociations.Select(x => x.Key + " - " + x.Value.Type.FullName).ToArray();
                HttpContext.Current.Application["FrontendStaticPagesDebugList"] = listaPa;
                string[] listaDocs = NetFrontend.FrontendDocumentsClassesPool.Classes.Select(x => x.Key + ") " + x.Value.Assembly + " - " + x.Value.FullName).ToArray();
                HttpContext.Current.Application["FrontendDocumentsClassesPoolDebugList"] = listaDocs;
            }


            if (CreateSessionFactory())
            {
                DoAdditionalConfiguration();
                GenerateXmlMappingForSystemApplication();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="asm"></param>
        private static void ParseType(Type type, Assembly asm)
        {
            try 
            {
                if (type.BaseType.FullName == ("NetCms.Structure.Applications.Application"))
                    NetCms.Structure.Applications.ApplicationsPool.ParseType(type, asm);

                else if (type.BaseType.FullName == "NetCms.Pages.PagesSwitcher")
                {
                    if (!NetCms.Pages.PagesHandler.SwitchersTypes.Contains(type))
                        NetCms.Pages.PagesHandler.SwitchersTypes.Add(type);
                }

                else if (!type.IsAbstract && type.BaseType.FullName == "NetCms.Vertical.VerticalApplication" || type.BaseType.FullName == "NetCms.Vertical.VerticalApplicationWithService" || type.BaseType.FullName == "NetCms.Vertical.VerticalApplicationPackage")
                    NetCms.Vertical.VerticalApplicationsPool.ParseType(type, asm);

                else if (type.BaseType.FullName == "NetCms.Users.Activities.ActivitiesData")
                    NetCms.Users.Activities.ActivitiesDataPool.ParseType(type, asm);

                else if (type.BaseType.FullName == "NetCms.WebAPI.AbstractBaseController")
                    NetCms.WebAPI.WebAPIControllerPool.ParseType(type, asm);

                else if (type.BaseType.FullName == "LoginManager.Controls.LoginControl")
                    LoginManager.Models.LoginControlsPool.ParseType(type, asm);

                else if (type.BaseType.FullName == "NetCms.CmsApp.ShortCodes.ShortCode")
                    NetCms.CmsApp.ShortCodes.ShortCodesPool.ParseType(type, asm);

                else
                {
                    NetCms.Homepages.ModuliHomepageClassesPool.ParseType(type);
                    NetFrontend.FrontendDocumentsClassesPool.ParseType(type);
                    NetCms.Front.Static.StaticPagesCollector.ParseType(type);
                }
                G2Core.Caching.Invalidation.InvalidationHandler.AnalyzeType(type);
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage
                    (
                    Diagnostics.TraceLevel.Fatal, 
                    "Errore BOOT del  " + type.ToString() + " " + ex.Message, 
                    "", "", 
                    ex, 
                    "AssemblyLoader");
                throw ex;
            }            
        }
        /// <summary>
        /// Inizializza la configurazione locale delle applicazioni verticali installate se necessaria
        /// </summary>
        private static void DoAdditionalConfiguration()        
        {
            var appz = NetCms.Vertical.VerticalApplicationsPool.Assemblies.Cast<NetCms.Vertical.VerticalApplication>()
                                                        .Where(x => !x.IsSystemApplication && 
                                                               x.InstallState == Vertical.VerticalApplication.InstallStates.Installed &&
                                                               x.NeedAdditionalConfiguration);
            foreach (NetCms.Vertical.VerticalApplication application in appz)
            {
                application.DoAdditionalConfigurationForExternalDB();
            }                                              
        }

        private static void GenerateXmlMappingForSystemApplication()
        {
            var appz = NetCms.Structure.Applications.ApplicationsPool.Assemblies.Cast<NetCms.Structure.Applications.Application>().Where(x => x.IsSystemApplication);

            foreach (NetCms.Structure.Applications.Application app in appz)
            {
                app.GenerateXmlFromHbm();
            }
        }

        private static bool CreateSessionFactory()
        {
            try
            {
                FluentSessionProvider.Instance.CreateSessionFactory();
                return true;
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Fatal, "Errore nella creazione della session factory " + ex.Message ,"","",ex,"AssemblyLoader");
                return false;
            }
        }
    }
}