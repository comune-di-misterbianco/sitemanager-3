﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using LabelsManager;
using LoginManager.Utility;

namespace LoginManager.Controls
{
    public class LoginBox : WebControl
    {
        public LoginBox(int idNetwork) : base (HtmlTextWriterTag.Div)
        {
            IdNetwork = idNetwork;          
        }

        private int IdNetwork;

        public static Labels Labels
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(LoginManagerLabels)) as Labels;
            }
        }

        public virtual string LoginBoxTitleLabel
        {
            get { return _LoginBoxTitleLabel; }
            set { _LoginBoxTitleLabel = value; }
        }
        private string _LoginBoxTitleLabel = Labels[LoginManagerLabels.LabelsList.LoginBoxTitle];

        public virtual string LoginBoxTextLabel
        {
            get { return _LoginBoxTextLabel; }
            set { _LoginBoxTextLabel = value; }
        }
        private string _LoginBoxTextLabel = Labels[LoginManagerLabels.LabelsList.LoginBoxText];

        private ICollection<WebControl> _LoginControls;
        private ICollection<WebControl> LoginControls
        {
            get
            {
                if (_LoginControls == null)
                {
                    _LoginControls = new List<WebControl>();
                    _LoginControls.Add(LoginManager.BusinessLogic.LoginSettingsBL.GetLoginControl(IdNetwork));                    
                }
                return _LoginControls;
            }
        }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.CssClass = "login_container";

            #region loginInfo
            WebControl loginInfo = new WebControl(HtmlTextWriterTag.Div);
            loginInfo.CssClass = "login_info";

            //appendo i controlli literal con le labels al posto del controllo web prensente nel config.xml
            WebControl h1 = new WebControl(HtmlTextWriterTag.H1);
            h1.Controls.Add(new LiteralControl(LoginBoxTitleLabel));
            loginInfo.Controls.Add(h1);

            WebControl p = new WebControl(HtmlTextWriterTag.P);
            p.Controls.Add(new LiteralControl(LoginBoxTextLabel));
            loginInfo.Controls.Add(p);

            #endregion

            this.Controls.Add(loginInfo);

            #region loginBox
            //WebControl loginBox = new WebControl(HtmlTextWriterTag.Div);
            //loginBox.CssClass = "login_box";

            /*********************************************************************************************************************************************/
            /*   appende alla pagina il controllo di login secondo le modalità di autenticazione configurate in modo globale o per la network corrente   */
            /*********************************************************************************************************************************************/
            
            //loginBox.Controls.Add(LoginManager.BusinessLogic.LoginSettingsBL.GetLoginControl(IdNetwork));
           
            if (LoginControls.Any())
            {
                WebControl divContainer = new WebControl(HtmlTextWriterTag.Div);
                divContainer.CssClass = "container";

                WebControl divRow = new WebControl(HtmlTextWriterTag.Div);
                divRow.CssClass = "row";
                
                string bl = "border-left";
                int i = 0;
                foreach (var control in LoginControls)
                {
                    WebControl loginBox = new WebControl(HtmlTextWriterTag.Div);
                    loginBox.CssClass = "login_box col " + ((i>1 && i < LoginControls.Count) ? bl : "");
                    loginBox.Controls.Add(control);
                    divRow.Controls.Add(loginBox);
                }

                divContainer.Controls.Add(divRow);
                this.Controls.Add(divContainer);
            }
            
            #endregion

            
        }
    }
}
