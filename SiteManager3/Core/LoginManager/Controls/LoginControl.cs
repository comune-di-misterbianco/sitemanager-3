﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Web.UI.WebControls;
using LabelsManager;
using System.Web;
using System.Web.UI;
using NHibernate;
using NetCms.Users;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Web.Security;
using LoginManager.Models;
using LoginManager.Utility;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace LoginManager.Controls 
{
    public abstract class LoginControl : WebControl , ILoginControl
    {
        protected virtual NetCms.Configurations.IHttpsHandler HttpsHandler
        {
            get
            {
                return NetCms.Configurations.Generics.GetHttpsHandler();
            }
        }

        public LoginControl() : this("")
        {
            this.RedirectUrl = HttpContext.Current.Request.Url.ToString();
        }

        public LoginControl(string redirectURL, bool mostraRegistrazione = false, bool mostraRecuperoPassword = false, string linkReg = "") : base(HtmlTextWriterTag.Div)
        {
            this.RedirectUrl = redirectURL;
            this.CssClass = "LoginControl";
            this.MostraRegistrazione = mostraRegistrazione;
            this.MostraRecuperoPassword = mostraRecuperoPassword;
            this.LinkRegistrazione = linkReg;
        }

        public LoginControl(LoginManager.Models.LoginSetting settings)
        {
            CssClass = "LoginControl";

            RedirectUrl = settings.RedirectUrl;
            MostraRegistrazione = settings.ShowRegistrationLink;
            MostraRecuperoPassword = settings.ShowPasswordRecoveryLink;
            LinkRegistrazione = settings.RegistrationtUrl;

            DebugModeEnabled = settings.ShowDebugControl || NetCms.Configurations.Debug.DebugLoginEnabled;
        }

        #region Properties

        public virtual string IDsOffset
        {
            get
            {
                if (_IDsOffset == null)
                    _IDsOffset = "_LoginControl" + this.LoginsControlsCount;
                return _IDsOffset;
            }
        }
        private string _IDsOffset;

        public virtual G2Core.Common.RequestVariable ForgotPasswordRequest
        {
            get
            {
                if (_ForgotPasswordRequest == null)
                    _ForgotPasswordRequest = new G2Core.Common.RequestVariable("forgot", G2Core.Common.RequestVariable.RequestType.QueryString);
                return _ForgotPasswordRequest;
            }
        }
        private G2Core.Common.RequestVariable _ForgotPasswordRequest;

        //Indicizza il numero di controlli login aggiunti alla pagina.
        public int LoginsControlsCount
        {
            get
            {
                if (_LoginsControlsCount == 0)
                {
                    if (HttpContext.Current.Items.Contains("LoginsControlsCount"))
                    {
                        _LoginsControlsCount = int.Parse(HttpContext.Current.Items["LoginsControlsCount"].ToString());
                        _LoginsControlsCount++;
                        HttpContext.Current.Items["LoginsControlsCount"] = _LoginsControlsCount;
                    }
                    else
                        HttpContext.Current.Items["LoginsControlsCount"] = _LoginsControlsCount = 1;
                }
                return _LoginsControlsCount;
            }
        }
        private int _LoginsControlsCount;

        public virtual G2Core.Common.RequestVariable PostbackChecker
        {
            get
            {
                if (_PostbackChecker == null)
                    _PostbackChecker = new G2Core.Common.RequestVariable("loginpostback" + this.IDsOffset, G2Core.Common.RequestVariable.RequestType.Form);
                return _PostbackChecker;
            }
        }
        private G2Core.Common.RequestVariable _PostbackChecker;

        public string RedirectUrl
        {
            get { return _RedirectUrl; }
            protected set { _RedirectUrl = value; }
        }
        private string _RedirectUrl;

        private int LoginErrorCount
        {
            get
            {
                return (HttpContext.Current.Session["LoginTry"] != null) ? (int)HttpContext.Current.Session["LoginTry"] : 0;
            }
        }

        private bool _MostraRegistrazione;
        public bool MostraRegistrazione
        {
            get { return _MostraRegistrazione; }
            set { _MostraRegistrazione = value;  }
        }

        private bool _MostraRecuperoPassword;
        public bool MostraRecuperoPassword
        {
            get { return _MostraRecuperoPassword; }
            set { _MostraRecuperoPassword = value; }
        }

        private string _LinkRegistrazione;
        public string LinkRegistrazione
        {
            get { return _LinkRegistrazione; }
            set { _LinkRegistrazione = value; }
        }
       
        protected bool DebugModeEnabled
        {
            get;
            set;
        }

        private bool isHeadLogin = false;
        public bool IsHeadLogin
        {
            get { return isHeadLogin; }
            set { isHeadLogin = value; }
        }

        public string AbsoluteNetworkFrontRoot
        {
            get; set;
        }

        protected string ForgotPasswordUrl
        {
            get
            {
                return HttpsHandler.ConvertPageAddressAsSecure("?forgot=true");
            }
        }

        public bool NotifySent
        {
            get
            {
                return _NotifySent;
            }
            private set
            {
                _NotifySent = value;
            }
        }
        private bool _NotifySent;

        public virtual WebControl NotifiyList
        {
            get
            {
                if (_NotifiyList == null)
                {
                    _NotifiyList = new WebControl(HtmlTextWriterTag.Div);
                }
                return _NotifiyList;
            }
        }
        private WebControl _NotifiyList;

        public virtual WebControl NotifiyArea
        {
            get
            {
                if (_NotifiyArea == null)
                {
                    _NotifiyArea = new WebControl(HtmlTextWriterTag.Div);
                    _NotifiyArea.CssClass = "NotifyArea";
                    if (HttpContext.Current.Session != null)
                    {
                        if (HttpContext.Current.Session["AnotherBrowserLogged"] != null)
                        {
                            AddNotify(Labels[LoginManagerLabels.LabelsList.AccessFromOtherBrowser], NotifyLevel.warning);
                            HttpContext.Current.Session.Remove("AnotherBrowserLogged");
                        }
                        if (HttpContext.Current.Session["SSOUSER_NOTENABLED"] != null && HttpContext.Current.Session["SSOUSER_NOTENABLED"] == "0")
                        {
                            AddNotify(Labels[LoginManagerLabels.LabelsList.NotGrantedUser], NotifyLevel.warning);
                            HttpContext.Current.Session["SSOUSER_NOTENABLED"] = "1";
                        }
                    }

                }
                return _NotifiyArea;
            }
        }
        private WebControl _NotifiyArea;

        #endregion

        #region Labels
        public static Labels Labels
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(LoginManagerLabels)) as Labels;
            }
        }

        public virtual string UserName_TextFieldLabel
        {
            get { return _UserName_TextFieldLabel;  }
            set { _UserName_TextFieldLabel = value; }
                
         }
        private string _UserName_TextFieldLabel = Labels[LoginManagerLabels.LabelsList.Username];

        public virtual string Password_TextFieldLabel
        {
            get { return _Password_TextFieldLabel; }
            set { _Password_TextFieldLabel = value; }

        }
        private string _Password_TextFieldLabel = Labels[LoginManagerLabels.LabelsList.Password];

        public virtual string LoginButtonLabel
        {
            get { return _LoginButtonLabel; }
            set { _LoginButtonLabel = value; }
        }
        private string _LoginButtonLabel = Labels[LoginManagerLabels.LabelsList.AccessButtonLabel];

        public virtual string ResetButtonLabel
        {
            get { return _ResetButtonLabel; }
            set { _ResetButtonLabel = value; }
        }
        private string _ResetButtonLabel = Labels[LoginManagerLabels.LabelsList.Cancel];

        public virtual string UserRegistrationLinkLabel
        {
            get { return _UserRegistrationLinkLabel; }
            set { _UserRegistrationLinkLabel = value; }
        }
        private string _UserRegistrationLinkLabel = Labels[LoginManagerLabels.LabelsList.UserRegistration];        

        public virtual string RegistrationLinkLabel
        {
            get { return _RegistrationLinkLabel; }
            set { _RegistrationLinkLabel = value; }
        }
        private string _RegistrationLinkLabel = Labels[LoginManagerLabels.LabelsList.UserRegistration];

        public virtual string RecoverPasswordLinkLabel
        {
            get { return _RecoverPasswordLinkLabel; }
            set { _RecoverPasswordLinkLabel = value; }
        }
        private string _RecoverPasswordLinkLabel = Labels[LoginLabels.LabelsList.PasswordRecovery];

        #endregion

        #region Buttons

        private Button _LoginButton;
        public virtual Button LoginButton
        {
            get
            {
                if (_LoginButton == null)
                {
                    _LoginButton = new Button();
                    _LoginButton.ID = PostbackChecker.Key;
                    _LoginButton.Text = this.LoginButtonLabel;
                    _LoginButton.CssClass = "btn btn-primary";
                    _LoginButton.Click += loginButton_Click;
                }
                return _LoginButton;
            }

            set
            {
                _LoginButton = value;
            }
        }

        public virtual Button ResetButton
        {
            get
            {
                if (_ResetButton == null)
                {
                    _ResetButton = new Button();
                    _ResetButton.ID = "Undo" + IDsOffset;
                    _ResetButton.Text = this.ResetButtonLabel;
                    _ResetButton.CssClass = "btn btn-primary";
                    _ResetButton.Click += reset_Click;
                }
                return _ResetButton;
            }
        }
        private Button _ResetButton;

        #endregion

        #region Methods

        public void AddNotify(string notify, NotifyLevel notifyType)
        {
            WebControl par = new WebControl(HtmlTextWriterTag.P);
            par.CssClass = "alert alert-" + notifyType.ToString();

            par.Controls.Add(new LiteralControl(notify));

            this.NotifiyList.Controls.Add(par);
        }       

        protected void AddLoginTry(string userName)
        {
            if (HttpContext.Current.Session["LoginTry"] != null)
            {
                int exval = (int)HttpContext.Current.Session["LoginTry"];
                HttpContext.Current.Session["LoginTry"] = ++exval;
            }
            else
                HttpContext.Current.Session["LoginTry"] = 1;

            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Warning, "L'utente '" + userName + "' ha tentato di accedere al sistema senza successo (tot. " + HttpContext.Current.Session["LoginTry"] + " volte consecutive)", "", "", "");
        }

        public delegate ICollection<KeyValuePair<int, bool>> CollectGrantsDelegate(User user);

        protected abstract ICollection<KeyValuePair<int, bool>> CollectGrants(User _user);

        static void CallbackMethod(IAsyncResult ar)
        {
            AsyncResult result = (AsyncResult)ar;

            CollectGrantsDelegate caller = (CollectGrantsDelegate)result.AsyncDelegate;

            ReturnMessage mess = (ReturnMessage)result.GetReplyMessage();

            HttpSessionState session = (HttpSessionState)ar.AsyncState;

            ICollection<KeyValuePair<int, bool>> _FoldersCanView = (List<KeyValuePair<int, bool>>)mess.ReturnValue;
            if (session != null)
            {
                User currUser = (User)session[NetCms.Configurations.Generics.BackAccountSessionKey];

                if (currUser != null)
                {
                    if (session["FoldersCanViewUSER_" + currUser.ID] != null)
                    {
                        lock (session["FoldersCanViewUSER_" + currUser.ID])
                        {
                            session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                        }
                    }
                    else
                        session["FoldersCanViewUSER_" + currUser.ID] = _FoldersCanView;
                }
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.ID = "LoginControl" + this.LoginsControlsCount;

            this.Controls.Add(NotifiyArea);

            if (this.ForgotPasswordRequest.StringValue == "done")
                this.AddNotify(Labels[LoginManagerLabels.LabelsList.NewPasswordSentByMail], NotifyLevel.info);

            if (this.ForgotPasswordRequest.StringValue == "true")
                InitForgotPasswordControls();
            else
            {
                if (this.ForgotPasswordRequest.StringValue == "confirm")
                    ConfirmChangePwd();

                InitLoginControls();
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (NotifiyList.Controls.Count > 0)
            {
                NotifiyArea.Controls.Add(this.NotifiyList);
                if (IsHeadLogin)
                {
                    HiddenField loginTest = new HiddenField();
                    loginTest.ID = "loginTest" + this.ID;
                    loginTest.Value = "loginError";
                    this.Controls.Add(loginTest);
                }
            }
        }

        public virtual void InitLoginControls()
        {
            Panel loginPanel = new Panel();
            loginPanel.DefaultButton = LoginButton.ID;
            this.Controls.Add(loginPanel);

            WebControl formContainer = new WebControl(HtmlTextWriterTag.Div);
            formContainer.CssClass = "login-form form-horizontal";
            //this.Controls.Add(formContainer);

            #region Username
            Label UserName_Label = new Label();
            UserName_Label.Text = UserName_TextFieldLabel;
            UserName_Label.ID = "UserName_Label" + this.IDsOffset;
            UserName_Label.AssociatedControlID = "username" + this.IDsOffset;
            UserName_Label.CssClass = "control-label";

            TextBox UserName = new TextBox();
            UserName.CssClass = "textbox form-control";
            UserName.ID = "username" + this.IDsOffset;
            UserName.Attributes.Add("onkeydown", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.ID + "','')");

            WebControl fieldBox = new WebControl(HtmlTextWriterTag.Div);
            fieldBox.CssClass = "form-group";
            fieldBox.Controls.Add(UserName_Label);
            fieldBox.Controls.Add(UserName);
            formContainer.Controls.Add(fieldBox);
            #endregion

            #region Password
            Label Password_Label = new Label();
            Password_Label.Text = Password_TextFieldLabel;
            Password_Label.ID = "Password_Label" + this.IDsOffset;
            Password_Label.AssociatedControlID = "password" + this.IDsOffset;

            HtmlInputPassword Password = new HtmlInputPassword();
            Password.Attributes["class"] = "textbox form-control";
            Password.ID = "password" + this.IDsOffset;
            Password.Attributes.Add("onkeydown", "javascript:if (event.keyCode == 13) __doPostBack('" + LoginButton.ID + "','')");

            fieldBox = new WebControl(HtmlTextWriterTag.Div);
            fieldBox.CssClass = "form-group";
            fieldBox.Controls.Add(Password_Label);
            fieldBox.Controls.Add(Password);

            formContainer.Controls.Add(fieldBox);
            #endregion

            if (MostraRecuperoPassword || MostraRegistrazione)
            {
                WebControl row = new WebControl(HtmlTextWriterTag.Div);
                row.CssClass = "row";

                WebControl divButton = new WebControl(HtmlTextWriterTag.Div);
                divButton.CssClass = "col-md-6 col-xs-5";
                divButton.Controls.Add(LoginButton);
                
                row.Controls.Add(divButton);

                WebControl divLink = new WebControl(HtmlTextWriterTag.Div);
                divLink.CssClass = "col-md-6 col-xs-7 text-right";

                WebControl links = new WebControl(HtmlTextWriterTag.Ul);
                links.CssClass = "list-inline";
                WebControl liLink = new WebControl(HtmlTextWriterTag.Li);                

                if (MostraRecuperoPassword)
                {
                    liLink = new WebControl(HtmlTextWriterTag.Li);
                    liLink.CssClass = "list-inline-item";
                    WebControl forgotPasswordLink = new WebControl(HtmlTextWriterTag.A);
                    forgotPasswordLink.CssClass = "btn btn-secondary forgot";
                    forgotPasswordLink.Attributes.Add("href", ForgotPasswordUrl);
                    forgotPasswordLink.Controls.Add(new LiteralControl(RecoverPasswordLinkLabel));
                    liLink.Controls.Add(forgotPasswordLink);
                    links.Controls.Add(liLink);
                }

                if (MostraRegistrazione)
                {
                    liLink = new WebControl(HtmlTextWriterTag.Li);
                    liLink.CssClass = "list-inline-item";
                    WebControl showRegLink = new WebControl(HtmlTextWriterTag.A);
                    showRegLink.CssClass = "btn btn-secondary reg";
                    showRegLink.Attributes.Add("href", LinkRegistrazione);
                    showRegLink.Controls.Add(new LiteralControl(RegistrationLinkLabel));
                    liLink.Controls.Add(showRegLink);
                    links.Controls.Add(liLink);
                }

                divLink.Controls.Add(links);
                row.Controls.Add(divLink);
                //  this.Controls.Add(divButton);
                //this.Controls.Add(row);               
                formContainer.Controls.Add(row);
            }
            else
            {
                fieldBox = new WebControl(HtmlTextWriterTag.Div);
                fieldBox.CssClass = "form-group";
                fieldBox.Controls.Add(LoginButton);

                formContainer.Controls.Add(fieldBox);
            }

            loginPanel.Controls.Add(formContainer);
        }              
        
        #endregion

        #region Events
        public virtual void loginButton_Click(object sender, EventArgs e)
        {
            G2Core.Common.RequestVariable userName = new G2Core.Common.RequestVariable("username" + this.IDsOffset, G2Core.Common.RequestVariable.RequestType.Form);
            G2Core.Common.RequestVariable password = new G2Core.Common.RequestVariable("password" + this.IDsOffset, G2Core.Common.RequestVariable.RequestType.Form);

            if (userName.IsValid() && password.IsValid() && password.StringValue.Length > 0)
            {
                string MD5Pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(password.StringValue, "MD5").ToString();

                User user = UsersBusinessLogic.GetActiveUserByUserNameAndPassword(userName.StringValue, MD5Pwd);

                if (user != null)
                {
                    NetCms.Users.LoginResult result = NetCms.Users.AccountManager.TryLoginUser(user, MD5Pwd);
                    if (result.Result == LoginResult.LoginResults.Logged)
                    {                    
                        HttpSessionState webSession = HttpContext.Current.Session;

                        CollectGrantsDelegate myAction = new CollectGrantsDelegate(CollectGrants);

                        myAction.BeginInvoke(user, new AsyncCallback(CallbackMethod), webSession);

                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'utente '" + userName.StringValue + "' ha effettuato il Login al sistema.");
                      
                        if (RedirectUrl.Length == 0)
                            RedirectUrl = "/";

                        NetCms.Diagnostics.Diagnostics.Redirect(HttpsHandler.ConvertPageAddressAsNonSecure(RedirectUrl));
                    }
                    else
                    {
                        AddLoginTry(userName.StringValue);
                        AddNotify(Labels[LoginManagerLabels.LabelsList.YourAccountWasDisabled], NotifyLevel.warning);
                        NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "L'account dell'utente '" + userName.StringValue + "' è stato disabilitato a causa della scadenza dei 180 giorni dall'accesso.", "", "", "");
                    }
                }
                else
                {
                    AddLoginTry(userName.StringValue);
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "Tentativo di login al sistema fallito con il seguente nome utente: '" + userName.StringValue + "' ha effettuato il Login al sistema.", "", "", "");
                    AddNotify(Labels[LoginManagerLabels.LabelsList.NotValidUserAndPass], NotifyLevel.danger);
                }
            }
            else
            {
                AddLoginTry(userName.StringValue);
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Verbose, "Tentativo di login al sistema fallito con il seguente nome utente: '" + userName.StringValue + "' ha effettuato il Login al sistema.", "", "", "");
                AddNotify(Labels[LoginManagerLabels.LabelsList.NotValidUserAndPass], NotifyLevel.danger);
            }
        }    

        public virtual void reset_Click(object sender, EventArgs e)
        {
           // var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();

            string url = HttpsHandler.GetCurrentPageAddressAsNonSecure().Replace("?forgot=true", "");

            NetCms.Diagnostics.Diagnostics.Redirect(url);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void ask_Click(object sender, EventArgs e)
        {            
            G2Core.Common.RequestVariable usernameReq = new G2Core.Common.RequestVariable("username" + this.IDsOffset, G2Core.Common.RequestVariable.RequestType.Form);
            
            User user = UsersBusinessLogic.GetActiveUserByUserName(usernameReq.StringValue);

            if (user != null)
            {
                string hash = user.HashInfoForValidationCheck();

                //var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();

                string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString();

                string urlToMail = HttpsHandler.ConvertPageAddressAsSecure("http://" + ip + AbsoluteNetworkFrontRoot + "/default.aspx?u=" + user.UserName + "&h=" + hash + "&" + ForgotPasswordRequest.Key + "=confirm");

                user.SendConfirmChangePwdMail(urlToMail);

                this.AddNotify(Labels[LoginManagerLabels.LabelsList.SentRequestForChangePw], NotifyLevel.info);
            }
            else
                this.AddNotify(Labels[LoginManagerLabels.LabelsList.NotExistingUser], NotifyLevel.danger);
        }

        #endregion

        /* Elementi riferiti al controllo per il recupero delle credenziali */
        #region WebControl per recupero credenziali

        // proprietà 
        public virtual string RecoverPasswordButtonLabel
        {
            get { return _RecoverPasswordButtonLabel; }
            set { _RecoverPasswordButtonLabel = value; }
        }
        private string _RecoverPasswordButtonLabel = Labels[LoginLabels.LabelsList.AskPassword];

        // metodi

        /// <summary>
        /// Inizializzazione dei controlli form per il recupero della password - Metodo da spostare in altro WebControl 
        /// </summary>
        public virtual void InitForgotPasswordControls()
        {
            string message = "";

            #region Costruisco i Controlli form

            TextBox UserName = new TextBox();
            UserName.CssClass = "textbox";
            UserName.ID = "username" + IDsOffset;

            Label UserName_Label = new Label();
            UserName_Label.Text = UserName_TextFieldLabel;
            UserName_Label.ID = "UserName_Label" + IDsOffset;
            UserName_Label.AssociatedControlID = UserName.ID;


            Button ask = new Button();
            ask.Text = Labels[LoginManagerLabels.LabelsList.AskPassword];
            ask.ID = "Ask" + IDsOffset;
            ask.Click += new EventHandler(ask_Click);
            ask.CssClass = "btn btn-primary mr-2";

            Button reset = new Button();
            reset.Text = Labels[LoginManagerLabels.LabelsList.Cancel];
            reset.ID = "Undo" + IDsOffset;
            reset.Click += new EventHandler(reset_Click);

            #endregion

            this.Attributes["class"] = "LoginFormContainer";

            HtmlGenericControl info = new HtmlGenericControl("div");
            info.Attributes["class"] = "LoginInfo";
            info.InnerHtml = message;
            this.Controls.Add(info);

            WebControl FormContainer = new WebControl(HtmlTextWriterTag.P);
            FormContainer.CssClass = "FormContainer";

            FormContainer.Controls.Add(UserName_Label);
            FormContainer.Controls.Add(UserName);

            FormContainer.Controls.Add(UserName_Label);
            FormContainer.Controls.Add(UserName);

            this.Controls.Add(FormContainer);

            FormContainer = new WebControl(HtmlTextWriterTag.P);
            FormContainer.CssClass = "FormContainer";

            //this.Controls.Add(FormContainer);

            //Attacco il controllo submit
            FormContainer = new WebControl(HtmlTextWriterTag.P);

            FormContainer.Controls.Add(ask);
            FormContainer.Controls.Add(reset);

            this.Controls.Add(FormContainer);
        }

        // bottoni 
        private Button _RecoverPasswordButton;
        public virtual Button RecoverPasswordButton
        {
            get
            {
                if (_RecoverPasswordButton == null)
                {
                    _RecoverPasswordButton = new Button();
                    _RecoverPasswordButton.ID = "Ask" + IDsOffset;
                    _RecoverPasswordButton.Text = RecoverPasswordButtonLabel;
                    _RecoverPasswordButton.Click += ask_Click;
                    _RecoverPasswordButton.CssClass = "btn btn-primary mr-2";
                }
                return _RecoverPasswordButton;
            }

            set { _RecoverPasswordButton = value; }
        }

        // eventi

        /// <summary>
        /// Evento per la richiesta di una nuova password  - da spostare in altro WebControl
        /// </summary>
        public virtual void ConfirmChangePwd()
        {
            G2Core.Common.RequestVariable usernameReq = new G2Core.Common.RequestVariable("u" + this.IDsOffset, G2Core.Common.RequestVariable.RequestType.QueryString);

            User user = UsersBusinessLogic.GetActiveUserByUserName(usernameReq.StringValue);

            if (user != null)
            {
                G2Core.Common.RequestVariable hashReq = new G2Core.Common.RequestVariable("h" + this.IDsOffset, G2Core.Common.RequestVariable.RequestType.QueryString);

                if (hashReq.StringValue == user.HashInfoForValidationCheck())
                {
                    user.SendNewPassword();

                   // var httpsHandler = NetCms.Configurations.Generics.GetHttpsHandler();

                    HttpContext.Current.Response.Redirect(HttpsHandler.ConvertPageAddressAsSecure("?" + ForgotPasswordRequest.Key + "=done"));
                }
                else
                    this.AddNotify(Labels[LoginManagerLabels.LabelsList.NotCorrectInputForPwReset], NotifyLevel.danger);
            }
            else
                this.AddNotify(Labels[LoginManagerLabels.LabelsList.NotExistingUser], NotifyLevel.danger);
        } 
        #endregion
    }
}

