﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace LoginManager.Controls
{
    public abstract class LoginControlDebug : LoginControl
    {
        public LoginControlDebug()
        {
        }

        #region Properties

        public override G2Core.Common.RequestVariable PostbackChecker
        {
            get
            {
                if (_PostbackChecker == null)
                    _PostbackChecker = new G2Core.Common.RequestVariable("debugloginpostback" + this.IDsOffset, G2Core.Common.RequestVariable.RequestType.Form);
                return _PostbackChecker;
            }
        }
        private G2Core.Common.RequestVariable _PostbackChecker;

        private string _DebugLoginButtonCssClass = "debugloginbutton";
        public virtual string DebugLoginButtonCssClass
        {
            get { return _DebugLoginButtonCssClass; }
            set { _DebugLoginButtonCssClass = value; }
        }

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {


        }

        #endregion

        #region Buttons

        public override Button LoginButton
        {
            get
            {
                if (_LoginButton == null)
                {
                    _LoginButton = new Button();
                    _LoginButton.ID = PostbackChecker.Key;
                    _LoginButton.CssClass = DebugLoginButtonCssClass;
                    _LoginButton.Text = DebugLoginButtonLabel;
                    _LoginButton.Click += loginButton_Click;
                }
                return _LoginButton;
            }
        }
        private Button _LoginButton;


        #endregion


        #region Labels

        public virtual string DebugLoginButtonLabel
        {
            get { return _DebugLoginButtonLabel; }
            set { _DebugLoginButtonLabel = value; }
        }
        private string _DebugLoginButtonLabel = Labels[Utility.LoginManagerLabels.LabelsList.DebugLoginButtonLabel];

        #endregion
    }
}
