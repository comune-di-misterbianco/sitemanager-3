﻿using LabelsManager;
using LoginManager.Models;
using LoginManager.Utility;
using NetCms.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginManager.Controls
{
    public class LoginHeaderControl : WebControl
    {
        public static Labels Labels
        {
            get
            {
                return LabelsCache.GetTypedLabels(typeof(LoginManagerLabels)) as Labels;
            }
        }

        public LoginHeaderControl(int idNetwork, string absoluteFrontRoot, bool userIsLogged, User currentUser) : base(HtmlTextWriterTag.Div)
        {
            IdNetwork = idNetwork;
            CurrentUser = currentUser;
            UserIsLogged = userIsLogged;
            AbsoluteFrontRoot = absoluteFrontRoot;
            this.CssClass = "LoginHeaderControl";
        }
        public LoginHeaderControl(int idNetwork, string absoluteFrontRoot, bool userIsLogged, User currentUser, string parentTheme ,bool ext) : base(HtmlTextWriterTag.Div)
        {
            IdNetwork = idNetwork;
            CurrentUser = currentUser;
            UserIsLogged = userIsLogged;
            AbsoluteFrontRoot = absoluteFrontRoot;
            ParentTheme = parentTheme;
            
            this.CssClass = "LoginHeaderControl";
            this.ID = "loginContainer"; // necessario per retrocompatibilità grafica

            if (!UserIsLogged)
                this.Controls.Add(GetLoginHeaderContainer(ParentTheme));
            else
                if (Settings.PagePosition == LoginSetting.Position.Intestazione)
                this.Controls.Add(new LoginManager.Controls.LoginUserControl(CurrentUser, AbsoluteFrontRoot, ParentTheme, true) { CssClass = "content" });
        }

        private int IdNetwork;
        private User CurrentUser;
        private bool UserIsLogged;
        private string AbsoluteFrontRoot;

        public string ParentTheme
        {
            get;
            set;
        }

        private LoginSetting _Settings;
        public LoginSetting Settings
        {
            get {
                if (_Settings == null)
                    _Settings = LoginManager.BusinessLogic.LoginSettingsBL.GetSettingByIdNetwork(IdNetwork);
                return _Settings;

            }
            set { _Settings = value; }
        }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.ID = "loginContainer"; // necessario per retrocompatibilità grafica

            if (!UserIsLogged)
                this.Controls.Add(GetLoginHeaderContainer(""));
            else
                if (Settings.PagePosition == LoginSetting.Position.Intestazione)
                this.Controls.Add(new LoginManager.Controls.LoginUserControl(CurrentUser, AbsoluteFrontRoot) { CssClass = "content" });
        }

        private WebControl GetLoginHeaderContainer(string parentTheme)
        {
            WebControl content = new WebControl(HtmlTextWriterTag.Div);
            content.CssClass = "content";


            if (parentTheme.ToLower() == "Agid2019".ToLower())
            {
                if (Settings.ShowRegistrationLink && !string.IsNullOrEmpty(Settings.RegistrationtUrl))
                {
                    WebControl a_reg = new WebControl(HtmlTextWriterTag.A);
                    a_reg.Attributes["href"] = /*Settings.RegistrationtUrl*/ Labels[LoginManagerLabels.LabelsList.SigninUrl];
                    a_reg.CssClass = "btn d-none d-lg-inline";
                    a_reg.Attributes["title"] = Labels[LoginManagerLabels.LabelsList.Signin];
                    string strCodereg = @"<span>" + Labels[LoginManagerLabels.LabelsList.Signin] + "</span>";
                    a_reg.Controls.Add(new LiteralControl(strCodereg));

                    content.Controls.Add(a_reg);
                }


                WebControl a = new WebControl(HtmlTextWriterTag.A);
                a.Attributes["href"] = /*Settings.LoginPageUrls.FirstOrDefault()*/
                    Labels[LoginManagerLabels.LabelsList.LoginUrl];
                a.CssClass = "btn btn-primary btn-icon " + (!Settings.ShowRegistrationLink ? "btn-full": "no-rounded");
                a.Attributes["title"] = Labels[LoginManagerLabels.LabelsList.LoginArea];


                string strCode = 
                                @"<span class=""rounded-icon"">
                                    <svg class=""icon icon-primary"">
                                        <use xlink:href=""/bootstrap-italia/dist/svg/sprite.svg#it-user""></use>
                                    </svg>
                                </span>
                                <span class=""d-none d-lg-block"">" + Labels[LoginManagerLabels.LabelsList.LoginArea] + "</span>";

                a.Controls.Add(new LiteralControl(strCode));
                content.Controls.Add(a);
            }
            else
            {
                

                if (Settings.ShowRegistrationLink && !string.IsNullOrEmpty(Settings.RegistrationtUrl))
                {
                    WebControl regControl = new WebControl(HtmlTextWriterTag.Div);
                    regControl.ID = "Reg";

                    WebControl linkReg = new WebControl(HtmlTextWriterTag.A);

                    linkReg.Attributes["href"] = /*Settings.RegistrationtUrl*/ Labels[LoginManagerLabels.LabelsList.SigninUrl];

                    linkReg.Controls.Add(new LiteralControl("<span>" + Labels[LoginManagerLabels.LabelsList.UserRegistration] + "</span>"));

                    regControl.Controls.Add(linkReg);
                    content.Controls.Add(regControl);
                }

                WebControl loginButton = new WebControl(HtmlTextWriterTag.A);
                loginButton.Controls.Add(new LiteralControl("<span>" + Labels[LoginManagerLabels.LabelsList.AccessButtonLabel] + "</span>"));

                if (Settings.PagePosition == LoginSetting.Position.Corpo)
                {
                    loginButton.Attributes["href"] = Labels[LoginManagerLabels.LabelsList.LoginUrl];
                    /*Settings.LoginPageUrls.FirstOrDefault();*/ // TODO: verificare se l'url punta correttamente alla pagina di accesso
                    content.Controls.Add(loginButton);
                }
                else if (Settings.PagePosition == LoginSetting.Position.Intestazione)
                {
                    loginButton.ID = "loginButton";
                    loginButton.Attributes["href"] = /*Settings.LoginPageUrls.FirstOrDefault();*/
                        Labels[LoginManagerLabels.LabelsList.LoginUrl];
                    content.Controls.Add(loginButton);

                    WebControl clr = new WebControl(HtmlTextWriterTag.Div);
                    clr.CssClass = "clr";
                    content.Controls.Add(clr);

                    WebControl loginBox = new WebControl(HtmlTextWriterTag.Div);
                    loginBox.ID = "loginBox";

                    loginBox.Controls.Add(LoginManager.BusinessLogic.LoginSettingsBL.GetLoginControl(IdNetwork, Settings));

                    //loginCtrl.IsHeadLogin = true;
                    //loginBox.Controls.Add(loginCtrl);

                    content.Controls.Add(loginBox);
                }

                
            }   
            return content;        
        }      
    }
}
