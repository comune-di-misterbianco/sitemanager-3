﻿using LabelsManager;
using LoginManager.Models;
using LoginManager.Utility;
using NetCms.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginManager.Controls
{
    public class LoginUserControl : WebControl
    {
        public static Labels Labels
        {
            get
            {                
                return LabelsCache.GetTypedLabels(typeof(LoginManagerLabels)) as Labels;
            }          
        }

        public LoginUserControl(User currentuser, string absoluteFrontRoot) : base(HtmlTextWriterTag.Div)
        {                        
            CurrentUser = currentuser;
            AbsoluteFrontRoot = absoluteFrontRoot;
        }

        public LoginUserControl(User currentuser, string absoluteFrontRoot, string parentTheme, bool initControl = false) : base(HtmlTextWriterTag.Div)
        {
            CurrentUser = currentuser;
            AbsoluteFrontRoot = absoluteFrontRoot;
            ParentTheme = parentTheme;

            if (initControl)
            InitControl();
        }      

        #region Labels 
        public virtual string LoggedUserWelcomeMsg
        {
            get { return _LoggedUserWelcomeMsg; }
            set { _LoggedUserWelcomeMsg = value; }

        }
        private string _LoggedUserWelcomeMsg = Labels[LoginManagerLabels.LabelsList.LoggedUserWelcomeMsg];


        public virtual string InfoProfiloLabelLink
        {
            get { return _InfoProfiloLabelLink; }
            set { _InfoProfiloLabelLink = value; }

        }
        private string _InfoProfiloLabelLink = Labels[LoginManagerLabels.LabelsList.InfoProfiloLabelLink];

        public virtual string EditProfiloLabelLink
        {
            get { return _EditProfiloLabelLink; }
            set { _EditProfiloLabelLink = value; }

        }
        private string _EditProfiloLabelLink = Labels[LoginManagerLabels.LabelsList.EditProfiloLabelLink];

        public virtual string EditPasswordLabelLink
        {
            get { return _EditPasswordLabelLink; }
            set { _EditPasswordLabelLink = value; }

        }
        private string _EditPasswordLabelLink = Labels[LoginManagerLabels.LabelsList.EditPasswordLabelLink];

        public virtual string NotificheListLabelLink
        {
            get { return _NotificheListLabelLink; }
            set { _NotificheListLabelLink = value; }

        }
        private string _NotificheListLabelLink = Labels[LoginManagerLabels.LabelsList.NotificheListLabelLink];

        public virtual string ChiarimentiListLabelLink
        {
            get { return _ChiarimentiListLabelLink; }
            set { _ChiarimentiListLabelLink = value; }

        }
        private string _ChiarimentiListLabelLink = Labels[LoginManagerLabels.LabelsList.ChiarimentiListLabelLink];

        public virtual string LogoutLabelLink
        {
            get { return _LogoutLabelLink; }
            set { _LogoutLabelLink = value; }

        }
        private string _LogoutLabelLink = Labels[LoginManagerLabels.LabelsList.LogoutLabelLink];

        #endregion

        #region CssClass
        private string _LoggedUserCssClass;
        public virtual string LoggedUserCssClass
        {
            get { return "userinfo"; }
            set { _LoggedUserCssClass = value; }
        }
        #endregion


        #region Properties

        private User CurrentUser;
        private string AbsoluteFrontRoot;
        public virtual string LogoutReturnUrl { get; set; }

        public string _LogoutUrl;
        public virtual string LogoutUrl
        {
            get { return AbsoluteFrontRoot + "?do=logout&goto=" + LogoutReturnUrl; }
            set { _LogoutUrl = value; }
        }

        public string _LogoutLinkCss = "LogoutLink";
        public virtual string LogoutLinkCss
        {
            get { return _LogoutLinkCss; }
            set { _LogoutLinkCss = value; }
        }

        private string _ProfiloPageUrl;
        public virtual string ProfiloPageUrl
        {
            get { return AbsoluteFrontRoot + "/user/profilo.aspx"; }
            set { _ProfiloPageUrl = value; }
        }

        private string _ProfiloLinkCss = "ProfiloLink";
        public string ProfiloLinkCss
        {
            get { return _ProfiloLinkCss; }
            set { _ProfiloLinkCss = value; }
        }

        private string _EditProfiloPageUrl;
        public virtual string EditProfiloPageUrl
        {
            get { return AbsoluteFrontRoot + "/user/modifica_profilo.aspx";  }
            set { _EditProfiloPageUrl = value; }
        }

        private string _EditProfiloLinkCss = "ModificaProfiloLink";
        public string EditProfiloLinkCss
        {
            get { return _EditProfiloLinkCss; }
            set { _EditProfiloLinkCss = value; }
        }

        private string _EditPPasswordPageUrl;
        public virtual string EditPasswordPageUrl
        {
            get { return AbsoluteFrontRoot + "/user/changepw.aspx"; }
            set { _EditPPasswordPageUrl = value; }
        }

        private string _EditPasswordLinkCss = "ModificaPasswordLink";
        public string EditPasswordLinkCss
        {
            get { return _EditPasswordLinkCss; }
            set { _EditPasswordLinkCss = value; }
        }

        private string _NotificheListPageUrl;
        public virtual string NotificheListPageUrl
        {
            get { return AbsoluteFrontRoot + "/notifiche/elenco.aspx"; }
            set { _NotificheListPageUrl = value; }
        }

        private string _NotificheListLinkCss = "ElencoNotificheLink";
        public string NotificheListLinkCss
        {
            get { return _NotificheListLinkCss; }
            set { _NotificheListLinkCss = value; }
        }

        private string _ChiarimentieListPageUrl;
        public virtual string ChiarimentiListPageUrl
        {
            get { return AbsoluteFrontRoot + "/chiarimenti/default.aspx"; }
            set { _ChiarimentieListPageUrl = value; }
        }

        private string _ChiarimentiListLinkCss = "ElencoNotificheLink";
        public string ChiarimentiListLinkCss
        {
            get { return _ChiarimentiListLinkCss; }
            set { _ChiarimentiListLinkCss = value; }
        }
     
        public LoginUserItem ProfiloLink
        {
            get
            {                
                return new LoginUserItem(InfoProfiloLabelLink, ProfiloPageUrl, ProfiloLinkCss, LoginUserItem.MenuItemType.Link);
            }
        }

        public LoginUserItem EditProfiloLink
        {
            get
            {                
                return new LoginUserItem(EditProfiloLabelLink, EditProfiloPageUrl, EditProfiloLinkCss, LoginUserItem.MenuItemType.Link);
            }
        }

        public LoginUserItem ModificaPasswordLink
        {
            get
            {                
                return new LoginUserItem(EditPasswordLabelLink, EditPasswordPageUrl, EditPasswordLinkCss, LoginUserItem.MenuItemType.Link);
            }
        }

        public LoginUserItem ElencoNotificheLink
        {
            get
            {             
                return new LoginUserItem(NotificheListLabelLink, NotificheListPageUrl, NotificheListLinkCss, LoginUserItem.MenuItemType.Link);
            }
        }

        public LoginUserItem ElencoChiarimentiLink
        {
            get
            {                
                return new LoginUserItem(ChiarimentiListLabelLink, ChiarimentiListPageUrl, ChiarimentiListLinkCss, LoginUserItem.MenuItemType.Link);
            }
        }

        public LoginUserItem LogoutLink
        {
            get
            {
                return new LoginUserItem(LogoutLabelLink, LogoutUrl, LogoutLinkCss, LoginUserItem.MenuItemType.Link);
            }
        }

        private List<LoginUserItem> _Items;
        protected virtual List<LoginUserItem> Items
        {
            get {
                if (_Items == null)
                    _Items = new List<LoginUserItem>();
                return _Items;
            }
            set
            {
                _Items = value;
            }
        }
        
        public virtual List<LoginUserItem> MenuItems
        {
            get;
            set;
        }

        public string ParentTheme
        {
            get;
            set;
        }

        #endregion


        #region Methods

        protected virtual void InitItems()
        {
            Items.Add(ProfiloLink);

            if (CurrentUser.CustomAnagrafe != null && !CurrentUser.CustomAnagrafe.UseSpid)
            {
                Items.Add(EditProfiloLink);
                Items.Add(ModificaPasswordLink);
            }

          //  Items.Add(new LoginUserItem(LoginUserItem.MenuItemType.Divider));
            
            if (NetCms.Configurations.Generics.NotifyEnabled)
            {                
                Items.Add(ElencoNotificheLink);
            }

            if (NetCms.Configurations.Generics.ChiarimentiEnabled)
            {             
                Items.Add(ElencoChiarimentiLink);
            }

            if (MenuItems != null && MenuItems.Any())
            {
                foreach(LoginUserItem item in MenuItems)
                {
                    Items.Add(item);
                }
            }

           // Items.Add(new LoginUserItem(LoginUserItem.MenuItemType.Divider));         
        }

        private WebControl GetItems(string pTheme)
        {
            WebControl li;
            WebControl elencoitems = new WebControl(HtmlTextWriterTag.Ul);

            if (pTheme.ToLower() == "Agid2019".ToLower())
            {
                WebControl row = new WebControl(HtmlTextWriterTag.Div);
                row.CssClass = "row";

                WebControl col = new WebControl(HtmlTextWriterTag.Div);
                col.CssClass = "col-12";

                WebControl listwrapper = new WebControl(HtmlTextWriterTag.Div);
                listwrapper.CssClass = "link-list-wrapper";

                elencoitems.CssClass = "link-list";

                foreach (LoginUserItem item in Items)
                {
                    li = new WebControl(HtmlTextWriterTag.Li);
                    if (item.ItemType == LoginUserItem.MenuItemType.Divider)
                    {
                        li.Controls.Add(new LiteralControl(@"<span class=""divider""></span>"));
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(item.CssClass))
                            li.CssClass = item.CssClass;

                        WebControl a = new WebControl(HtmlTextWriterTag.A);
                        a.Attributes["href"] = item.Url;
                        a.CssClass = "list-item";
                        a.Controls.Add(new LiteralControl("<span>" + item.Label + "</span>"));

                        li.Controls.Add(a);
                    }

                    elencoitems.Controls.Add(li);
                }

                li = new WebControl(HtmlTextWriterTag.Li);

                WebControl logout = new WebControl(HtmlTextWriterTag.A);
                logout.Attributes["href"] = LogoutUrl;
                logout.CssClass = "list-item left-icon";

                string strLogout = @"<svg class=""icon icon-primary icon-sm left"">
                                      <use xlink:href=""/bootstrap-italia/dist/svg/sprite.svg#it-external-link"" ></use>    
                                     </svg>
                                     <span class=""font-weight-bold"">"+ LogoutLabelLink + @"</span>";


                logout.Controls.Add(new LiteralControl(strLogout));

                li.Controls.Add(logout);
                elencoitems.Controls.Add(li);

                listwrapper.Controls.Add(elencoitems);
                col.Controls.Add(listwrapper);
                row.Controls.Add(col);
                return row;
            }
            else
            {

                elencoitems.CssClass = "Linklist";
                foreach (LoginUserItem item in Items)
                {
                    li = new WebControl(HtmlTextWriterTag.Li);
                    if (item.ItemType == LoginUserItem.MenuItemType.Divider)
                    {
                        li.CssClass = "divider";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(item.CssClass))
                            li.CssClass = item.CssClass + " u-padding-r-all";

                        WebControl a = new WebControl(HtmlTextWriterTag.A);
                        a.Attributes["href"] = item.Url;
                        a.Controls.Add(new LiteralControl(item.Label));

                        li.Controls.Add(a);
                    }

                    elencoitems.Controls.Add(li);
                }

                li = new WebControl(HtmlTextWriterTag.Li);
                li.CssClass = " u-padding-r-all";
                
                WebControl logout = new WebControl(HtmlTextWriterTag.A);
                logout.Attributes["href"] = LogoutUrl;
                logout.CssClass = LogoutLinkCss;
                logout.Controls.Add(new LiteralControl(LogoutLabelLink));

                li.Controls.Add(logout);
                elencoitems.Controls.Add(li);
                return elencoitems;
            }
           
        }

        #endregion




        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            InitControl();
        }


        protected virtual void InitControl()
        {
            InitItems();

            string nomeCompleto = (!string.IsNullOrEmpty(CurrentUser.NomeCompleto)) ? CurrentUser.NomeCompleto : CurrentUser.UserName;

            if (ParentTheme.ToLower() == "Agid2019".ToLower())
            {
                WebControl div = new WebControl(HtmlTextWriterTag.Div);
                div.CssClass = "it-user-wrapper nav-item dropdown";

                string userBlock =
@"<a aria-expanded=""false"" class=""btn btn-primary btn-icon btn-full"" data-toggle=""dropdown"" href=""#"">
    <span class=""rounded-icon"">      
        <svg class=""icon icon-primary"">
            <use xlink:href=""/bootstrap-italia/dist/svg/sprite.svg#it-user""></use>
        </svg>
    </span>
    <span class=""d-none d-lg-block""> " + nomeCompleto+@"</span>
    <svg class=""icon icon-white d-none d-lg-block"">
        <use xlink:href=""/bootstrap-italia/dist/svg/sprite.svg#it-expand""></use>
    </svg>
</a>";
                div.Controls.Add(new LiteralControl(userBlock));

                WebControl menu = new WebControl(HtmlTextWriterTag.Div);
                menu.CssClass = "dropdown-menu";

                menu.Controls.Add(GetItems(this.ParentTheme));
                div.Controls.Add(menu);

                this.Controls.Add(div);
            }
            else
            {
                WebControl p_username = new WebControl(HtmlTextWriterTag.A);
                p_username.CssClass = "Button Button--info account-btn";
                p_username.Attributes["data-menu-trigger"] = "option-menu";
                p_username.Attributes["href"] = "#option-menu";

                WebControl span_username = new WebControl(HtmlTextWriterTag.Span);
                span_username.CssClass = LoggedUserCssClass;
                span_username.Controls.Add(new LiteralControl(string.Format(LoggedUserWelcomeMsg, nomeCompleto)));
                p_username.Controls.Add(span_username);
                this.Controls.Add(p_username);

                WebControl menu = new WebControl(HtmlTextWriterTag.Div);
                menu.ID = "option-menu";
                menu.Attributes["data-menu"] = "";
                menu.CssClass = "Dropdown-menu u-borderShadow-m u-background-white";

                menu.Controls.Add(GetItems(this.ParentTheme));

                this.Controls.Add(menu);
            }
        }


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

        }
    }

   
}
