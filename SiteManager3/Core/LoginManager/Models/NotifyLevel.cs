﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoginManager.Models
{
    public enum NotifyLevel
    {
        success,
        info,
        warning,
        danger
    }
}
