﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace LoginManager.Models.Mappings
{
    public class LoginSettingMapping : ClassMap<LoginSetting>
    {
        public LoginSettingMapping()
        {
            Table("config_login_settings");
            Id(x => x.ID);                       
            Map(x => x.LoginControlNamespace);
            Map(x => x.LoginControlAssemblyName);
            Map(x => x.RifNetwork);
            Map(x => x.ShowDebugControl);
            Map(x => x.ShowPasswordRecoveryLink);
            Map(x => x.ShowRegistrationLink);
            Map(x => x.RedirectUrl);
            Map(x => x.RegistrationtUrl);
            Map(x => x.AutenticationUrl);
            Map(x => x.PasswordRecoveryUrl);
            Map(x => x.PagePosition);
        }
    }
}
