﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoginManager.Models
{
    public class LoginUserItem
    {
        public LoginUserItem(MenuItemType itemType)
        {

        }
        public LoginUserItem(string label, string url, string cssclass, MenuItemType itemType)
        {
            Label = label;
            Url = url;
            CssClass = cssclass;
            ItemType = itemType;            
        }

        public string Label { get; set; }
        public string Url { get; set; }
        public string CssClass { get; set; }

        public MenuItemType ItemType { get; set; }

        public enum MenuItemType
        {
            Divider,
            Link,
        }
    }
}
