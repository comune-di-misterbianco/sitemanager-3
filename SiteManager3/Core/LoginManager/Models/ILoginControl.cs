﻿using G2Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace LoginManager.Models
{
    public interface ILoginControl 
    {
        RequestVariable ForgotPasswordRequest { get; }

        string IDsOffset { get; }

        bool IsHeadLogin { get; set; }
        Button LoginButton { get; set; }
        string LoginButtonLabel { get; set; }
        int LoginsControlsCount { get; }

        WebControl NotifiyArea { get; }
        WebControl NotifiyList { get; }
        bool NotifySent { get; }
        RequestVariable PostbackChecker { get; }
        Button RecoverPasswordButton { get; set; }
        string RecoverPasswordButtonLabel { get; set; }
        string RecoverPasswordLinkLabel { get; set; }
        string RedirectUrl { get; }
        Button ResetButton { get; }
        string ResetButtonLabel { get; set; }
        string UserRegistrationLinkLabel { get; set; }

        void AddNotify(string notify, NotifyLevel notifyLevel);
    }
}
