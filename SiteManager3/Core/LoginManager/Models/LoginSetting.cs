﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoginManager.Models
{
    public class LoginSetting
    {
        public virtual int ID { get; set; }           

        public virtual string LoginControlNamespace { get; set; }

        public virtual string LoginControlAssemblyName { get; set; }

        public virtual int RifNetwork { get; set; }

        public virtual string RedirectUrl { get; set; }

        public virtual string RegistrationtUrl { get; set; }

        public virtual string AutenticationUrl { get; set; }

        public virtual string PasswordRecoveryUrl { get; set; }

        public virtual bool ShowRegistrationLink { get; set; }

        public virtual bool ShowPasswordRecoveryLink { get; set; }

        public virtual bool ShowDebugControl { get; set; }

        private string[] _LoginPageUrls = new string[] { "/user/accesso.aspx" , "/en/user/signin.aspx" };

        public virtual string[] LoginPageUrls {
            get { return _LoginPageUrls ; }
            set { _LoginPageUrls = value; }
        }


        /* verificare possibilità di associazione dell'anagrafe per la registrazione*/
        public virtual ICollection<string> AnagrafiSupportate { get; set; }

        public virtual Position PagePosition { get; set; }

        public enum Position
        {
            Intestazione,
            Modale,
            Corpo,            
        }

        public enum Columns
        {
            ID,                      
            LoginControlNamespace,
            LoginControlAssemblyPath,
            RifNetwork,
            RedirectUrl,
            AutenticationUrl,
            RegistrationtUrl,
            PasswordRecoveryUrl,
            PagePosition,
            ShowDebugControl,
            ShowPasswordRecoveryLink,
            ShowRegistrationLink
        }
    }
}
