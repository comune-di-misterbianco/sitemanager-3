﻿using LoginManager.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace LoginManager.Models
{
    public static class LoginControlsPool
    {
        private const string AssembliesApplicationKey = "LoginManager.Models.LoginControlsPool";

        private static Dictionary<string, string> _Assemblies;
        public static Dictionary<string, string> Assemblies
        {
            get
            {
                if(_Assemblies == null)
                {
                    if (HttpContext.Current.Application[AssembliesApplicationKey] != null)
                        _Assemblies = (Dictionary<string, string>)HttpContext.Current.Application[AssembliesApplicationKey];
                    else
                        HttpContext.Current.Application[AssembliesApplicationKey] = _Assemblies = new Dictionary<string, string>();

                }
                return _Assemblies;
            }
        }

        public static void ParseType(Type type, Assembly asm)
        {
            if (type.BaseType == Type.GetType("LoginManager.Controls.LoginControl"))
            {
                Assemblies.Add(type.FullName, asm.FullName);
            }
        }
    }
}
