﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoginManager.Utility
{
    public class LoginManagerLabels : LabelsManager.Labels
    {
        public const String ApplicationKey = "LoginManagerLabels";

        public LoginManagerLabels(): base(ApplicationKey)
        {

        }
        public enum LabelsList
        {
            AccessButtonLabel,
            AccessButtonLabelAD,
            AccessFromOtherBrowser,
            NotGrantedUser,
            YourAccountWasDisabled,
            NotValidUserAndPass,
            NewPasswordSentByMail,
            Username,
            Password,
            PasswordRecovery,
            UserRegistration,
            AskPassword,
            Cancel,
            SentRequestForChangePw,
            NotExistingUser,
            NotCorrectInputForPwReset,
            DebugLoginButtonLabel,
            LoggedUserWelcomeMsg,            
            InfoProfiloLabelLink,
            EditProfiloLabelLink,
            EditPasswordLabelLink,
            NotificheListLabelLink,
            ChiarimentiListLabelLink,
            LogoutLabelLink,
            LoginBoxTitle,
            LoginBoxText,
            Signin,
            LoginArea,
            LoginUrl,
            SigninUrl
        }
    }
}
