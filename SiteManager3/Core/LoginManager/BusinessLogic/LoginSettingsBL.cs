﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NetCms.DBSessionProvider;
using LoginManager.Models;
using GenericDAO.DAO;
using NetService.Utility.RecordsFinder;
using System.Web.UI.WebControls;
using System.Reflection;
using LoginManager.Controls;
using NetService.Utility.ValidatedFields;
using System.Web.UI;

namespace LoginManager.BusinessLogic
{
    public class LoginSettingsBL
    {
        private const int GeneralSettingIdentify = 0;

        public static ISession GetSession()
        {
            return FluentSessionProvider.Instance.GetSession();
        }

        public static LoginSetting GetByID(int id, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            CriteriaNhibernateDAO<LoginSetting> dao = new CriteriaNhibernateDAO<LoginSetting>(innerSession);

            return dao.GetById(id);
        }

        public static List<LoginSetting> GetAllSettings(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            CriteriaNhibernateDAO<LoginSetting> dao = new CriteriaNhibernateDAO<LoginSetting>(innerSession);

            List<LoginSetting> settings = dao.FindAll().ToList();

            return settings;
        }

        public static LoginSetting GetGeneralSetting(ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            //return GetSettingByIdNetwork(GeneralSettingIdentify, innerSession);

            CriteriaNhibernateDAO<LoginSetting> dao = new CriteriaNhibernateDAO<LoginSetting>(innerSession);

            SearchParameter spIdNetwork = new SearchParameter("", Models.LoginSetting.Columns.RifNetwork.ToString(), GeneralSettingIdentify, Finder.ComparisonCriteria.Equals, false);

            LoginSetting setting = dao.GetByCriteria(new SearchParameter[] { spIdNetwork });

            return setting;
        }

        public static LoginSetting GetSettingByIdNetwork(int idNetwork, ISession innerSession = null, bool returnGeneralIfNull = false)
        {
            if (innerSession == null)
                innerSession = GetSession();            
 
            CriteriaNhibernateDAO<LoginSetting> dao = new CriteriaNhibernateDAO<LoginSetting>(innerSession);

            SearchParameter spIdNetwork = new SearchParameter("", Models.LoginSetting.Columns.RifNetwork.ToString(), idNetwork, Finder.ComparisonCriteria.Equals, false);

            LoginSetting setting = dao.GetByCriteria(new SearchParameter[] { spIdNetwork });

            if (setting == null && !returnGeneralIfNull)
                return GetGeneralSetting(innerSession);

            return setting;
        }

        public static bool DeleteSettingByIdNetwork(int idNetwork, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            bool esito = false;

            using (ITransaction tx = innerSession.BeginTransaction())
            {
                try
                {
                    LoginSetting setting = GetSettingByIdNetwork(idNetwork, innerSession);

                    CriteriaNhibernateDAO<LoginSetting> dao = new CriteriaNhibernateDAO<LoginSetting>(innerSession);
                    dao.Delete(setting);
                    tx.Commit();
                    esito = true;
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);
                    tx.Rollback();
                }
            }

            return esito;
        }
    
        public static bool SaveSetting(VfManager form, int idNetwork, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            bool esito = false;

            using (ITransaction tx = innerSession.BeginTransaction())
            {
                try
                {
                    LoginSetting setting = GetSettingByIdNetwork(idNetwork, innerSession, true);
                    if (setting == null)
                        setting = new LoginSetting();

                    // gestire il valore di ritorno del controllo dropdownlist per il LoginControl
                    bool filled = form.FillObjectAdvanced(setting, innerSession);

                    if (filled)
                    {
                        string logincontroltype = string.Empty;
                        if (form.Fields.Find(x => x.Key == LoginSetting.Columns.LoginControlNamespace.ToString()).PostbackValueObject != null)
                        {
                            logincontroltype = form.Fields.Find(x => x.Key == LoginSetting.Columns.LoginControlNamespace.ToString()).PostbackValueObject.ToString();

                            KeyValuePair<string, string> item = LoginControlsPool.Assemblies.Where(x => x.Key == logincontroltype).FirstOrDefault();

                            string objectFullname = item.Key;
                            string assemblyname = item.Value;

                            setting.LoginControlAssemblyName = assemblyname;
                            setting.LoginControlNamespace = objectFullname;
                            setting.RifNetwork = idNetwork;

                            CriteriaNhibernateDAO<LoginSetting> dao = new CriteriaNhibernateDAO<LoginSetting>(innerSession);
                            dao.SaveOrUpdate(setting);
                            tx.Commit();
                            esito = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);
                    tx.Rollback();
                }
            }

            return esito;
        }

        public static WebControl GetLoginControl(int idNetwork, LoginSetting loginSetting = null, ISession innerSession = null)
        {
            if (innerSession == null)
                innerSession = GetSession();

            // recupera i settaggi di login per la network corrente con id = idNetwork
            // nel caso in cui non esisto settaggi specifici per la network recupera quelli generici predefiniti            

            try
            {
                if (loginSetting == null)
                    loginSetting = GetSettingByIdNetwork(idNetwork, innerSession);

                if (loginSetting == null)
                    loginSetting = GetSettingByIdNetwork(GeneralSettingIdentify, innerSession);

                // strutturare meglio i parametri necessari di default da passare al controllo web
                object[] @params = { loginSetting };

                Assembly asm = Assembly.Load(loginSetting.LoginControlAssemblyName);

                Type loginControlType = asm.GetType(loginSetting.LoginControlNamespace);

                LoginControl loginControlInstance = (LoginControl)Activator.CreateInstance(loginControlType, @params);

                loginControlInstance.ID = "logincontrol_" + loginSetting.ID;

                return loginControlInstance;            
            }
            catch (Exception ex)
            {
                NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, ex.Message);
                return new WebControl(HtmlTextWriterTag.Div);
            }
            
           
        }       
    }
}
