﻿using NetService.Utility.ValidatedFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoginManager.Models;

namespace LoginManager.Forms
{
    public class LoginSettingForm : VfManager
    {
        public LoginSettingForm(string controlID, int rifNetwork, LoginSetting setting)
            : base(controlID)
        {
            CurrentSetting = setting;

            //this.Fields.Add(Name);
            this.Fields.Add(LoginControls);
            this.Fields.Add(Position);            

            this.Fields.Add(AutenticationUrl);

            this.Fields.Add(ShowRegistrationLink);
            this.Fields.Add(RegistrationtUrl);

            this.Fields.Add(ShowPasswordRecoveryLink);
            this.Fields.Add(PasswordRecoveryUrl);

            this.Fields.Add(RedirectUrl);

            this.Fields.Add(ShowDebugControl);

            if (setting != null)
            {
                this.DataBindAdvanced(setting);             
            }
        }

        // considerare se includere una modalità di redirect dopo il login
        // ad esempio: homepage, rimani sulla pagina corrente, url specificato nella proprietà RedirectUrl

        private LoginSetting CurrentSetting;

        private VfTextBox _RedirectUrl;
        public VfTextBox RedirectUrl
        {
            get
            {
                if (_RedirectUrl == null)
                {
                    _RedirectUrl = new VfTextBox(Models.LoginSetting.Columns.RedirectUrl.ToString(), "Indirizzo di redirect dopo accesso", Models.LoginSetting.Columns.RedirectUrl.ToString(), InputTypes.Text);
                    _RedirectUrl.Required = false;
                }

                return _RedirectUrl;
            }
        }

        private VfTextBox _RegistrationtUrl;
        public VfTextBox RegistrationtUrl
        {
            get
            {
                if (_RegistrationtUrl == null)
                {
                    _RegistrationtUrl = new VfTextBox(Models.LoginSetting.Columns.RegistrationtUrl.ToString(), "Indirizzo della pagina di registrazione", Models.LoginSetting.Columns.RegistrationtUrl.ToString(), InputTypes.Text);
                    _RegistrationtUrl.Required = false;
                }

                return _RegistrationtUrl;
            }
        }

        private VfTextBox _PasswordRecoveryUrl;
        public VfTextBox PasswordRecoveryUrl
        {
            get
            {
                if (_PasswordRecoveryUrl == null)
                {
                    _PasswordRecoveryUrl = new VfTextBox(Models.LoginSetting.Columns.PasswordRecoveryUrl.ToString(), "Indirizzo della pagina di recupero delle credenziali", Models.LoginSetting.Columns.PasswordRecoveryUrl.ToString(), InputTypes.Text);
                    _PasswordRecoveryUrl.Required = false;
                }

                return _PasswordRecoveryUrl;
            }
        }

        private VfTextBox _AutenticationUrl;
        public VfTextBox AutenticationUrl
        {
            get
            {
                if (_AutenticationUrl == null)
                {
                    _AutenticationUrl = new VfTextBox(Models.LoginSetting.Columns.AutenticationUrl.ToString(), "Indirizzo di autenticazione", Models.LoginSetting.Columns.AutenticationUrl.ToString(), InputTypes.Text);
                    _AutenticationUrl.Required = false;
                }

                return _AutenticationUrl;
            }
        }

        public VfDropDown _Position;
        public VfDropDown Position
        {
            get
            {
                if(_Position == null)
                {
                    _Position = new VfDropDown(LoginSetting.Columns.PagePosition.ToString(), "Posizione", LoginSetting.Columns.PagePosition.ToString());
                    _Position.Required = true;
                    foreach (LoginSetting.Position item in Enum.GetValues(typeof(LoginSetting.Position))) {
                        _Position.Options.Add(item.ToString(),item.ToString());
                    }
                }
                return _Position;
            }
        }

        private VfDropDown _LoginControls;
        public VfDropDown LoginControls
        {
            get
            {
                if (_LoginControls == null)
                {
                    _LoginControls = new VfDropDown(LoginSetting.Columns.LoginControlNamespace.ToString(), "LoginControl", LoginSetting.Columns.LoginControlNamespace.ToString(), typeof(string));
                    _LoginControls.Required = true;
                   // _LoginControls.BindField = false;

                    foreach (KeyValuePair<string,string> item in LoginControlsPool.Assemblies)
                    {
                        //string itemPattern = "{asm}|{Namespace}|{ObjType}";
                        //string itemValue = itemPattern.Replace("{asm}", item.Value).Replace("{Namespace}", item.Key).Replace("{ObjType}", item.Key); //string itemValue = itemPattern.Replace("{asm}",item.Value).Replace("{Namespace}", itemType.Namespace).Replace("{ObjType}",itemType.Name);

                        //if (CurrentSetting != null && item.Key == CurrentSetting.LoginControlNamespace)
                        //{
                        //    _LoginControls.Options.Add(item.Key, itemValue);
                        //    _LoginControls.DefaultValue = new KeyValuePair<string, string>(item.Key, item.Key);
                        //}
                        //else
                            _LoginControls.Options.Add(item.Key, item.Key);
                    }
                }
                return _LoginControls;
            }
        }

        private VfCheckBox _ShowDebugControl;
        private VfCheckBox ShowDebugControl
        {
            get
            {
                if (_ShowDebugControl == null)
                {
                    _ShowDebugControl = new VfCheckBox(LoginSetting.Columns.ShowDebugControl.ToString(), "Abilita l'accesso debug" , LoginSetting.Columns.ShowDebugControl.ToString());
                    _ShowDebugControl.Required = false;                                       
                }
                return _ShowDebugControl;
            }
        }

        private VfCheckBox _ShowPasswordRecoveryLink;
        private VfCheckBox ShowPasswordRecoveryLink
        {
            get
            {
                if (_ShowPasswordRecoveryLink == null)
                {
                    _ShowPasswordRecoveryLink = new VfCheckBox(LoginSetting.Columns.ShowPasswordRecoveryLink.ToString(), "Visualizza il collegamento per il recupero delle credenziali", LoginSetting.Columns.ShowPasswordRecoveryLink.ToString());
                    _ShowPasswordRecoveryLink.Required = false;
                }
                return _ShowPasswordRecoveryLink;
            }
        }

        private VfCheckBox _ShowRegistrationLink;
        private VfCheckBox ShowRegistrationLink
        {
            get
            {
                if (_ShowRegistrationLink == null)
                {
                    _ShowRegistrationLink = new VfCheckBox(LoginSetting.Columns.ShowRegistrationLink.ToString(), "Visualizza il collegamento per la registrazione", LoginSetting.Columns.ShowRegistrationLink.ToString());
                    _ShowRegistrationLink.Required = false;
                }
                return _ShowRegistrationLink;
            }
        }
        
    }
}
