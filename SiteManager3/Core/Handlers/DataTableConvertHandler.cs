﻿using System;
using System.Web;
using System.Data;
using System.Web.SessionState;
using System.Web.UI;
using NetCms.Connections;
using NetCms.Networks;
using System.IO;

namespace NetCms.Handlers
{
    public class DatatableConvertHandler : IHttpHandler
    {
        // Summary:
        //     Gets a value indicating whether another request can use the System.Web.IHttpHandler
        //     instance.
        //
        // Returns:
        //     true if the System.Web.IHttpHandler instance is reusable; otherwise, false.
        public bool IsReusable { get { return false; } }

        // Summary:
        //     Enables processing of HTTP Web requests by a custom HttpHandler that implements
        //     the System.Web.IHttpHandler interface.
        //
        // Parameters:
        //   context:
        //     An System.Web.HttpContext object that provides references to the intrinsic
        //     server objects (for example, Request, Response, Session, and Server) used
        //     to service HTTP requests.
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Url.AbsolutePath.Contains("export-table-data"))
            {
                string path = NetCms.Configurations.Paths.AbsoluteRoot + "/user-exported-data/";
                NetUtility.Report.DataTableConvert.DatatableUrlHandledConverter converter = new NetUtility.Report.DataTableConvert.DatatableUrlHandledConverter(path);
                if (converter.IsValid)
                {
                    converter.ConvertAndSave();
                    path = converter.SavedFilePath;

                    /*
                       if (strstr($HTTP_USER_AGENT,"MSIE"))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Content-type: application-download");
            header("Content-Length: $size");
            header("Content-Disposition attachment; filename=MyPDF.pdf");
            header("Content-Transfer-Encoding: binary");
        }
        else
        {
            header("Content-type: application-download");
            header("Content-Length: $size");
            header("Content-Disposition attachment; filename=MyPDF.pdf");
        }

        readfile($file); 
                     */

                    FileStream fs = File.OpenRead(path);
                    byte[] content = new byte[fs.Length];
                    fs.Read(content, 0, content.Length);

                    context.Response.Buffer = true;
                    context.Response.Clear();
                    string cdfn = path.Substring(path.LastIndexOf('\\') + 1, path.Length - path.LastIndexOf('\\') - 1);
                    if (HttpContext.Current.Request.Browser.Browser == "IE")
                    {
                        context.Response.AddHeader("Pragma", "public");
                        context.Response.AddHeader("Expires", "0");
                        context.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                        context.Response.AddHeader("Content-Type", "application-download");
                        context.Response.AddHeader("Content-Length", fs.Length.ToString());
                        context.Response.AddHeader("Content-Disposition", "attachment; filename=" + cdfn);
                        context.Response.AddHeader("Content-Transfer-Encoding", "binary");
                        context.Response.AddHeader("Content-Description", "File Transfer");
                        context.Response.AddHeader("X-Download-Options", "noopen"); // IE7 & IE8?
                        context.Response.AddHeader("X-Content-Type-Options", "nosniff"); // IE8 bonus?
                    }
                    else
                    {
                        context.Response.AddHeader("Content-Type", "application-download");
                        context.Response.AddHeader("Content-Length", fs.Length.ToString());
                        context.Response.AddHeader("Content-Disposition", "attachment; filename=" + cdfn);
                    }

                    fs.Close();
                    context.Response.ContentType = converter.SavedFileMimeType;

                    if (File.Exists(path)) File.Delete(path);

                    context.Response.BinaryWrite(content);
                    context.Response.End();
                }
            }
            else
            {
                string requestedFile = context.Server.MapPath(context.Request.FilePath);
                context.Response.ContentType = GetContentType(requestedFile);
                context.Response.TransmitFile(requestedFile);
                context.Response.End();
            }
        }

        private string GetContentType(string filename)
        {
            // used to set the encoding for the reponse stream
            string res = null;
            FileInfo fileinfo = new FileInfo(filename);

            if (fileinfo.Exists)
            {
                switch (fileinfo.Extension.Remove(0, 1).ToLower())
                {
                    case "pdf": res = "application/pdf"; break;
                    case "csv": res = "text/csv"; break;
                    case "xml": res = "text/csv"; break;
                    case "xls": res = "application/vnd.ms-excel"; break;
                    case "xlsx": res = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; break;
                    case "docx": res = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"; break;
                }

                return res;
            }
            return null;
        } 
    }
}