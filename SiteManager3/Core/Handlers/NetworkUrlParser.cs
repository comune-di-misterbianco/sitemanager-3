using System;
using System.Data;
using NetCms.Networks;
using NetCms.Connections;
using System.Linq;
using System.Web;

namespace NetCms.Handlers
{
    public class NetworkUrlParser
    {
        bool SiteHasRoot
        {
            get
            {
                return Configurations.Paths.AbsoluteRoot.Length>0;
            }
        }

        private DefaultNetworkData DefaultNetwork
        {
            get
            {
                return _DefaultNetwork ?? (_DefaultNetwork = new DefaultNetworkData());
            }
        }
        private DefaultNetworkData _DefaultNetwork;

        public NetCms.Connections.Connection Connection
        {
            get { return _Connection; }
            private set { _Connection = value; }
        }
        private NetCms.Connections.Connection _Connection;
        
        public string PageName
        {
            get
            {
                if (_PageName == null)
                {
                    string[] path = Uri.PathAndQuery.Split('/');
                    string theoric = path[path.Length - 1];
                    path = theoric.Split('?');
                    theoric = path[0];
                    if (!theoric.Contains(".aspx"))
                        theoric = Configurations.Generics.DefaultPageName;
                    _PageName = theoric.ToLower().Trim();
                }
                return HttpContext.Current.Server.UrlDecode(_PageName);
            }
        }
        private string _PageName;

        public string Path
        {
            get
            {
                if (_Path == null)
                {
                    _Path = HttpContext.Current.Server.UrlDecode(Uri.PathAndQuery).ToLower();
                    _Path = _Path.Replace(PageName.ToLower(), "").Split('?')[0];
                }
                return HttpContext.Current.Server.UrlDecode(_Path);
            }
        }
        private string _Path;

        public string FolderPath
        {
            get
            {
                if (_FolderPath == null && this.NetworkFound)
                {
                    _FolderPath = "";
                    int startingPoint = 0;

                    if (SiteHasRoot) 
                        startingPoint++;
                    
                    if (!this.DefaultNetworkLoaded || this.DefaultNetwork.Root.Length > 0) 
                        startingPoint++;

                    for (int i = startingPoint; i < Parts.Length; i++)
                        _FolderPath += "/" + Parts[i];

                }
                return HttpContext.Current.Server.UrlDecode(_FolderPath);
            }
        }
        private string _FolderPath;

        private string[] _Parts;
        private string[] Parts
        {
            get
            {
                if (_Parts == null)
                {
                    char[] chars = { '/' };
                    _Parts = Path.ToLower().Split(chars, StringSplitOptions.RemoveEmptyEntries);
                }
                return _Parts;
            }
        }

        public Uri Uri
        {
            get
            {
                return _uri;
            }
        }
        private Uri _uri;

        public Faces CurrentFace
        {
            get { return _CurrentFace; }
            private set { _CurrentFace = value; }
        }
        private Faces _CurrentFace;

        private int BackOfficeKeyPosition
        {
            get { return SiteHasRoot ? 1 : 0; }
        }

        public string NetworkName
        {
            get { return _NetworkName; }
            private set { _NetworkName = value; }
        }
        private string _NetworkName;

        private int _NetworkID;
        public int NetworkID
        {
            get 
            {
                if (NetworkFound && _NetworkID == 0)
                {
                    //DataTable networksTable = new NetCms.Networks.NetworksData();
                    //DataRow[] Records = networksTable.Select("Nome_Network LIKE '" + NetworkName + "'");
                    //if(Records.Length>0)
                    //    _NetworkID = Records[0]["id_Network"].ToString();
                    var networksTable = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Select(x => x.Value); //new NetCms.Networks.NetworksData();
                    //var Records = networksTable.Where(x=>x.Enabled && x.SystemName == NetworkName);
                    //if (Records.Count() > 0)
                    //    _NetworkID = Records.First().ID;
                    var RecordNetwork = networksTable.First(x => x.Enabled && x.SystemName == NetworkName);
                    if (RecordNetwork != null)
                        _NetworkID = RecordNetwork.ID;
                }
                return _NetworkID; 
            }
        }

        public bool NetworkFound
        {
            get { return _NetworkFound; }
            private set { _NetworkFound = value; }
        }
        private bool _NetworkFound;

        public bool DefaultNetworkLoaded
        {
            get { return _DefaultNetworkLoaded; }
            private set { _DefaultNetworkLoaded = value; }
        }
        private bool _DefaultNetworkLoaded;

        private NetCms.Networks.NetworksStates _NetworkStatus = NetCms.Networks.NetworksStates.OutsideNetworks;
        public NetCms.Networks.NetworksStates NetworkStatus
        {
            get
            {
                return _NetworkStatus;
            }
            private set { _NetworkStatus = value; }
        }

        public NetworkUrlParser(Uri uri, NetCms.Connections.Connection Connection)
        {
            _uri = uri;
            this.Connection = Connection;
            Parse();
        }

        public void Parse()
        {
            //Controllo se nel percorso � presente il nome della cartella del gestore
            if (this.Path.StartsWith(Configurations.Paths.AbsoluteSiteManagerRoot))
            {
                if (Parts.Length > BackOfficeKeyPosition + 1)
                {
                    string tmpNetworkName = Parts[BackOfficeKeyPosition + 1];
                    if (CheckIfNetworkExist(tmpNetworkName))
                    {
                        NetworkName = tmpNetworkName;
                        NetworkStatus = NetworksStates.InsideNetwork;
                        this.NetworkFound = true;
                    }
                }
                CurrentFace = Faces.Admin;
            }
            else
            {
                CurrentFace = Faces.Front;

                if (!Path.StartsWith(Configurations.Paths.AbsoluteSiteManagerRoot))  // ha senso ricontrollarlo???
                {
                    int FrontNetworkKeyPosition = this.SiteHasRoot ? 1 : 0;
                    if (Parts.Length <= FrontNetworkKeyPosition)
                    {
                            NetworkName = DefaultNetwork.Name;
                            NetworkStatus = NetworksStates.InsideNetwork;
                            this.NetworkFound = true;
                            this.DefaultNetworkLoaded = true;
                    }
                    else
                    {
                        bool networkFound = CheckIfNetworkExist(Parts[FrontNetworkKeyPosition]);

                        if (networkFound)
                        {
                            NetworkStatus = NetworksStates.InsideNetwork;
                            NetworkName = Parts[FrontNetworkKeyPosition];
                            this.NetworkFound = true;
                        }
                        else
                        {
                            NetworkStatus = DefaultNetwork.Name == null ?  NetworksStates.OutsideNetworks : NetworksStates.InsideNetwork;
                            NetworkName = DefaultNetwork.Name;
                            this.NetworkFound = true;
                            this.DefaultNetworkLoaded = true;
                        }
                    }
                }
            }
        }

        private bool CheckIfNetworkExist(string networkName)
        {
            var networksTable = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Select(x => x.Value);//new NetCms.Networks.NetworksData();
            int RecordsCount = networksTable.Count(x => x.Enabled && x.StatoCMS == 1 && x.SystemName == networkName);
            //DataTable networksTable = new NetCms.Networks.NetworksData();
            //DataRow[] Records = networksTable.Select("Enabled_Network = 1 AND Cms_Network = 1 AND Nome_Network LIKE '" + networkName + "'");
            return RecordsCount == 1;
        }
      
        private class DefaultNetworkData
        {
            public int ID { get; private set; }
            public string Root { get; private set; }
            public string Name { get; private set; }

            public DefaultNetworkData()
            {
                InitDefaultNetworkData();
            }

            private void InitDefaultNetworkData()
            {
                var networksTable = NetCms.Networks.NetworksManager.GlobalIndexer.AllNetworks.Select(x => x.Value);//new NetCms.Networks.NetworksData();
                var Records = networksTable.Where(x =>x.Enabled && x.Predefinita);
                int count = Records.Count();
                
                //DataTable networksTable = new NetCms.Networks.NetworksData();
                //DataRow[] Records = networksTable.Select("Default_Network = 1");
                if (count == 0)
                {
                    Exception ex = new Exception("E' stata richiesta la Network impostata come default, ma nessuna Network � impostata come default sul database");
                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, ex.Message, "", "", "83");
                }
                if (count > 1)
                {
                    Exception ex = new Exception("E' stata richiesta la Network impostata come default, ma ne sono impostate pi� di una Network come default sul database");
                    /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                    ExLog.SaveLog(0, ex.Message, this, NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, ex.Message, "", "", "84");
                }
                
                var net = Records.First();

                Name = net.SystemName; // Records[0]["Nome_Network"].ToString();
                ID = net.ID; // Records[0]["id_Network"].ToString();
                Root = net.RootPath;  //Records[0]["WebRoot_Network"].ToString();
            }
        }
    }
}