using System;
using System.Web;
using System.Web.UI;
using LanguageManager.BusinessLogic;
using NetCms.Networks;

namespace NetCms.Handlers
{
    public class NetworkHandler : IHttpHandlerFactory
    {
        public G2Core.Caching.Cache Cache
        {
            get
            {
                if (_Cache == null)
                    _Cache = new G2Core.Caching.Cache();
                return _Cache;
            }
        }
        private G2Core.Caching.Cache _Cache;

        private NetCms.Connections.Connection _Connection;
        private NetCms.Connections.Connection Connection
        {
            get
            {
                //if (_Connection == null)
                //    _Connection = NetCms.Connections.ConnectionsManager.CommonConnection;
                //return _Connection;

                NetCms.Connections.ConnectionsManager ConnManager = new NetCms.Connections.ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }

        public Network CurrentNetwork
        {
            get 
            {                 
                return _CurrentNetwork; 
            }
            private set
            {
                _CurrentNetwork = value;
                NetworksManager.SetCurrentActiveNetwork(CurrentNetwork);
            }
        }
        private Network _CurrentNetwork;

        private bool HandlePageContent
        {
            get
            {
                bool checkstatus = true;
                
                foreach(string folder in Configurations.Generics.SystemFolders)                
                    checkstatus = checkstatus && !Url.Contains("/" + folder + "/");
                
                foreach(string file in Configurations.Generics.SystemFiles)                
                    checkstatus = checkstatus && !Url.Contains("/" + file);

                return checkstatus;
            }
        }

        public string Url
        {
            get { return _Url; }
            private set { _Url = value; }
        }
        private string _Url;
        
        public static G2Core.Common.StringCollection ClearPages
        {
            get
            {
                if (_ClearPages == null)
                {
                    string[] values = System.Web.Configuration.WebConfigurationManager.AppSettings["ClearPagesFiles"].ToLower().Split(',');
                    _ClearPages = new G2Core.Common.StringCollection(values);
                }
                return _ClearPages;
            }
        }
        private static G2Core.Common.StringCollection _ClearPages;

        public NetworkUrlParser Parser
        {
            get { return _Parser; }
            private set { _Parser = value; }
        }
        private NetworkUrlParser _Parser;

        private HttpContext _Context;
        private HttpContext Context
        {
            get
            {
                return _Context;
            }
            set { _Context = value; }
        }

        public IHttpHandler GetHandler(HttpContext context, string requestType, string url, string pathTranslated)
        {
            //FrontDebugLogger.PageLogger.SaveTime();
            //FrontDebugLogger.PageLogger.Log("GetHandler -> Start");
            //FrontDebugLogger.PageLogger.SaveTime();

            Context = context;
                        
            IHttpHandler Page = null;
            if (Context != null && Context.Request.UrlReferrer != null && url.EndsWith("errorPage.aspx"))
            {
                int statusCode = -1;
                // check if querystring contains error code 
                if (!string.IsNullOrEmpty(context.Request.QueryString["e"]))
                    statusCode = int.Parse(Context.Request.QueryString["e"].ToString());

                Url = url.ToLower();

                if (statusCode != -1)
                    Page = BuildErrorPage(statusCode,"");
            }
            else
            {
                if (LanguageManager.Utility.Config.EnableMultiLanguage)
                {
                    Url = LanguageBusinessLogic.GetUrlWithoutLanguage(url.ToLower());
                    if (LanguageBusinessLogic.HasCurrentSession())
                    {
                        if (!url.EndsWith("errorPage.aspx"))
                            LanguageBusinessLogic.CurrentLanguage = LanguageBusinessLogic.GetLanguageFromUrl(url.ToLower());
                        else
                            LanguageBusinessLogic.CurrentLanguage = LanguageBusinessLogic.GetLanguageFromUrl(Context.Request.Url.AbsoluteUri.ToLower());
                    }
                }
                else
                    Url = url.ToLower();

                Page = BuildPage();
            }
            

            try
            {
                //Codice per evitare il tasto indietro malefico
                context.Response.ClearHeaders();
                context.Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(+1));
                context.Response.Cache.SetCacheability(HttpCacheability.NoCache); 
                context.Response.Cache.SetCacheability(HttpCacheability.Private);
                context.Response.Cache.SetNoStore();
                context.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
                //context.Response.AppendHeader("Cache-Control", "no-cache"); //HTTP 1.1
                //context.Response.AppendHeader("Cache-Control", "private"); // HTTP 1.1
                //context.Response.AppendHeader("Cache-Control", "no-store"); // HTTP 1.1
                //context.Response.AppendHeader("Cache-Control", "must-revalidate"); // HTTP 1.1
                //context.Response.AppendHeader("Cache-Control", "max-stale=0"); // HTTP 1.1 
                //context.Response.AppendHeader("Cache-Control", "post-check=0"); // HTTP 1.1 
                //context.Response.AppendHeader("Cache-Control", "pre-check=0"); // HTTP 1.1 
                context.Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.1 
                context.Response.AppendHeader("Keep-Alive", "timeout=3, max=993"); // HTTP 1.1 
                //context.Response.AppendHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT"); // HTTP 1.1 
            }
            catch { }

            if (Page == null)
                Page = PageParser.GetCompiledPageInstance(url, pathTranslated, context);

            //FrontDebugLogger.PageLogger.Log("GetHandler -> End");
            return Page;
        }
        
        public void ReleaseHandler(IHttpHandler handler)
        {

        }

        public IHttpHandler BuildPage()
        {
            Uri addr = new Uri(LanguageBusinessLogic.GetUrlWithoutLanguage(Context.Request.Url.ToString()));
            Parser = new NetworkUrlParser(addr, this.Connection);
            IHttpHandler page = null;

            if (!HttpContext.Current.Items.Contains(NetworksManager.NetworkStatusPageItemsKey))
               HttpContext.Current.Items.Add(NetworksManager.NetworkStatusPageItemsKey, Parser.NetworkStatus);

            if (!HttpContext.Current.Items.Contains(NetworksManager.NetworkFaceItemsKey))
               HttpContext.Current.Items.Add(NetworksManager.NetworkFaceItemsKey, Parser.CurrentFace);

            if (Parser.NetworkStatus == NetworksStates.InsideNetwork)
            {
                int NetworkId = Parser.NetworkID;

                G2Core.Caching.Visibility.CachingVisibility visibility = Parser.CurrentFace == Faces.Front ? G2Core.Caching.Visibility.CachingVisibility.Global : G2Core.Caching.Visibility.CachingVisibility.User;
                NetworkKey networkKey = new NetworkKey(NetworkId, visibility);
                string cacheKey = networkKey.GetKey();
                if (Cache.Contains(cacheKey))
                    CurrentNetwork = (Network)Cache[cacheKey];
                else
                {
                    if (Parser.CurrentFace == Faces.Admin)
                        CurrentNetwork = new NetCms.Structure.WebFS.StructureNetwork(networkKey);
                    else
                    {
                        networkKey = new NetworkKey(NetworkId, G2Core.Caching.Visibility.CachingVisibility.Global);
                        //CurrentNetwork = new NetCms.Structure.WebFS.StructureNetwork(networkKey);
                        CurrentNetwork = new NetCms.Front.FrontendNetwork(Parser.NetworkName);
                    }
                }
                if (Parser.CurrentFace == Faces.Admin)
                {
                    ApplicationContext.SetCurrentComponent(ApplicationContext.BackofficeGenericComponentName);
                    page = BuildBackofficePage();
                }
                else
                {
                    ApplicationContext.SetCurrentComponent(ApplicationContext.FrontendGenericComponentName);
                    FrontendHandler frontendHandler = new FrontendHandler()
                    {
                        Context = this.Context,
                        Url = this.Url,
                        CurrentNetwork = this.CurrentNetwork,
                        HandlePageContent = this.HandlePageContent,
                        Parser = this.Parser
                    };

                    page = frontendHandler.BuildPage();
                }
            }
            else
                page = Parser.CurrentFace == Faces.Admin ? BuildBackofficePage() : BuildErrorPage();

            return page;
        }
        
        public IHttpHandler BuildBackofficePage()
        {
            string ParsedUrl = Configurations.Paths.AbsoluteSiteManagerRoot + "/";

            if (ClearPages.Contains(this.Parser.PageName)) ParsedUrl += Configurations.Generics.ClearPageName;
            else 
                if( this.Parser.PageName.Contains("-async"))
                    ParsedUrl += Configurations.Generics.AsyncPageName;
                else
                    ParsedUrl += Configurations.Generics.DefaultPageName;
            return PageParser.GetCompiledPageInstance(ParsedUrl, Context.Server.MapPath(ParsedUrl), Context);
        }

        public IHttpHandler BuildErrorPage(int httpResponseStatusCode = -1, string httpResponseStatus = "")
        {
            string ParsedUrl = ParsedUrl = Configurations.Paths.AbsoluteRoot + "/models/error.aspx";

            if (!string.IsNullOrEmpty(httpResponseStatus))
                Context.Response.Status = httpResponseStatus;

            if (httpResponseStatusCode > 0)
                Context.Items["StatusCode"] = httpResponseStatusCode; 

            IHttpHandler page = page = PageParser.GetCompiledPageInstance(ParsedUrl, Context.Server.MapPath(ParsedUrl), Context);

            return page;
        }        
    }
}