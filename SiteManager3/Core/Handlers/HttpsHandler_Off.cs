﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Configurations;
using System.Web;

namespace NetCms.Handlers
{
    /// <summary>
    /// Questo handler gestisce i comportamenti riguardo Https ne progetto Sax.
    /// Attualmente il codice è provvisorio, andrà ridefinito quando saranno chiaramente definite le modalità.
    /// </summary>
    public class HttpsHandler_Off : IHttpsHandler
    {
        
        /// <summary>
        /// Esegue il redirect alls tessa pagina ma in Https
        /// </summary>
        //public void RedirectToSecureRequest()
        //{
        //    if (!IsSecureConnection &&
        //        HttpContext.Current != null &&
        //        HttpContext.Current.Request != null &&
        //        HttpContext.Current.Response != null)
        //    {
        //        HttpContext.Current.Response.Redirect(GetCurrentPageAddressAsSecure());
        //    }
        //    else
        //        throw new InvalidOperationException("Il contesto Http non è disponibile.");
        //}

        /// <summary>
        /// Esegue il redirect alls tessa pagina ma in Http
        /// </summary>
        //public void RedirectToNonSecureRequest()
        //{
        //    if (!IsSecureConnection &&
        //        HttpContext.Current != null &&
        //        HttpContext.Current.Request != null &&
        //        HttpContext.Current.Response != null)
        //    {
        //        HttpContext.Current.Response.Redirect(GetCurrentPageAddressAsNonSecure());
        //    }
        //    else
        //        throw new InvalidOperationException("Il contesto Http non è disponibile.");
        //}

        /// <summary>
        /// Restituisce l'indirizzo della pagina corrente convertito in https
        /// </summary>
        //public string GetCurrentPageAddressAsSecure()
        //{
        //    if (HttpContext.Current != null &&
        //        HttpContext.Current.Request != null &&
        //        HttpContext.Current.Response != null)
        //    {
        //        return ConvertPageAddressAsSecure(HttpContext.Current.Request.Url.ToString());
        //    }
        //    else
        //        throw new InvalidOperationException("Il contesto Http non è disponibile.");
        //}

        /// <summary>
        /// Restituisce l'indirizzo della pagina corrente convertito in http
        /// </summary>
        //public string GetCurrentPageAddressAsNonSecure()
        //{
        //    if (HttpContext.Current != null &&
        //        HttpContext.Current.Request != null &&
        //        HttpContext.Current.Response != null)
        //    {
        //        return ConvertPageAddressAsNonSecure(HttpContext.Current.Request.Url.ToString());
        //    }
        //    else
        //        throw new InvalidOperationException("Il contesto Http non è disponibile.");
        //}

        /// <summary>
        /// Restituisce l'indirizzo della pagina corrente convertito in https
        /// </summary>
        public string ConvertPageAddressAsSecure(string address)
        {
            return address;
        }

        /// <summary>
        /// Restituisce l'indirizzo della pagina corrente convertito in http
        /// </summary>
        //public string ConvertPageAddressAsNonSecure(string address)
        //{
        //    return address.Replace("?secureConnectionIsActive=true&", "?")
        //                  .Replace("&secureConnectionIsActive=true&", "&")
        //                  .Replace("&secureConnectionIsActive=true", "")
        //                  .Replace("?secureConnectionIsActive=true", "");
        //}                  
               
        public bool IsSecureConnection
        {
            get
            {
                return true;
            }
        }
       

        /// <summary>
        /// L'indirizzo al quale fare riferimento come pagina di Login per il frontend.
        /// L'indirizzo deve contenere il segnaposto {0} che verrà rimpiazzato a runtime dal nome della network corrente
        /// Esempio "https://www.net-serv.it/website/{0}/login.aspx"
        /// </summary>
        public string FrontendLoginPageAddress
        {
            get
            {
                return NetCms.Configurations.Paths.AbsoluteRoot + "/{0}/login.aspx";
            }
        }

        /// <summary>
        /// L'indirizzo al quale fare riferimento come pagina di Login per il backoffice.
        /// </summary>
        public string BackofficeLoginPageAddress
        {
            get
            {
                return NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/login.aspx";
            }
        }

        public void RedirectToSecureRequest()
        {
            throw new NotImplementedException();
        }

        public void RedirectToNonSecureRequest()
        {
            throw new NotImplementedException();
        }

        public string GetCurrentPageAddressAsSecure()
        {
            if (HttpContext.Current != null &&
                HttpContext.Current.Request != null &&
                HttpContext.Current.Response != null)
            {
                return ConvertPageAddressAsSecure(HttpContext.Current.Request.Url.ToString());
            }
            else
                throw new InvalidOperationException("Il contesto Http non è disponibile.");
        }

        public string GetCurrentPageAddressAsNonSecure()
        {
            if (HttpContext.Current != null &&
                HttpContext.Current.Request != null &&
                HttpContext.Current.Response != null)
            {
                return ConvertPageAddressAsNonSecure(HttpContext.Current.Request.Url.ToString());
            }
            else
                throw new InvalidOperationException("Il contesto Http non è disponibile.");
        }

        public string ConvertPageAddressAsNonSecure(string address)
        {
            return address;
        }
    }
}
