using System;
using System.Web;
using System.Data;
using System.Linq;
using System.Web.SessionState;
using System.Web.UI;
using NetCms.Connections;
using NetCms.Networks;
using NetCms.Vertical;
using LanguageManager.BusinessLogic;
using NetCms.Structure.WebFS.FileSystemBusinessLogic;

namespace NetCms.Handlers
{
    public class FrontendHandler:CmsHandler, IRequiresSessionState
    {
        public FrontendHandler()
        {
        }

        public CmsConfigs.Model.Config CurrentConfig
        {
            get
            {
                return CmsConfigs.Model.GlobalConfig.GetConfigByNetworkID(CurrentNetwork.ID);
            }
        }

        public override IHttpHandler BuildPage()
        {            
            string ParsedUrl = this.Url;
            Uri uri = Context.Request.Url;

            bool error = false;

            if (uri.ToString().Contains(this.CurrentNetwork.Paths.AbsoluteFrontRoot + "/docs/doc.aspx?id="))
            {
                string[] split = uri.ToString().Split(new string[] { this.CurrentNetwork.Paths.AbsoluteFrontRoot + "/docs/doc.aspx?id=" }, StringSplitOptions.RemoveEmptyEntries);
                int id = 0;
                if (int.TryParse(split[1].ToString(), out id))
                {
                    var document =  NetFrontend.FrontendDocument.Fabricate(id);
                    if (document != null)
                    {
                        this.Context.Response.Redirect(document.FrontendUrl, true);                        
                        return null;
                    }
                    else error = true;
                }
                else error = true;
            }

            if (error) // || Context.Server.MapPath(ParsedUrl).EndsWith("error404.aspx"))
                //ParsedUrl = Configurations.Paths.AbsoluteRoot + "/models/error.aspx";
                //throw new HttpException(404, "File Not Found");  
                throw new NetCms.Exceptions.PageNotFound404Exception(Url);
            else
            {
                #region Elaboro l'URL per aggiungere le informazioni di stile
                if (!uri.Query.Contains("stile") && Context.Request.UrlReferrer != null)
                {
                    string query = Context.Request.UrlReferrer.Query;
                    if (query.Contains("?stile=") || query.Contains("&stile="))
                    {
                        string[] qarray = { "stile" };
                        qarray = query.Split(qarray, StringSplitOptions.None);
                        if (qarray.Length > 1)
                        {
                            string querystring = qarray[1];
                            if (querystring.Contains("&"))
                                querystring.Remove(querystring.IndexOf('&'));
                            querystring = "stile=" + querystring.Replace("=", "");
                            string redirectquery = Context.Request.Url.Query;
                            if (!redirectquery.Contains("?"))
                                querystring = "?" + querystring;
                            else
                                querystring = "&" + querystring;

                            redirectquery = uri.ToString() + querystring;

                            NetCms.Diagnostics.Diagnostics.Redirect(redirectquery);
                        }
                    }
                }
                #endregion

                string path = "";
                //Rimuove dal ParsedUrl il riferimento alla network per elaborare solo il contenuto della pagina
                if (!string.IsNullOrWhiteSpace(NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot))
                    path = ParsedUrl.Replace(NetCms.Networks.NetworksManager.CurrentActiveNetwork.Paths.AbsoluteFrontRoot.ToLower(), string.Empty).ToLower();
                else
                    path = ParsedUrl.ToLower();
                //Gestione delle pagine statiche
                if (NetCms.Front.Static.StaticPagesCollector.ClassesKeysAssociations.ContainsKey(/*path*/ Context.Request.Path))
                {
                    var attribute = NetCms.Front.Static.StaticPagesCollector.ClassesKeysAssociations[/*path*/ Context.Request.Path];
                    NetCms.Front.Static.StaticPagesCollector.CurrentStaticPage = attribute;

                    ParsedUrl = Configurations.Paths.AbsoluteRoot + CurrentConfig.ModelsFolder + "/static.aspx";
                }
                else
                {
                    //Controllo se il ramo nel quale si trova il documento � relativo ad un Applicazione Verticale
                    NetCms.Vertical.VerticalApplication vericalApp = VerticalApplicationsPool.GetCurrentActiveApplication();
                    if (vericalApp != null && !ParsedUrl.ToLower().Contains("errorpage.aspx"))
                    {
                        HttpContext.Current.Items["VerticalAppCurrentAbsoluteWebRoot"] = vericalApp.FrontendURL;
                        //Questa era la riga precedente, adesso la gestione avviene specificando la pagina in una propriet� della classe VerticalApplication
                        //ParsedUrl = Configurations.Paths.AbsoluteRoot + "/models/vapp.aspx";

                        // 
                        string customPageLocation = vericalApp.CustomPageLocation.Contains("/models/") ? vericalApp.CustomPageLocation.Replace("/models/", CurrentConfig.ModelsFolder) : vericalApp.CustomPageLocation;
                        //string customPageLocation = vericalApp.CustomPageLocation;
                        ParsedUrl = Configurations.Paths.AbsoluteRoot + customPageLocation;
                    }
                    else
                    {
                        //Verifico che la pagina sia tra quelle di CMS
                        if (Context.Server.MapPath(ParsedUrl).Contains(".aspx") && HandlePageContent)
                        {
                            //Se lo � :
                            //  1)Controllo la disponibilit� della pagina
                            if (!CheckDocStatus())
                            {
                                //throw new NetCms.Exceptions.PageNotFound404Exception(Url);
                                if (NetCms.Configurations.Debug.GenericEnabled)
                                    throw new HttpException(404, "File Not Found");
                            }
                            else
                            {                               
                               //  2)Reindirizzo la richiesta alla pagina unica che elabora i contenuti di CMS (models/default.aspx)                                
                               string modelsFolder = (CurrentConfig == null) ? string.Empty : CurrentConfig.ModelsFolder;
                               ParsedUrl = Configurations.Paths.AbsoluteRoot + modelsFolder + (modelsFolder.EndsWith(@"/") ? "" : "/" ) + "default.aspx";
                                
                                // check if ParsedUrl exist
                                if (NetCms.Configurations.ThemeEngine.Status == Configurations.ThemeEngine.ThemeEngineStatus.on)
                                {
                                    if (!System.IO.File.Exists(Context.Server.MapPath(ParsedUrl)))
                                    {
                                        if (CurrentConfig.CurrentTheme.ParentTheme != null)                                        
                                            ParsedUrl = Configurations.Paths.AbsoluteRoot + CurrentConfig.CurrentTheme.ParentTheme.ModelFolder + (modelsFolder.EndsWith(@"/") ? "" : "/") + "default.aspx";                                        
                                        else                                        
                                            ParsedUrl = Configurations.Paths.AbsoluteRoot + NetCms.Themes.Parser.SystemTheme.ModelFolder + (modelsFolder.EndsWith(@"/") ? "" : "/") + "default.aspx";                                        
                                    }
                                }

                            }
                        }
                    }
                }
            }

            IHttpHandler page = null;

            if (!NetCms.Configurations.Debug.RawExceptionsEnabled)
            {
                try
                {
                    page = PageParser.GetCompiledPageInstance(ParsedUrl, Context.Server.MapPath(ParsedUrl), Context);
                }
                catch(Exception ex)
                {
                    if (ex is HttpCompileException)
                    {
                        Diagnostics.Diagnostics.TraceException(ex, "", "", typeof(FrontendHandler));
                        throw new HttpException(500, "Internal Server Error");
                    }
                    // throw new NetCms.Exceptions.PageNotFound404Exception(Url);
                    
                    // TODO: salvare il log su altra destinazione 
                    // throw new HttpException(404, "File Not Found");
                }
            }
            else
                page = PageParser.GetCompiledPageInstance(ParsedUrl, Context.Server.MapPath(ParsedUrl), Context);

            return page;
        }
        protected enum Status{ NotFound, NotGranted, FoundAndGranted  }
        protected bool CheckDocStatus()
        {
            bool status = false;
            string folderpath = Parser.FolderPath;
            //DataRow[] rows;
            int rowsCount = 0;
            string PageName = Parser.PageName;
            if (folderpath.Length > 0) // se vero si riferisce alla root folder
            {
                if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang == 0)
                {
                    folderpath = FolderBusinessLogic.TranslatePathOnTheFly(LanguageBusinessLogic.CurrentLanguage.ID, CurrentNetwork.ID, folderpath);
                    PageName = DocumentBusinessLogic.TranslateNameOnTheFly(LanguageBusinessLogic.CurrentLanguage.ID, CurrentNetwork.ID, PageName, folderpath);
                }


                //Controllo se il ramo nel quale si trova il documento � disponibile/esistente
                string sql = ParentsFolderSQL(folderpath);

                //rows = NetFrontend.DAL.FileSystemDAL.ListFoldersRecordsBySqlConditions(sql);
                //if (rows.Length == 0)
                //if (LanguageManager.Utility.Config.EnableMultiLanguage && LanguageManager.BusinessLogic.LanguageBusinessLogic.CurrentLanguage.Default_Lang == 0)
                //{
                    //string lastPart = folderpath.Split('/').Last();
                    //rowsCount = NetFrontend.DAL.FileSystemDAL.CountFoldersRecordsBySqlConditions(sql + " OR LOWER(Label_FolderLang) = '" + lastPart.Replace("-", " ") + "'");
                //}
                //else
                //{
                    rowsCount = NetFrontend.DAL.FileSystemDAL.CountFoldersRecordsBySqlConditions(sql);
                //}

                
                if (rowsCount == 0)
                    status = false;

                ////Controllo se il ramo nel quale si trova il documento � relativo ad un Cartella di Applicazioni Verticali
                //string vericalsql = VerticalTreeSQL(folderpath);               
                //rowsCount = NetFrontend.DAL.FileSystemDAL.CountFoldersRecordsBySqlConditions(vericalsql);
                ////if (rowsCount == 0)
                //if (rowsCount > 0)
                //    return true;

                
                // Check is default.aspx
                if (PageName == Configurations.Generics.DefaultPageName)
                {
                    //Controllo che la pagina sia raggiungibile
                    var row = NetFrontend.DAL.FileSystemDAL.GetDocumentRecordBySqlConditions("Name_Doc = '" + PageName.Replace("'", "''") + "' AND Path_Folder = '" + folderpath.ToLower().Replace("'", "''") + "'");
                    //bool pagefinded = row != null;
                    status = row != null;

                    if (!status)
                    {
                        //Controllo se il ramo nel quale si trova il documento � relativo ad un Cartella di Applicazioni di Cms
                        string cmsappsql = CmsAppsTreeSQL(folderpath);
                        rowsCount = NetFrontend.DAL.FileSystemDAL.CountFoldersRecordsBySqlConditions(cmsappsql);
                        if (rowsCount > 0)
                            status = true;
                        else
                        {
                            //Controllo se il ramo nel quale si trova il documento � relativo ad un Cartella di Applicazioni Verticali
                            string vericalsql = VerticalTreeSQL(folderpath);
                            rowsCount = NetFrontend.DAL.FileSystemDAL.CountFoldersRecordsBySqlConditions(vericalsql);
                            if (rowsCount == 0)
                                status = true;
                        }
                    }

                   // return pagefinded;
                }

                //Controllo che la pagina sia raggiungibile
                if (PageName != Configurations.Generics.DefaultPageName)
                {                    
                    var row = NetFrontend.DAL.FileSystemDAL.GetDocumentRecordBySqlConditions("Name_Doc = '" + PageName.Replace("'", "''") + "' AND Path_Folder = '" + folderpath.ToLower().Replace("'", "''") + "'");

                    status = row != null;
                    int stato = 0;

                    if (status)
                        int.TryParse(row["Stato_Doc"].ToString(), out stato);

                    if ((!status && Configurations.Generics.DefaultPageName != PageName) || (status && stato == 2))
                        status = false;

                }
                
                //Controllo se il ramo nel quale si trova il documento � non raggiungibile
                sql += " AND Stato_Folder = 2";
                //rows = NetFrontend.DAL.FileSystemDAL.ListFoldersRecordsBySqlConditions(sql);
                //if (rows.Length > 0)
                rowsCount = NetFrontend.DAL.FileSystemDAL.CountFoldersRecordsBySqlConditions(sql);
                //if (rowsCount == 0)
                //    return false;
                if (rowsCount > 0)
                {                    
                    status = false;
                }

                // � stata richiamata la pagina di anteprima delle revisioni?
                if (!status && PageName.ToLower() == Configurations.Generics.PreviewPageName)
                        status = true;
            }
            else
            {
                // check sulla root folder
                if (PageName == "" || PageName.ToLower() == "default.aspx")
                    status = true;
            }

            return status;
        }
        
        protected string ParentsFolderSQL(string folderpath)
        {
            string folders = "Network_Folder = " + this.CurrentNetwork.ID + " AND (";

            string[] array = folderpath.Split('/');
            string pathtot = "";
            bool first = true;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i].Length > 0)
                {
                    if (!first)
                        folders += " OR ";
                    else
                        first = false;

                    pathtot += "/" + array[i].Replace("'", "''");

                    folders += " Path_Folder = '" + pathtot + "'";
                }
            }

            folders += ")";
            return folders;
        }
        
        protected string VerticalTreeSQL(string folderpath)
        {
            string folders = "Network_Folder = " + this.CurrentNetwork.ID + " AND (";

            string[] array = folderpath.Split('/');
            string pathtot = "";
            bool first = true;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i].Length > 0)
                {
                    if (!first)
                        folders += " OR ";
                    else
                        first = false;

                    pathtot += "/" + array[i].Replace("'", "''");

                    folders += " Path_Folder = '" + pathtot + "'";
                }
            }

            folders += ") AND HomeType_Folder > 0 AND System_Folder = 5";
            return folders;
        }

        protected string CmsAppsTreeSQL(string folderpath)
        {
            string folders = "Network_Folder = " + this.CurrentNetwork.ID + " AND (";

            string[] array = folderpath.Split('/');
            string pathtot = "";
            bool first = true;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i].Length > 0)
                {
                    if (!first)
                        folders += " OR ";
                    else
                        first = false;

                    pathtot += "/" + array[i].Replace("'", "''");

                    folders += " Path_Folder = '" + pathtot + "'";
                }
            }

            folders += ") AND HomeType_Folder > 0";
            
            return folders;
        }
    }
}