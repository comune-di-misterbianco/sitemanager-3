using System;
using System.Web;
using System.Data;
using System.Web.SessionState;
using System.Web.UI;
using NetCms.Connections;
using NetCms.Networks;
using NetCms.Vertical;
namespace NetCms.Handlers
{
    public abstract class CmsHandler
    {
        public Network CurrentNetwork { get; set; }
        public bool HandlePageContent { get; set; }
        public HttpContext Context { get; set; }
        public NetworkUrlParser Parser { get; set; }
        public string Url { get; set; }

        public CmsHandler()
        {
        }

        public abstract IHttpHandler BuildPage();
    }
}