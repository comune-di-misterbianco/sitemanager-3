﻿using System;
using System.Web;

namespace NetCms.Handlers
{
    public class ApplicationModule : IHttpModule
    {
        public void Dispose()
        {
            //clean-up code here.
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += this.Application_BeginRequest;
            context.EndRequest += this.Application_EndRequest;
        }

        private void Application_BeginRequest(Object source,
             EventArgs e)
        {
            if (NetCms.Configurations.Paths.Extension == ".aspx")
            {
                HttpApplication application = (HttpApplication)source;
                HttpContext context = application.Context;
                string page = NetCms.Configurations.Paths.PageName;
            }
        }

        private void Application_EndRequest(Object source, EventArgs e)
        {
            if (NetCms.Configurations.Paths.Extension == ".aspx")
            {
                HttpApplication application = (HttpApplication)source;
                HttpContext context = application.Context;
                string page = NetCms.Configurations.Paths.PageName;
            }
        }
    }
}
