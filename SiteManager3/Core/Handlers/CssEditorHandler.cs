﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using NetCms;

namespace NetCms.Handlers
{
    public class CssEditorHandler : IHttpHandler
    {
        public bool IsReusable { get { return false; } }

        public void ProcessRequest(HttpContext context)
        {

            string network = System.Web.HttpContext.Current.Request["network"].ToString();

            string cssCode = NetCms.GUI.CssForEditor.GetCssCode(network);
            
            context.Response.Buffer = true;
            context.Response.Clear();
            context.Response.AddHeader("Content-Type", "text/css");                           
            context.Response.ContentType = "text/css";
            context.Response.Write(cssCode);
            context.Response.End();
        }
    }
}
