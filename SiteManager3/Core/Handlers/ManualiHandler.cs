﻿using System;
using System.Web;
using System.Data;
using System.Web.SessionState;
using System.Web.UI;
using NetCms.Connections;
using NetCms.Networks;
using System.IO;

namespace NetCms.Handlers
{
    public class ManualiHandler : IHttpHandler
    {
        public bool IsReusable { get { return false; } }

        public void ProcessRequest(HttpContext context)
        {
            string path = context.Request.MapPath(context.Request.Path);
            FileStream fs = File.OpenRead(path);
            byte[] content = new byte[fs.Length];
            fs.Read(content, 0, content.Length);

            context.Response.Buffer = true;
            context.Response.Clear();
            string cdfn = path.Substring(path.LastIndexOf('\\') + 1, path.Length - path.LastIndexOf('\\') - 1);
            if (HttpContext.Current.Request.Browser.Browser == "IE")
            {
                context.Response.AddHeader("Pragma", "public");
                context.Response.AddHeader("Expires", "0");
                context.Response.AddHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                context.Response.AddHeader("Content-Type", "application-download");
                context.Response.AddHeader("Content-Length", fs.Length.ToString());
                context.Response.AddHeader("Content-Disposition", "attachment; filename=" + cdfn);
                context.Response.AddHeader("Content-Transfer-Encoding", "binary");
                context.Response.AddHeader("Content-Description", "File Transfer");
                context.Response.AddHeader("X-Download-Options", "noopen"); // IE7 & IE8?
                context.Response.AddHeader("X-Content-Type-Options", "nosniff"); // IE8 bonus?
            }
            else
            {
                context.Response.AddHeader("Content-Type", "application-download");
                context.Response.AddHeader("Content-Length", fs.Length.ToString());
                context.Response.AddHeader("Content-Disposition", "attachment; filename=" + cdfn);
            }

            fs.Close();
            context.Response.ContentType = "application/pdf";
            context.Response.BinaryWrite(content);
            context.Response.End();
        }
    }
}