﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Users;
using NetCms.Grants.Interfaces;

namespace NetCms.Grants
{
    public abstract class ObjectProfileCriteria
    {
        public ObjectProfileCriteria()
        {}

        public virtual int ID
        { get; set; }

        public abstract ObjectProfile ObjectProfile
        { get; set; }

        public virtual IGrantsCriteria Criteria
        { get; set; }

        public virtual int Value
        { get; set; }

        public virtual User Updater
        { get; set; }

        public override int GetHashCode()
        {
            int hashCode = 0;
            hashCode = hashCode ^ ObjectProfile.ID.GetHashCode() ^ Criteria.ID.GetHashCode();
            return hashCode;
        }

        public override bool Equals(object obj)
        {
            var toCompare = obj as ObjectProfileCriteria;
            if (toCompare == null)
            {
                return false;
            }
            return (this.GetHashCode() == toCompare.GetHashCode());
        }
    }
}
