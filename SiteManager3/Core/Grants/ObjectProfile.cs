﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Users;
using NetCms.Networks.WebFS;
using NetCms.Grants.Interfaces;

namespace NetCms.Grants
{
    public abstract class ObjectProfile
    {
        public ObjectProfile()
        {
            //CriteriaCollection = new HashSet<ObjectProfileCriteria>();
        }

        public virtual int ID
        { get; set; }

        public virtual IGrantsFindable WfsObject
        { get; set; }

        public virtual Profile Profile
        { get; set; }

        public virtual User Creator
        { get; set; }

        public abstract ICollection<ObjectProfileCriteria> CriteriaCollection
        { get; set; }

    }
}
