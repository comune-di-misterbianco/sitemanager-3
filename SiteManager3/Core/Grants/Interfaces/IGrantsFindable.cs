﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetCms.Networks.Grants;
using System.Data;
using NetCms.Users;
using NHibernate;
using NetCms.Networks;

namespace NetCms.Grants.Interfaces
{
    public interface IGrantsFindable
    {
        int ID { get; }

        GrantObjectType GrantObjectType { get; }

        GrantsFinder Criteria { get; }

        bool Grant { get; }

        #region NeedToShow_Loaded -> Specifica se la cartella deve essere visualizzata a causa di un figlio che ha bisogno di essere visualizzato.

        bool NeedToShow { get; }

        #endregion

        GrantsFinder CriteriaByProfile(Profile profile);

        GrantsFinder CriteriaByProfileAndNetwork(Profile profile, NetworkKey networkKey);

        GrantValue CriteriaForApplication(CriteriaTypes criteria, string appId);

        ICollection<ObjectProfile> GrantObjectProfiles { get; }

        //Metodo di cancellazione permessi collegato all'associazione oggetto-profilo con transazione interna
        void DeleteGrantsForAssociation(int associationID);
        //Metodo di aggiunta permesso collegato all'associazione oggetto-profilo con transazione interna
        bool AddGrant(int associationID, int grantValue, int idCriterio, int creatorUserID);

        /* I seguenti metodi sono stati aggiunti poichè vi è la necessità di utilizzarli in sequenza del tipo cancellazione tutti-inserimento in ciclo for,
         * in modo da permettere l'uso all'interno di un'unica transazione e sessione interna.
         */
        //Metodo di cancellazione permessi collegato all'associazione oggetto-profilo senza transazione interna
        void DeleteGrantsForAssociation(int associationID,ISession session);
        //Metodo di aggiunta permesso collegato all'associazione oggetto-profilo senza transazione interna
        void AddGrant(int associationID, int grantValue, int idCriterio, int creatorUserID, ISession session);

        //Questo metodo consente di sapere se il profilo scelto è già associato all'oggetto di cui si stanno impostando i permessi
        bool CheckIfProfileIsAssociated(int profileId, ISession session = null);
        //Questo metodo consente di sapere se il profilo scelto è già associato all'oggetto di cui si stanno impostando i permessi
        bool CheckIfProfileIsAssociated(int profileId,int networkId, ISession session = null);

        //Crea l'associazione tra l'oggetto e il profilo (utente o gruppo), per poter impostare i permessi
        bool AddGrantProfile(int profileId);
        //Crea l'associazione tra l'oggetto e il profilo (utente o gruppo), per poter impostare i permessi
        bool AddGrantProfile(int profileId, int networkId);

        //Elimina l'associazione oggetto/profilo, passando l'id dell'associazione
        bool DeleteGrantProfile(int objectProfileId);

        //Crea l'associazione tra l'oggetto e il profilo (utente o gruppo), per poter impostare i permessi senza transazione interna
        ObjectProfile AddGrantProfile(int profileId, ISession session);
        //Crea l'associazione tra l'oggetto e il profilo (utente o gruppo), per poter impostare i permessi senza transazione interna
        ObjectProfile AddGrantProfile(int profileId, int networkId, ISession session);

        //Elimina l'associazione oggetto/profilo, passando l'id dell'associazione senza transazione interna
        bool DeleteGrantProfile(int objectProfileId, ISession session);
    }
}
