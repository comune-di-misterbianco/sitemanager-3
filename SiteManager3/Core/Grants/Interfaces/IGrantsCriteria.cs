﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Grants.Interfaces
{
    public interface IGrantsCriteria
    {
        int ID { get; }
        string Nome { get; }
        string Key { get; }
    }
}
