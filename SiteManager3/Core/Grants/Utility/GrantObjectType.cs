﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Grants
{
    public enum GrantObjectType
    {
        Document,
        Folder,
        VerticalApplication
    }
}
