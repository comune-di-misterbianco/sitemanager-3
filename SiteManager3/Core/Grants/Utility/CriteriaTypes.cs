﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Grants
{
    public enum CriteriaTypes
    {
        Show,
        Create,
        Modify,
        Delete,
        Grants,
        NewFolder,
        Redactor,
        Publisher,
        Bandi,
        Advanced,
        FoldersVisibility,
        Frontend
    }
}
