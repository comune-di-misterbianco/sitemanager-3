﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.Grants
{
    public static class GrantsUtility
    {
        public static string CriteriaSwitch(CriteriaTypes type)
        {
            switch (type)
            {
                case CriteriaTypes.Advanced:
                    return "advanced";
                case CriteriaTypes.Create:
                    return "create";
                case CriteriaTypes.Delete:
                    return "delete";
                case CriteriaTypes.Grants:
                    return "grants";
                case CriteriaTypes.NewFolder:
                    return "newfolder";
                case CriteriaTypes.Redactor:
                    return "redactor";
                case CriteriaTypes.Publisher:
                    return "publisher";
                case CriteriaTypes.Bandi:
                    return "bandi";
                case CriteriaTypes.Show:
                    return "show";
                case CriteriaTypes.Modify:
                    return "modify";
                case CriteriaTypes.FoldersVisibility:
                    return "foldersvisibility";
                case CriteriaTypes.Frontend:
                    return "frontend";
            }

            return null;
        }

        public static CriteriaTypes CriteriaSwitch(string type)
        {
            switch (type)
            {
                case "advanced":
                    return CriteriaTypes.Advanced;
                case "create":
                    return CriteriaTypes.Create;
                case "delete":
                    return CriteriaTypes.Delete;
                case "grants":
                    return CriteriaTypes.Grants;
                case "newfolder":
                    return CriteriaTypes.NewFolder;
                case "redactor":
                    return CriteriaTypes.Redactor;
                case "publisher":
                    return CriteriaTypes.Publisher;
                case "bandi":
                    return CriteriaTypes.Bandi;
                case "show":
                    return CriteriaTypes.Show;
                case "modify":
                    return CriteriaTypes.Modify;
                case "foldersvisibility":
                    return CriteriaTypes.FoldersVisibility;
                case "frontend":
                    return CriteriaTypes.Frontend;
            }

            throw new Exception("Criterio non esistente");
        }
    }
}
