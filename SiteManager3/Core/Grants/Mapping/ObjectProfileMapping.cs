﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Grants.Mapping
{
    public class ObjectProfileMapping : ClassMap<ObjectProfile>
    {
        public ObjectProfileMapping()
        {
            Table("object_profiles");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_ObjectProfile");

            References(x => x.Profile).LazyLoad(Laziness.False)
                 .ForeignKey("object_profiles_profile")
                 .Column("Profile_ObjectProfile")
                 .Fetch.Select();

            References(x => x.Creator)
                 .ForeignKey("object_profiles_creator")
                 .Column("Creator_ObjectProfile")
                 .Fetch.Select();

        }
    }
}
