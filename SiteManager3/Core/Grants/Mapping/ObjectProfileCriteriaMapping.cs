﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace NetCms.Grants.Mapping
{
    public class ObjectProfileCriteriaMapping : ClassMap<ObjectProfileCriteria>
    {
        public ObjectProfileCriteriaMapping()
        {
            Table("object_profiles_criteria");
            Cache.NonStrictReadWrite();
            Id(x => x.ID).Column("id_ObjectProfileCriteria");
          
            Map(x => x.Value).Column("Value_ObjectProfileCriteria");

            //References(x => x.Criteria)
            //     .ForeignKey("object_profiles_criteria_criterio")
            //     .Column("Criteria_ObjectProfileCriteria")
            //     .Fetch.Select();

            References(x => x.Updater)
                 .ForeignKey("object_profiles_criteria_updater")
                 .Column("Updater_ObjectProfileCriteria")
                 .Fetch.Select();
        }
    }
}
