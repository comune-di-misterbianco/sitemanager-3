﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
//using NetCms.Structure.Grants;
using NetCms.Users;
using NetCms.Grants.Interfaces;
using NetCms.Grants;

namespace NetCms.Structure.WebFS
{
    public class ObjectProfileAssociation
    {
        //private NetCms.Connections.Connection _Connection;
        //private NetCms.Connections.Connection Connection
        //{
        //    get
        //    {
        //        if (_Connection == null)
        //            _Connection = NetCms.Connections.ConnectionsManager.CurrentNetworkConnection;
        //        return _Connection;
        //    }
        //}

        public int ID
        {
            get { return _ID; }
            private set { _ID = value; }
        }
        private int _ID;

        public Profile Profile
        {
            get { return _Profile; }
            private set { _Profile = value; }
        }
        private Profile _Profile;

        public string ObjectID
        {
            get { return GrantObject.ID.ToString(); }
        }
        private string _ObjectID;

        public IGrantsFindable GrantObject
        {
            get { return _GrantObject; }
            private set { _GrantObject = value; }
        }
        private IGrantsFindable _GrantObject;

        public bool AssociationFound
        {
            get { return this.ID > 0; }
        }

        public ObjectProfileAssociation(IGrantsFindable Object, int associationID)
        {
            //string sql = "";

            //if (wfsObject.ObjectType == NetCms.Networks.WebFS.WebFSObjectTypes.Document)
            //    sql = "SELECT * FROM docprofiles WHERE id_DocProfile = " + associationID;
            //else
            //    sql = "SELECT * FROM folderprofiles WHERE id_FolderProfile = " + associationID;

            //DataTable queryResult = this.Connection.SqlQuery(sql);

            //if (queryResult.Rows.Count > 0) Init(wfsObject,queryResult.Rows[0]);
            this.GrantObject = Object;
            if (Object.GrantObjectProfiles != null)
            {
                var associations = Object.GrantObjectProfiles.Where(x => x.ID == associationID);
                if (associations.Count() > 0)
                {
                    
                    ObjectProfile associationProfile = associations.First();
                    this.Profile = associationProfile.Profile;
                    this.ID = associationProfile.ID;
                }
            }
        }
               
        //public ObjectProfileAssociation(NetCms.Networks.WebFS.CmsWebFSObject wfsObject, DataRow data)
        //{
        //    Init(wfsObject, data);
        //}

        //private void Init(NetCms.Networks.WebFS.CmsWebFSObject wfsObject, DataRow data)
        //{
        //    this.WfsObject = wfsObject;
        //    if (wfsObject.ObjectType == NetCms.Networks.WebFS.WebFSObjectTypes.Document)
        //    {
        //        //this.Profile = new Profile(data["Profile_DocProfile"].ToString());
        //        this.Profile = ProfileBusinessLogic.GetById(int.Parse(data["Profile_DocProfile"].ToString()));
        //        this.ID = data["id_DocProfile"].ToString();
        //    }
        //    else
        //    {
        //        //this.Profile = new Profile(data["Profile_FolderProfile"].ToString());
        //        this.Profile = ProfileBusinessLogic.GetById(int.Parse(data["Profile_FolderProfile"].ToString()));
        //        this.ID = data["id_FolderProfile"].ToString();
        //    }
        //}

        public bool IsSuperAdminForThis(UserProfile ProfileToCheck)
        {
            return (!Profile.IsUser) &&  (Profile as GroupProfile).Group.ImSuperAdmin(ProfileToCheck.User);
            /*if (Profile.IsUser)
                return false;
            else
            {
                Group gruppo = Profile.Group;
                do
                {
                    if (gruppo.GetSuperAdmins().Contains(ProfileToCheck.User)) return true;
                    gruppo = gruppo.Parent;
                }
                while (gruppo != null);

                return false;
            }*/
        }
    }
}
