﻿using NetCms.Users;
using NetCms.Grants;

namespace NetCms.Networks.Grants
{
    public abstract class GrantsFinder
    {
        private NetCms.Connections.Connection _Conn;
        protected NetCms.Connections.Connection Conn
        {
            get
            {
                //if (_Conn == null)
                //    _Conn = NetCms.Connections.ConnectionsManager.CommonMySqlConnection;
                //return _Conn;
                NetCms.Connections.ConnectionsManager ConnManager = new NetCms.Connections.ConnectionsManager();
                return ConnManager.CommonConnection;
            }
        }


        public Profile Profile { get; private set; }

        public GrantsFinder(Profile profile)
        {
            this.Profile = profile;
        }

        public virtual GrantValue this[string criterio]
        {
            get
            {
                GrantValue value;

                value = this.Profile.IsUser ? GrantValueForUser(criterio) :
                                              GrantValueForGroup(criterio);

                return value;
            }
        }

        public virtual GrantValue this[CriteriaTypes criterio]
        {
            get
            {
                GrantValue value;

                value = this.Profile.IsUser ? GrantValueForUser(GrantsUtility.CriteriaSwitch(criterio)) :
                                              GrantValueForGroup(GrantsUtility.CriteriaSwitch(criterio));

                return value;
            }
        }

        public abstract GrantValue CheckForGrant(string criterio);

        protected abstract GrantValue GrantValueForGroup(string criterio, int network = 0);

        protected abstract int GetIdCriterio(string criterio);

        protected abstract GrantValue GrantValueForUser(string criterio, int network = 0);

        public abstract GrantValue NeedToShow(string criterio, int network = 0);
    }
}
