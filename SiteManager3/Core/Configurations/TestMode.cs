using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;


/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Configurations
{
    public static class TestMode
    {
        public static System.Collections.Specialized.NameValueCollection Options
        {
            get
            {
                return _Options ?? (_Options = InitCollection());
            }
        }
        private static System.Collections.Specialized.NameValueCollection _Options;
        private static System.Collections.Specialized.NameValueCollection InitCollection()
        {
            System.Collections.Specialized.NameValueCollection coll = null;
            try
            {
                coll = (System.Collections.Specialized.NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("TestMode");
            }
            catch {}
            if(coll == null)
                coll = new System.Collections.Specialized.NameValueCollection();
            return coll;
        }
         
        public static bool GetOptionEnableStatus(string key)
        {
            lock (Options)
            {
                bool result = false;
                string opt = Options[key];
                if (opt != null) bool.TryParse(opt, out result);
                return result;
            }
        }
        public static string GetOptionValue(string key)
        {
            lock (Options)
            {
                string opt = Options[key];
                return opt;
            }
        }

        public static bool GenericEnabled
        {
            get
            {
                return (_GenericEnabled ?? (_GenericEnabled = new bool?(GetOptionEnableStatus("enableGenericTest")))).Value;
            }
        }
        private static bool? _GenericEnabled;
    }
}