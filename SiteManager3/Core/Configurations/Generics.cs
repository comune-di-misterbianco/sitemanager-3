using System;
using System.Configuration;
using System.Web;
using System.Linq;
using System.Xml;
using System.Reflection;
using System.Net;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Configurations
{
    public enum ConfigurationStates { NotLoaded = 0,Enabled = 1, Disabled = 2,Error = 3, NotSpecified }

    public static class Generics
    {
        public const string UserCustomDebugMode = "UserCustomDebugMode";
        public const string DefaultPageName = "default.aspx";
        public const string AsyncPageName = "async.aspx";
        public const string DebugKey = "EnableNetServiceDebug";
        public const string StylePageName = "style.aspx";
        public const string ClearPageName = "clear.aspx";
        public const string PreviewPageName = "preview.aspx";

        /// <summary>
        /// Specifica se per questo portale � stato il Log di tutte le query Sql effettuate a livello di Back-Office
        /// </summary>
        public static string BackAccountSessionKey
        {
            get
            {
                if (_BackAccountSessionKey == null)
                    _BackAccountSessionKey = System.Web.Configuration.WebConfigurationManager.AppSettings["SiteManagerBackAccountSessionKey"].ToLower();
                
                return _BackAccountSessionKey;
            }
        }
        private static string _BackAccountSessionKey;

        public static string BackAccountTokenID
        {
            get
            {
                if (_BackAccountTokenID == null)
                    _BackAccountTokenID = System.Web.Configuration.WebConfigurationManager.AppSettings["BackAccountTokenID"].ToLower();

                return _BackAccountTokenID;
            }
        }
        private static string _BackAccountTokenID;

        /// <summary>
        /// Specifica se per questo portale � stato il Log di tutte le query Sql effettuate a livello di Back-Office
        /// </summary>
        public static ConfigurationStates SqlLogging
        {
            get
            {
                if (_SqlLogging == ConfigurationStates.NotLoaded)
                    _SqlLogging = XmlConfig.GetBooleanAttribute("/Portal/configs/log", "enable") ? ConfigurationStates.Enabled : ConfigurationStates.Disabled;
                
                return _SqlLogging;
            }
        }
        private static ConfigurationStates _SqlLogging;

        /// <summary>
        /// Specifica il quantitativo di query di log da tenere in cache prima di registrarle sul database.
        /// Default: 30;
        /// </summary>
        public static int SqlLoggingSubmitLimit
        {
            get
            {
                if (_SqlLoggingSubmitLimit == -1)
                {
                    _SqlLoggingSubmitLimit = XmlConfig.GetIntegerAttribute("/Portal/configs/log", "submitlimit");
                    if (_SqlLoggingSubmitLimit == int.MinValue) _SqlLoggingSubmitLimit = 30;
                }

                return _SqlLoggingSubmitLimit;
            }

        }
        private static int _SqlLoggingSubmitLimit = -1;

        /// <summary>
        /// Elenco dei nomi della cartelle di sistema del Sito Web
        /// Queste non vengono elaborate dall'handler delle pagine di cms del frontend
        /// Permettendo l'utilizzo di applicazioni esterne al CMS
        /// </summary>
        public static string[] SystemFolders
        {
            get
            {
                if (_SystemFolders == null)
                    _SystemFolders = System.Web.Configuration.WebConfigurationManager.AppSettings["SystemFolders"].ToLower().Split(',');
                
                return _SystemFolders;
            }
        }
        private static string[] _SystemFolders;

        /// <summary>
        /// Elenco dei nomi dei file di sistema del Sito Web
        /// Questi non vengono elaborati dall'handler delle pagine di cms del frontend
        /// Permettendo l'utilizzo di applicazioni esterne al CMS
        /// </summary>
        public static string[] SystemFiles
        {
            get
            {
                if (_SystemFiles == null)
                    _SystemFiles = System.Web.Configuration.WebConfigurationManager.AppSettings["SystemFiles"].ToLower().Split(',');
                
                return _SystemFiles;
            }
        }
        private static string[] _SystemFiles;
        
        public static bool UseSecureConnection
        {
            get
            {
                if (_UseSecureConnection == null)
                    _UseSecureConnection = new bool?(System.Web.Configuration.WebConfigurationManager.AppSettings["LoginProtocol"] != null && 
                                                     System.Web.Configuration.WebConfigurationManager.AppSettings["LoginProtocol"].ToLower() == "https");

                return _UseSecureConnection.Value;
            }
        }
        private static bool? _UseSecureConnection;

        public static int MinimumPasswordLength
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["MinimumPasswordLength"] != null)
                {
                    string pwLenght = System.Web.Configuration.WebConfigurationManager.AppSettings["MinimumPasswordLength"];
                    return int.Parse(pwLenght);
                }                
                return 12;
            }
        }

        public static G2Core.Common.StringCollection Servers
        {
            get
            {
                if (_Servers == null)
                {
                    _Servers = new G2Core.Common.StringCollection();
                    XmlNodeList oNodeList = Configurations.XmlConfig.GetNodes("/Portal/configs/servers/host");
                    int i = -1;
                    while (oNodeList != null && oNodeList.Count > ++i)
                    {
                        if (oNodeList[i] != null && oNodeList[i].Attributes["ip"] != null)
                            _Servers.Add(oNodeList[i].Attributes["ip"].Value);
                    }
                }
                return _Servers;
            }
        }
        private static G2Core.Common.StringCollection _Servers;
        /// <summary>
        /// Indica lo stato di encrypting delle password usate nel web.config
        /// </summary>
        private static string _PasswordCripted;
        public static bool PasswordCripted 
        {
            get
            {
                bool output = false;
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["PasswordCripted"] != null)
                {
                    _PasswordCripted = System.Web.Configuration.WebConfigurationManager.AppSettings["PasswordCripted"].ToLower();
                    if (_PasswordCripted != null && _PasswordCripted == "true")
                        output = true;
                }
                return output;
            }
        }

        private static string _NotifyEnabled;
        public static bool NotifyEnabled
        {
            get
            {
                bool output = false;
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["NotifyEnabled"] != null)
                {
                    _NotifyEnabled = System.Web.Configuration.WebConfigurationManager.AppSettings["NotifyEnabled"].ToLower();
                    if (_NotifyEnabled != null && _NotifyEnabled == "true")
                        output = true;
                }
                return output;
            }
        }

        private static string _ChiarimentiEnabled;
        public static bool ChiarimentiEnabled
        {
            get
            {
                bool output = false;
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["ChiarimentiEnabled"] != null)
                {
                    _ChiarimentiEnabled = System.Web.Configuration.WebConfigurationManager.AppSettings["ChiarimentiEnabled"].ToLower();
                    if (_ChiarimentiEnabled != null && _ChiarimentiEnabled == "true")
                        output = true;
                }
                return output;
            }
        }


        


        private static string _MostValuableGrant;
        public static bool MostValuableGrant
        {
            get
            {

                if (_MostValuableGrant == null)
                    _MostValuableGrant = XmlConfig.GetAttribute("/Portal/configs/grants", "winnergrant");
                
                return _MostValuableGrant == "allow";
            }
        }

        private static bool? _TemplatesEnable;
        public static bool TemplatesEnable
        {
            get
            {

                if (_TemplatesEnable == null)
                    _TemplatesEnable = XmlConfig.GetBooleanAttribute("/Portal/configs/templates", "enable");

                return _TemplatesEnable.Value;
            }
        }

        private static string _WebEditorWidth;
        public static string WebEditorWidth
        {
            get
            {

                if (_WebEditorWidth == null)
                    _WebEditorWidth = XmlConfig.GetAttribute("/Portal/configs/webeditor", "width");

                return _WebEditorWidth;
            }
        }
        
        private static bool? _EnableFrontMenuGrants;
        public static bool EnableFrontMenuGrants
        {
            get
            {
                if (_EnableFrontMenuGrants == null)
                    _EnableFrontMenuGrants = new bool?(XmlConfig.GetBooleanAttribute("/Portal/configs", "frontMenuGrants"));

                return _EnableFrontMenuGrants.Value;
            }
        }

        /// <summary>
        /// Specifica se per questo portale � stato il Log di tutte le query Sql effettuate a livello di Back-Office
        /// </summary>
        public static string HttpsHandlerClassName
        {
            get
            {
                if (_HttpsHandlerClassName == null)
                    _HttpsHandlerClassName = System.Web.Configuration.WebConfigurationManager.AppSettings["HttpsHandlerClass"];

                return _HttpsHandlerClassName;
            }
        }
        private static string _HttpsHandlerClassName;

        public static IHttpsHandler GetHttpsHandler()
        {
            AppDomain MyDomain = AppDomain.CurrentDomain;
            Assembly[] AssembliesLoaded = MyDomain.GetAssemblies();



            var foundType = (from a in AssembliesLoaded
                            from type in a.GetTypes()
                            where type.FullName == HttpsHandlerClassName //&& typeof(IHttpsHandler).IsAssignableFrom(type)
                            select type).FirstOrDefault();
            if (foundType == null)
            {
                throw new InvalidOperationException("Non � stato possibile trovare la classe associata alla stringa di configurazione 'HttpsHandlerClass'. Verificare nel web.config che il 'value' della voce 'HttpsHandlerClass' sia correttamente configurato.");
            }
            else
            {
                return System.Activator.CreateInstance(foundType) as IHttpsHandler;
            }
        }


        public static LoginBoxModeType LoginBoxMode
        {
            get
            {
                string loginMode = System.Web.Configuration.WebConfigurationManager.AppSettings["LoginBoxMode"].ToLower();

                switch (loginMode)
                {
                    //default:
                    //    _LoginBoxMode = LoginBoxModeType.off;
                    //    break;
                    case "off":
                        _LoginBoxMode = LoginBoxModeType.off;
                        break;
                    case "modulo":
                        _LoginBoxMode = LoginBoxModeType.modulo;
                        break;
                    case "head":
                        _LoginBoxMode = LoginBoxModeType.head;
                        break;
                }
                return _LoginBoxMode;
            }
        }
        private static LoginBoxModeType _LoginBoxMode;

        public enum LoginBoxModeType 
        {
            off,
            modulo,
            head
        }

        public static LoginModeType LoginMode
        {
            get
            {
                string loginMode = System.Web.Configuration.WebConfigurationManager.AppSettings["LoginMode"].ToLower();

                switch (loginMode)
                {                   
                    case "standard":
                        _LoginMode = LoginModeType.Standard;
                        break;
                    case "onlyad":
                        _LoginMode = LoginModeType.Active_Directory;
                        break;
                    case "mixed":
                        _LoginMode = LoginModeType.Mixed;
                        break;
                }
                return _LoginMode;
            }
        }
        private static LoginModeType _LoginMode;

        public enum LoginModeType
        {
            Standard,
            Active_Directory,
            Mixed,
        }

        public static bool LoginManager
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["LoginManager"] != null)
                    if (System.Web.Configuration.WebConfigurationManager.AppSettings["LoginManager"].ToLower() == "true")
                        return true;
                return false;
            }
        }

        public static string ActivationUrl
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["ActivationUrl"] != null)
                    return System.Web.Configuration.WebConfigurationManager.AppSettings["ActivationUrl"];
                else
                    return "";
            }
        }


        public static string ActiveDirectoryDomain
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["ActiveDirectoryDomain"] != null)
                    return System.Web.Configuration.WebConfigurationManager.AppSettings["ActiveDirectoryDomain"].ToString();
                return string.Empty;
            }
        }

        public static string ActiveDirectoryPath
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["ActiveDirectoryPath"] != null)
                    return System.Web.Configuration.WebConfigurationManager.AppSettings["ActiveDirectoryPath"].ToString();
                return string.Empty;
            }
        }

        public static string ActiveDirectoryUser
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["ActiveDirectoryUser"] != null)
                    return System.Web.Configuration.WebConfigurationManager.AppSettings["ActiveDirectoryUser"].ToString();
                return string.Empty;
            }
        }

        public static string ActiveDirectoryPw
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["ActiveDirectoryPw"] != null)
                    return System.Web.Configuration.WebConfigurationManager.AppSettings["ActiveDirectoryPw"].ToString();
                return string.Empty;
            }
        }

        #region semantic_config

        public static bool EnableSematic
        {
            get {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["EnableSematic"] != null)
                    if (System.Web.Configuration.WebConfigurationManager.AppSettings["EnableSematic"].ToLower() == "true")
                        return true;
                return false;
            }
        }

        public static bool EnableFrontSemanticResults
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["EnableFrontSemanticResults"] != null)
                    if (System.Web.Configuration.WebConfigurationManager.AppSettings["EnableFrontSemanticResults"].ToLower() == "true")
                        return true;
                return false;
            }
        }

        public static bool EnableFrontSolrSearchEngine
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["EnableFrontSolrSearchEngine"] != null)
                    if (System.Web.Configuration.WebConfigurationManager.AppSettings["EnableFrontSolrSearchEngine"].ToLower() == "true")
                        return true;
                return false;
            }
        }

        public static bool EnableBackSolrSearchEngine
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["EnableBackSolrSearchEngine"] != null)
                    if (System.Web.Configuration.WebConfigurationManager.AppSettings["EnableBackSolrSearchEngine"].ToLower() == "true")
                        return true;
                return false;
            }
        }

        #endregion

        public static bool EnableFrontContentDisclosure
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["EnableFrontContentDisclosure"] != null)
                    if (System.Web.Configuration.WebConfigurationManager.AppSettings["EnableFrontContentDisclosure"].ToLower() == "true")
                        return true;
                return false;
            }
        }

        public static string GetClientIPAddress
        {
            get
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;

                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        return addresses[0];
                    }
                }

                return context.Request.ServerVariables["REMOTE_ADDR"];
            }
        }

        public static string ExternalAppUrl
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAppUrl"] != null)
                    return System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAppUrl"];
                else
                    return "";
            }
        }
        public static string ExternalAppQueryParam
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAppQueryParam"] != null)
                {
                    return System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAppQueryParam"];
                }
                else
                    return "";
            }
        }

        public static string SsoSharedCookieDomain
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["ssoSharedCookieDomain"] != null)
                {
                    return System.Web.Configuration.WebConfigurationManager.AppSettings["ssoSharedCookieDomain"];
                }
                else
                    return "";
            }
        }


        #region SPID
        public static bool SpidEnabled
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["SpidEnabled"] != null)
                    if (System.Web.Configuration.WebConfigurationManager.AppSettings["SpidEnabled"].ToLower() == "true")
                        return true;
                return false;
            }
        }

        public static string SpidButton
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["SpidButton"] != null)
                    return System.Web.Configuration.WebConfigurationManager.AppSettings["SpidButton"].ToLower();
                return "";
            }
        }

        public static bool SpidEnableAggregatorSubjects
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings
                    ["EnableAggregatorSubjects"].ToLower() == "true")
                    return true;
                return false;
            }
        }

        //public static string SpidAggregatorSubjectsName
        //{
        //    get
        //    {
        //        if (System.Web.Configuration.WebConfigurationManager.AppSettings
        //            ["AggregatorSubjectsName"] != null)
        //            return System.Web.Configuration.WebConfigurationManager.AppSettings
        //                ["AggregatorSubjectsName"].ToLower();
        //        return "";
        //    }
        //}

        public static string SpidBaseUrl
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings
                    ["SpidBaseUrl"] != null)
                    return System.Web.Configuration.WebConfigurationManager.AppSettings
                        ["SpidBaseUrl"]/*.ToLower()*/;
                return "";
            }
        }

        public static string SpidClientId
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings
                    ["SpidClientId"] != null)
                    return System.Web.Configuration.WebConfigurationManager.AppSettings
                        ["SpidClientId"]/*.ToLower()*/;
                return "";
            }
        }

        public static string SpidClientSecret
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings
                    ["SpidClientSecret"] != null)
                    return System.Web.Configuration.WebConfigurationManager.AppSettings
                        ["SpidClientSecret"]/*.ToLower()*/;
                return "";
            }
        }

        public static string SpidClientIdBS
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings
                    ["SpidClientIdBS"] != null)
                    return System.Web.Configuration.WebConfigurationManager.AppSettings
                        ["SpidClientIdBS"]/*.ToLower()*/;
                return "";
            }
        }

        public static string SpidClientSecretBS
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings
                    ["SpidClientSecretBS"] != null)
                    return System.Web.Configuration.WebConfigurationManager.AppSettings
                        ["SpidClientSecretBS"]/*.ToLower()*/;
                return "";
            }
        }

        #endregion

        /// <summary>
        /// Parametro di configurazione per il reset della password dell'utente - viene usato per inibire l'obbligo di modifica della password
        /// </summary>
        public static bool EnableResetPassword
        {
            get
            {
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["EnableResetPassword"] != null)
                {
                    string valueKey = System.Web.Configuration.WebConfigurationManager.AppSettings["EnableResetPassword"].ToLower();
                    if (!string.IsNullOrEmpty(valueKey) && Convert.ToBoolean(valueKey))                  
                        return true;
                }
                return false;
            }
        }

    }


}