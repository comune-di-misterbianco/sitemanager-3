using System;
using System.Reflection;
using System.Collections;
using System.Web;
/// <summary>
/// Summary description for xNodeCollection
/// </summary>

namespace NetCms.Configurations
{
    public interface IHttpsHandler
    {
        /// <summary>
        /// Esegue il redirect alls tessa pagina ma in Https
        /// </summary>
        void RedirectToSecureRequest();

        /// <summary>
        /// Esegue il redirect all stessa pagina ma in Http
        /// </summary>
        void RedirectToNonSecureRequest();

        /// <summary>
        /// Restituisce l'indirizzo della pagina corrente convertito in https
        /// </summary>
        string GetCurrentPageAddressAsSecure();
        string ConvertPageAddressAsSecure(string address);

        /// <summary>
        /// Restituisce l'indirizzo della pagina corrente convertito in http
        /// </summary>
        string GetCurrentPageAddressAsNonSecure();
        string ConvertPageAddressAsNonSecure(string address);

        bool IsSecureConnection { get; }

        /// <summary>
        /// L'indirizzo al quale fare riferimento come pagina di Login per il frontend.
        /// L'indirizzo deve contenere il segnaposto {0} che verr� rimpiazzato a runtime dal nome della network corrente
        /// Esempio "https://www.net-serv.it/website/{0}/login.aspx"
        /// </summary>
        string FrontendLoginPageAddress { get; }

        /// <summary>
        /// L'indirizzo al quale fare riferimento come pagina di Login per il backoffice.
        /// </summary>
        string BackofficeLoginPageAddress { get; }
    }
    /// <summary>
    /// Questo handler gestisce i comportamenti riguardo Https.
    /// Nello specifico chiamando il metoto "ConvertHttpRequestToHttps" viene fatto un redirect alla stessa pagina ma in Https
    /// </summary>
    public class GenericHttpsHandler : IHttpsHandler
    {
        /// <summary>
        /// Esegue il redirect alls tessa pagina ma in Https
        /// </summary>
        public void RedirectToSecureRequest()
        {
            if (!IsSecureConnection &&
                HttpContext.Current != null &&
                HttpContext.Current.Request != null &&
                HttpContext.Current.Response != null)
            {
                HttpContext.Current.Response.Redirect(GetCurrentPageAddressAsSecure());
            }
            else
                throw new InvalidOperationException("Il contesto Http non � disponibile.");
        }

        /// <summary>
        /// Esegue il redirect alls tessa pagina ma in Https
        /// </summary>
        public void RedirectToNonSecureRequest()
        {
            if (!IsSecureConnection &&
                HttpContext.Current != null &&
                HttpContext.Current.Request != null &&
                HttpContext.Current.Response != null)
            {
                HttpContext.Current.Response.Redirect(GetCurrentPageAddressAsNonSecure());
            }
            else
                throw new InvalidOperationException("Il contesto Http non � disponibile.");
        }

        /// <summary>
        /// Restituisce l'indirizzo della pagina corrente convertito in https
        /// </summary>
        public string GetCurrentPageAddressAsSecure()
        {
            if (HttpContext.Current != null &&
                HttpContext.Current.Request != null &&
                HttpContext.Current.Response != null)
            {
                return ConvertPageAddressAsSecure(HttpContext.Current.Request.Url.ToString());
            }
            else
                throw new InvalidOperationException("Il contesto Http non � disponibile.");
        }

        /// <summary>
        /// Restituisce l'indirizzo della pagina corrente convertito in http
        /// </summary>
        public string GetCurrentPageAddressAsNonSecure()
        {
            if (HttpContext.Current != null &&
                HttpContext.Current.Request != null &&
                HttpContext.Current.Response != null)
            {
                return ConvertPageAddressAsNonSecure(HttpContext.Current.Request.Url.ToString());
            }
            else
                throw new InvalidOperationException("Il contesto Http non � disponibile.");
        }
        
        /// <summary>
        /// Restituisce l'indirizzo della pagina corrente convertito in https
        /// </summary>
        public string ConvertPageAddressAsSecure(string address)
        {
            return address.Replace("http://", "https://");
        }

        /// <summary>
        /// Restituisce l'indirizzo della pagina corrente convertito in http
        /// </summary>
        public string ConvertPageAddressAsNonSecure(string address)
        {
            return address.Replace("https://", "http://");
        }

        /// <summary>
        /// L'indirizzo al quale fare riferimento come pagina di Login per il frontend.
        /// L'indirizzo deve contenere il segnaposto {0} che verr� rimpiazzato a runtime dal nome della network corrente
        /// Esempio "https://www.net-serv.it/website/{0}/login.aspx"
        /// </summary>
        public string FrontendLoginPageAddress
        {
            get
            {
                return NetCms.Configurations.Paths.AbsoluteRoot + "/{0}/login.aspx";
            }
        }

        /// <summary>
        /// L'indirizzo al quale fare riferimento come pagina di Login per il backoffice.
        /// </summary>
        public string BackofficeLoginPageAddress
        {
            get
            {
                return NetCms.Configurations.Paths.AbsoluteSiteManagerRoot + "/login.aspx";
            }
        }

        public bool IsSecureConnection
        {
            get
            {
                return NetCms.Configurations.Debug.DebugHttpsJumpLoginEnabled ||
                       (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.IsSecureConnection);
            }
        }
    }
}