﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace NetCms.Configurations
{
    public enum CmsStatusEnum
    {
        [Description("Nessuno stato")]
        nostatus,
        [Description("Running")]
        ok,
        [Description("Presenza di errori")]
        error,
        [Description("Presenza di errori di errori di sistema")]
        syserror,
        [Description("Presenza di errori di connessione al database")]
        dberror,
        [Description("Cms in manutenzione")]
        maintenance
    }
}
