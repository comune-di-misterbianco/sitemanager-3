using System;
using System.Reflection;
using System.Collections;
using System.Web;
/// <summary>
/// Summary description for xNodeCollection
/// </summary>

namespace NetCms
{
    public static class ApplicationContext 
    {
        public const string BackofficeGenericComponentName = "Backoffice";
        public const string FrontendGenericComponentName = "Frontend";
        private const string ApplicationContextCurrentComponentItemsKey = Diagnostics.Diagnostics.ApplicationContextCurrentComponentItemsKey;

        public static string CurrentComponent
        {
            get
            {
                if(HttpContext.Current.Items.Contains(ApplicationContextCurrentComponentItemsKey))
                    return HttpContext.Current.Items[ApplicationContextCurrentComponentItemsKey] as string;
                else return string.Empty;
            }
            private set
            {
                HttpContext.Current.Items[ApplicationContextCurrentComponentItemsKey] = value;
            }
        }

        public static void SetCurrentComponent(string currentComponent)
        {
            CurrentComponent = currentComponent;
        }
        public static void SetCurrentComponent(Assembly currentComponentAssembly)
        {
            try
            {
                var assemblyDescriptionAttribute = (AssemblyDescriptionAttribute)Attribute.GetCustomAttribute(currentComponentAssembly, typeof(AssemblyDescriptionAttribute));
                CurrentComponent = assemblyDescriptionAttribute.Description;
            }
            catch 
            { }
        }
    }
}