﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace NetCms.Configurations
{
    public static class Scripts
    {
        private static List<CmsScript> _CmsScripts;
        public static List<CmsScript> CmsScripts
        {
            get
            {
                if (_CmsScripts == null)
                {
                    _CmsScripts = new List<CmsScript>();

                    XmlNodeList nodeList = NetCms.Configurations.XmlConfig.GetNodes("/Portal/configs/cmsscripts/script");

                    foreach (XmlNode node in nodeList)
                    {
                        XmlNodeList cssNodes = null;

                        if (node.ChildNodes != null && node.ChildNodes.Count > 0)
                            cssNodes = node.ChildNodes;

                        string path = node.Attributes["path"].Value;

                        if (!path.ToLower().Contains("http://") && !path.ToLower().Contains("https://"))
                            path = NetCms.Configurations.Paths.AbsoluteRoot + "/" + node.Attributes["path"].Value;
                        
                        _CmsScripts.Add(new CmsScript(
                                        node.Attributes["name"].Value,
                                        path,
                                        GetCmsScriptCss(cssNodes)
                                        ));
                    }
                }
                return _CmsScripts;
            }
        }
        
        public static CmsScript JQuery
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "jquery").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script Jquery non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                }   
            }            
        }

        public static CmsScript JQueryMigrate
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "jquerymigrate").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script JqueryMigrate non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                }
            }
        }

        public static CmsScript JQueryMask
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "jquerymask").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script JQueryMask non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                }
            }
        }


        public static CmsScript JQueryBlockUI
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "jqueryBlockUI").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script jqueryBlockUI non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                } 
            }
        }


        public static CmsScript JQueryUI
        {
            get
            {
                try 
                {  
                    return CmsScripts.Where(x => x.Nome == "jqueryui").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script JqueryUI non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                } 
            }
        }       

        public static CmsScript JQueryUITimepicker 
        {
           get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "jqueryui-timepicker").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script jqueryui-timepicker non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                } 
           }
        }

        public static CmsScript JQueryTools
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "jquerytools").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script Jquerytools non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                } 
            }
        }
        
        public static CmsScript SwfObject
        {
            get
            {
                try 
                {
                 return CmsScripts.Where(x => x.Nome == "swfobject").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script Swfobject non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                } 
            }

        }       
        
        public static CmsScript Thickbox
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "thickbox").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script Thickbox non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                }  

            }
        }

        public static CmsScript Uploadify 
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "uploadify").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script Uploadify non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                } 

            }
        }

        public static CmsScript JQueryPagination
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "jquerypagination").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script jquerypagination non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                } 
            }
        }       

        public static CmsScript Timeliner
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "timeliner").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script Timeliner non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                } 
            }
        }

        public static CmsScript Flexigrid
        {
            get
            {
                try 
                {  
                    return CmsScripts.Where(x => x.Nome == "flexigrid").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script Flexigrid non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                } 
            }
        }

        public static CmsScript Filepicker
        {
            get
            {                
                try 
                {                
                    return CmsScripts.Where(x => x.Nome == "filepicker").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script Filepicker non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                }                
            }
        }

        public static CmsScript CKEditor
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "ckeditor").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script Ckeditor non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                } 
            }
        }

        public static CmsScript jsPlumb
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "jsplumb").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script Jsplumb non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                }  

            }
        }

        public static CmsScript Bootstrap
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "bootstrap").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script bootstrap non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                }

            }
        }

        public static CmsScript Momentjs
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "momentjs").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script momentjs non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                }

            }
        }

        //public static CmsScript DateTimePickerEonasdan
        //{
        //    get
        //    {
        //        try
        //        {
        //            return CmsScripts.Where(x => x.Nome == "dateTimePickerEonasdan").First();
        //        }
        //        catch (Exception ex)
        //        {
        //            NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script dateTimePickerEonasdan non è presente nella sezione cmsscript del config.xml." + ex.Message);
        //            throw;
        //        }

        //    }
        //}

        public static CmsScript DatepickerUxsolutions
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "datepickerUxsolutions").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script datepickerUxsolutions non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                }

            }
        }


        public static CmsScript DatePickerTempusdominus
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "datePickerTempusdominus").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script Bootstrap 4 DatePicker Tempusdominus non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                }

            }
        }


        public static CmsScript Popper
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "popper").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script Popper.js non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                }

            }
        }

        public static CmsScript ColorBox
        {
            get
            {
                try
                {
                    return CmsScripts.Where(x => x.Nome == "colorbox").First();
                }
                catch (Exception ex)
                {
                    NetCms.Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Error, "Errore: lo script colorbox non è presente nella sezione cmsscript del config.xml." + ex.Message);
                    throw;
                }

            }
        }

        private static List<string> GetCmsScriptCss(XmlNodeList cssnodes)
        {
            List<string> cssScript = new List<string>();
            if (cssnodes != null)
            {
                foreach (XmlNode node in cssnodes)
                {
                    string path = node.Attributes["path"].Value;
                    if (!path.ToLower().Contains("http") && !path.ToLower().Contains("https"))
                        cssScript.Add(NetCms.Configurations.Paths.AbsoluteRoot + "/" + node.Attributes["path"].Value);
                    else
                        cssScript.Add(path);
                }
            }
            return cssScript;
        }
    }

    public class CmsScript 
    {
        public CmsScript(string nome, string path, List<string> css) 
        {
            Nome = nome;
            Path = path;
            Css = css;
        }

        public string Nome { get; set; }
        public string Path { get; set; }
        public List<string> Css { get; set; }
        public string HtmlHeadCode 
        {
            get {

                string htmlcode = @"<script type=""text/javascript"" src=""" + Path + @"""></script>"+ System.Environment.NewLine;
                foreach (string cssPath in Css)
                {
                    htmlcode += @"<link rel=""stylesheet"" type=""text/css"" href=""" + cssPath + @""" />"+ System.Environment.NewLine;
                }
                return htmlcode;
            }
        }
    }

}
