﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace NetCms.Exceptions
{
    public class ExceptionsHandler
    {
        public virtual Exception ExceptionToHandle { get; set; }
        public string UserName { get; set; } 
        public string UserID { get; set; } 

        public ExceptionsHandler(Exception ex)
        {
            this.ExceptionToHandle = ex;
            UserID = "";
            UserName = "";
        }

        public void HandleException()
        {
            //Code that runs when an unhandled error occurs
            if (!NetCms.Configurations.Debug.RawExceptionsEnabled)
            {
                var ex = ExceptionToHandle;

                if (ex != null)
                {
                    var exceptionHandled = HandleException(ex);

                    if (!exceptionHandled && ExceptionToHandle.InnerException != null)
                    {
                        ex = ExceptionToHandle.InnerException;
                        exceptionHandled = HandleException(ex);

                        System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);
                        System.Diagnostics.StackFrame sf = st.GetFrame(0);

                        NetCms.Diagnostics.Diagnostics.TraceUnhandledError(sf, ex.Message, UserID, UserName);

                    }
                    else if (!exceptionHandled)
                    {
                        System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace(ex, true);
                        System.Diagnostics.StackFrame sf = st.GetFrame(0);
                        NetCms.Diagnostics.Diagnostics.TraceUnhandledError(sf, ex.Message, UserID, UserName, ex);
                    }
                                                           
                }
            }
        }

        private bool HandleException(Exception ex)
        {
            if (ex is G2Core.Connections.NoDBException)
            {
                G2Core.Connections.NoDBException customException = ex as G2Core.Connections.NoDBException;
                customException.TraceError(ex, UserName, UserID);
                NetCms.Diagnostics.Diagnostics.Redirect(NetCms.Configurations.Paths.AbsoluteRoot + "/models/error_nodb.htm");
                return true;
            }

            if (ex is NetCms.Exceptions.NotGrantedException)
            {
                NetCms.Exceptions.NotGrantedException customException = ex as NetCms.Exceptions.NotGrantedException;
                customException.TraceError();                
                return true;
            }            

            if (ex is NetCms.Exceptions.ServerError500Exception)
            {
                NetCms.Exceptions.ServerError500Exception customException = ex as NetCms.Exceptions.ServerError500Exception;
                customException.TraceError();                
                return true;
            }

            if (ex is NullReferenceException)
            {                
                return true;
            }

            if (ex is ArgumentNullException)
            {                
                return true;
            }

            return false;
        }
    }
}
