using System;

using System.Configuration;
using System.Web;
using System.Web.Security;

using System.Xml;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Configurations
{
    public class Paths
    {
        private static string _AbsoluteRoot;
        public static string AbsoluteRoot
        {
            get
            {
                if (_AbsoluteRoot == null)
                {
                    _AbsoluteRoot = string.Empty;
                    string RootPathValue = System.Web.Configuration.WebConfigurationManager.AppSettings["RootPath"].ToLower();
                    if (RootPathValue.Length > 0)
                        _AbsoluteRoot += RootPathValue.ToLower();
                }
                return _AbsoluteRoot;
            }
        }

        private static string _PhysicalRoot;
        public static string PhysicalRoot
        {
            get
            {
                if (_PhysicalRoot == null)
                {
                    _PhysicalRoot = HttpContext.Current.Server.MapPath(AbsoluteRoot.Length > 0 ? AbsoluteRoot : "/").ToLower();
                    
                }
                return _PhysicalRoot;
            }
        }

        private static string _AbsoluteConfigRoot;
        public static string AbsoluteConfigRoot
        {
            get
            {               

                if (_AbsoluteConfigRoot == null)
                {
                    string configServerPath = PhysicalRoot + "configs";
                    if (!System.IO.Directory.Exists(configServerPath))
                    {
                        string rootPathValue = AbsoluteRoot.Replace("/", "");//("website");
                        configServerPath = PhysicalRoot.Replace(rootPathValue, rootPathValue + "_configs");
                    }
                    _AbsoluteConfigRoot = configServerPath;
                }

                return _AbsoluteConfigRoot;
            }
        }

        private static string _AbsoluteExportRoot;
        public static string AbsoluteExportRoot
        {
            get
            {

                if (_AbsoluteExportRoot == null)
                {
                    string exportServerPath = PhysicalRoot + "exported-data";
                    if (!System.IO.Directory.Exists(exportServerPath))
                     {
                        System.IO.Directory.CreateDirectory(exportServerPath);                       
                     }
                    _AbsoluteExportRoot = exportServerPath + "\\";
                }

                return _AbsoluteExportRoot;
            }
        }

        private static string _AbsoluteSiteManagerRoot;
        public static string AbsoluteSiteManagerRoot
        {
            get
            {
                if (_AbsoluteSiteManagerRoot == null)
                    _AbsoluteSiteManagerRoot = AbsoluteRoot + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["SiteManagerRoot"].ToLower();
                
                return _AbsoluteSiteManagerRoot;
            }
        }

        public static string LabelsPath
        {
            get
            {
                return System.Web.Configuration.WebConfigurationManager.AppSettings["LabelsPath"].ToString();
            }
        }   

        public static string PageName
        {
            get
            {
                string sPath = System.Web.HttpContext.Current.Request.Url.LocalPath;
                System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
                string sRet = oInfo.Name;
                return sRet.ToLower();
            }
        }
        public static string Extension
        {
            get
            {
                string sPath = System.Web.HttpContext.Current.Request.Url.LocalPath;
                System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
                string sRet = oInfo.Extension;
                return sRet.ToLower();
            }
        }
        
        public static bool UseDefaultNetwork
        {
            get
            {
                if (_UseDefaultNetwork == null)
                    _UseDefaultNetwork = new bool?(System.Web.Configuration.WebConfigurationManager.AppSettings["UseDefaultNetwork"] != null &&
                                                     System.Web.Configuration.WebConfigurationManager.AppSettings["UseDefaultNetwork"].ToLower() == "true");

                return _UseDefaultNetwork.Value;
            }
        }
        private static bool? _UseDefaultNetwork;

        public static bool CurrentRequestIsSecure
        {
            get
            {
                return HttpContext.Current != null &&
                       HttpContext.Current.Request != null &&
                       HttpContext.Current.Request.IsSecureConnection;
            }
        }

        private static string _BackOfficeSkinPath;
        public static string BackOfficeSkinPath
        {
            get
            {

                if (_BackOfficeSkinPath == null)
                {
                    if (System.Web.Configuration.WebConfigurationManager.AppSettings["BackOfficeSkinPath"] != null)
                    {
                        _BackOfficeSkinPath = System.Web.Configuration.WebConfigurationManager.AppSettings["BackOfficeSkinPath"].ToLower();
                        _BackOfficeSkinPath = AbsoluteRoot + _BackOfficeSkinPath.ToLower();
                    }
                    else
                        _BackOfficeSkinPath = AbsoluteRoot + "/cms/css/admin/";
                        
                }
                return _BackOfficeSkinPath;
            }
        }

        /// <summary>
        /// Ritorna l'indirizzo tipo http://www.contoso.com:8080/ recuperato dalla contesto corrente
        /// </summary>
        public static string BaseUri
        {
            get
            {
                string baseUri = string.Empty;

                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                {
                    Uri uri = System.Web.HttpContext.Current.Request.Url;

                    string baseSchema = HttpContext.Current.Request.ServerVariables.Get("HTTP_X_URL_SCHEME");

                    string uriSchema = (string.IsNullOrEmpty(baseSchema) ? HttpContext.Current.Request.Url.Scheme : baseSchema);

                    baseUri = uriSchema + "://" + System.Web.HttpContext.Current.Request.Url.Host;

                    if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Url.Port.ToString()) && (System.Web.HttpContext.Current.Request.Url.Port != 80 && System.Web.HttpContext.Current.Request.Url.Port != 443))
                        baseUri += ":" + System.Web.HttpContext.Current.Request.Url.Port.ToString();
                }

                return baseUri;
            }

        }

        /// <summary>
        /// Ritorna l'indirizzo tipo http://www.contoso.com:8080/webroot/ recuperato dalla contesto corrente e seguito dalla root folder del cms
        /// </summary>
        public static string WebApiBasePath
        {
            get
            {                
                return BaseUri + NetCms.Configurations.Paths.AbsoluteRoot; 
            }
        }

        public static string FileMaintenancePath
        {
            get
            {
                return HttpContext.Current.Server.MapPath("~/App_Data/maintenance.txt");
            }
        }

    }
}