﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCms.CmsApp.Configuration
{
    public class Configuration
    {
        private const string DebugKey = "EnableNetServiceDebug";

        public string ApplicationSystemName
        {
            get
            {
                return _ApplicationSystemName;
            }
            private set
            {
                _ApplicationSystemName = value;
            }
        }
        private string _ApplicationSystemName;

        private static string DebugModeValue;
        public static bool DebugModeEnabled
        {
            get
            {
                if (DebugModeValue == null)
                {
                    if (System.Web.Configuration.WebConfigurationManager.AppSettings[DebugKey] != null)
                        DebugModeValue = System.Web.Configuration.WebConfigurationManager.AppSettings[DebugKey].ToLower();
                }
                return DebugModeValue == "on" || DebugModeValue == "advanced";
            }
        }

        public Configuration(string applicationSystemName)
        {
            this.ApplicationSystemName = applicationSystemName;
        }

        // The method
        private string GetSettings(string Key)
        {
            System.Collections.Specialized.NameValueCollection obj = (System.Collections.Specialized.NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig(this.ApplicationSystemName);
            return obj[Key].ToString();
        }

        public string this[string key]
        {
            get
            {
                return GetSettings(key);
            }
        }
    }
}
