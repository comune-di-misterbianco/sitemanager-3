using System;
using System.Reflection;
using System.Collections;
/// <summary>
/// Summary description for xNodeCollection
/// </summary>

namespace NetCms
{
    public class AssembliesCollection : IEnumerable
    {
        private G2Collection coll;

        public int Count { get { return coll.Count; } }

        public AssembliesCollection()
        {
            coll = new G2Collection();
        }

        public void Add(string key, Assembly Assembly)
        {
            coll.Add(key, Assembly);
        }

        public Assembly this[int i]
        {
            get
            {
                Assembly value = (Assembly)coll[coll.Keys[i]];
                return value;
            }
        }

        public Assembly this[string key]
        {
            get
            {
                Assembly field = (Assembly)coll[key];
                return field;
            }
        }
    #region Enumerator

    public IEnumerator GetEnumerator()
    {
        return new CollectionEnumerator(this);
    }

    private class CollectionEnumerator : IEnumerator
    {
        private int CurentPos = -1;
        private AssembliesCollection Collection;
        public CollectionEnumerator(AssembliesCollection coll)
        {
            Collection = coll;
        }
        public object Current
        {
            get
            {
                return Collection[CurentPos];
            }
        }
        public bool MoveNext()
        {
            if (CurentPos < Collection.Count - 1)
            {
                CurentPos++;
                return true;
            }
            else
                return false;
        }
        public void Reset()
        {
            CurentPos = -1;
        }
    }
    #endregion
    }
}