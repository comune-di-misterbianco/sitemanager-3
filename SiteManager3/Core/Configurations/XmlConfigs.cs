﻿using System;
using System.Xml;
using System.Web;

namespace NetCms.Configurations
{
    public class XmlConfig
    {
        private static XmlNode xmlConfigs;
        private static XmlNode XmlConfigs
        {
            get
            {
                if (xmlConfigs == null)
                    xmlConfigs = Document.DocumentElement;

                return xmlConfigs;
            }
        }
        private static XmlDocument document;
        public static XmlDocument Document
        {
            get
            {
                if (document == null)
                {
                    document = new XmlDocument();
                    try
                    {
                        document.Load(Paths.AbsoluteConfigRoot + "\\config.xml");
                    }
                    catch (Exception ex)
                    {
                        /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                        ExLog.SaveLog(0, ex.Message, "NetCms.Configurations.XmlConfig", NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, "Errore nel caricamento del file xml di configurazione", "", "", "79", ex, typeof(XmlConfig));                        
                    }
                }
                return document;
            }
        }
        public static XmlNode GetNodeContent(string path)
        {
            lock(XmlConfigs)
            {
                XmlNodeList oNodeList = XmlConfigs.SelectNodes(path);
                if (oNodeList != null && oNodeList.Count == 1)
                    return oNodeList[0];
                else
                    return null;
            }
        }

        public static XmlNodeList GetNodes(string path)
        {
            return XmlConfigs.SelectNodes(path);
        }

        public static string GetNodeXml(string path)
        {
            XmlNode oNode = GetNodeContent(path);
            if (oNode != null)
                return oNode.InnerXml;
            else
                return null;
        }

        public static string GetAttribute(string path, string attributeName)
        {
            XmlNode oNode = GetNodeContent(path);
            if (oNode != null)
                if (oNode.Attributes[attributeName] != null)
                    return oNode.Attributes[attributeName].Value;

            return null;
        }

        public static bool GetOptionAttribute(string path, string attributeName, string optionToCheck)
        {
            string attribute = GetAttribute(path, attributeName);
            if (attribute != null)
                return attribute == optionToCheck;

            return false;
        }

        public static int GetIntegerAttribute(string path, string attributeName)
        {
            string attribute = GetAttribute(path, attributeName);
            int value = 0;
            if (attribute == null || !int.TryParse(attribute, out value))
                value = int.MinValue;

            return value;
        }

        public static bool GetBooleanAttribute(string path, string attributeName)
        {
            return GetOptionAttribute(path, attributeName, "true");
        }

        public static void SetAttributeValue(string path, string attributeName, string attributeValue)
        {
            XmlNode n = XmlConfigs.SelectSingleNode(path);
            n.Attributes[attributeName].Value = attributeValue;

            XmlDocument oXmlDoc = new XmlDocument();
            try
            {
                string xmlFilePath = Paths.AbsoluteConfigRoot + "\\config.xml";
                oXmlDoc.Save(xmlFilePath);
            }
            catch (Exception ex)
            {
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, "NetCms.Configurations.XmlConfig", NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, "Errore nel salvataggio del file xml di configurazione a seguito di un tentativo di settaggio di un valore per un attributo", "", "", "80");

            }

        }

        public static void SetNodeValue(string path, string nodeValue)
        {
            XmlNode n = XmlConfigs.SelectSingleNode(path);
            n.InnerText = nodeValue;

            XmlDocument oXmlDoc = Document;
            try
            {
                string xmlFilePath = Paths.AbsoluteConfigRoot + "\\config.xml";
                oXmlDoc.Save(xmlFilePath);
            }
            catch (Exception ex)
            {
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, "NetCms.Configurations.XmlConfig", NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, "Errore nel salvataggio del file xml di configurazione a seguito di un tentativo di settaggio di un valore per un nodo", "", "", "81");

            }
        }

        public static bool Save()
        {
            try
            {
                string xmlFilePath = Paths.AbsoluteConfigRoot + "\\config.xml";
                Document.Save(xmlFilePath);
                return true;
            }
            catch (Exception ex)
            {
                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
                ExLog.SaveLog(0, ex.Message, "NetCms.Configurations.XmlConfig", NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, "Non è stato possibile salvare il file xml di configurazione", "", "", "82");
                return false;
            }
        }
        
        public static string RemoveCDATA(string text)
        {
            if (text == null)
                return "";

            string str = text.Replace("<![CDATA[", "");
            str = str.Replace("]]>", "");

            return str;
        }
    }
}
