﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace NetCms.Exceptions
{
    public abstract class NetCmsPageException : System.Exception
    {
        public string Url { get; private set; }
        public NetCmsPageException(string message, string url) : base(message) { Url = url; }

        public abstract void TraceError();
    }
   
    public class PageNotFound404Exception : NetCmsPageException
    {
        public PageNotFound404Exception() : base("Pagina non trovata.", HttpContext.Current.Request.Url.ToString()) { }
        public PageNotFound404Exception(string url) : base("Pagina non trovata.", url) { }
        
        public override void TraceError()
        {
            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Warning, "Pagina non trovata. '" + Url + "'", "", "");
        }
    }

    public class ServerError500Exception : NetCmsPageException
    {
        public Exception InternalException { get; private set; }

        public ServerError500Exception(string message) : base("Errore interno del server. " + message, HttpContext.Current.Request.Url.ToString()) { }
        public ServerError500Exception(Exception innerException) : base("Errore interno del server.", HttpContext.Current.Request.Url.ToString()) { InternalException = innerException; }
        
        public ServerError500Exception(string url, string message) : base("Errore interno del server. " + message, url) { }
        public ServerError500Exception(string url, string message, Exception innerException) : base("Errore interno del server. " + message, url) { InternalException = innerException; }
        public ServerError500Exception(string url, Exception innerException) : base("Errore interno del server.", url) { InternalException = innerException; }
        public override void TraceError()
        {
            if (Message != null)
                NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Error, Message, "", "");
            if(InnerException != null)
                NetCms.Diagnostics.Diagnostics.TraceException(InnerException, "", "");
        }
    }

    public class NotGrantedException : NetCmsPageException
    {
        public NotGrantedException() : base("Utente non autorizzato a visualizzare la pagina.", HttpContext.Current.Request.Url.ToString()) { }
        public NotGrantedException(string url) : base("Utente non autorizzato a visualizzare la pagina.", url) { }
        public override void TraceError()
        {
            NetCms.Diagnostics.Diagnostics.TraceMessage(Diagnostics.TraceLevel.Warning, "Utente non autorizzato a visualizzare la pagina. '" + Url + "'", "", "");
        }
    }
}
