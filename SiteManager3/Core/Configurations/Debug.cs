using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;


/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Configurations
{
    public static class Debug
    {
        public static System.Collections.Specialized.NameValueCollection Options
        {
            get
            {
                return _Options ?? (_Options = InitCollection());
            }
        }
        private static System.Collections.Specialized.NameValueCollection _Options;
        private static System.Collections.Specialized.NameValueCollection InitCollection()
        {
            System.Collections.Specialized.NameValueCollection coll = null;
            try
            {
                coll = (System.Collections.Specialized.NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("NetServiceDebug");
            }
            catch {}
            if(coll == null)
                coll = new System.Collections.Specialized.NameValueCollection();
            return coll;
        }
         
        public static bool GetOptionEnableStatus(string key)
        {
            lock (Options)
            {
                bool result = false;
                string opt = Options[key];
                if (opt != null) bool.TryParse(opt, out result);
                return result;
            }
        }

        public static bool DebugLoginEnabled
        {
            get
            {
                return (_DebugLoginEnabled ?? (_DebugLoginEnabled = new bool?(GetOptionEnableStatus("enableDebugLogin")))).Value;
            }
        }
        private static bool? _DebugLoginEnabled;

        public static bool DebugHttpsJumpLoginEnabled
        {
            get
            {
                return (_DebugHttpsJumpLoginEnabled ?? (_DebugHttpsJumpLoginEnabled = new bool?(GetOptionEnableStatus("enableHttpsJumpLogin")))).Value;
            }
        }
        private static bool? _DebugHttpsJumpLoginEnabled;

        public static bool GenericEnabled
        {
            get
            {
                return (_GenericEnabled ?? (_GenericEnabled = new bool?(GetOptionEnableStatus("enableGenericDebug")))).Value;
            }
        }
        private static bool? _GenericEnabled;

        public static bool RawExceptionsEnabled
        {
            get
            {
                return (_RawExceptionsEnabled ?? (_RawExceptionsEnabled = new bool?(GetOptionEnableStatus("enableRawExceptions")))).Value;
            }
        }
        private static bool? _RawExceptionsEnabled;
        
        public static bool SqlExceptionsEnabled
        {
            get
            {
                return (_SqlExceptionsEnabled ?? (_SqlExceptionsEnabled = new bool?(GetOptionEnableStatus("enableSqlExceptions")))).Value;
            }
        }
        private static bool? _SqlExceptionsEnabled;

        public static bool FrontTimingsEnabled
        {
            get
            {
                return (_FrontTimingsEnabled ?? (_FrontTimingsEnabled = new bool?(GetOptionEnableStatus("frontTimingsEnabled")))).Value;
            }
        }
        private static bool? _FrontTimingsEnabled;

        public static bool ApplicationsInstallationEnabled
        {
            get
            {
                return (_ApplicationsInstallationEnabled ?? (_ApplicationsInstallationEnabled = new bool?(GetOptionEnableStatus("applicationsInstallationEnabled")))).Value;
            }
        }
        private static bool? _ApplicationsInstallationEnabled;
    }
}