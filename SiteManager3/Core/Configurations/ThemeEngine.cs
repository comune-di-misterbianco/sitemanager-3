﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCms.Configurations
{
    public class ThemeEngine
    {
        public static ThemeEngineStatus Status
        {
            get
            {
              string value = System.Web.Configuration.WebConfigurationManager.AppSettings["ThemeEngineStatus"].ToString();
              return (!string.IsNullOrEmpty(value) ? SharedUtilities.EnumUtilities.ToEnum<ThemeEngineStatus>(value) : ThemeEngineStatus.off);               
            }
        }

        public static string ThemesFolder
        {
            get
            {
                return NetCms.Configurations.Paths.AbsoluteRoot + "/" + System.Web.Configuration.WebConfigurationManager.AppSettings["ThemesFolder"].ToString();
            }
        }

        public enum ThemeEngineStatus
        {
            on,
            off
        }
    }
}
