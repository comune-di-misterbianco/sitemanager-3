using System.Xml;
using System.Configuration;
using System.Linq;

/// <summary>
/// Summary description for Configs
/// </summary>
namespace NetCms.Configurations
{
    public static class PortalData
    {
        private static int _ID;
        public static int ID
        {
            get
            {
                if (_ID == 0)
                    _ID = XmlConfig.GetIntegerAttribute("/Portal", "id");
                
                return _ID;
            }
        }

        private static string _smtphost;
        public static string SmtpHost
        {
            get
            {
                if (_smtphost == null )
                    _smtphost = ConfigurationManager.AppSettings["SmtpHost"].ToString();                    
                return _smtphost;
            }
        }

        private static string _smtpport;
        public static string SmtpPort
        {
            get
            {
                if (_smtpport == null )
                    _smtpport = ConfigurationManager.AppSettings["SmtpPort"].ToString();                    
                return _smtpport;
            }
        }

        private static string _smtpUser;
        public static string SmtpUser
        {
            get
            {
                if (_smtpUser == null )
                    _smtpUser = ConfigurationManager.AppSettings["SmtpUser"].ToString();                    
                    
                return _smtpUser;
            }
        }

        private static string _smtpPassword;
        public static string SmtpPassword
        {
            get
            {
                if (_smtpPassword == null)
                {                    
                    _smtpPassword = ConfigurationManager.AppSettings["SmtpPassword"].ToString();
                    if (NetCms.Configurations.Generics.PasswordCripted)
                    {
                        NetCms.Diagnostics.DecodePass decoder = new NetCms.Diagnostics.DecodePass();
                        _smtpPassword = decoder.DecodeString(_smtpPassword);
                    }
                }

                return _smtpPassword;
            }
        }

        private static string _EnableSSL;
        public static bool EnableSSL
        {
            get
            {
                if (_EnableSSL == null)
                    _EnableSSL = ConfigurationManager.AppSettings["EnableSsl"].ToString();                    

                return _EnableSSL == "true";
            }
        }

        private static string _DisableServerCertificateValidation;
        public static bool DisableServerCertificateValidation
        {
            get
            {
                if (_DisableServerCertificateValidation == null)
                    _DisableServerCertificateValidation = ConfigurationManager.AppSettings["DisableServerCertificateValidation"].ToString();                

                return _DisableServerCertificateValidation == "false";
            }
        }

        private static string _eMail;
        public static string eMail
        {
            get
            {
                if (_eMail == null)
                    _eMail = XmlConfig.GetAttribute("/Portal", "email");

                return _eMail;
            }
        }

        private static string _Nome;
        public static string Nome
        {
            get
            {
                if (_Nome == null)
                    _Nome = XmlConfig.GetAttribute("/Portal", "name");

                return _Nome;
            }
        }

        private static string _TitoloCms;
        public static string TitoloCms
        {
            get
            {
                if (_TitoloCms == null)
                    _TitoloCms = XmlConfig.GetAttribute("/Portal", "cmstitle");

                return _TitoloCms;
            }
        }
        

        private static int _UploadFileSizeLimit;
        public static int UploadFileSizeLimit 
        {
            get {
                _UploadFileSizeLimit = 4000; // Valore predefinito bloccato a 4MB
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFileSizeLimit"] != null)
                {
                    _UploadFileSizeLimit = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["UploadFileSizeLimit"].ToString());
                }
                return _UploadFileSizeLimit;
            }
        }

        private static string _BreadCrumStartLabel;
        public static string BreadCrumStartLabel 
        {
            get {
                _BreadCrumStartLabel = "";
                if (System.Web.Configuration.WebConfigurationManager.AppSettings["BreadCrumStartLabel"] != null) 
                {
                    _BreadCrumStartLabel = System.Web.Configuration.WebConfigurationManager.AppSettings["BreadCrumStartLabel"].ToString();
                }
                return _BreadCrumStartLabel;
            }
        }

        /// <summary>
        /// Ritorna l'indirizzo del portale corrente comprensivo di protocollo eventuale porta tcp
        /// </summary>
        public static string PortalAddress
        {
            get
            {
                //Return variable declaration
                var appPath = string.Empty;

                //Getting the current context of HTTP request
                var context = System.Web.HttpContext.Current;

                //Checking the current context content
                if (context != null)
                {
                    //Formatting the fully qualified website url/name
                    appPath = string.Format("{0}://{1}{2}",
                                            context.Request.Url.Scheme,
                                            context.Request.Url.Host,
                                            context.Request.Url.Port == 80
                                                ? string.Empty
                                                : ":" + context.Request.Url.Port);
                }

                if (!appPath.EndsWith("/"))
                    appPath += "/";

                return appPath;
            }
        }

        public static string PortalFakeDomain
        {
            get {

                var fakeDomain = string.Empty;

                if (System.Web.Configuration.WebConfigurationManager.AppSettings["PortalFakeDomain"] != null)
                {
                    fakeDomain = System.Web.Configuration.WebConfigurationManager.AppSettings["PortalFakeDomain"].ToString();
                }

                return fakeDomain;
            }
        }

        #region TOKEN CONFIG
        private static string _TokenKey;
        public static string TokenKey
        {
            get
            {
                _TokenKey = "";
                if (ConfigurationManager.AppSettings["tokenKey"] != null)
                {
                    _TokenKey = ConfigurationManager.AppSettings["tokenKey"].ToString();
                }
                return _TokenKey;
            }
        }

        private static string _TokenAuthenticationIssuer;
        public static string TokenAuthenticationIssuer
        {
            get
            {
                _TokenAuthenticationIssuer = "";
                if (ConfigurationManager.AppSettings["tokenAuthenticationIssuer"] != null)
                {
                    _TokenAuthenticationIssuer = ConfigurationManager.AppSettings["tokenAuthenticationIssuer"].ToString();
                }
                return _TokenAuthenticationIssuer;
            }
        }

        private static string[] _TokenAuthenticationAudience;
        public static string[] TokenAuthenticationAudience
        {
            get
            {
                _TokenAuthenticationAudience = null;
                if (ConfigurationManager.AppSettings["tokenAuthenticationAudience"] != null)
                {
                    _TokenAuthenticationAudience = ConfigurationManager.AppSettings["tokenAuthenticationAudience"].ToString().Split(',').Select(p => p.Trim()).ToArray(); ;
                }
                return _TokenAuthenticationAudience;
            }
        }

        private static double _ExpiryMinutes;
        public static double ExpiryMinutes
        {
            get
            {
                _ExpiryMinutes = 0;
                if (ConfigurationManager.AppSettings["expiryMinutes"] != null)
                {
                    _ExpiryMinutes = System.Convert.ToDouble(ConfigurationManager.AppSettings["expiryMinutes"].ToString());
                }
                return _ExpiryMinutes;
            }
        }

        private static string _DefaultClientId;
        public static string DefaultClientId
        {
            get
            {
                _DefaultClientId = "";
                if (ConfigurationManager.AppSettings["defaultClientId"] != null)
                {
                    _DefaultClientId = ConfigurationManager.AppSettings["defaultClientId"].ToString();
                }
                return _DefaultClientId;
            }
        }

        #endregion

        #region Protocol
        private static bool? _enableProtocol;
        public static bool EnableProtocol
        {
            get
            {
                if (_enableProtocol == null)
                {
                    bool res;
                    if(System.Boolean.TryParse(ConfigurationManager.AppSettings["EnableProtocol"].ToString(), out res))
                        _enableProtocol = res;
                }
                return _enableProtocol.Value;
            }
        }
        private static string _protocolUrl;
        public static string ProtocolUrl
        {
            get
            {
                if (_protocolUrl == null)
                    _protocolUrl =
                        ConfigurationManager.AppSettings["ProtocolUrl"].ToString();
                return _protocolUrl;
            }
        }

        private static string _protocolUsername;
        public static string ProtocolUsername
        {
            get
            {
                if (_protocolUsername == null)
                    _protocolUsername = 
                        ConfigurationManager.AppSettings["ProtocolUsername"].ToString();
                return _protocolUsername;
            }
        }
        private static string _protocolPassword;
        public static string ProtocolPassword
        {
            get
            {
                if (_protocolPassword == null)
                    _protocolPassword =
                        ConfigurationManager.AppSettings["ProtocolPassword"].ToString();
                return _protocolPassword;
            }
        }

        private static string _protocolArchive;
        public static string ProtocolArchive
        {
            get
            {
                if (_protocolArchive == null)
                    _protocolArchive =
                        ConfigurationManager.AppSettings["ProtocolArchive"].ToString();
                return _protocolArchive;
            }
        }

        private static string _protocolDocumentType;
        public static string ProtocolDocumentType
        {
            get
            {
                if (_protocolDocumentType == null)
                    _protocolDocumentType =
                        ConfigurationManager.AppSettings["ProtocolDocumentType"].ToString();
                return _protocolDocumentType;
            }
        }
        #endregion
    }
}