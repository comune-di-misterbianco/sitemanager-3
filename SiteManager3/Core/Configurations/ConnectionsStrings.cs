//using System;


///// <summary>
///// Summary description for Configs
///// </summary>
//namespace NetCms.Configurations
//{
//    public static class ConnectionsStrings
//    {
//        /// <summary>
//        /// Specifica la stringa di connessione al database per la connessione principale.
//        /// </summary>
//        private static string _ConnectionsSet;
//        public static string ConnectionsSet
//        {
//            get
//            {
//                if (_ConnectionsSet == null)
//                {
//                    string offset = "";
//                    string use = Configurations.XmlConfig.GetAttribute("/Portal/connections", "use");
//                    if (!string.IsNullOrEmpty(use) && use!="default")
//                        offset = "_" + use;

//                    _ConnectionsSet = "/Portal/connections/connectionStrings" + offset;
//                }
//                return _ConnectionsSet;
//            }
//        }

//        /// <summary>
//        /// Specifica la stringa di connessione al database per la connessione principale.
//        /// </summary>
//        private static string _MainDB;
//        public static string MainDB
        //{
        //    get
        //    {
//                if(_MainDB == null)
        //        {
//                    //if (System.Web.Configuration.WebConfigurationManager.ConnectionStrings[MainDbWebConfigurationKey] == null)
//                    if (Configurations.XmlConfig.GetAttribute(ConnectionsSet + "/maindb", "connectionString") == null)
        //            {
//                        Exception ex = new Exception("Stringa di connessione al database principale non specificata nel file 'config.xml'");
        //                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
        //                ExLog.SaveLog(0, ex.Message, "NetCms.Configurations.ConnectionsStrings", NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
//                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, ex.Message, "", "", "72");
        //            }
        //            else
        //            {
//                        string strConn = Configurations.XmlConfig.GetAttribute(ConnectionsSet + "/maindb", "connectionString");
        //                if (NetCms.Configurations.Generics.PasswordCripted)
        //                {
        //                    NetCms.Diagnostics.DecodePass decoder = new NetCms.Diagnostics.DecodePass();
        //                    strConn = decoder.DecodeStringConn(strConn);
                        
        //                if (strConn.Length == 0)
        //                {
//                                Exception ex = new Exception("La password della stringa di connessione \"maindb\", presente all'interno del file config.xml, non � stata criptata.");
//                                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, ex.Message, "", "", "143");
        //                }
        //            }
//                        _MainDB = strConn;
        //        }
        //    }
//                return _MainDB;
        //}
//        }

//         /// <summary>
//        /// Specifica la stringa di connessione al database per la connessione principale.
//        /// </summary>
//        //private static string _LogDB;
//        //public static string LogDB
//        //{
//        //    get
//        //    {
//        //        if(_LogDB == null)
//        //        {
//        //            //if (System.Web.Configuration.WebConfigurationManager.ConnectionStrings[LogDbWebConfigurationKey] == null)
//        //            if (Configurations.XmlConfig.GetAttribute(ConnectionsSet + "/logdb", "connectionString") == null)
//        //            {
//        //                Exception ex = new Exception("Stringa di connessione al database di log non specificata nel file 'config.xml'");
//        //                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
//        //                ExLog.SaveLog(0, ex.Message, "NetCms.Configurations.ConnectionsStrings", NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
//        //                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, ex.Message, "", "", "73");
//        //            }
//        //            else
//        //            {
//        //                string strConn  = Configurations.XmlConfig.GetAttribute(ConnectionsSet + "/logdb", "connectionString");
//        //                if (NetCms.Configurations.Generics.PasswordCripted)
//        //                {
//        //                    NetCms.Diagnostics.DecodePass decoder = new NetCms.Diagnostics.DecodePass();
//        //                    strConn = decoder.DecodeStringConn(strConn);
//        //                }
                        
//        //                if (strConn.Length == 0)
//        //                {
//        //                    Exception ex = new Exception("La password della stringa di connessione \"logdb\", presente all'interno del file config.xml, non � stata criptata.");
//        //                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, ex.Message, "", "", "144");
//        //                }
//        //                _LogDB = strConn;
//        //            }
//        //        }
//        //        return _LogDB;
//        //    }
//        //}


//        /// <summary>
//        /// Specifica la stringa di connessione al database per la connessione principale.
//        /// </summary>
//        //private static string _CrmDB;
//        //public static string CrmDB
//        //{
//        //    get
//        //    {
//        //        if (_CrmDB == null)
//        //        {
//        //            //if (System.Web.Configuration.WebConfigurationManager.ConnectionStrings[LogDbWebConfigurationKey] == null)
//        //            if (Configurations.XmlConfig.GetAttribute(ConnectionsSet + "/crmdb", "connectionString") == null)
//        //            {
//        //                Exception ex = new Exception("Stringa di connessione al database di crm non specificata nel file 'config.xml'");
//        //                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
//        //                ExLog.SaveLog(0, ex.Message, "NetCms.Configurations.ConnectionsStrings", NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
//        //                Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, ex.Message, "", "", "74");
//        //            }
//        //            else
//        //            {
//        //                string strConn = Configurations.XmlConfig.GetAttribute(ConnectionsSet + "/crmdb", "connectionString");
//        //                if (NetCms.Configurations.Generics.PasswordCripted)
//        //                {
//        //                    NetCms.Diagnostics.DecodePass decoder = new NetCms.Diagnostics.DecodePass();
//        //                    strConn = decoder.DecodeStringConn(strConn);
//        //                }

//        //                if (strConn.Length == 0)
//        //                {
//        //                    Exception ex = new Exception("La password della stringa di connessione \"crmdb\", presente all'interno del file config.xml, non � stata criptata.");
//        //                    Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, ex.Message, "", "", "145");
//        //                }
//        //                _CrmDB = strConn;
//        //            }
//        //        }
//        //        return _CrmDB;
//        //    }
//        //}

//        /// <summary>
//        /// Specifica la stringa di connessione al database per la connessione principale.
//        /// </summary>
//        private static string _GlobalDB;
//        public static string GlobalDB
        //{
        //    get
        //    {
//                if (_GlobalDB == null)
        //        {
        //            //if (System.Web.Configuration.WebConfigurationManager.ConnectionStrings[LogDbWebConfigurationKey] == null)
//                    if (Configurations.XmlConfig.GetAttribute(ConnectionsSet + "/globaldb", "connectionString") == null)
        //            {
//                        Exception ex = new Exception("Stringa di connessione al database global non specificata nel file 'config.xml'");
        //                /*NetCms.LogAndTrance.ExceptionLogger ExLog = new NetCms.LogAndTrance.ExceptionLogger();
        //                ExLog.SaveLog(0, ex.Message, "NetCms.Configurations.ConnectionsStrings", NetCms.LogAndTrance.ExceptionLogger.ErrorLevel.Error, ex);*/
//                        Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, ex.Message, "", "", "75");
        //            }
        //            else
        //            {
//                        string strConn = Configurations.XmlConfig.GetAttribute(ConnectionsSet + "/globaldb", "connectionString");
        //                if (NetCms.Configurations.Generics.PasswordCripted)
        //                {
        //                    NetCms.Diagnostics.DecodePass decoder = new NetCms.Diagnostics.DecodePass();
        //                    strConn = decoder.DecodeStringConn(strConn);
        //                }

        //                if (strConn.Length == 0)
        //                {
//                            Exception ex = new Exception("La password della stringa di connessione \"globaldb\", presente all'interno del file config.xml, non � stata criptata.");
//                            Diagnostics.Diagnostics.TraceMessage(NetCms.Diagnostics.TraceLevel.Fatal, ex.Message, "", "", "146");
        //                }
//                        _GlobalDB = strConn;
//                    }
//                }
//                return _GlobalDB;
        //            }
        //        }
        //    }
        //}
